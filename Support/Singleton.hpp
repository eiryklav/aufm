#pragma once

#include <memory>

template<class Derived>
class Singleton
{
public:
  static Derived* Instance()
  {
    static typename Derived::singleton_ptr_t instance(Derived::createInstance());
    return &getReference(instance);
  }

private:
  
  typedef std::unique_ptr<Derived> singleton_ptr_t;
  
  inline static Derived *createInstance() { return new Derived(); }
  inline static Derived &getReference(const singleton_ptr_t &ptr) { return *ptr; }

protected :
  Singleton() {}

private :
  Singleton(const Singleton &) = delete;
  Singleton& operator=(const Singleton &) = delete;
  Singleton(Singleton &&) = delete;
  Singleton& operator=(Singleton &&) = delete;
};
