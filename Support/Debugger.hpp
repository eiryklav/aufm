#pragma once
#include <iostream>
#include <assert.h>
#include <unistd.h>
#include <termios.h>
/*
#define __USE_GNU
#include <dlfcn.h>
*/
#ifndef RELEASE

#define PRE               "## "
#define PREFIX(str)       std::cout << PRE << str;
#define FUNC_IN()         PREFIX("FuncIn: ")  std::cout << __func__ << std::endl;
#define FUNC_OUT()        PREFIX("FuncOut: ") std::cout << __func__ << std::endl;
#define NEST_IN(f)        PREFIX("NestIn: ")  std::cout << #f << std::endl;
#define NEST_OUT(f)       PREFIX("NestOut: ") std::cout << #f << std::endl;
#define PRINT(s)          std::cout << PRE << s  << std::endl;
#define PRINT_IF(cond,s)  if(cond) { PRINT(s) }
#define WATCH(var)        PREFIX(#var << ": ") std::cout << var << std::endl;
#define CDUMP(container)  for(auto& e: container) { PRINT(#container) WATCH(e) }
#define AUFM_ASSERT(x,s)  if(!(x)) { PRINT("Aufm assertion failed: " << s) } assert(x)
#define STOP()            PRINT("STOP()") for(;;);
#define HERE_IS_OK(s)     PRINT("Here is ok. ("<<#s<<")") STOP()
#define PARSER_DUMP()     { PREFIX("Parser dump: '") for(auto tmp = iter; tmp!=end; tmp++) {std::cout << *tmp;} std::cout << "'\n"; }
#define BREAK_POINT()     PRINT("Breakpoint: in function: " << __func__ << ", line: " << __LINE__) PRINT("Press any key to continue.") rewind(stdin); { char buf = 0;struct termios old = {};if (tcgetattr(0, &old) < 0)perror("tcsetattr()");old.c_lflag &= ~ICANON;old.c_lflag &= ~ECHO;old.c_cc[VMIN] = 1;old.c_cc[VTIME] = 0;if (tcsetattr(0, TCSANOW, &old) < 0)perror("tcsetattr ICANON");if (read(0, &buf, 1) < 0)perror ("read()");old.c_lflag |= ICANON;old.c_lflag |= ECHO;if (tcsetattr(0, TCSADRAIN, &old) < 0)perror ("tcsetattr ~ICANON"); } PRINT("BREAK OUT")

#else

#define PRE
#define PREFIX(str)
#define FUNC_IN()
#define FUNC_OUT()
#define NEST_IN(s)
#define NEST_OUT(s)
#define PRINT(s)
#define PRINT_IF(cond,s)
#define WATCH(var)
#define CDUMP(container)
#define AUFM_ASSERT(x,s)  assert(x)
#define STOP()
#define HERE_IS_OK(s)
#define PARSER_DUMP()
#define BREAK_POINT()

#endif