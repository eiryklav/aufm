#include <string>
#include <vector>
#include <unordered_map>
#include <complex>
#include "../Lib/picojson.h"
#include "Misc.hpp"

class StandingComponentsLoader
{
  struct StandingComponent {
    std::string       componentName;
    std::complex<int> position;
    bool              isAnimated;
    double            interevalTime;
    int               maxCount;
    StandingComponent(std::string const& na, std::complex<int> const& pos, bool anim, double ivt, int mxcnt)
    : componentName(na), position(pos), isAnimated(anim), interevalTime(ivt), maxCount(mxcnt) {}
  };

  using OrderInLayer = int;
  std::unordered_map<OrderInLayer, StandingComponent> result_;
  
  inline StandingComponentsLoader(std::string jsonString)
  {
    picojson::value v;
    auto err = picojson::parse(v, jsonString);
    if(!err.empty()) { AUFM_ASSERT(false, "Can't load jsonString in StandingComponentsLoader"); }
    auto& arr = v.get<picojson::array>();
    for(auto& item: arr) {
      auto& o = item.get<picojson::object>();

      auto posX = std::stoi(o["Position"].get<picojson::array>()[0].get<std::string>());
      auto posY = std::stoi(o["Position"].get<picojson::array>()[1].get<std::string>());

      result_.emplace(
        std::stoi(o["OrderInLayer"].get<std::string>()),
        StandingComponent{
          o["ComponentName"].get<std::string>(),
          {posX, posY},
          static_cast<bool>(std::stoi(o["IsAnimated"].get<std::string>())),
          std::stod(o["IntervalTime"].get<std::string>()),
          std::stoi(o["MaxCount"].get<std::string>())
        }
      );
    }
  }
};