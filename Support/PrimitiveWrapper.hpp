#pragma once

#include <cmath>

#define PRIMITIVE_WRAPPER_DEF(wrapperType, primitive) \
  class wrapperType \
  { \
  protected:  \
    primitive val = 0;  \
    explicit wrapperType(primitive v) : val(v) {} \
  public: \
    auto get() const { return val; }  \
    operator primitive() const { return val; }  \
    bool operator < (const wrapperType& rhs) const { return val < rhs.get(); }\
  };

#define PRIM_WRAP_INHERITED_CTOR_DEF(type, wrapperType, primitive)  \
  type() : PrimitiveWrapper::wrapperType(0) {}  \
  explicit type(primitive val) : PrimitiveWrapper::wrapperType(val) {}

#define PRIM_WRAP_INHERITED_OPERATOR_DEF(type, primitive)  \
    auto operator + (type const& rhs) const { return type(val+rhs.get()); } \
    auto operator - (type const& rhs) const { return type(val-rhs.get()); } \
    auto operator * (type const& rhs) const { return type(val*rhs.get()); } \
    auto operator / (type const& rhs) const { return type(val/rhs.get()); } \
    auto operator % (type const& rhs) const { return type(std::fmod(val, rhs.get())); } \
    auto operator += (type const& rhs) { val += rhs.get(); }  \
    auto operator -= (type const& rhs) { val -= rhs.get(); }  \
    auto operator *= (primitive  const& rhs) { val *= rhs; }  \
    auto operator /= (primitive  const& rhs) { val /= rhs; }  \
    auto operator %= (type const& rhs) { val = std::fmod(val, rhs.get()); }

namespace PrimitiveWrapper
{
  PRIMITIVE_WRAPPER_DEF(Integer, int)
  PRIMITIVE_WRAPPER_DEF(Double, double)
}