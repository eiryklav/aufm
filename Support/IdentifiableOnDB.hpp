#pragma once

#include <iostream>

class IdentifiableOnDB
{
public:
	static int constexpr InvalidID = -1;
	
private:
	int id = InvalidID;

public:
	inline IdentifiableOnDB(int id) : id(id) { }
	inline auto expired() const { return id < 0; }
	inline auto getIDOnDB() const
	{
		if(expired()) {
      AUFM_ASSERT(false, "Invalid ID: " << id <<  " has been referenced.");
    }
		return id;
	}
  
  inline bool operator == (IdentifiableOnDB const& rhs) const { return id == rhs.getIDOnDB(); }
  inline bool operator != (IdentifiableOnDB const& rhs) const { return id != rhs.getIDOnDB(); }
};