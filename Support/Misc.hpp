#pragma once

#include <string>
#include <complex>
#include <cstdlib>
#include <random>
#include <sstream>

#include "../Lib/picojson.h" 
#include "PrimitiveWrapper.hpp"
#include "Debugger.hpp"

namespace Misc {
  // どこで定義すべきか悩むものをここに置く
  enum class EquipmentType {
    Weapon, Protector, Accessory
  };
}

namespace Misc
{
  static std::random_device device;               // 初期シードのみrandom_deviceで生成する
  static std::mt19937       generator(device());  // MT19937 (2^19937 - 1 の周期を持つメルセンヌ・ツイスタ)を擬似乱数生成器とする。

  inline auto randomRange(double min, double max)
  {
    return std::uniform_real_distribution<double>(min, max)(generator);
  }

  inline auto randomRange(int min, int max)
  {
    return std::uniform_int_distribution<int>(min, max)(generator);
  }

  inline auto randomBernoulli(double p)
  {
  	return std::bernoulli_distribution(p)(generator);
  }

  inline auto zeroPadding(std::string const& str, int numOfDigits)
  {
    std::string ret = str;
    std::size_t const len = str.size();
    if(len > numOfDigits) { std::runtime_error("String size exceeds the limit(numOfDigits)"); }
    for(std::size_t i=len; i<numOfDigits; i++) {
      ret = '0' + ret;
    }
    return ret;
  }

  template<class T>
  class RangeTester
  {
  protected:
    T lower, upper;

    inline auto failed() const { std::runtime_error("Range test failed."); }

  public:
    explicit RangeTester(T lower, T upper)
    : lower(lower), upper(upper)
    {}

    inline auto LTE(T val) const     { if( lower <= val   )               { return; } else { failed(); } }
    inline auto GTE(T val) const     { if( val   <= upper )               { return; } else { failed(); } }
    inline auto LTE_GTE(T val) const { if( lower <= val && val <= upper ) { return; } else { failed(); } }
  };

  class ProbRangeTester : public RangeTester<int>
  {
    int sum;

  public:
    ProbRangeTester()
      : RangeTester(0, 100), sum(0)
    {}

    inline auto add(int val) { LTE_GTE(val); sum += val; }
    inline auto validateSum() const { if(sum == 100) { return; } else { failed(); } }
  };

  inline auto Z100RangeTest(int val) { if(0<=val&&val<=100) return; else { AUFM_ASSERT(false, "Z100RangeTest Error"); } }

  template<class T>
  inline auto getRandomIndexAtProbTVec(std::vector<std::pair<int, T>> const& v)
    -> int
  {
    auto rval = randomRange(1, 100);
    int sum = 0;
    for(size_t i=0; i<v.size();i++) {
      if(i && v[i-1].first > v[i].first) { std::runtime_error("probability ascending sorted vector should be used."); }
      sum += v[i].first;
      if(rval <= sum) {
        return static_cast<int>(i);
      }
    }
    return -1;
  }
  
  // グランドリストからMAPに対応する文字列を抜き出す
  inline auto getStringCorrespondToWorld(std::string const& jsonString, std::string const& worldName)
    -> std::string
  {
    picojson::value v;
    auto err = picojson::parse(v, jsonString);
    if(!err.empty()) { AUFM_ASSERT(false, "JSON: " << jsonString << " Parse error!"); }
    auto& o = v.get<picojson::object>();
    AUFM_ASSERT(o.find(worldName) != o.end(), "Cannnot find key 'worldName' in json object.");
    return o[worldName].get<std::string>();
  }
  
  inline auto getIntPointFromJSON(picojson::array const& jsonPoint)
    -> std::complex<int>
  {
    AUFM_ASSERT(jsonPoint.size() == 2, "just two points required.");
    return {static_cast<int>(jsonPoint[0].get<double>()),
      static_cast<int>(jsonPoint[1].get<double>())};
  }
  
  inline auto getDoublePointFromJSON(picojson::array const& jsonPoint)
    -> std::complex<double>
  {
    AUFM_ASSERT(jsonPoint.size() == 2, "just two points required.");
    return {jsonPoint[0].get<double>(), jsonPoint[1].get<double>()};
  }
  
}