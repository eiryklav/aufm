#pragma once

class AutoIncrementedID {
private:
  int nextID = 0;
public:
  inline AutoIncrementedID() {}
  inline AutoIncrementedID(int startID) : nextID(startID) {}
  inline auto getNextID() -> int { return nextID++; }
};