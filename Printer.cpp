//
//  Printer.cpp
//  Aufm
//
//  Created by moti on 2015/06/10.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

#include "Game.hpp"
#include "Printer.hpp"

namespace Printer
{
  namespace Messages { namespace Registered
  {
    // loadWorldConfigFromJSON() で登録される
    namespace Shops {
      namespace Weapon {
        std::string EnterText;
        std::string ExitText;
      }
      namespace Protector {
        std::string EnterText;
        std::string ExitText;
      }
      namespace Accessory {
        std::string EnterText;
        std::string ExitText;
      }
    }
  }}
  
  Message::Message(MessageType type, std::string const& text, bool alert, std::string position, bool hidden)
  : type_(type), stateType_(GameMgr->getTopState().getState()->getStateType()), text_(text), timeStamp_(GameMgr->getTimeMgr()->getNowRealTime()), isAlert_(alert), position_(position), hidden_(hidden)
  {
    lineNum_ = 1;
    for(auto const& c: text_) { lineNum_ += c == '\n'; }
  }
  
  Conversation::Conversation(std::string const& speakerName, std::string const& content, std::string const& summary, bool alert)
  : speakerName_(speakerName), content_(content), summary_(summary), timeStamp_(GameMgr->getTimeMgr()->getNowRealTime()), isAlert_(alert) {}
}
