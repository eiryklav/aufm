//
//  BackgroundAccessoryProtocol.swift
//  BackgroundSupport
//
//  Created by moti on 2015/08/13.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import SceneKit

protocol BackgroundAccessoryProtocol {
  init(scnView: SCNView, scnScene: SCNScene)
  func run()
}
