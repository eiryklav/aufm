#include "ChrData.hpp"
#include "../Data/Database.hpp"
#include "../BattleSystem/BattleAI.hpp"

#include "../Printer.hpp"
#include "../Support/Debugger.hpp"

namespace ChrData
{
  
  std::string DefaultChrName = "__DEFAULT_CHR_NAME__";
  
  ChrBasicStat::ChrBasicStat()
      : money_(-1), LV(-1), Exp(-1), HP(-1), Ma(-1), ATK(-1), DEF(-1), SP(-1), LCK(-1), INT(-1) {}
  
  auto ChrBasicStat::setOwner(std::weak_ptr<Character> owner) -> void { owner_ = owner; }
  auto ChrBasicStat::setATK(Attack val) -> void { ATK = val; }
  auto ChrBasicStat::setDEF(Defense val) -> void { DEF = val; }
  auto ChrBasicStat::setSP(Speed val) -> void { SP = val; }
  auto ChrBasicStat::setLuck(Luck val) -> void { LCK = val; }
  
  ChrBasicStat::ChrBasicStat(Money      mone,
                             Level      lv,
                             Experience ex,
                             HitPoint   hp,
                             Mana       ma,
                             Attack     atk,
                             Defense    def,
                             Speed      sp,
                             Luck       lck,
                             Intelligence itl,
                             std::shared_ptr<Character> owner,  // ファクトリで遅延
                             Attributions attrs,
                             EquipmentID  weapon,
                             EquipmentID  protector,
                             EquipmentID  accessory,
                             std::shared_ptr<BattleSystem::BattleAI> const& battleAI
                             )
  : money_(mone), LV(lv), Exp(ex), HP(hp), Ma(ma), ATK(atk), DEF(def), SP(sp), LCK(lck), INT(itl), owner_(owner), attributions_(attrs), weapon_(weapon), protector_(protector), accessory_(accessory), battleAI_(battleAI)
  {
  }
  
  auto ChrBasicStat::setBattleAI(std::string const& name)
    -> void
  {
    if(name == "SimpletonAI") {
      battleAI_ = std::make_shared<BattleSystem::SimpletonAI>();

    }
    else if(name == "StandardAI") {
      battleAI_ = std::make_shared<BattleSystem::StandardAI>();
    }
    else if(name == "DummyAI") {
      AUFM_ASSERT(false, "This is dummy AI.");
    }
    else {
      AUFM_ASSERT(false, "No match AI Name.");
    }
    
    auto o = owner_.lock();
    if(o->getChrType() == ChrData::Character::ChrType::Player) {
      GameMgr->getLogger()->getPlayerStatusLog(o->getIDOnDB()).pushAIName(battleAI_->getName());
    }
    
  }
  
  auto ChrBasicStat::recordStatus()
    -> void
  {
    auto o = owner_.lock();
    auto characterID = o->getIDOnDB();
    
    GameMgr->getLogger()->getPlayerStatusLog(characterID)
      .pushLV(std::make_shared<Level>(LV));
    GameMgr->getLogger()->getPlayerStatusLog(characterID)
      .pushMaxHP(std::make_shared<HitPoint>(HP));
    GameMgr->getLogger()->getPlayerStatusLog(characterID)
      .pushHP(std::make_shared<HitPoint>(o->getCurrStat()->getHP()));
    GameMgr->getLogger()->getPlayerStatusLog(characterID)
      .pushMana(std::make_shared<Mana>(o->getCurrStat()->getMana()));
    GameMgr->getLogger()->getPlayerStatusLog(characterID)
      .pushMaxMana(std::make_shared<Mana>(Ma));
    GameMgr->getLogger()->getPlayerStatusLog(characterID)
      .pushATK(std::make_shared<Attack>(ATK));
    GameMgr->getLogger()->getPlayerStatusLog(characterID)
      .pushDEF(std::make_shared<Defense>(DEF));
    GameMgr->getLogger()->getPlayerStatusLog(characterID)
      .pushSP(std::make_shared<Speed>(SP));
    GameMgr->getLogger()->getPlayerStatusLog(characterID)
      .pushCondition(o->getCurrStat()->getCondition());
  }
  
  auto ChrBasicStat::levelUp()
  {
    LV  = Level(LV + 1);
    HP  = HitPoint(HP   + 3 * Misc::randomRange(7, 20));
    Ma  = Mana    (Ma   + 3 * Misc::randomRange(3, 10));
    ATK = Attack  (ATK  + 3 * Misc::randomRange(3, 15));
    DEF = Defense (DEF  + 3 * Misc::randomRange(3, 15));
    SP  = Speed   (SP   + 3 * Misc::randomRange(5, 18));
    
    auto o = owner_.lock();
    
    auto const jsonString = GameMgr->getDatabase()->getChrStatusRecord(o->getIDOnDB())["MasterActionsWithLevel"];
    picojson::value v;
    auto err = picojson::parse(v, jsonString);
    if(!err.empty()) { AUFM_ASSERT(false, "JSON Parse error: " << err << "\nJSON string is " << jsonString); }
    auto& jsonObject = v.get<picojson::object>();
    auto levelString = "Level-" + std::to_string(LV.get());
    if(jsonObject.find(levelString) != jsonObject.end()) {
      auto const& arr = jsonObject[levelString].get<picojson::array>();
      for(auto const& item: arr) {
        auto const actionClassID = item.get<std::string>();
        o->addAction(actionClassID);
        GameMgr->getLogger()->pushMessage(Printer::Messages::masteredAction(
          Misc::getStringCorrespondToWorld(GameMgr->getDatabase()->getActionRecord(actionClassID)["Name"], GameMgr->getWorldName()))
        );
        GameMgr->getLogger()->getPlayerStatusLog(o->getIDOnDB()).pushMasteredAction(actionClassID);
      }
    }
    
    recordStatus();
  }
  
  auto ChrCurrStat::initWithBasicStat(std::shared_ptr<const ChrBasicStat> const& stat)
    -> void
  {
    owner_ = stat->getOwner();
    HP  = stat->getHP();
    Ma  = stat->getMana();
    ATK = stat->getATK();
    DEF = stat->getDEF();
    SP  = stat->getSP();
    condition = 0;
  }

  auto ChrBasicStat::acquireExperience(Experience const& addValue)
    -> bool
  {
    bool res = false;
    Exp += addValue;
    for(;LV < Player::CounterStop.LV;) {
      if(Exp >= calcExpFromLevel()) { levelUp(); res = true; }
      else { break; }
    }
    
    GameMgr->getLogger()->getPlayerStatusLog(owner_.lock()->getIDOnDB()).pushExp(std::make_shared<Experience>(Exp));
    
    return res;
  }

  Character::Character(ChrIDOnDB idOnDB): IdentifiableOnDB(idOnDB){}
  
  auto Character::configure(ChrName const& name, std::shared_ptr<Character> const& owner)
    -> void
  {
    auto rec = GameMgr->getDatabase()->getChrStatusRecord(getIDOnDB());

    if(name == DefaultChrName) { name_ = Misc::getStringCorrespondToWorld(rec["Name"], GameMgr->getWorldName()); }
    else { name_ = name; }
    
    if(rec["FullName"] != "-") {  // "" や無意味な文字列である場合、以下でJSON parse errorとなる
      fullName_ = Misc::getStringCorrespondToWorld(rec["FullName"], GameMgr->getWorldName());
    }
    
    basicStat_ = std::make_shared<ChrBasicStat>(
      Money       (std::stoi(rec["Gold"])),
      Level       (std::stoi(rec["LV"])),
      Experience  (std::stoi(rec["Exp"])),
      HitPoint    (std::stoi(rec["HP"])),
      Mana        (std::stoi(rec["Mana"])),
      Attack      (std::stoi(rec["ATK"])),
      Defense     (std::stoi(rec["DEF"])),
      Speed       (std::stoi(rec["SP"])),
      Luck        (std::stoi(rec["LCK"])),
      Intelligence(std::stoi(rec["INTL"]))
    );
    
    basicStat_->setOwner(owner);

    {
      auto const& jsonString = rec["Equipment"];
      picojson::value v;
      auto err = picojson::parse(v, jsonString);
      if(!err.empty()) { AUFM_ASSERT(false, "JSON Parse error!"); }
      auto& o = v.get<picojson::object>();
      if(!o.empty()) {
        // 4桁の数字はプレイヤーキャラと定義する
        using EQType = Misc::EquipmentType;
        // !!! "weapon" などが lower camel となっていることに注意。いづれ upper camel に統一せよ。
        basicStat_->setEquipmentID(EQType::Weapon,    static_cast<int>(o["Weapon"].get<double>()));
        basicStat_->setEquipmentID(EQType::Protector, static_cast<int>(o["Protector"].get<double>()));
        basicStat_->setEquipmentID(EQType::Accessory, static_cast<int>(o["Accessory"].get<double>()));
      }
    }

    basicStat_->setBattleAI(rec["ArtificialIntelligence"]);

    {
      ChrBasicStat::Attributions attrs;
      auto const& jsonString = rec["Attributions"];
      picojson::value v;
      auto err = picojson::parse(v, jsonString);
      if(!err.empty()) { AUFM_ASSERT(false, "JSON Parse error!"); }
      auto& arr = v.get<picojson::array>();
      for(auto const& e: arr) {
        attrs.push_back(static_cast<int>(e.get<double>()));
      }
      basicStat_->setAttributions(attrs);
    }

    currStat_ = std::make_shared<ChrCurrStat>();
    currStat_->initWithBasicStat(basicStat_);
  }
  
  auto Character::addAction(std::string const& classID)
    -> void
  {
    auto exist = GameMgr->getDatabase()->getActionRecord(classID);
    if(std::find(masteredActions_.begin(), masteredActions_.end(), classID) != masteredActions_.end()) {
      AUFM_ASSERT(false, "Duplicate spells. (Already has acquired)");
    }
    masteredActions_.emplace_back(classID);
  }
  
  auto Character::isMasteredAction(std::string const& classID)
    -> bool
  {
    GameMgr->getDatabase()->getActionRecord(classID); // existence assertion
    return std::find(masteredActions_.begin(), masteredActions_.end(), classID) != masteredActions_.end();
  }
  
  auto ChrCurrStat::increaseHP(HitPoint const& val)
    -> void
  {
    AUFM_ASSERT(val >= 0, "HPは減らない in increaseHP()");
    HP += val;
    
    auto MaxHP = owner_.lock()->getBasicStat()->getHP();
    if(HP > MaxHP) { HP = MaxHP; }
    
    auto o = owner_.lock();
    if(o->getChrType() == Character::ChrType::Player) {
      GameMgr->getLogger()->getPlayerStatusLog(owner_.lock()->getIDOnDB()).pushHP(std::make_shared<HitPoint>(HP));
    }
  }
  
  auto ChrCurrStat::decreaseHP(HitPoint const& val)
    -> void
  {
    AUFM_ASSERT(val >= 0, "HPは増えない in decreaseHP()");
    HP -= val;
    if(HP <= 0) { HP = HitPoint(0); }
    
    auto o = owner_.lock();
    if(o->getChrType() == Character::ChrType::Player) {
      GameMgr->getLogger()->getPlayerStatusLog(owner_.lock()->getIDOnDB()).pushHP(std::make_shared<HitPoint>(HP));
    }
  }
  
  auto ChrCurrStat::increaseMana(Mana const& val)
    -> void
  {
    AUFM_ASSERT(val >= 0, "Manaは減らない in increaseMana()");
    Ma += val;
    
    auto MaxMa = owner_.lock()->getBasicStat()->getMana();
    if(Ma > MaxMa) { Ma = MaxMa; }
    
    auto o = owner_.lock();
    if(o->getChrType() == Character::ChrType::Player) {
      GameMgr->getLogger()->getPlayerStatusLog(o->getIDOnDB()).pushMana(std::make_shared<Mana>(Ma));
    }
  }
  
  auto ChrCurrStat::decreaseMana(Mana const& val)
    -> void
  {
    AUFM_ASSERT(val >= 0, "Manaは増えない in decreaseMana()");
    Ma -= val;
    AUFM_ASSERT(Ma >= 0, "Manaは非負整数");
    
    auto o = owner_.lock();
    if(o->getChrType() == Character::ChrType::Player) {
      GameMgr->getLogger()->getPlayerStatusLog(o->getIDOnDB()).pushMana(std::make_shared<Mana>(Ma));
    }
  }
  
  auto ChrCurrStat::dieSuddenly()
    -> void
  {
    HP = HitPoint(0);
    condition |= dead;
    
    auto o = owner_.lock();
    if(o->getChrType() == Character::ChrType::Player) {
      GameMgr->getLogger()->getPlayerStatusLog(o->getIDOnDB()).pushHP(std::make_shared<HitPoint>(HP));
      GameMgr->getLogger()->getPlayerStatusLog(o->getIDOnDB()).pushCondition(condition);
    }
  }
  
  auto ChrCurrStat::setCondition(int stat)
    -> void
  {
    condition |= stat;
  }

  auto const Player::CounterStop = ChrBasicStat{
    Money       (99999999),
    Level       (99),
    Experience  (99999999),
    HitPoint    (999),
    Mana        (999),
    Attack      (999),
    Defense     (999),
    Speed       (999),
    Luck        (999),
    Intelligence(999)
  };

  auto const Enemy::CounterStop = ChrBasicStat{
    Money       (99999999),
    Level       (99),
    Experience  (99999999),
    HitPoint    (999999),
    Mana        (999),
    Attack      (999),
    Defense     (999),
    Speed       (999),
    Luck        (999),
    Intelligence(999)
  };
}
