CREATE TABLE Attributions
(
  ID        INTEGER PRIMARY KEY,
  Flame     INTEGER,
  Ice       INTEGER,
  Ascent    INTEGER
);