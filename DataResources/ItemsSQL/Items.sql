CREATE TABLE Items
(
  ID          INTEGER   PRIMARY KEY,
  Name        NVARCHAR,
  Effect      NVARCHAR,
  Permanent   BOOLEAN,
  Description NVARCHAR,
  Flavor      NVARCHAR
);