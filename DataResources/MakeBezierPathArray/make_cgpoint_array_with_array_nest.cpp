#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

int main() {

  string s, t; while(cin >> t) s+= t;
  rep(i, s.size()) {
    if(i && s[i] == '[') {
      s = s.substr(0, i) + "CGPointMake(" + s.substr(i+1);
    }
    if(i != s.size()-1 && s[i] == ']')  {
      s[i] = ')';
    }
  }

  cout << endl;
  cout << s << endl;

  return 0;
}