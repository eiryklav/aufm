#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>
#include <assert.h>
#include <array>
#include <cstdio>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

int main() {

  double offset_x, offset_y;
  cout << "start point of bezier path: "; cin >> offset_x >> offset_y;
  
  string s;
  for(string t; cin >> t;) s += t;

  cout << "\n------------------\n";
  cout << s << endl;
  rep(i, s.size()) {
    if(s[i] == 't' && isdigit(s[i+1])) { s[i] = s[i+1] = ' '; }
    //if(i > 0 && isalpha(s[i-1]) && isdigit(s[i])) { s[i-1] = s[i] = ' '; }
    if(!isdigit(s[i]) && s[i] != '.' && s[i] != '-') { s[i] = ' '; }
    if(i > 0 && !isdigit(s[i-1]) && s[i] == '.') { s[i] = ' '; }
  }

  cout << "\n------------------\n";
  cout << s << endl;
  cout << "\n------------------\n";

  vector<double> ret;
  stringstream ss(s);
  while(ss >> s) {
    ret.push_back(stod(s));
  }

  assert(ret.size() % 2 == 0 && ret.size() > 2);

  if(offset_x != 114514) {
    offset_x -= ret[0], offset_y -= ret[1];
  }
  else {
    offset_x = offset_y = 0;
  }
  for(int i=0; i<ret.size(); i+=2) {
    ret[i] += offset_x;
    ret[i+1] += offset_y;
  }

  for(int i=0; i<ret.size(); i+=2) {
    if(i == 0) { cout << "["; }
    else { cout << ", "; }
    cout << "[" << ret[i] << ',' << ret[i+1] << "]";
  }
  cout << "]\n";

  return 0;
}