CREATE TABLE Protectors
(
  ID            INTEGER   PRIMARY KEY,
  Name          NVARCHAR,
  Attributions  NVARCHAR,
  Gold          INTEGER,
  Rank          INTEGER,
  IsStoryItem   BOOLEAN
);