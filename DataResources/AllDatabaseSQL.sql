CREATE TABLE Accessories
(
  ID            INTEGER   PRIMARY KEY,
  Name          NVARCHAR,
  Attributions  NVARCHAR,
  Gold          INTEGER,
  Rank          INTEGER,
  IsStoryItem   BOOLEAN
);/*
  ID:       INTEGER (3桁, 0埋め)
  Name:     JSON Object "{{"Sicerelia": "フラヴァス"}, {"DQ7": "神様"}}"
  
  雨の獣＝ステータス、出現地点、EXP、ドロップアイテム･･････フレーバー。

  ex.シスレリアの龍、フラヴァス　Dragon in Sicerelia, Fravace
  HP  1200
  Ma  5000 
  ATK   189
  DEF   97 
  SP    213
  LCK   44
  INT   79

  EXP:      INTEGER
  ItemDrops:    JSON Array  [["50", "0001"], ["25", "0101"], ["25", "-1"]]
  
  ATTR(EQP):    JSON Array ["0001", "0101"] / 昇天無効、ATK依存攻撃カット20%
  AI:       JSON ...AI / 最大効率ＡＩ(やばそう)
  
  以下AIが受けもつ（敵の覚えている技を項目として列挙させる必要が生じたらその時はその時。多分別に文字列を登録することとなる）
  ・リア
  ・セレリア
  ・なぎ払い（ATK依存全体１）
  ・フラヴ（炎熱単体１）
  ・アヴァル（水流全体２）
  ・ヴァーセフ（雷撃全体３）
  ・フラヴァス（？）

*/

CREATE TABLE Characters
(
  ID INTEGER PRIMARY KEY,
  Name NVARCHAR,
  FullName NVARCHAR,
  Exp INTEGER,
  ItemDrops NVARCHAR,
  FlavorText NVARCHAR,
  Gold INTEGER,
  LV INTEGER,
  HP INTEGER,
  Mana INTEGER,
  ATK INTEGER,
  DEF INTEGER,
  SP INTEGER,
  LCK INTEGER,
  INTL INTEGER,
  Equipment NVARCHAR,
  Attributions NVARCHAR,
  ArtificialIntelligence NVARCHAR,
  MasteredActions NVARCHAR,
  MasterActionsWithLevel NVARCHAR,
  AllActionsToMasater NVARCHAR,
  ChangeAI NVARCHAR
);CREATE TABLE Items
(
  ID          INTEGER   PRIMARY KEY,
  Name        NVARCHAR,
  Effect      NVARCHAR,
  Permanent   BOOLEAN,
  Description NVARCHAR,
  Flavor      NVARCHAR
);CREATE TABLE Weapons
(
  ID            INTEGER   PRIMARY KEY,
  Name          NVARCHAR,
  Attributions  NVARCHAR,
  Gold          INTEGER,
  Rank          INTEGER,
  IsStoryItem   BOOLEAN
);CREATE TABLE Attributions
(
  ID        INTEGER PRIMARY KEY,
  Flame     INTEGER,
  Ice       INTEGER,
  Ascent    INTEGER
);CREATE TABLE Protectors
(
  ID            INTEGER   PRIMARY KEY,
  Name          NVARCHAR,
  Attributions  NVARCHAR,
  Gold          INTEGER,
  Rank          INTEGER,
  IsStoryItem   BOOLEAN
);
/*
	Class: 			A0001 など
	Name:			JSON形式 "{{"Sicerelia": "レクイス"}, {"幻の大地": "ザキ"}}"
	Mana:			消費Mana(符号付きにしようか？)
	Effect:
		Effect 		:= +(Text | Action | Invocation | SetTar)
		Text 		:= "Text" ":" DQuot (string literals) DQuot
		DQuot		:= """
		Invocation 	:= "Invocation" ":" "Class" 				// 再帰的になる
		SetTar		:= "SetTarget" ":" """ ("ForOne" | "ForAll" | "Myself") """
		Action		:= (Subs ":" """ Expression """
					| 	Cond ":" """ Expression(0-100) """)
		Subs		:= "Damage" | "Heal"
		Cond 		:= "Poison" | "Palsy" | "Death"
		Expression	:= Term *(Operator+- Term)
		Operator+-	:= "+", "-"
		Term		:= Factor *(Operator*\/ Factor)
		Operator*\/	:= "*", "/"
		Factor		:= (signed) ( (int) | "(" Expression ")" | Func )
		Func		:= ^[数字記号]+(alphabet)[...]	// Ex) RAND[(int),(int)] | BERNOULLI[(int 0-100)]

		Macro		:= Macro[Args...] // 別の文に置換可能なもの

		* Action は、すべて左から順に評価される。
		* C++の実装に データを入力したくないので Special をボツにして、
		  代わりにログを出力させる Text を導入した。これによって、技の施行は Effect をパースするのみで良くなる
		* Invocation によって、再帰処理を可能にして「FFXの『ものまね』」程度は Effect の記述のみで完結？（仕様未定）
		* REPなどはマクロとして導入すべきか？

	// RequiredArgs:	Effect が要求する引数, 特に要求がなければ無視される。
					引数のセパレータは ','
	NoSystemText:	「AはBを唱えた！」のような定型文を表示させない。
*/
	
CREATE TABLE Actions
(
	Class 				NCHAR(5) PRIMARY KEY,
	Name    	    NVARCHAR,
	Mana					INTEGER,
	Effect  			NVARCHAR,
	NoSystemText	BOOLEAN,
	Description		NVARCHAR,
	Flavor				NVARCHAR
);
INSERT INTO Accessories (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (1,'{"SicereliaKingdom":"アクセサリA", "DQ":"ペンダントA"}','[1, 2]',10,1,0);
INSERT INTO Accessories (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (2,'{"SicereliaKingdom":"アクセサリB", "DQ":"ペンダントA"}','[1, 2]',10,2,0);
INSERT INTO Accessories (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (3,'{"SicereliaKingdom":"アクセサリC", "DQ":"ペンダントA"}','[1, 2]',10,3,0);
INSERT INTO Accessories (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (4,'{"SicereliaKingdom":"アクセサリD", "DQ":"ペンダントA"}','[1, 2]',10,4,0);
INSERT INTO Accessories (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (5,'{"SicereliaKingdom":"アクセサリE", "DQ":"ペンダントA"}','[1, 2]',10,5,0);
INSERT INTO Accessories (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (6,'{"SicereliaKingdom":"アクセサリA", "DQ":"ペンダントA"}','[1, 2]',10,6,0);
INSERT INTO Accessories (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (7,'{"SicereliaKingdom":"アクセサリA", "DQ":"ペンダントA"}','[1, 2]',10,7,0);
INSERT INTO Accessories (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (8,'{"SicereliaKingdom":"アクセサリA", "DQ":"ペンダントA"}','[1, 2]',10,8,0);
INSERT INTO Accessories (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (9,'{"SicereliaKingdom":"すごいアクセ", "DQ":"ロトの紋章"}','[1, 2]',10,9,1);
INSERT INTO Accessories (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (10,'{"SicereliaKingdom":"アクセサリA", "DQ":"ペンダントA"}','[1, 2]',10,10,0);
INSERT INTO Accessories (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (11,'{"SicereliaKingdom":"アクセサリA", "DQ":"ペンダントA"}','[1, 2]',10,11,0);
INSERT INTO Accessories (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (12,'{"SicereliaKingdom":"アクセサリA", "DQ":"ペンダントA"}','[1, 2]',10,12,0);
INSERT INTO Accessories (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (13,'{"SicereliaKingdom":"アクセサリA", "DQ":"ペンダントA"}','[1, 2]',10,13,0);
INSERT INTO Accessories (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (14,'{"SicereliaKingdom":"アクセサリA", "DQ":"ペンダントA"}','[1, 2]',10,14,0);
INSERT INTO Accessories (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (15,'{"SicereliaKingdom":"アクセサリA", "DQ":"ペンダントA"}','[1, 2]',10,15,0);
INSERT INTO Accessories (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (16,'{"SicereliaKingdom":"アクセサリA", "DQ":"ペンダントA"}','[1, 2]',10,16,0);
INSERT INTO Accessories (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (17,'{"SicereliaKingdom":"アクセサリA", "DQ":"ペンダントA"}','[1, 2]',10,17,0);
INSERT INTO Accessories (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (18,'{"SicereliaKingdom":"アクセサリA", "DQ":"ペンダントA"}','[1, 2]',10,18,0);
INSERT INTO Accessories (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (19,'{"SicereliaKingdom":"アクセサリA", "DQ":"ペンダントA"}','[1, 2]',10,19,0);
INSERT INTO Accessories (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (20,'{"SicereliaKingdom":"アクセサリA", "DQ":"ペンダントA"}','[1, 2]',10,20,0);
INSERT INTO Accessories (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (21,'{"SicereliaKingdom":"アクセサリA", "DQ":"ペンダントA"}','[1, 2]',10,21,0);
INSERT INTO Accessories (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (22,'{"SicereliaKingdom":"アクセサリA", "DQ":"ペンダントA"}','[1, 2]',10,22,0);
INSERT INTO Accessories (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (23,'{"SicereliaKingdom":"アクセサリA", "DQ":"ペンダントA"}','[1, 2]',10,23,0);
INSERT INTO Accessories (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (24,'{"SicereliaKingdom":"アクセサリA", "DQ":"ペンダントA"}','[1, 2]',10,24,0);
INSERT INTO Accessories (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (25,'{"SicereliaKingdom":"アクセサリA", "DQ":"ペンダントA"}','[1, 2]',10,25,0);
INSERT INTO Accessories (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (26,'{"SicereliaKingdom":"アクセサリA", "DQ":"ペンダントA"}','[1, 2]',10,26,0);
INSERT INTO Accessories (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (27,'{"SicereliaKingdom":"アクセサリA", "DQ":"ペンダントA"}','[1, 2]',10,27,0);
INSERT INTO Accessories (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (28,'{"SicereliaKingdom":"アクセサリA", "DQ":"ペンダントA"}','[1, 2]',10,28,0);
INSERT INTO Accessories (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (29,'{"SicereliaKingdom":"アクセサリA", "DQ":"ペンダントA"}','[1, 2]',10,29,0);
INSERT INTO Accessories (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (30,'{"SicereliaKingdom":"アクセサリA", "DQ":"ペンダントA"}','[1, 2]',10,30,0);
INSERT INTO Characters (ID,Name,FullName,Exp,ItemDrops,FlavorText,Gold,LV,HP,Mana,ATK,DEF,SP,LCK,INTL,Attributions,Equipment,ArtificialIntelligence,MasteredActions,MasterActionsWithLevel,AllActionsToMasater,ChangeAI) VALUES (1,'{"SicereliaKingdom":"スライム", "AnotherMAP":"神様"}','-',5,'[[50, 1], [25, 2], [25, -1]]','スライムスライムあああ',8,1,6,10,10,10,9,10,10,'[]','{}','SimpletonAI','[]','-','-','-');
INSERT INTO Characters (ID,Name,FullName,Exp,ItemDrops,FlavorText,Gold,LV,HP,Mana,ATK,DEF,SP,LCK,INTL,Attributions,Equipment,ArtificialIntelligence,MasteredActions,MasterActionsWithLevel,AllActionsToMasater,ChangeAI) VALUES (2,'{"SicereliaKingdom":"ドラキー", "AnotherMAP":"神様"}','-',8,'[[50, 0001], [25, 2], [25, -1]','ドラキーのテキスト',12,1,8,10,10,10,6,10,10,'[1, 2]','{}','SimpletonAI','[]','-','-','-');
INSERT INTO Characters (ID,Name,FullName,Exp,ItemDrops,FlavorText,Gold,LV,HP,Mana,ATK,DEF,SP,LCK,INTL,Attributions,Equipment,ArtificialIntelligence,MasteredActions,MasterActionsWithLevel,AllActionsToMasater,ChangeAI) VALUES (3,'{"SicereliaKingdom":"ホイミスライム", "AnotherMAP":"神様"}','-',13,'[[50, 0001], [25, 2], [25, -1]','ベススライムのテキスト',12,1,8,10,10,10,10,10,10,'[1, 2]','{}','StandardAI','["SA0001L"]','-','-','-');
INSERT INTO Characters (ID,Name,FullName,Exp,ItemDrops,FlavorText,Gold,LV,HP,Mana,ATK,DEF,SP,LCK,INTL,Attributions,Equipment,ArtificialIntelligence,MasteredActions,MasterActionsWithLevel,AllActionsToMasater,ChangeAI) VALUES (4,'{"SicereliaKingdom":"スライム", "AnotherMAP":"神様"}','-',20,'[[50, 0001], [25, 2], [25, -1]','スライムスライムあああ',8,1,6,10,10,10,10,10,10,'[]','{}','SimpletonAI','[]','-','-','-');
INSERT INTO Characters (ID,Name,FullName,Exp,ItemDrops,FlavorText,Gold,LV,HP,Mana,ATK,DEF,SP,LCK,INTL,Attributions,Equipment,ArtificialIntelligence,MasteredActions,MasterActionsWithLevel,AllActionsToMasater,ChangeAI) VALUES (5,'{"SicereliaKingdom":"スライム", "AnotherMAP":"神様"}','-',30,'[[50, 0001], [25, 2], [25, -1]','ドラキーのテキスト',12,1,8,10,10,10,10,10,10,'[1, 2]','{}','SimpletonAI','[]','-','-','-');
INSERT INTO Characters (ID,Name,FullName,Exp,ItemDrops,FlavorText,Gold,LV,HP,Mana,ATK,DEF,SP,LCK,INTL,Attributions,Equipment,ArtificialIntelligence,MasteredActions,MasterActionsWithLevel,AllActionsToMasater,ChangeAI) VALUES (6,'{"SicereliaKingdom":"スライム", "AnotherMAP":"神様"}','-',40,'[[50, 0001], [25, 2], [25, -1]','ベススライムのテキスト',12,1,8,10,10,10,10,10,10,'[1, 2]','{}','SimpletonAI','[]','-','-','-');
INSERT INTO Characters (ID,Name,FullName,Exp,ItemDrops,FlavorText,Gold,LV,HP,Mana,ATK,DEF,SP,LCK,INTL,Attributions,Equipment,ArtificialIntelligence,MasteredActions,MasterActionsWithLevel,AllActionsToMasater,ChangeAI) VALUES (7,'{"SicereliaKingdom":"スライム", "AnotherMAP":"神様"}','-',50,'[[50, 0001], [25, 2], [25, -1]','スライムスライムあああ',8,1,6,10,10,10,10,10,10,'[]','{}','SimpletonAI','[]','-','-','-');
INSERT INTO Characters (ID,Name,FullName,Exp,ItemDrops,FlavorText,Gold,LV,HP,Mana,ATK,DEF,SP,LCK,INTL,Attributions,Equipment,ArtificialIntelligence,MasteredActions,MasterActionsWithLevel,AllActionsToMasater,ChangeAI) VALUES (8,'{"SicereliaKingdom":"スライム", "AnotherMAP":"神様"}','-',60,'[[50, 0001], [25, 2], [25, -1]','ドラキーのテキスト',12,1,8,10,10,10,10,10,10,'[1, 2]','{}','SimpletonAI','[]','-','-','-');
INSERT INTO Characters (ID,Name,FullName,Exp,ItemDrops,FlavorText,Gold,LV,HP,Mana,ATK,DEF,SP,LCK,INTL,Attributions,Equipment,ArtificialIntelligence,MasteredActions,MasterActionsWithLevel,AllActionsToMasater,ChangeAI) VALUES (9,'{"SicereliaKingdom":"スライム", "AnotherMAP":"神様"}','-',70,'[[50, 0001], [25, 2], [25, -1]','ベススライムのテキスト',12,1,8,10,10,10,10,10,10,'[1, 2]','{}','SimpletonAI','[]','-','-','-');
INSERT INTO Characters (ID,Name,FullName,Exp,ItemDrops,FlavorText,Gold,LV,HP,Mana,ATK,DEF,SP,LCK,INTL,Attributions,Equipment,ArtificialIntelligence,MasteredActions,MasterActionsWithLevel,AllActionsToMasater,ChangeAI) VALUES (10,'{"SicereliaKingdom":"スライム", "AnotherMAP":"神様"}','-',80,'[[50, 0001], [25, 2], [25, -1]','スライムスライムあああ',8,1,6,10,10,10,10,10,10,'[]','{}','SimpletonAI','[]','-','-','-');
INSERT INTO Characters (ID,Name,FullName,Exp,ItemDrops,FlavorText,Gold,LV,HP,Mana,ATK,DEF,SP,LCK,INTL,Attributions,Equipment,ArtificialIntelligence,MasteredActions,MasterActionsWithLevel,AllActionsToMasater,ChangeAI) VALUES (11,'{"SicereliaKingdom":"スライム", "AnotherMAP":"神様"}','-',90,'[[50, 0001], [25, 2], [25, -1]','ドラキーのテキスト',12,1,8,10,10,10,10,10,10,'[1, 2]','{}','SimpletonAI','[]','-','-','-');
INSERT INTO Characters (ID,Name,FullName,Exp,ItemDrops,FlavorText,Gold,LV,HP,Mana,ATK,DEF,SP,LCK,INTL,Attributions,Equipment,ArtificialIntelligence,MasteredActions,MasterActionsWithLevel,AllActionsToMasater,ChangeAI) VALUES (12,'{"SicereliaKingdom":"スライム", "AnotherMAP":"神様"}','-',100,'[[50, 0001], [25, 2], [25, -1]','ベススライムのテキスト',12,1,8,10,10,10,10,10,10,'[1, 2]','{}','SimpletonAI','[]','-','-','-');
INSERT INTO Characters (ID,Name,FullName,Exp,ItemDrops,FlavorText,Gold,LV,HP,Mana,ATK,DEF,SP,LCK,INTL,Attributions,Equipment,ArtificialIntelligence,MasteredActions,MasterActionsWithLevel,AllActionsToMasater,ChangeAI) VALUES (13,'{"SicereliaKingdom":"スライム", "AnotherMAP":"神様"}','-',110,'[[50, 0001], [25, 2], [25, -1]','スライムスライムあああ',8,1,6,10,10,10,10,10,10,'[]','{}','SimpletonAI','[]','-','-','-');
INSERT INTO Characters (ID,Name,FullName,Exp,ItemDrops,FlavorText,Gold,LV,HP,Mana,ATK,DEF,SP,LCK,INTL,Attributions,Equipment,ArtificialIntelligence,MasteredActions,MasterActionsWithLevel,AllActionsToMasater,ChangeAI) VALUES (14,'{"SicereliaKingdom":"スライム", "AnotherMAP":"神様"}','-',120,'[[50, 0001], [25, 2], [25, -1]','ドラキーのテキスト',12,1,8,10,10,10,10,10,10,'[1, 2]','{}','SimpletonAI','[]','-','-','-');
INSERT INTO Characters (ID,Name,FullName,Exp,ItemDrops,FlavorText,Gold,LV,HP,Mana,ATK,DEF,SP,LCK,INTL,Attributions,Equipment,ArtificialIntelligence,MasteredActions,MasterActionsWithLevel,AllActionsToMasater,ChangeAI) VALUES (15,'{"SicereliaKingdom":"スライム", "AnotherMAP":"神様"}','-',130,'[[50, 0001], [25, 2], [25, -1]','ベススライムのテキスト',12,1,8,10,10,10,10,10,10,'[1, 2]','{}','SimpletonAI','[]','-','-','-');
INSERT INTO Characters (ID,Name,FullName,Exp,ItemDrops,FlavorText,Gold,LV,HP,Mana,ATK,DEF,SP,LCK,INTL,Attributions,Equipment,ArtificialIntelligence,MasteredActions,MasterActionsWithLevel,AllActionsToMasater,ChangeAI) VALUES (16,'{"SicereliaKingdom":"スライム", "AnotherMAP":"神様"}','-',140,'[[50, 0001], [25, 2], [25, -1]','スライムスライムあああ',8,1,6,10,10,10,10,10,10,'[]','{}','SimpletonAI','[]','-','-','-');
INSERT INTO Characters (ID,Name,FullName,Exp,ItemDrops,FlavorText,Gold,LV,HP,Mana,ATK,DEF,SP,LCK,INTL,Attributions,Equipment,ArtificialIntelligence,MasteredActions,MasterActionsWithLevel,AllActionsToMasater,ChangeAI) VALUES (17,'{"SicereliaKingdom":"スライム", "AnotherMAP":"神様"}','-',150,'[[50, 0001], [25, 2], [25, -1]','ドラキーのテキスト',12,1,8,10,10,10,10,10,10,'[1, 2]','{}','SimpletonAI','[]','-','-','-');
INSERT INTO Characters (ID,Name,FullName,Exp,ItemDrops,FlavorText,Gold,LV,HP,Mana,ATK,DEF,SP,LCK,INTL,Attributions,Equipment,ArtificialIntelligence,MasteredActions,MasterActionsWithLevel,AllActionsToMasater,ChangeAI) VALUES (18,'{"SicereliaKingdom":"スライム", "AnotherMAP":"神様"}','-',160,'[[50, 0001], [25, 2], [25, -1]','ベススライムのテキスト',12,1,8,10,10,10,10,10,10,'[1, 2]','{}','SimpletonAI','[]','-','-','-');
INSERT INTO Characters (ID,Name,FullName,Exp,ItemDrops,FlavorText,Gold,LV,HP,Mana,ATK,DEF,SP,LCK,INTL,Attributions,Equipment,ArtificialIntelligence,MasteredActions,MasterActionsWithLevel,AllActionsToMasater,ChangeAI) VALUES (19,'{"SicereliaKingdom":"スライム", "AnotherMAP":"神様"}','-',170,'[[50, 0001], [25, 2], [25, -1]','ドラキーのテキスト',12,1,8,10,10,10,10,10,10,'[1, 2]','{}','SimpletonAI','[]','-','-','-');
INSERT INTO Characters (ID,Name,FullName,Exp,ItemDrops,FlavorText,Gold,LV,HP,Mana,ATK,DEF,SP,LCK,INTL,Attributions,Equipment,ArtificialIntelligence,MasteredActions,MasterActionsWithLevel,AllActionsToMasater,ChangeAI) VALUES (20,'{"SicereliaKingdom":"スライム", "AnotherMAP":"神様"}','-',180,'[[50, 0001], [25, 2], [25, -1]','ベススライムのテキスト',12,1,8,10,10,10,10,10,10,'[1, 2]','{}','SimpletonAI','[]','-','-','-');
INSERT INTO Characters (ID,Name,FullName,Exp,ItemDrops,FlavorText,Gold,LV,HP,Mana,ATK,DEF,SP,LCK,INTL,Attributions,Equipment,ArtificialIntelligence,MasteredActions,MasterActionsWithLevel,AllActionsToMasater,ChangeAI) VALUES (101,'{"SicereliaKingdom":"フラヴァス", "AnotherMAP":"魔王"}','-',190,'[[50, 0001], [25, 2], [25, -1]','先頭に数値を含まない文字列abcd1234',1234,1,1200,5000,189,97,213,44,79,'[1, 101]','{}','SimpletonAI','[]','-','-','-');
INSERT INTO Characters (ID,Name,FullName,Exp,ItemDrops,FlavorText,Gold,LV,HP,Mana,ATK,DEF,SP,LCK,INTL,Attributions,Equipment,ArtificialIntelligence,MasteredActions,MasterActionsWithLevel,AllActionsToMasater,ChangeAI) VALUES (1001,'{"SicereliaKingdom":"アルス"}','-',0,'[]','-',0,1,53,8,10,10,15,10,10,'[]','{"Weapon":1, "Protector":1, "Accessory":1}','StandardAI','["SA0001L", "SG0001", "SG0002"]','{   "Level-3": ["SA0001M"],   "Level-10":  ["SA0001H"] }','["SG0001", "SG0002", "SG0003", "SA0001L", "SA0001M", "SA0001H"]','[]');
INSERT INTO Characters (ID,Name,FullName,Exp,ItemDrops,FlavorText,Gold,LV,HP,Mana,ATK,DEF,SP,LCK,INTL,Attributions,Equipment,ArtificialIntelligence,MasteredActions,MasterActionsWithLevel,AllActionsToMasater,ChangeAI) VALUES (1002,'{"SicereliaKingdom":"ヨスガ"}','{"SicereliaKingdom":"ヨスガ＝シスレリア"}',0,'[]','-',0,1,34,8,10,20,12,10,10,'[]','{"Weapon":1, "Protector":1, "Accessory":1}','SimpletonAI','["SG0001"]','{   "Level-3":  ["SG0002"],   "Level-7": ["SA0001L"] }','["SG0001", "SG0002", "SG0003", "SA0001L"]','[   {     "Condition" : "ATK >= DEF*2.0",     "AI" : "AttackerYosugaAI"   },   {     "Condition" : "DEF >= ATK*2.0",     "AI" : "DefenderYosugaAI"   },   {     "Condition" : "((ATK-DEF < 10) || (DEF-ATK < 10)) && ATK > 100",     "AI" : "BalancerYosugaAI"   } ]');
INSERT INTO Characters (ID,Name,FullName,Exp,ItemDrops,FlavorText,Gold,LV,HP,Mana,ATK,DEF,SP,LCK,INTL,Attributions,Equipment,ArtificialIntelligence,MasteredActions,MasterActionsWithLevel,AllActionsToMasater,ChangeAI) VALUES (1003,'{"SicereliaKingdom":"引きこもり"}','{"SicereliaKingdom":"引きこもりの少女"}',0,'[]','-',0,1,12,8,10,10,8,10,10,'[]','{"Weapon":1, "Protector":1, "Accessory":1}','SimpletonAI','[]','{}','[]','[]');
INSERT INTO Items (ID,Name,Effect,Permanent,Description,Flavor) VALUES (1,'{"SicereliaKingdom":"薬草", "DRAGONQUEST":"やくそう"}','Heal:"20+RAND[-2,2]"',0,'個体のHPが20程度回復する','{"SicereliaKingdom": "薬草のフレーバー"}');
INSERT INTO Items (ID,Name,Effect,Permanent,Description,Flavor) VALUES (2,'{"SicereliaKingdom":"薬草＋", "DRAGONQUEST":"やくそう"}','Heal:"40+RAND[-2,2]"',0,'個体のHPが40程度回復する','{"SicereliaKingdom": "薬草＋のふれ（ｒｙ"}');
INSERT INTO Items (ID,Name,Effect,Permanent,Description,Flavor) VALUES (3,'{"SicereliaKingdom":"薬草＋＋", "DRAGONQUEST":"やくそう"}','Heal:"Target.MaxHP-Target.HP"',0,'個体のHPが完全回復する','{"SicereliaKingdom": "薬草＋＋のふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれふれ"}');
INSERT INTO Items (ID,Name,Effect,Permanent,Description,Flavor) VALUES (4,'{"SicereliaKingdom":"薬草", "DRAGONQUEST":"やくそう"}','Heal:"20+RAND[-2,2]"',0,'','');
INSERT INTO Items (ID,Name,Effect,Permanent,Description,Flavor) VALUES (5,'{"SicereliaKingdom":"薬草", "DRAGONQUEST":"やくそう"}','Heal:"20+RAND[-2,2]"',0,'','');
INSERT INTO Items (ID,Name,Effect,Permanent,Description,Flavor) VALUES (6,'{"SicereliaKingdom":"薬草", "DRAGONQUEST":"やくそう"}','Heal:"20+RAND[-2,2]"',0,'','');
INSERT INTO Items (ID,Name,Effect,Permanent,Description,Flavor) VALUES (7,'{"SicereliaKingdom":"薬草", "DRAGONQUEST":"やくそう"}','Heal:"20+RAND[-2,2]"',0,'','');
INSERT INTO Items (ID,Name,Effect,Permanent,Description,Flavor) VALUES (8,'{"SicereliaKingdom":"薬草", "DRAGONQUEST":"やくそう"}','Heal:"20+RAND[-2,2]"',0,'','');
INSERT INTO Items (ID,Name,Effect,Permanent,Description,Flavor) VALUES (9,'{"SicereliaKingdom":"薬草", "DRAGONQUEST":"やくそう"}','Heal:"20+RAND[-2,2]"',0,'','');
INSERT INTO Items (ID,Name,Effect,Permanent,Description,Flavor) VALUES (10,'{"SicereliaKingdom":"薬草", "DRAGONQUEST":"やくそう"}','Heal:"20+RAND[-2,2]"',0,'','');
INSERT INTO Items (ID,Name,Effect,Permanent,Description,Flavor) VALUES (11,'{"SicereliaKingdom":"薬草", "DRAGONQUEST":"やくそう"}','Heal:"20+RAND[-2,2]"',0,'','');
INSERT INTO Items (ID,Name,Effect,Permanent,Description,Flavor) VALUES (12,'{"SicereliaKingdom":"薬草", "DRAGONQUEST":"やくそう"}','Heal:"20+RAND[-2,2]"',0,'','');
INSERT INTO Items (ID,Name,Effect,Permanent,Description,Flavor) VALUES (13,'{"SicereliaKingdom":"薬草", "DRAGONQUEST":"やくそう"}','Heal:"20+RAND[-2,2]"',0,'','');
INSERT INTO Items (ID,Name,Effect,Permanent,Description,Flavor) VALUES (14,'{"SicereliaKingdom":"薬草", "DRAGONQUEST":"やくそう"}','Heal:"20+RAND[-2,2]"',0,'','');
INSERT INTO Items (ID,Name,Effect,Permanent,Description,Flavor) VALUES (15,'{"SicereliaKingdom":"薬草", "DRAGONQUEST":"やくそう"}','Heal:"20+RAND[-2,2]"',0,'','');
INSERT INTO Items (ID,Name,Effect,Permanent,Description,Flavor) VALUES (16,'{"SicereliaKingdom":"薬草", "DRAGONQUEST":"やくそう"}','Heal:"20+RAND[-2,2]"',0,'','');
INSERT INTO Items (ID,Name,Effect,Permanent,Description,Flavor) VALUES (17,'{"SicereliaKingdom":"薬草", "DRAGONQUEST":"やくそう"}','Heal:"20+RAND[-2,2]"',0,'','');
INSERT INTO Items (ID,Name,Effect,Permanent,Description,Flavor) VALUES (18,'{"SicereliaKingdom":"薬草", "DRAGONQUEST":"やくそう"}','Heal:"20+RAND[-2,2]"',0,'','');
INSERT INTO Items (ID,Name,Effect,Permanent,Description,Flavor) VALUES (19,'{"SicereliaKingdom":"薬草", "DRAGONQUEST":"やくそう"}','Heal:"20+RAND[-2,2]"',0,'','');
INSERT INTO Items (ID,Name,Effect,Permanent,Description,Flavor) VALUES (20,'{"SicereliaKingdom":"薬草", "DRAGONQUEST":"やくそう"}','Heal:"20+RAND[-2,2]"',0,'','');
INSERT INTO Items (ID,Name,Effect,Permanent,Description,Flavor) VALUES (21,'{"SicereliaKingdom":"薬草", "DRAGONQUEST":"やくそう"}','Heal:"20+RAND[-2,2]"',0,'','');
INSERT INTO Items (ID,Name,Effect,Permanent,Description,Flavor) VALUES (22,'{"SicereliaKingdom":"薬草", "DRAGONQUEST":"やくそう"}','Heal:"20+RAND[-2,2]"',0,'','');
INSERT INTO Items (ID,Name,Effect,Permanent,Description,Flavor) VALUES (23,'{"SicereliaKingdom":"薬草", "DRAGONQUEST":"やくそう"}','Heal:"20+RAND[-2,2]"',0,'','');
INSERT INTO Items (ID,Name,Effect,Permanent,Description,Flavor) VALUES (24,'{"SicereliaKingdom":"薬草", "DRAGONQUEST":"やくそう"}','Heal:"20+RAND[-2,2]"',0,'','');
INSERT INTO Items (ID,Name,Effect,Permanent,Description,Flavor) VALUES (25,'{"SicereliaKingdom":"薬草", "DRAGONQUEST":"やくそう"}','Heal:"20+RAND[-2,2]"',0,'','');
INSERT INTO Items (ID,Name,Effect,Permanent,Description,Flavor) VALUES (26,'{"SicereliaKingdom":"薬草", "DRAGONQUEST":"やくそう"}','Heal:"20+RAND[-2,2]"',0,'','');
INSERT INTO Items (ID,Name,Effect,Permanent,Description,Flavor) VALUES (27,'{"SicereliaKingdom":"薬草", "DRAGONQUEST":"やくそう"}','Heal:"20+RAND[-2,2]"',0,'','');
INSERT INTO Items (ID,Name,Effect,Permanent,Description,Flavor) VALUES (28,'{"SicereliaKingdom":"薬草", "DRAGONQUEST":"やくそう"}','Heal:"20+RAND[-2,2]"',0,'','');
INSERT INTO Items (ID,Name,Effect,Permanent,Description,Flavor) VALUES (29,'{"SicereliaKingdom":"薬草", "DRAGONQUEST":"やくそう"}','Heal:"20+RAND[-2,2]"',0,'','');
INSERT INTO Items (ID,Name,Effect,Permanent,Description,Flavor) VALUES (30,'{"SicereliaKingdom":"薬草", "DRAGONQUEST":"やくそう"}','Heal:"20+RAND[-2,2]"',0,'','');
INSERT INTO Weapons (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (1,'{"SicereliaKingdom":"剣1", "DQ":"鋼の剣"}','[1, 101]',10,1,0);
INSERT INTO Weapons (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (2,'{"SicereliaKingdom":"剣2", "DQ":"鋼の剣"}','[1, 101]',10,2,0);
INSERT INTO Weapons (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (3,'{"SicereliaKingdom":"剣3", "DQ":"鋼の剣"}','[1, 101]',10,3,0);
INSERT INTO Weapons (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (4,'{"SicereliaKingdom":"剣4", "DQ":"鋼の剣"}','[1, 101]',10,4,0);
INSERT INTO Weapons (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (5,'{"SicereliaKingdom":"剣5", "DQ":"鋼の剣"}','[1, 101]',10,5,0);
INSERT INTO Weapons (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (6,'{"SicereliaKingdom":"剣6", "DQ":"鋼の剣"}','[1, 101]',10,6,0);
INSERT INTO Weapons (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (7,'{"SicereliaKingdom":"剣7", "DQ":"鋼の剣"}','[1, 101]',10,7,0);
INSERT INTO Weapons (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (8,'{"SicereliaKingdom":"剣8", "DQ":"鋼の剣"}','[1, 101]',10,8,0);
INSERT INTO Weapons (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (9,'{"SicereliaKingdom":"ロトの剣", "DQ":"ロトの剣"}','[1, 101]',10,9,1);
INSERT INTO Weapons (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (10,'{"SicereliaKingdom":"剣10", "DQ":"鋼の剣"}','[1, 101]',10,10,0);
INSERT INTO Weapons (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (11,'{"SicereliaKingdom":"剣", "DQ":"鋼の剣"}','[1, 101]',10,11,0);
INSERT INTO Weapons (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (12,'{"SicereliaKingdom":"剣", "DQ":"鋼の剣"}','[1, 101]',10,12,0);
INSERT INTO Weapons (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (13,'{"SicereliaKingdom":"剣", "DQ":"鋼の剣"}','[1, 101]',10,13,0);
INSERT INTO Weapons (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (14,'{"SicereliaKingdom":"剣", "DQ":"鋼の剣"}','[1, 101]',10,14,0);
INSERT INTO Weapons (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (15,'{"SicereliaKingdom":"剣", "DQ":"鋼の剣"}','[1, 101]',10,15,0);
INSERT INTO Weapons (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (16,'{"SicereliaKingdom":"剣", "DQ":"鋼の剣"}','[1, 101]',10,16,0);
INSERT INTO Weapons (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (17,'{"SicereliaKingdom":"剣", "DQ":"鋼の剣"}','[1, 101]',10,17,0);
INSERT INTO Weapons (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (18,'{"SicereliaKingdom":"剣", "DQ":"鋼の剣"}','[1, 101]',10,18,0);
INSERT INTO Weapons (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (19,'{"SicereliaKingdom":"剣", "DQ":"鋼の剣"}','[1, 101]',10,19,0);
INSERT INTO Weapons (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (20,'{"SicereliaKingdom":"剣", "DQ":"鋼の剣"}','[1, 101]',10,20,0);
INSERT INTO Weapons (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (21,'{"SicereliaKingdom":"剣", "DQ":"鋼の剣"}','[1, 101]',10,21,0);
INSERT INTO Weapons (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (22,'{"SicereliaKingdom":"剣", "DQ":"鋼の剣"}','[1, 101]',10,22,0);
INSERT INTO Weapons (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (23,'{"SicereliaKingdom":"剣", "DQ":"鋼の剣"}','[1, 101]',10,23,0);
INSERT INTO Weapons (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (24,'{"SicereliaKingdom":"剣", "DQ":"鋼の剣"}','[1, 101]',10,24,0);
INSERT INTO Weapons (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (25,'{"SicereliaKingdom":"剣", "DQ":"鋼の剣"}','[1, 101]',10,25,0);
INSERT INTO Weapons (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (26,'{"SicereliaKingdom":"剣", "DQ":"鋼の剣"}','[1, 101]',10,26,0);
INSERT INTO Weapons (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (27,'{"SicereliaKingdom":"剣", "DQ":"鋼の剣"}','[1, 101]',10,27,0);
INSERT INTO Weapons (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (28,'{"SicereliaKingdom":"剣", "DQ":"鋼の剣"}','[1, 101]',10,28,0);
INSERT INTO Weapons (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (29,'{"SicereliaKingdom":"剣", "DQ":"鋼の剣"}','[1, 101]',10,29,0);
INSERT INTO Weapons (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (30,'{"SicereliaKingdom":"剣", "DQ":"鋼の剣"}','[1, 101]',10,30,0);
INSERT INTO Attributions (ID,Flame,Ice,Ascent) VALUES (1,110,80,0);
INSERT INTO Attributions (ID,Flame,Ice,Ascent) VALUES (2,110,80,80);
INSERT INTO Attributions (ID,Flame,Ice,Ascent) VALUES (3,110,80,20);
INSERT INTO Attributions (ID,Flame,Ice,Ascent) VALUES (4,110,80,100);
INSERT INTO Attributions (ID,Flame,Ice,Ascent) VALUES (5,110,80,0);
INSERT INTO Attributions (ID,Flame,Ice,Ascent) VALUES (6,110,80,0);
INSERT INTO Attributions (ID,Flame,Ice,Ascent) VALUES (7,110,80,0);
INSERT INTO Attributions (ID,Flame,Ice,Ascent) VALUES (8,110,80,0);
INSERT INTO Attributions (ID,Flame,Ice,Ascent) VALUES (9,110,80,0);
INSERT INTO Attributions (ID,Flame,Ice,Ascent) VALUES (10,110,80,0);
INSERT INTO Attributions (ID,Flame,Ice,Ascent) VALUES (11,110,80,0);
INSERT INTO Attributions (ID,Flame,Ice,Ascent) VALUES (12,110,80,0);
INSERT INTO Attributions (ID,Flame,Ice,Ascent) VALUES (13,110,80,0);
INSERT INTO Attributions (ID,Flame,Ice,Ascent) VALUES (14,110,80,0);
INSERT INTO Attributions (ID,Flame,Ice,Ascent) VALUES (15,110,80,0);
INSERT INTO Attributions (ID,Flame,Ice,Ascent) VALUES (16,110,80,0);
INSERT INTO Attributions (ID,Flame,Ice,Ascent) VALUES (17,110,80,0);
INSERT INTO Attributions (ID,Flame,Ice,Ascent) VALUES (18,110,80,0);
INSERT INTO Attributions (ID,Flame,Ice,Ascent) VALUES (19,110,80,0);
INSERT INTO Attributions (ID,Flame,Ice,Ascent) VALUES (20,110,80,0);
INSERT INTO Attributions (ID,Flame,Ice,Ascent) VALUES (21,110,80,0);
INSERT INTO Attributions (ID,Flame,Ice,Ascent) VALUES (22,110,80,0);
INSERT INTO Attributions (ID,Flame,Ice,Ascent) VALUES (23,110,80,0);
INSERT INTO Attributions (ID,Flame,Ice,Ascent) VALUES (24,110,80,0);
INSERT INTO Attributions (ID,Flame,Ice,Ascent) VALUES (25,110,80,0);
INSERT INTO Attributions (ID,Flame,Ice,Ascent) VALUES (26,110,80,0);
INSERT INTO Attributions (ID,Flame,Ice,Ascent) VALUES (27,110,80,0);
INSERT INTO Attributions (ID,Flame,Ice,Ascent) VALUES (28,110,80,0);
INSERT INTO Attributions (ID,Flame,Ice,Ascent) VALUES (29,110,80,0);
INSERT INTO Attributions (ID,Flame,Ice,Ascent) VALUES (30,110,80,0);
INSERT INTO Attributions (ID,Flame,Ice,Ascent) VALUES (101,110,80,0);
INSERT INTO Protectors (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (1,'{"SicereliaKingdom":"防具A", "DQ":"皮の盾"}','[1, 101]',10,1,0);
INSERT INTO Protectors (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (2,'{"SicereliaKingdom":"防具B", "DQ":"皮の盾"}','[1, 101]',10,2,0);
INSERT INTO Protectors (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (3,'{"SicereliaKingdom":"防具C", "DQ":"皮の盾"}','[1, 101]',10,3,0);
INSERT INTO Protectors (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (4,'{"SicereliaKingdom":"防具D", "DQ":"皮の盾"}','[1, 101]',10,4,0);
INSERT INTO Protectors (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (5,'{"SicereliaKingdom":"防具E", "DQ":"皮の盾"}','[1, 101]',10,5,0);
INSERT INTO Protectors (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (6,'{"SicereliaKingdom":"防具F", "DQ":"皮の盾"}','[1, 101]',10,6,0);
INSERT INTO Protectors (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (7,'{"SicereliaKingdom":"防具A", "DQ":"皮の盾"}','[1, 101]',10,7,0);
INSERT INTO Protectors (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (8,'{"SicereliaKingdom":"防具A", "DQ":"皮の盾"}','[1, 101]',10,8,0);
INSERT INTO Protectors (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (9,'{"SicereliaKingdom":"すごい鎧", "DQ":"ロトの鎧"}','[1, 101]',10,9,1);
INSERT INTO Protectors (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (10,'{"SicereliaKingdom":"防具A", "DQ":"皮の盾"}','[1, 101]',10,10,0);
INSERT INTO Protectors (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (11,'{"SicereliaKingdom":"防具A", "DQ":"皮の盾"}','[1, 101]',10,11,0);
INSERT INTO Protectors (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (12,'{"SicereliaKingdom":"防具A", "DQ":"皮の盾"}','[1, 101]',10,12,0);
INSERT INTO Protectors (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (13,'{"SicereliaKingdom":"防具A", "DQ":"皮の盾"}','[1, 101]',10,13,0);
INSERT INTO Protectors (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (14,'{"SicereliaKingdom":"防具A", "DQ":"皮の盾"}','[1, 101]',10,14,0);
INSERT INTO Protectors (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (15,'{"SicereliaKingdom":"防具A", "DQ":"皮の盾"}','[1, 101]',10,15,0);
INSERT INTO Protectors (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (16,'{"SicereliaKingdom":"防具A", "DQ":"皮の盾"}','[1, 101]',10,16,0);
INSERT INTO Protectors (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (17,'{"SicereliaKingdom":"防具A", "DQ":"皮の盾"}','[1, 101]',10,17,0);
INSERT INTO Protectors (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (18,'{"SicereliaKingdom":"防具A", "DQ":"皮の盾"}','[1, 101]',10,18,0);
INSERT INTO Protectors (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (19,'{"SicereliaKingdom":"防具A", "DQ":"皮の盾"}','[1, 101]',10,19,0);
INSERT INTO Protectors (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (20,'{"SicereliaKingdom":"防具A", "DQ":"皮の盾"}','[1, 101]',10,20,0);
INSERT INTO Protectors (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (21,'{"SicereliaKingdom":"防具A", "DQ":"皮の盾"}','[1, 101]',10,21,0);
INSERT INTO Protectors (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (22,'{"SicereliaKingdom":"防具A", "DQ":"皮の盾"}','[1, 101]',10,22,0);
INSERT INTO Protectors (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (23,'{"SicereliaKingdom":"防具A", "DQ":"皮の盾"}','[1, 101]',10,23,0);
INSERT INTO Protectors (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (24,'{"SicereliaKingdom":"防具A", "DQ":"皮の盾"}','[1, 101]',10,24,0);
INSERT INTO Protectors (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (25,'{"SicereliaKingdom":"防具A", "DQ":"皮の盾"}','[1, 101]',10,25,0);
INSERT INTO Protectors (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (26,'{"SicereliaKingdom":"防具A", "DQ":"皮の盾"}','[1, 101]',10,26,0);
INSERT INTO Protectors (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (27,'{"SicereliaKingdom":"防具A", "DQ":"皮の盾"}','[1, 101]',10,27,0);
INSERT INTO Protectors (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (28,'{"SicereliaKingdom":"防具A", "DQ":"皮の盾"}','[1, 101]',10,28,0);
INSERT INTO Protectors (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (29,'{"SicereliaKingdom":"防具A", "DQ":"皮の盾"}','[1, 101]',10,29,0);
INSERT INTO Protectors (ID,Name,Attributions,Gold,Rank,IsStoryItem) VALUES (30,'{"SicereliaKingdom":"防具A", "DQ":"皮の盾"}','[1, 101]',10,30,0);
INSERT INTO Actions (Class,Name,Mana,Effect,NoSystemText,Description,Flavor) VALUES ('SG0001','{"SicereliaKingdom": "レクイス", "AnotherMAP": "ザキ"}',8,'Death:"20"',0,'個体を少しの確率で昇天させる','{"SicereliaKingdom": "ふれええええええええええええいいいばああああああ"}');
INSERT INTO Actions (Class,Name,Mana,Effect,NoSystemText,Description,Flavor) VALUES ('SG0002','{"SicereliaKingdom":"ルレクイス"}',13,'Death[ForAllTargets]:"30"',0,'敵全体に対し、それぞれ少しの確率で昇天させる','{"SicereliaKingdom": "ふれふれふれいばああああああああああああああああああああ"}');
INSERT INTO Actions (Class,Name,Mana,Effect,NoSystemText,Description,Flavor) VALUES ('SG0003','{"SicereliaKingdom": "ヨスガノハテーアイ・ルレクイス"}',0,'Text:"いずれにせよ最後の呪文になると思うの。\n二度と口をきけない私を、あなたが抱き止めてくれたなら、\nきっと心だけは帰ってこれるよ。" Text: SYSINVTEXT Death[ForAllTargets]:"100"  Death[Myself]:"100"',1,'ヨスガノハテ、キミノタメナラ','{"SicereliaKingdom": "きっと、これが最後の呪文になると思うの。もし君が私を抱いていてくれたなら、心だけは帰って来られる。そんな気がするんだ。"}');
INSERT INTO Actions (Class,Name,Mana,Effect,NoSystemText,Description,Flavor) VALUES ('SA0001L','{"SicereliaKingdom": "サイス", "FFVIII": "ケアル"}',3,'Heal:"20+RAND[-2,2]"',0,'個体のHPを20程度回復させる','{"SicereliaKingdom": "あいうあいうあいうあいあうういあいうあ"}');
INSERT INTO Actions (Class,Name,Mana,Effect,NoSystemText,Description,Flavor) VALUES ('SA0001M','{"SicereliaKingdom": "サイサー", "FFVIII": "ケアルラ"}',3,'Heal:"50+RAND[-2,2]"',0,'個体のHPを50程度回復させる','{"SicereliaKingdom": "あああああああああああああああああああああああ"}');
INSERT INTO Actions (Class,Name,Mana,Effect,NoSystemText,Description,Flavor) VALUES ('SA0001H','{"SicereliaKingdom": "サイスレル", "FFVIII": "ケアルガ"}',3,'Heal:"Target.MaxHP-Target.HP"',0,'個体のHPを完全回復させる','{"SicereliaKingdom": "aaaaaaaaaaaaaaaaaaa"}');
INSERT INTO Actions (Class,Name,Mana,Effect,NoSystemText,Description,Flavor) VALUES ('PA0001','{"SicereliaKingdom":"爆裂拳", "AnotherMAP":"４回殴る"}',0,'Damage:"REP[4, (Actor.ATK*2 / Target.DEF) * Bernoulli[80] ]"',0,'ぽかぽかなぐる','{"SicereliaKingdom": "かきくけこ"}');
