#include <iostream>
#include <algorithm>
#include <vector>
#include <string>
#include <sstream>
#include <boost/algorithm/string.hpp>

using namespace std;

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

string const Separator = "\t";

int main(int argc, char** argv) {

  vector<string> items;

  string str; getline(cin, str);
  vector<string> v;
  boost::algorithm::split(v, str, boost::is_any_of(Separator));
  int M = v.size();
  rep(i, M) {
    items.push_back(v[i]);
  }

  vector<vector<string>> contents;
  vector<bool> is_column_string(M, false);
  int N = 0;
  while(getline(cin, str)) {
    N++;
    vector<string> v;
    boost::algorithm::split(v, str, boost::is_any_of(Separator));
    rep(i, M) {
      if(!isdigit(v[i][0])) {         // 先頭が数値化どうかで判断。double型などもあるので
        is_column_string[i] = true;
      }
      else {
        v[i] = to_string(stoi(v[i])); // !!! INTEGER ONLY !!!
      }
    }
    contents.push_back(v);
  }

  if(argc < 2) {
    cerr << "Usage: ./csv2sql [table name] < [csv file]";
    exit(1);
  }

  cerr << "#(rows): " << N << endl;

  string table = argv[1];
  cerr << "table name: " << table << endl;
  rep(i, N) {
    cout  << "INSERT INTO " << table << " (";
    rep(j, M) {
      if(j) cout << ',';
      cout << items[j];
    }

    cout << ") VALUES (";
    rep(j, M) {
      if(j) cout << ',';
      if(is_column_string[j]) { cout << '\''; }
      cout << contents[i][j];
      if(is_column_string[j]) { cout << '\''; }
    }

    cout << ");\n";
  }
  return 0;
}