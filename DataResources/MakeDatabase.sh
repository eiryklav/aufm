#!/bin/sh

rm database.db

./tsv2sql Accessories   < accessories.tsv   > AccessoriesSQL/Content.sql
./tsv2sql Characters    < characters.tsv    > CharactersSQL/Content.sql
./tsv2sql Items         < items.tsv         > ItemsSQL/Content.sql
./tsv2sql Weapons       < weapons.tsv       > WeaponsSQL/Content.sql
./tsv2sql Attributions  < attributions.tsv  > AttributionsSQL/Content.sql
./tsv2sql Protectors    < protectors.tsv    > ProtectorsSQL/Content.sql
./tsv2sql Actions       < actions.tsv       > ActionsSQL/Content.sql

cat AccessoriesSQL/Accessories.sql CharactersSQL/Characters.sql ItemsSQL/Items.sql WeaponsSQL/Weapons.sql AttributionsSQL/Attributions.sql ProtectorsSQL/Protectors.sql ActionsSQL/Actions.sql > AllDatabaseSQL.sql
cat AccessoriesSQL/Content.sql CharactersSQL/Content.sql ItemsSQL/Content.sql WeaponsSQL/Content.sql AttributionsSQL/Content.sql ProtectorsSQL/Content.sql ActionsSQL/Content.sql >> AllDatabaseSQL.sql

db=database.db

sqlite3 database.db <<END
.read AllDatabaseSQL.sql
END