
/*
	ClassP: 	物理攻撃 ('P'hysical Attack)
	SYSINVTEXT:「AはBをはなった！」
*/

-- 単体通常攻撃
INSERT INTO Actions
(
	Class,
	Name,
	TargetType,
	NoSystemText
)
VALUES
(
	'P0001',
	'{{"Sicerelia": "攻撃"}, {"幻の大地": "攻撃"}}',
	'Text: "@Actorの攻撃！"
	AND
	Damage: "Actor.ATK*2 / Target.DEF + Rand[-1,1]"',
	1
);

-- 爆裂拳
INSERT INTO Actions
(
	Class,
	Name,
	TargetType,
	NoSystemText
)
VALUES
(
	'P0001',
	'{{"ほげ国": "ボコボコ"}, {"幻の大地": "ばくれつけん"}}',
	'Damage: "REP[4, (Actor.ATK*2 / Target.DEF) * Bernoulli[80] ]"'	-- 2割の確率でミスする攻撃を4回
);
