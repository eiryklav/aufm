
/*
	ClassSR: 	回復魔法 (Spell :: 'R'ecover)
	SYSINVTEXT:	「AはBを唱えた！」
*/

-- サイス
INSERT INTO Actions
(
	Class,
	Name,
	TargetType,
	TargetForGroup,
	Effect
)
VALUES
(
	'SR0001',
	'{{"Sicerelia": "サイス"}, {"幻の大地": "ホイミ"}}',
	'Heal:30+-4'
);

-- サイサー
INSERT INTO Actions
(
	Class,
	Name,
	TargetType,
	TargetForGroup,
	Effect
)
VALUES
(
	'SR0002',
	'{{"Sicerelia": "サイサー"}, {"幻の大地": "ベホマラー"}}',
	'Heal:100+-10'
);

-- サイスレル
INSERT INTO Actions
(
	Class,
	Name,
	TargetType,
	TargetForGroup,
	Effect
)
VALUES
(
	'SR0003',
	'{{"Sicerelia": "サイスレル"}, {"幻の大地": "ベホマズン"}}',
	'Heal:999'
);
