
/*
	Class: 			A0001 など
	Name:			JSON形式 "{{"Sicerelia": "レクイス"}, {"幻の大地": "ザキ"}}"
	Mana:			消費Mana(符号付きにしようか？)
	Effect:
		Effect 		:= +(Text | Action | Invocation | SetTar)
		Text 		:= "Text" ":" DQuot (string literals) DQuot
		DQuot		:= """
		Invocation 	:= "Invocation" ":" "Class" 				// 再帰的になる
		SetTar		:= "SetTarget" ":" """ ("ForOne" | "ForAll" | "Myself") """
		Action		:= (Subs ":" """ Expression """
					| 	Cond ":" """ Expression(0-100) """)
		Subs		:= "Damage" | "Heal"
		Cond 		:= "Poison" | "Palsy" | "Death"
		Expression	:= Term *(Operator+- Term)
		Operator+-	:= "+", "-"
		Term		:= Factor *(Operator*\/ Factor)
		Operator*\/	:= "*", "/"
		Factor		:= (signed) ( (int) | "(" Expression ")" | Func )
		Func		:= ^[数字記号]+(alphabet)[...]	// Ex) RAND[(int),(int)] | BERNOULLI[(int 0-100)]

		Macro		:= Macro[Args...] // 別の文に置換可能なもの

		* Action は、すべて左から順に評価される。
		* C++の実装に データを入力したくないので Special をボツにして、
		  代わりにログを出力させる Text を導入した。これによって、技の施行は Effect をパースするのみで良くなる
		* Invocation によって、再帰処理を可能にして「FFXの『ものまね』」程度は Effect の記述のみで完結？（仕様未定）
		* REPなどはマクロとして導入すべきか？

	// RequiredArgs:	Effect が要求する引数, 特に要求がなければ無視される。
					引数のセパレータは ','
	NoSystemText:	「AはBを唱えた！」のような定型文を表示させない。
*/
	
CREATE TABLE Actions
(
	Class 				NCHAR(5) PRIMARY KEY,
	Name    	    NVARCHAR,
	Mana					INTEGER,
	Effect  			NVARCHAR,
	NoSystemText	BOOLEAN,
	Description		NVARCHAR,
	Flavor				NVARCHAR
);
