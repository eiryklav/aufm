
/*
	ClassSH: 	昇天魔法
	SYSINVTEXT: 「AはBを唱えた！」
*/

-- レクイス
INSERT INTO Actions (
	Class,
	Name,
	TargetType,
	TargetForGroup,
	Effect,
	NoSystemText
)
VALUES
(
	'SH0001',
	'{{"Sicerelia": "レクイス"}, {"幻の大地": "ザキ"}}',
	'Death:"20"'
);

-- ルレクイス
INSERT INTO Actions (
	Class,
	Name,
	Effect
)
VALUES
(
	'SH0002',
	'{{"Sicerelia": "ルレクイス"}, {"幻の大地": "ザラキーマ"}}',
	'Death:"40"'
);

-- ヨスガノハテ ー アイ・ルレクイス
INSERT INTO Actions (
	Class,
	Name,
	Effect,
	NoSystemText
)
VALUES
(
	'SH0003',
	'{"Sicerelia": "ヨスガノハテーアイ・ルレクイス", "FFVIII": "ジ・エンド"}',
	'
	Text:"いずれにせよ最後の呪文になると思うの。\n二度と口をきけない私を、あなたが抱き止めてくれたなら、\nきっと心だけは帰ってこれるよ。"
	Text: SYSINVTEXT
	Death:"100"
	Invocation: “H9999”
	'
	-- "H9999": 自殺用の利便魔法
	,
	TRUE
);


