/*
  ID:       INTEGER (3桁, 0埋め)
  Name:     JSON Object "{{"Sicerelia": "フラヴァス"}, {"DQ7": "神様"}}"
  
  雨の獣＝ステータス、出現地点、EXP、ドロップアイテム･･････フレーバー。

  ex.シスレリアの龍、フラヴァス　Dragon in Sicerelia, Fravace
  HP  1200
  Ma  5000 
  ATK   189
  DEF   97 
  SP    213
  LCK   44
  INT   79

  EXP:      INTEGER
  ItemDrops:    JSON Array  [["50", "0001"], ["25", "0101"], ["25", "-1"]]
  
  ATTR(EQP):    JSON Array ["0001", "0101"] / 昇天無効、ATK依存攻撃カット20%
  AI:       JSON ...AI / 最大効率ＡＩ(やばそう)
  
  以下AIが受けもつ（敵の覚えている技を項目として列挙させる必要が生じたらその時はその時。多分別に文字列を登録することとなる）
  ・リア
  ・セレリア
  ・なぎ払い（ATK依存全体１）
  ・フラヴ（炎熱単体１）
  ・アヴァル（水流全体２）
  ・ヴァーセフ（雷撃全体３）
  ・フラヴァス（？）

*/

CREATE TABLE Characters
(
  ID INTEGER PRIMARY KEY,
  Name NVARCHAR,
  FullName NVARCHAR,
  Exp INTEGER,
  ItemDrops NVARCHAR,
  FlavorText NVARCHAR,
  Gold INTEGER,
  LV INTEGER,
  HP INTEGER,
  Mana INTEGER,
  ATK INTEGER,
  DEF INTEGER,
  SP INTEGER,
  LCK INTEGER,
  INTL INTEGER,
  Equipment NVARCHAR,
  Attributions NVARCHAR,
  ArtificialIntelligence NVARCHAR,
  MasteredActions NVARCHAR,
  MasterActionsWithLevel NVARCHAR,
  AllActionsToMasater NVARCHAR,
  ChangeAI NVARCHAR
);