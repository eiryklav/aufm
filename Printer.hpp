#pragma once

#include <iostream>
#include <sstream>
#include <queue>
#include <iomanip>

#include "GameState/GameState.hpp"

#include "Support/Debugger.hpp"
#include "TimeManager.hpp"

namespace Printer
{
  
  enum MessageType: int
  {
    /*
     主テロップ: 状態変化, 仲間になる, 仲間(プレイヤー)の死亡
     副テロップ: ...
     シナリオ会話: 従者が存在＆フラグを踏む
     */
    Default = 0,
    EnterCity,
    EnterDungeon,
    GetRareItem,
    AddServant,
    FailedToAddServantBecauseOfOverflow,
    FailedToAddServantBecauseOfInvitationRatio,
    PlayerSideDeath,
    ScenarioConversation,
    
    SysEndConditionCompleteTargetForDefeat, // FOR DEBUG
    SysEndConditionEndOfScenarioSuccess,
    SysEndConditionEndOfScenarioFailure,
    SysEndConditionPlayerHasDied,
    SysEndConditionTimeLimitExceeded,
    SysEndConditionDeadEndOfRoad,
    SysEndConditionDeadEndInDungeon,
    SysBattleStart,
    SysBattleEnd,
    SysNoEncount,
    SysLampMoverGenerated,
    SysLampMoverKilled,
  };
  
  class Message
  {
  private:
    MessageType           type_;
    StateType             stateType_;
    std::string           text_;
    int                   lineNum_;   // ログの行数の調整などに使いたかったら利用する
    TimeManager::RealTime timeStamp_;
    bool                  isAlert_;
    std::string           position_;
    bool                  hidden_;    // ログをプレイヤーに見せるかどうか
    
  public:
    Message(MessageType type, std::string const& text = "", bool alert = false, std::string position = "", bool hidden = false);
    
    inline auto getType()         const                   { return type_; }
    inline auto getStateType()    const                   { return stateType_; }
    inline auto getText()         const -> decltype(auto) { return (text_); }
    inline auto getTextLineNum()  const                   { return lineNum_; }
    inline auto getTimeStamp()    const -> decltype(auto) { return (timeStamp_); }
    inline auto isAlert()         const                   { return isAlert_; }
    inline auto getPosition()     const -> decltype(auto) { return (position_); }
    inline auto isHidden()        const                   { return hidden_; }
  };
  
  class Conversation
  {
  private:
    std::string speakerName_;
    std::string content_;
    std::string summary_;
    TimeManager::RealTime timeStamp_;
    bool        isAlert_;
    
  public:
    Conversation(std::string const& speakerName, std::string const& content, std::string const& summary, bool alert = false);
    inline auto getSpeakerName()  const -> decltype(auto) { return (speakerName_); }
    inline auto getContent()      const -> decltype(auto) { return (content_); }
    inline auto getSummary()      const -> decltype(auto) { return (summary_); }
    inline auto getTimeStamp()    const -> decltype(auto) { return (timeStamp_); }
    inline auto isAlert()         const                   { return isAlert_; }
  };
  
  struct Printable
  {
    virtual auto print() const -> std::string = 0;
  };

  namespace Messages
  {
#define RETURN_SYSTEM_MESSAGE(type, istream)\
  std::stringstream ss; ss << istream; \
  return Message(MessageType::type, ss.str(), false, "", true);
    
#define RETURN_MESSAGE(type, istream)\
    std::stringstream ss; ss << istream; \
    return Message(MessageType::type, ss.str());
    
#define RETURN_ALERT_MESSAGE(type, istream)\
  std::stringstream ss; ss << istream; \
  return Message(MessageType::type, ss.str(), true);
    
    namespace Registered
    {
      // loadWorldConfigFromJSON() で登録される
      namespace Shops {
        namespace Weapon {
          extern std::string EnterText;
          extern std::string ExitText;
        }
        namespace Protector {
          extern std::string EnterText;
          extern std::string ExitText;
        }
        namespace Accessory {
          extern std::string EnterText;
          extern std::string ExitText;
        }
      }
    }
    
    inline auto normalAttack(std::string const& actor)
    {
      RETURN_MESSAGE(Default, actor << "の攻撃！")
    }
    
    inline auto defeat(std::string const& target)
    {
      RETURN_MESSAGE(Default, target << "を倒した")
    }

    inline auto castSpecialAbility(std::string const& actor, std::string const& name)
    {
      RETURN_MESSAGE(Default, actor << "が" << name << "を放った！")
    }
    
    inline auto masteredAction(std::string const& actionName)
    {
      RETURN_MESSAGE(Default, actionName << "を覚えた")
    }

    inline auto castSpell(std::string const& actor, std::string const& name)
    {
      RETURN_MESSAGE(Default, actor << "が" << name << "を唱えた！")
    }
    
    inline auto getHeal(std::string const& target, int val)
    {
      RETURN_MESSAGE(Default, target << "のHPが" << val << "回復した")
    }

    inline auto getDamage(std::string const& target, int val)
    {
      std::stringstream ss;
      if(val == 0) {
        ss << target << "はダメージを受けなかった";
      }
      else {
        ss << target << "は" << val << "のダメージを受けた";
      }
      return Message(MessageType::Default, ss.str());
    }

    inline auto getDeath(std::string const& target, bool isPlayer)
    {
      std::stringstream ss(target+(isPlayer ? "は死んでしまった" : "を倒した"));
      if(isPlayer) { return Message(MessageType::PlayerSideDeath, ss.str()); }
      return Message(MessageType::Default, ss.str(), isPlayer);
    }
    
    #define TXT_GET_COND_DEF(type, message) \
    inline auto get##type(std::string const& target)  \
    { \
      std::stringstream ss(target+message); \
      return Message(MessageType::Default, ss.str());  \
    }
        
    #define TXT_ALREADY_GET_COND_DEF(type, message) \
    inline auto alreadyGet##type(std::string const& target)  \
    { \
      std::stringstream ss(target+message); \
      return Message(MessageType::Default, ss.str());  \
    }
        
    #define TXT_GAIN_STAT_DEF(type, what)  \
    inline auto gain##type(int val) \
    { \
      std::stringstream ss; \
      ss << what << " +" << val << " "; \
      return Message(MessageType::Default, ss.str());  \
    }

    TXT_GET_COND_DEF(Poison,      "は毒を浴びてしまった")
    TXT_GET_COND_DEF(Palsy,       "は麻痺してしまった")
    TXT_GET_COND_DEF(Unaffected,  "には効かなかった")

    TXT_ALREADY_GET_COND_DEF(Poison,  "は既に毒を浴びている")
    TXT_ALREADY_GET_COND_DEF(Palsy,   "は既に麻痺している")
    
    inline auto levelUp(std::string const& target)
    {
      RETURN_MESSAGE(Default, target << "のレベルが上がった")
    }
    
    TXT_GAIN_STAT_DEF(LV,   "レベル")
    TXT_GAIN_STAT_DEF(HP,   "HP")
    TXT_GAIN_STAT_DEF(Mana, "TP")
    TXT_GAIN_STAT_DEF(ATK,  "攻撃力")
    TXT_GAIN_STAT_DEF(DEF,  "防御力")
    TXT_GAIN_STAT_DEF(SP,   "素早さ")
    TXT_GAIN_STAT_DEF(LCK,  "運")
    TXT_GAIN_STAT_DEF(INT,  "賢さ")

    inline auto defeatedEnemyGroup()
    {
      RETURN_MESSAGE(Default, "魔物の群れをやっつけた")
    }
    
    inline auto enemyDroppedItem(std::string const& enemy)
    {
      RETURN_MESSAGE(Default, enemy << "は宝箱を落とした")
    }

    inline auto getItem(std::string const& item)
    {
      // アイテム入手は戦闘ログと共通にしているが分離するが必要ありそう。
      RETURN_MESSAGE(Default, item << "を手に入れた")
    }
    
    inline auto getRareItem(std::string const& item)
    {
      RETURN_MESSAGE(Default, item << "を手に入れた")
    }

    inline auto failedToGetItemDueToMaxDuplicates(std::string const& item)
    {
      RETURN_MESSAGE(Default, "しかし、" << item << "はこれ以上詰めることができない")
    }

    inline auto failedToGetItemDueToMaxAllItems(std::string const& item)
    {
      RETURN_MESSAGE(Default, "しかし、" << item << "はリュックがいっぱいで詰めることができない")
    }
    
    inline auto useItemInBattle(std::string const& actor, std::string const& item)
    {
      RETURN_MESSAGE(Default, actor << "は" << item << "を使った")
    }
    
    /*
    inline auto useItemForAllyInBattle(std::string const& actor, std::string const& ally, std::string const& item)
    {
      std::stringstream ss;
      ss << actor << "は" << ally << "に" << item << "を与えた";
      return Message(MessageType::Default, ss.str());
    }
    
    inline auto useItemAgainstOppoInBattle(std::string const& actor, std::string const& oppo, std::string const& item)
    {
      std::stringstream ss;
      ss << actor << "は" << oppo << "に" << item << "を放った";
      return Message(MessageType::Default, ss.str());
    }
     */
    
    inline auto openDrawer(std::string const& player)
    {
      RETURN_MESSAGE(Default, "はタンスを開けた。")
    }

    inline auto foundItEmpty()
    {
      RETURN_MESSAGE(Default, "しかし、中は空っぽだった")
    }

    inline auto getMoney(int val, std::string const& monetaryUnit)
    {
      RETURN_MESSAGE(Default, val << monetaryUnit << "手に入れた")
    }
    
    inline auto startRPG(std::string const& fullWorldName)
    {
      RETURN_ALERT_MESSAGE(Default, fullWorldName << "に入国しました")
    }
    
    #define TXT_ENTER_SHOP_DEF(shopType)  \
    inline auto enter ## shopType ## Shop() \
      { \
        return Message(MessageType::Default, Registered::Shops::shopType::EnterText);  \
      }
    
    TXT_ENTER_SHOP_DEF(Weapon)
    TXT_ENTER_SHOP_DEF(Protector)
    TXT_ENTER_SHOP_DEF(Accessory)

    inline auto listUpGoods(std::string const& eqpName, int val, std::string const& unitName)
    {
      RETURN_MESSAGE(Default, std::left << std::setw(20) << eqpName << "|" << std::right << std::setw(8) << val << unitName)
    }

    inline auto buyGoods(std::string const& goodsName, int val, std::string const& monetaryUnitName)
    {
      RETURN_MESSAGE(Default, goodsName << "を" << val << monetaryUnitName << "で購入した")
    }
    
    inline auto throwAwayGoodsSoonAfterTakeInHand(std::string const& goodsName, std::string const& whoseHand = "")
    {
      std::stringstream ss;
      if(whoseHand.empty()) {
        ss << "しかし、もはや" << goodsName << "は必要ないので捨てた";
      }
      else {
        ss << "しかし" << whoseHand << "は、必要ないので" << goodsName << "を捨てた";
      }
      return Message(MessageType::Default, ss.str());
    }
    
    inline auto letPlayerEquip(std::string const& player, std::string const& equipment)
    {
      RETURN_MESSAGE(Default, player << "は" << equipment << "を装備した")
    }
    
    /*
    inline auto buyEquipmentFor(std::string const& what, int val, std::string const& monetaryUnitName, std::string const& who)
    {
      std::stringstream ss;
      ss << what << "を" << val << monetaryUnitName << "で購入し、" << who << "が装備した";
      return Message(MessageType::Default, ss.str());
    }
     */
    
    #define TXT_EXIT_SHOP_DEF(shopType) \
      inline auto exit ## shopType ## Shop()  \
      { \
        return Message(MessageType::Default, Registered::Shops::shopType::ExitText); \
      }
    
    TXT_EXIT_SHOP_DEF(Weapon)
    TXT_EXIT_SHOP_DEF(Protector)
    TXT_EXIT_SHOP_DEF(Accessory)

    inline auto addServant(std::string const& character)
    {
      RETURN_MESSAGE(AddServant, character << "が仲間に加わった")
    }
    
    inline auto failedToAddServantBecauseOfOverflow(std::string const& character)
    {
      RETURN_MESSAGE(FailedToAddServantBecauseOfOverflow, character << "が仲間に加わろうとした。しかしこれ以上仲間を連れて行くことが出来ない")
    }
    
    inline auto failedToAddServantBecauseOfInvitationRatio(std::string const& character)
    {
      RETURN_MESSAGE(FailedToAddServantBecauseOfInvitationRatio, character << "が仲間に加わろうとした。しかし今は仲間に引き入れたくない")
    }
    
    inline auto putUpAtInnMaxHeal()
    {
      RETURN_MESSAGE(Default, "(FOR DEBUG - ステータスの回復量で観察すればOK)十分な睡眠をとることが出来た")
    }
    
    inline auto putUpAtInnHalfHeal()
    {
      RETURN_MESSAGE(Default, "(FOR DEBUG - ステータスの回復量で観察すればOK)睡眠が不十分だ")
    }
    
    inline auto endConditionEndOfScenarioSuccess()
    {
      RETURN_MESSAGE(SysEndConditionEndOfScenarioSuccess, "目標の討伐に成功した(type: 'success')")
    }
    
    inline auto endConditionEndOfScenarioFailure()
    {
      RETURN_MESSAGE(SysEndConditionEndOfScenarioFailure, "目標の討伐に失敗した(type: 'bad ending')")
    }
    
    inline auto endConditionPlayerHasDied()
    {
      RETURN_SYSTEM_MESSAGE(SysEndConditionPlayerHasDied, "")
    }
    
    inline auto endConditionTimeLimitExceeded()
    {
      RETURN_MESSAGE(SysEndConditionTimeLimitExceeded, "ディストピアを防ぎきれなかった(type: 'time limit exceeded')")
    }
    
    inline auto endConditionDeadEndInDungeon(std::string const& hero)
    {
      RETURN_MESSAGE(SysEndConditionDeadEndInDungeon, hero << "たちは遭難し、消息不明となった(type: 'dead end')")
    }
    
    inline auto endConditionDeadEndOfRoad(std::string const& hero)
    {
      RETURN_MESSAGE(SysEndConditionDeadEndOfRoad, hero << "たちは旅を終え、世界は終焉した(type: 'dead end')")
    }
    
    inline auto endConditionCompleteTargetForDefeat()
    {
      RETURN_MESSAGE(SysEndConditionCompleteTargetForDefeat, "(FOR DEBUG)討伐目標を完遂した")
    }
    
    inline auto scenarioConversation(std::string const& conversation)
    {
      RETURN_ALERT_MESSAGE(ScenarioConversation, conversation)
    }
    
    inline auto openTreasureDuringJourney(std::string const& hero)
    {
      RETURN_MESSAGE(Default, hero << "は道中で宝箱を見つけた")
    }
    
    inline auto encountEnemy(std::string const& name)
    {
      RETURN_MESSAGE(Default, name << "が現れた")
    }

    inline auto sysBattleStart(std::string const& encountTypePrefix,
                               int numOfEnemies,
                               std::string position,
                               std::string const& groupName = "") {
      std::stringstream ss;
      ss << encountTypePrefix << "エンカウント";
      if(numOfEnemies == 1) { ss << "　単体"; }
      else { ss << "　" << numOfEnemies << "体"; }
      if(!groupName.empty()) { ss << "　" << groupName; }
      return Message(MessageType::SysBattleStart, ss.str(), false, position, true);
    }
    
    inline auto sysBattleEnd(int defeatedTargetEnemiesNum)
    {
      // defeatedTargetEnemiesNum は現在未使用
      RETURN_SYSTEM_MESSAGE(SysBattleEnd, defeatedTargetEnemiesNum);
    }
    
    inline auto sysNoEncount()
    {
      RETURN_SYSTEM_MESSAGE(SysNoEncount, "");
    }
    
    inline auto sysLampMoverGenerated(std::string lampMoverID)
    {
      RETURN_SYSTEM_MESSAGE(SysLampMoverGenerated, lampMoverID);
    }
    
    inline auto sysLampMoverKilled(std::string lampMoverID)
    {
      RETURN_SYSTEM_MESSAGE(SysLampMoverKilled, lampMoverID);
    }
    
    
#undef RETURN_MESSAGE
#undef RETURN_ALERT_MESSAGE
#undef RETURN_SYSTEM_MESSAGE
  }
}