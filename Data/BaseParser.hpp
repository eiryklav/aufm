#pragma once

#include <iostream>
#include <string>
#include <assert.h>
#include <cstdlib>
#include <numeric>
#include <sstream>
#include <set>

#include "../Support/Misc.hpp"

class BaseParser
{
public:
	using number_type = double;

protected:
	using string_literal = std::string;
	using string_tuple_type = std::vector<std::string>;

	using separator = char;
	using reserved_word = std::string;
	using symbol = char;

	std::string input;
	std::string::const_iterator iter;
	std::string::const_iterator end;

protected:

	static char constexpr no_match_operator = 0;
	inline auto debugger() { PARSER_DUMP() }

	inline auto consume(char ch)
	{
		if(*iter == ch) { iter++; return; }
		AUFM_ASSERT(false, "consume() error");
	}

	inline auto consumeif(bool condition)
	{
		if(condition) { consume(*iter); return true; }
		else { return false; }
	}

	inline auto is_space_char(char ch)
	{
		return ch == ' ' || ch == '\t' || ch == '\n' || ch == '\r';
	}

	inline auto skip_space()
	{
		while(consumeif(is_space_char(*iter)));
	}

	inline auto eof_skipped()
	{
		skip_space();
		if(iter == end) { return true; }
		return false;
	}

	inline auto consume_skipped(char ch)
	{
		skip_space();
		consume(ch);
	}
  
  inline auto consumeif_char(char ch)
  {
    return consumeif(*iter == ch);
  }

	inline auto consumeif_char_skipped(char ch)
	{
		skip_space();
		return consumeif(*iter == ch);
	}

	inline auto escape_char()
	{
		if(consumeif(*iter == 'n')) { return '\n'; }
		AUFM_ASSERT(false, "not reserved escape character.");
	}

protected:

	std::set<symbol> const symbol_set = {
		':', '[', ']', ','
	};

	inline auto get_keyword()
	{
		skip_space();
		reserved_word ret;
		for(; iter!=end; iter++) {
			if(is_space_char(*iter) || symbol_set.count(*iter)) {
				return ret;
			}
			else {
				ret += *iter;
			}
		}
		AUFM_ASSERT(false, "Symbol has not been found.");
	}

	inline auto get_string_literal()
		-> string_literal
	{
		string_literal ret;
		if(consumeif_char_skipped('"')) {
			while(*iter != '"') {
				if(iter == end) { assert(false && "in get_string_literal(): not found closing DQuot"); }
				if(consumeif(*iter == '\\')) {
					ret += escape_char();
				}
				else {
					ret += *iter++;
				}
			}
			consume('"');
			return ret;
		}
		else {
			AUFM_ASSERT(false, "in get_string_literal(): not found opening DQuot");
		}
	}
	
	inline auto get_string_tuple()
		-> string_tuple_type
	{
		string_tuple_type ret;
		if(consumeif_char_skipped('(')) {
			do {
				ret.emplace_back(get_string_literal());
			} while(consumeif_char_skipped(','));
			consume(')');
			return ret;
		}
		else {
			AUFM_ASSERT(false, "not found opening bracket.");
		}
	}

private:

	inline auto init()
	{
		std::string nstr;
		for(iter=input.begin(), end = input.end(); iter!=end;) {
			if(*iter == '"') {
				nstr += '"'; nstr += get_string_literal(); nstr += '"';
			}
			else if(is_space_char(*iter)) {
				nstr += ' ';
				skip_space();
			}
			else {
				nstr += *iter;
				consume(*iter);
			}
		}

		input = nstr;
		iter = input.begin();
		end  = input.end();
	}

protected:

	BaseParser(std::string in)
		: input(in)
	{
		init();
	}

};

class ExpressionParser : public BaseParser
{
public:

	ExpressionParser(std::string in)
		: BaseParser(in)
	{ }

private:

	/*
   typedef char operator_symbol;
   operator_symbol static constexpr no_match_operator = 0;
   */

  enum class operator_symbol
  {
    unary_plus,
    unary_minus,
    unary_boolean_not,
    
    multiply,
    divide,
    
    add,
    subtract,
    
    and_,
    or_,
    
    gt,
    gte,
    lt,
    lte,
    
    equal_to,
    
    no_match_operator,
    
  };
  
	inline auto get_unary_op()
    -> operator_symbol
	{
    if(consumeif_char_skipped('+')) { return operator_symbol::unary_plus; }
    if(consumeif_char_skipped('-')) { return operator_symbol::unary_minus; }
    if(consumeif_char_skipped('!')) { return operator_symbol::unary_boolean_not; }
    return operator_symbol::no_match_operator;
	}
  
  inline auto get_binary_op_in_term()
    -> operator_symbol
  {
    if(consumeif_char_skipped('*')) { return operator_symbol::multiply; }
    if(consumeif_char_skipped('/')) { return operator_symbol::divide; }
    return operator_symbol::no_match_operator;
  }

	inline auto get_binary_op_in_add_subtract()
    -> operator_symbol
	{
    if(consumeif_char_skipped('+')) { return operator_symbol::add; }
    if(consumeif_char_skipped('-')) { return operator_symbol::subtract; }
    return operator_symbol::no_match_operator;
	}
  
  inline auto get_binary_op_in_boolean()
    -> operator_symbol
  {
    if(consumeif_char_skipped('&')) {
      if(consumeif_char('&')) { return operator_symbol::and_; }
      AUFM_ASSERT(false, "Unsupported bitwise operator");
    }
    if(consumeif_char_skipped('|')) {
      if(consumeif_char('|')) { return operator_symbol::or_; }
      AUFM_ASSERT(false, "Unsupported bitwise operator");
    }
    return operator_symbol::no_match_operator;
  }
  
  inline auto get_binary_op_in_relation()
    -> operator_symbol
  {
    if(consumeif_char_skipped('>')) {
      if(consumeif_char('=')) { return operator_symbol::gte; }
      return operator_symbol::gt;
    }
    if(consumeif_char_skipped('<')) {
      if(consumeif_char('=')) { return operator_symbol::lte; }
      return operator_symbol::lt;
    }
    if(consumeif_char_skipped('=')) {
      if(consumeif_char('=')) { return operator_symbol::equal_to; }
      AUFM_ASSERT(false, "Unsupported assignment operator");
    }
    return operator_symbol::no_match_operator;
  }

private:

	inline auto number()
	{
		std::string str;
		while(isdigit(*iter) || *iter == '.') {
			str += *iter++;
		}
		return std::stoi(str);
	}

	inline auto func()
		-> number_type
	{
		auto keyword = get_keyword();
		if(keyword == "RAND") {
			consume_skipped('[');
			auto a = expression(); consume_skipped(','); auto b = expression();
			consume_skipped(']');
			return Misc::randomRange(a, b);
		}
		if(keyword == "BERNOULLI") {
			consume_skipped('[');
			auto p = number();
			consume_skipped(']');
			return static_cast<int>( Misc::randomBernoulli(p) );
		}
    if(keyword == "MAX") {
      consume_skipped('[');
      auto a = expression(); consume_skipped(','); auto b = expression();
      consume_skipped(']');
      return std::max(a, b);
    }
    if(keyword == "MIN") {
      consume_skipped('[');
      auto a = expression(); consume_skipped(','); auto b = expression();
      consume_skipped(']');
      return std::min(a, b);
    }
		assert(false && "Undefined function");
	}

	inline auto factor()
		-> number_type
	{
		number_type ret = 1;

		auto unary_op = get_unary_op();
    auto boolean_not_flag = false;
    
    if(unary_op == operator_symbol::unary_minus) {
			ret = -1;
		}
    else if(unary_op == operator_symbol::unary_plus ||
            unary_op == operator_symbol::no_match_operator) {
			ret = +1;
		}
    else if(unary_op == operator_symbol::unary_boolean_not) {
      boolean_not_flag = true;
    }
		else {
			AUFM_ASSERT(false, "Undefined unary operator");
		}

		skip_space();

		if(isdigit(*iter)) {
			ret *= number();
		}
		else if(consumeif_char_skipped('(')) {
			ret *= expression();
			consume_skipped(')');
		}
		else {
			if(!isalpha(*iter)) { AUFM_ASSERT(false, "Undefined factor symbol"); }
			ret *= func();
		}
    
    if(boolean_not_flag) {
      if(ret == 0)  { ret = 1; }
      else          { ret = 0; }
    }

		return ret;
	}

	inline auto term()
		-> number_type
	{
		auto ret = factor();

		for(;;) {

			auto ope = get_binary_op_in_term();
      
      if(ope == operator_symbol::multiply) {
				auto f = factor();
				if(ret > std::numeric_limits<number_type>::max() / f) {
					AUFM_ASSERT(false, "Arithmetic over flow.");
				}
				ret *= f;
			}
      else if(ope == operator_symbol::divide) {
				auto f = factor();
				if(f < 1e-8) { AUFM_ASSERT(false, "Devide by 0"); }
				ret /= f;
			}
			else {
				break;
			}
		}
		return ret;
	}

public:

	inline auto add_subtract()
		-> number_type
	{
		auto ret = term();

		for(;;) {
			auto ope = get_binary_op_in_add_subtract();

      if(ope == operator_symbol::add) {
				auto t = term();
				if(ret > std::numeric_limits<number_type>::max() - t) {
					AUFM_ASSERT(false, "Arithmetic over flow.");
				}
				ret += t;
			}
      else if(ope == operator_symbol::subtract) {
				auto t = term();
				if(ret < std::numeric_limits<number_type>::lowest() / 2.0 + t) {
					AUFM_ASSERT(false, "type: " << (typeid(number_type).name()) << " std::numeric_limits<number_type>::lowest(): " << std::numeric_limits<number_type>::lowest() << " ret: " << ret << ", t: " << t << " / Arithmetic over flow.");
				}
				ret -= t;
			}
			else {
				break;
			}
		}
		return ret;
	}

  inline auto boolean_operation()
    -> number_type
  {
    auto ret = add_subtract();
    
    for(;;) {
      auto ope = get_binary_op_in_boolean();
      
      if(ope == operator_symbol::and_) {
        auto rhs = add_subtract();
        ret = ret && rhs;
      }
      else if(ope == operator_symbol::or_) {
        auto rhs = add_subtract();
        ret = ret || rhs;
      }
      else {
        break;
      }
    }
    return ret;
  }
  
  inline auto relational_operation()
    -> number_type
  {
    auto ret = boolean_operation();
    
    for(;;) {
      auto ope = get_binary_op_in_relation();
      
      if(ope == operator_symbol::gt) {
        auto rhs = boolean_operation();
        ret = ret > rhs;
      }
      else if(ope == operator_symbol::gte) {
        auto rhs = boolean_operation();
        ret = ret >= rhs;
      }
      else if(ope == operator_symbol::lt) {
        auto rhs = boolean_operation();
        ret = ret < rhs;
      }
      else if(ope == operator_symbol::lte) {
        auto rhs = boolean_operation();
        ret = ret <= rhs;
      }
      else if(ope == operator_symbol::equal_to) {
        auto rhs = boolean_operation();
        ret = ret == rhs;
      }
      else {
        break;
      }
    }
    return ret;
  }
  
  inline auto expression()
    -> number_type
  {
    return relational_operation();
  }
};