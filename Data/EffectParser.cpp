#include "EffectParser.hpp"

namespace EffectParser
{
	auto const statement::none = statement{statement_type::none, {}};

	auto Parser::parse_optional_target()
	{
		if(consumeif_char_skipped('['))	{
			auto const keyword = get_keyword();
			consume_skipped(']');
			if(keyword == "ForOneTarget")	{ return target_type::for_one_target;	}
			if(keyword == "ForAllTargets") 	{ return target_type::for_all_targets;	}
			if(keyword == "ForMyself") 		{ return target_type::for_myself;		}
			if(keyword == "ForOurselves")	{ return target_type::for_ourselves;	}
		}

		return target_type::for_one_target;
	}

	auto Parser::parse_subs(reserved_word const& keyword)
	{
		if(keyword == "Damage") {
			auto target = parse_optional_target();
			consume_skipped(':');
			ExpressionParser exp_parser(get_string_literal());
			return statement{statement_type::damage, {target, exp_parser.expression()}};
		}
		if(keyword == "Heal") {
			auto target = parse_optional_target();
			consume_skipped(':');
			ExpressionParser exp_parser(get_string_literal());
			return statement{statement_type::heal, {target, exp_parser.expression()}};
		}
		return statement::none;
	}
  
	auto Parser::parse_cond(reserved_word const& keyword)
	{
		if(keyword == "Poison") {
			auto target = parse_optional_target();
			consume_skipped(':');
			ExpressionParser exp_parser(get_string_literal());
			return statement{statement_type::poison, {target, exp_parser.expression()}};
		}
		if(keyword == "Palsy") {
			auto target = parse_optional_target();
			consume_skipped(':');
			ExpressionParser exp_parser(get_string_literal());
			return statement{statement_type::palsy, {target, exp_parser.expression()}};
		}
		if(keyword == "Death") {
			auto target = parse_optional_target();
			consume_skipped(':');
			ExpressionParser exp_parser(get_string_literal());
			return statement{statement_type::death, {target, exp_parser.expression()}};
		}
		return statement::none;
	}
  
	auto Parser::parse_action(reserved_word const& keyword)
	{
		auto ret = parse_subs(keyword);
		if(ret.type == statement_type::none) {
			ret = parse_cond(keyword);
		}
		return ret;
	}

	auto Parser::parse_text(reserved_word const& keyword)
	{
		if(keyword == "Text") {
			consume_skipped(':');
			return statement{statement_type::text, {get_string_literal()}};
		}
		return statement::none;
	}

	auto Parser::parse_invocation(reserved_word const& keyword)
	{
		if(keyword == "Invocation") {
			consume_skipped(':');
			auto const action_class_string = get_string_literal();
			auto const invocation = GameMgr->getDatabase()->getActionRecord(action_class_string)["Effect"];
			Parser pa(invocation);
			statement st;
			while(pa >> st) { recur_statements_.push(st); }
			return statement{statement_type::invocation, {}};
		}
		return statement::none;
	}
  
  auto Parser::parse_consume_item(reserved_word const& keyword)
  {
    // ConsumeItem : "0001(int)"
    if(keyword == "ConsumeItem") {
      consume_skipped(':');
      return statement{statement_type::consume_item, {std::stoi(get_string_literal())}};
    }
    return statement::none;
  }

	auto Parser::parse_effect(statement& st_ret)
		-> bool
	{
		// Effect := (Text | ConsumeItem | Action | Invocation) [*(Conjuctive (Text | Action | Invocation)]
		if(eof_skipped()) return false;
		auto keyword = get_keyword();
		if((st_ret = parse_text(keyword)).type          != statement_type::none) { return true; }
		if((st_ret = parse_invocation(keyword)).type    != statement_type::none) { return true; }
    if((st_ret = parse_consume_item(keyword)).type  != statement_type::none) { return true; }
		if((st_ret = parse_action(keyword)).type        != statement_type::none) { return true; }

		AUFM_ASSERT(false, "Undefined parse effect");
	}
}
