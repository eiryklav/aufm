#pragma once

#include <iostream>
#include <memory>
#include <unordered_map>
#include <vector>
#include <sqlite3.h>

#include "../Support/Misc.hpp"
#include "../Support/Debugger.hpp"

namespace DB
{
	
	using RecType     = std::unordered_map<std::string, std::string>;
	using RecordsType = std::vector<RecType>;

  namespace callbacks {
    auto static constexpr InvalidValue = "(INVALID)";
		inline int recorder(void *db_ptr, int argc, char **argv, char **azColName);
		inline int find_table(void *db_ptr, int argc, char **argv, char **azColName);
	}

	class Database
	{
	public:
    Database(){}
		Database(const char* fname);
		~Database();
    auto isExistActionRecord(std::string const& actionClass) -> bool;
		auto getActionRecord(std::string const& actionClass) -> RecType;
		auto getChrStatusRecord(int) -> RecType;
		auto getItemRecord(int) -> RecType;
		auto getWeaponRecord(int) -> RecType;
		auto getProtectorRecord(int) -> RecType;
		auto getAccessoryRecord(int) -> RecType;
		auto getEquipmentRecord(Misc::EquipmentType, int) -> RecType;

		inline auto getRetRecords() -> decltype(auto) { return (retRecords_); }
    inline auto setRetFindTable(int val) { retFindTable_ = val; }
    
  private:
    
    sqlite3*    db_;
    std::string targetingTable_;
    
    RecordsType retRecords_;
    int 				retFindTable_;
    
    auto querySelect(std::string field, std::string condition="");
    auto setTargetingTable(std::string const& tar);
    auto get(std::string const& field, std::string const& condition="");

	};
  
}