#include <iostream>

class c
{
private:
  std::string s_;// = "114514";

public:
  c(std::string const&s)
//    : s_(s)
  {
    std::cout << "ctor\n";
    s_ = s;
    std::cout << s_ << std::endl;
    std::cout << "kkk\n";
  }

  c(c const& rhs) {
    std::cout << "copy_ctor\n";
  }
  auto s() { return s_; }
};

auto func(c ci) {
  std::cout << ci.s() << std::endl;
  return std::move("1919893");
}

int main() {

  c ci("1145148101919893");
  std::cout << func(std::move(ci)) << std::endl;
  std::cout << func(std::move(ci)) << std::endl;
  std::cout << func(std::move(ci)) << std::endl;
  std::cout << func(std::move(ci)) << std::endl;
  std::cout << "JJJ\n";
  std::cout << func(ci) << std::endl;
  std::cout << func(ci) << std::endl;
  std::cout << func(ci) << std::endl;

  return 0;
}