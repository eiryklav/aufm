#include "Database.hpp"
#include <string>
#include <fstream>
//#include <dlfcn.h>
#include <unistd.h>

namespace DB
{

  auto Database::querySelect(std::string field, std::string condition)
  {
    retRecords_.clear();
		auto sql = "SELECT " + field + " FROM " + targetingTable_;
		if(!condition.empty()) { sql += " WHERE " + condition; }
		char* zErrMsg;
		sqlite3_exec(db_, sql.c_str(), callbacks::recorder, (void*)this, &zErrMsg);
 	}

  auto Database::setTargetingTable(std::string const& tar)
	{
		char sql[256]; char* zErrMsg;
		sprintf(sql, "SELECT COUNT(*) FROM sqlite_master WHERE TYPE='table' AND NAME='{%s}'", tar.c_str());
		sqlite3_exec(db_, sql, callbacks::find_table, (void*)this, &zErrMsg);
		if(retFindTable_ == -1) {
			AUFM_ASSERT(false, "Not found table \"" << tar << "\" in " << "\"" << targetingTable_ << "\"");
		}
		targetingTable_ = tar;
	}

	auto Database::get(std::string const& field, std::string const& condition)
	{
		if(targetingTable_.empty()) {
			AUFM_ASSERT(false, "Set table before using getValueFromRecord();");
		}
		querySelect(field, condition);
	}
  
	Database::Database(const char* fname)
	{
    std::ifstream ifs(fname);
		if(!ifs) {
			AUFM_ASSERT(false, "Database \"" << fname << "\" doesn't exist.");
		}
    
		if(sqlite3_open(fname, &db_) != 0) {
			AUFM_ASSERT(false, "Can't open database: \"" << fname << "\"");
		}
	}
  
	Database::~Database() { sqlite3_close(db_); }
  
  auto Database::isExistActionRecord(std::string const& actionClass)
    -> bool
  {
    setTargetingTable("Actions");
    get("*", "Class=\"" + actionClass + "\"");
    return retRecords_.size();
  }
  
	auto Database::getActionRecord(std::string const& actionClass)
		-> RecType
	{
		setTargetingTable("Actions");
		get("*", "Class=\"" + actionClass + "\"");
		AUFM_ASSERT(retRecords_.size(), "Not found specified Class.");
    return std::move(retRecords_[0]);
	}
  
	auto Database::getChrStatusRecord(int characterID)
		-> RecType
	{
		setTargetingTable("Characters");
		get("*", "ID=\"" + std::to_string(characterID) + "\"");
		AUFM_ASSERT(retRecords_.size(), "Not found specified ID.");
    return std::move(retRecords_[0]);
	}
  
	auto Database::getItemRecord(int itemID)
		-> RecType
	{
		setTargetingTable("Items");
		get("*", "ID=\"" + std::to_string(itemID) + "\"");
		AUFM_ASSERT(retRecords_.size(), "Not found specified ID.");
    return std::move(retRecords_[0]);
	}
  
	auto Database::getWeaponRecord(int itemID)
		-> RecType
	{
		setTargetingTable("Weapons");
		get("*", "ID=\"" + std::to_string(itemID) + "\"");
		AUFM_ASSERT(retRecords_.size(), "Not found specified ID.");
		return std::move(retRecords_[0]);
	}

	auto Database::getProtectorRecord(int itemID)
		-> RecType
	{
		setTargetingTable("Protectors");
		get("*", "ID=\"" + std::to_string(itemID) + "\"");
		AUFM_ASSERT(retRecords_.size(), "Not found specified ID.");
		return std::move(retRecords_[0]);
	}

	auto Database::getAccessoryRecord(int itemID)
		-> RecType
	{
		setTargetingTable("Accessories");
		get("*", "ID=\"" + std::to_string(itemID) + "\"");
		AUFM_ASSERT(retRecords_.size(), "Not found specified ID.");
		return std::move(retRecords_[0]);
	}

  auto Database::getEquipmentRecord(Misc::EquipmentType type, int eqpID)
		-> RecType
	{
    switch(type) {
      case Misc::EquipmentType::Weapon:     return std::move(getWeaponRecord(eqpID));
      case Misc::EquipmentType::Protector:  return std::move(getProtectorRecord(eqpID));
      case Misc::EquipmentType::Accessory:  return std::move(getAccessoryRecord(eqpID));
    }
		AUFM_ASSERT(false, "No match equipment.");
	}

	namespace callbacks
	{
		int recorder(void *db_ptr, int argc, char **argv, char **azColName) {
			RecType nrec;
			for(int i=0; i<argc; i++){
				nrec.emplace(std::string(azColName[i]),	(argv[i] ? std::string(argv[i]) : InvalidValue));
			}
			static_cast<Database*>(db_ptr)->getRetRecords().push_back(nrec);
			return SQLITE_OK;
		}

		int find_table(void *db_ptr, int argc, char **argv, char **azColName) {
			if(argc == 0) { static_cast<Database*>(db_ptr)->setRetFindTable(-1); } 		// テーブルが存在しない
			else 					{ static_cast<Database*>(db_ptr)->setRetFindTable(0); }			// テーブルが存在する
			return SQLITE_OK;
		}
	}

}