#pragma once

#include "../BattleSystem/BattleSystem.hpp"
#include "BaseParser.hpp"
#include <algorithm>
#include <queue>
#include <boost/variant.hpp>

namespace EffectParser
{
	enum class statement_type
	{
		none,
		/*---*/
		text,
    consume_item,
		damage,
		heal,
		poison,
		palsy,
		death,
		invocation
	};

	enum class target_type
	{
		for_one_target,
		for_all_targets,
		for_myself,
		for_ourselves
	};

	class statement;
	using argument = boost::variant<
		int,
		double,
		std::string,
		target_type
	>;

	class statement
	{
	public:
		statement_type type;
		std::vector<argument> args;
		
		statement static const none;

		inline statement(statement_type type, std::vector<argument> const& args)
			: type(type), args(args)
		{}

		inline statement()
			: statement(none)
		{}
	};

	class Parser : public BaseParser
	{
	private:

		auto parse_subs(reserved_word const&);
		auto parse_cond(reserved_word const&);
		auto parse_action(reserved_word const&);
		auto parse_text(reserved_word const&);
    auto parse_consume_item(reserved_word const&);
		auto parse_invocation(reserved_word const&);
		auto parse_effect(statement&) -> bool;
		auto parse_optional_target();

	private:
		std::queue<statement> recur_statements_;

	public:
		inline Parser(std::string const& in) : BaseParser(in) {}
		inline auto operator >> (statement& st)
		{
			if(recur_statements_.empty()) {
				return parse_effect(st);
			}
			else {
				st = recur_statements_.front();
				recur_statements_.pop();
				return true;
			}
		}
	};
}