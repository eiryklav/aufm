#include "../Game/Game.hpp"
#include "Items.hpp"
#include "../Printer.hpp"
#include "../Data/Database.hpp"
#include "../Support/Misc.hpp"

namespace Items
{
  auto Rucksack::pushItem(TypeList::ItemIDOnDB idOnDB)
    -> void
  {
    namespace MSG = Printer::Messages;
    auto const& itemName = Misc::getStringCorrespondToWorld(
      GameMgr->getDatabase()->getItemRecord(idOnDB)["Name"],
      GameMgr->getWorldName()
    );

    if(possessNum_[idOnDB] < CounterStop::NumOfDuplicates) {
      if(allItemsNum_ < CounterStop::NumOfAllItems) {
        possessNum_[idOnDB]++;
        allItemsNum_++;
        GameMgr->getLogger()->pushMessage(
          MSG::getItem(itemName)
        );
      }
      else {
        GameMgr->getLogger()->pushMessage(
          MSG::failedToGetItemDueToMaxAllItems(itemName)
        );
      }
    }
    else {
      GameMgr->getLogger()->pushMessage(
        MSG::failedToGetItemDueToMaxDuplicates(itemName)
      );
    }
  }

  auto Rucksack::popItem(TypeList::ItemIDOnDB idOnDB)
    -> void
  {
    if(possessNum_[idOnDB] > 0) {
      possessNum_[idOnDB]--;
    }
    else {
      AUFM_ASSERT(false, "Player hasn't item which id is " << idOnDB << ".");
    }
  }
  
  auto Rucksack::hasItem(TypeList::ItemIDOnDB idOnDB)
    -> bool
  {
    return possessNum_[idOnDB] > 0;
  }

}