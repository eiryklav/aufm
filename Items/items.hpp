#pragma once

#include <unordered_map>
#include <memory>

#include "../Support/IdentifiableOnDB.hpp"
#include "../Lib/picojson.h"

namespace Items
{
	namespace TypeList
	{
    using ItemIDOnDB 	= int;
		using ListType	 	= std::pair<ItemIDOnDB, int>;
	}

	class Rucksack
	{
	public:
		struct CounterStop
		{
			int static constexpr
				NumOfDuplicates = 99,
				NumOfAllItems  	= 20
				;
		};

	private:
		std::unordered_map<TypeList::ItemIDOnDB, int> possessNum_;
		int allItemsNum_;

		/*
			getNumOfPossessions() は作るな！外部に Rucksack の内容（実体がunordered_map）が知られたコードを書かれては困る！
			unordered_map で管理していることを前提に Rucksack クラスを使われてはならない。
		*/

	public:
		Rucksack(): allItemsNum_(0){}

		auto pushItem	(TypeList::ItemIDOnDB idOnDB) -> void;
		auto popItem	(TypeList::ItemIDOnDB idOnDB) -> void;
    auto hasItem  (TypeList::ItemIDOnDB idOnDB) -> bool;

		inline auto listUpContents()
		{
			std::vector<TypeList::ListType> ret;
			for(auto const& e: possessNum_) {
				if(e.second > 0) {
					ret.emplace_back(e);
				}
			}
			return ret;
		}

	};

}