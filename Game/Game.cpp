#include "Game.hpp"
#include "GameUI.hpp"
#include "GameStrategy.hpp"
#include "TimeManager.hpp"
#include "../GameState/GameState.hpp"
#include "../GameState/TitleState.hpp"
#include "../GameState/EndingState.hpp"
#include "../Character/ChrData.hpp"
#include "../Character/ChrStatus.hpp"

#include "../Support/Misc.hpp"
#include "../Support/Debugger.hpp"

#include <memory>
#include <fstream>
#include <functional>

auto Game::pushState(std::shared_ptr<GameState>&& s, std::function<void()>&& endStateCallBack)
	-> void
{
  stateStack_.emplace_back(s, endStateCallBack);
	PRINT("ステート開始: " << StateTypeString[stateStack_.back().getState()->getStateType()])
  kernelLogger_->notify(GameUI::Notification::StateChanged);
}

auto Game::popState()
	-> void
{
	PRINT("ステート終了: " << StateTypeString[stateStack_.back().getState()->getStateType()])
	if(stateStack_.empty()) { std::runtime_error("State stack is empty."); }
  stateStack_.back().getCallBack()();
	stateStack_.pop_back();
  kernelLogger_->notify(GameUI::Notification::StateChanged);
}

auto Game::changeState(std::shared_ptr<GameState>&& s, std::function<void()>&& endStateCallBack)
	-> void
{
	popState();
  pushState(std::move(s), std::move(endStateCallBack));
}

auto Game::getStateStack() const
  -> std::vector<GameStateWrapper> const&
{
  return stateStack_;
}

auto Game::clearStateStack()
  -> void
{
	while(!stateStack_.empty()) { popState(); }
}

auto Game::addServant(int id)
	-> bool
{
  if(!Misc::randomBernoulli(codeOfConduct_->getInvitation())) {
    kernelLogger_->pushMessage(Printer::Messages::failedToAddServantBecauseOfInvitationRatio(
      Misc::getStringCorrespondToWorld(database_->getChrStatusRecord(id)["Name"], worldName_)
    ));
    return false;
  }
  
  if(servants_.size() == ChrData::MaxNumOfServants) {
    kernelLogger_->pushMessage(Printer::Messages::failedToAddServantBecauseOfOverflow(
      Misc::getStringCorrespondToWorld(database_->getChrStatusRecord(id)["Name"], worldName_)
    ));
    return false;
  }
  
  // ここではじめて仲間に入れる
  kernelLogger_->insertEncountedPlayerSet(id);
  
  servants_.push_back(ChrData::makePlayerPtr(id));
  servants_.back()->getBasicStat()->setOwner(servants_.back());
  
	kernelLogger_->pushMessage(Printer::Messages::addServant(
		Misc::getStringCorrespondToWorld(database_->getChrStatusRecord(id)["Name"], worldName_)
	));
  
  return true;
}

auto Game::getNumOfServants()
  -> int
{
  return static_cast<int>(servants_.size());
}

auto Game::getNumOfPlayers()
	-> int
{
	return getNumOfServants() + 1;
}

auto Game::getAllPlayers()
	-> std::vector<std::shared_ptr<ChrData::Player>>
{
	std::vector<std::shared_ptr<ChrData::Player>> ret;
	ret.push_back(player_);
	std::copy(servants_.begin(), servants_.end(), back_inserter(ret));
	return ret;
}

auto Game::getMoney() const -> ChrData::Money const& { return (*playersMoney_); }

auto Game::addMoney(ChrData::Money val)
	-> void
{
	AUFM_ASSERT(val > 0, "Adding money needs to be positive.");
  (*playersMoney_) += val;
  kernelLogger_->getPlayerStatusLog(ChrData::UserIDOnDB).pushMoney(std::make_shared<ChrData::Money>(*playersMoney_));
}

auto Game::useMoney(ChrData::Money val)
	-> void
{
	AUFM_ASSERT(val > 0, "Using money needs to be positive.");
	(*playersMoney_) -= val;
  kernelLogger_->getPlayerStatusLog(ChrData::UserIDOnDB).pushMoney(std::make_shared<ChrData::Money>(*playersMoney_));
	if(*playersMoney_ < 0) { AUFM_ASSERT(false, "借金は実装されていない"); }
}

auto Game::tryToGetEquipmentForPlayer(int eqpID, Misc::EquipmentType eqpType, std::shared_ptr<ChrData::Player> const& player, std::string const& atPosition, bool payment)
  -> bool
{
  // playerとplayer_を混同してはいけない
  // atPositionがstringなのは、毎回JSONから読み取るのを避けるため
  auto currRec  = database_->getEquipmentRecord(eqpType, player->getBasicStat()->getEquipment(eqpType));
  auto newRec   = database_->getEquipmentRecord(eqpType, eqpID);
  
  auto currRank = std::stoi(currRec["Rank"]);
  auto newRank  = std::stoi(newRec["Rank"]);
  
  auto currName = Misc::getStringCorrespondToWorld(currRec["Name"], worldName_);
  auto newName  = Misc::getStringCorrespondToWorld(newRec["Name"], worldName_);
  
  if(currRank < newRank) {
    
    player->getBasicStat()->setEquipmentID(eqpType, eqpID);
    
    if(payment) {
      kernelLogger_->getPlayerStatusLog(ChrData::UserIDOnDB).pushWayOfUseMoney(player->getName() + "に、" + newName + "を購入");
    }
    
    kernelLogger_->getPlayerStatusLog(ChrData::UserIDOnDB).pushBrainkeep({GameUI::BrainkeepType::TrialChangeEquipment, player->getName() + "は" + currName + "を" + newName + "に換装(" + atPosition + ")", true});
    kernelLogger_->pushMessage(Printer::Messages::letPlayerEquip(player->getName(), newName));
    
    return true;
  }
  else {
    kernelLogger_->getPlayerStatusLog(ChrData::UserIDOnDB).pushBrainkeep({GameUI::BrainkeepType::TrialChangeEquipment, player->getName() + "は" + currName + "を保持し、" + newName + "を廃棄(" + atPosition + ")"});
    
    kernelLogger_->pushMessage(Printer::Messages::throwAwayGoodsSoonAfterTakeInHand(Misc::getStringCorrespondToWorld(newRec["Name"], worldName_), player->getName()));
    return false;
  }
}

auto Game::addSuspicious(int val)
	-> void
{
	// とりあえずただの累積和のみとする。
	// 短時間に何度も犯行を行った場合は逮捕されやすくなるなど。
	currentSuspicious_ += val;
}

auto Game::loadWorldConfigFromJSON()
{
	std::ifstream fs(dataResourcesPath_+"Worlds/"+worldName_+"/WorldConfig.json");
  if(fs.fail()) { AUFM_ASSERT(false, dataResourcesPath_+"Worlds/"+worldName_+"/WorldConfig.json"); }
  
  std::string str, jsonString;
	while(fs >> str) { jsonString += str; }
	picojson::value v;
	auto err = picojson::parse(v, jsonString);
  auto& o = v.get<picojson::object>();
	monetaryUnitName_ = o["MonetaryUnitName"].get<std::string>();
  
  fullWorldName_ = o["FullName"].get<std::string>();
  shortWorldName_ = o["ShortName"].get<std::string>();
  
  using namespace Printer::Messages::Registered;
  auto& shops_o = o["Shops"].get<picojson::object>();
  auto& weapon_o = shops_o["Weapon"].get<picojson::object>();
  
  Shops::Weapon::EnterText = weapon_o["EnterText"].get<std::string>();
  Shops::Weapon::ExitText  = weapon_o["ExitText"].get<std::string>();
  
  auto& protector_o = shops_o["Protector"].get<picojson::object>();
  Shops::Protector::EnterText = protector_o["EnterText"].get<std::string>();
  Shops::Protector::ExitText  = protector_o["ExitText"].get<std::string>();
  
  auto& accessory_o = shops_o["Accessory"].get<picojson::object>();
  Shops::Accessory::EnterText = accessory_o["EnterText"].get<std::string>();
  Shops::Accessory::ExitText  = accessory_o["ExitText"].get<std::string>();
}

auto Game::open(Game::WorldName const& worldName, std::string dataResourcesPath)
	-> void
{
  dataResourcesPath_ = dataResourcesPath;
	worldName_				= worldName;
	database_ 				= std::make_shared<DB::Database>((dataResourcesPath_+"database.db").c_str());
  kernelLogger_			= std::make_shared<GameUI::Logger>();
	timeManager_ 			= std::make_shared<TimeManager::Timer>();
	rucksack_					= std::make_shared<Items::Rucksack>();
}

auto Game::initGame(Game::PlayerName const& playerName,
										std::vector<std::pair<int, int>> const& tForD,
										int attitude,
                    int invitation,
                    int changeEquipmentProb,
                    int lootingDrawers)
	-> void
{
	targetForDefeat_ 	= std::make_shared<GameStrategy::TargetForDefeat>(tForD);
	codeOfConduct_		= std::make_shared<GameStrategy::CodeOfConduct>(attitude, invitation, changeEquipmentProb, lootingDrawers);
  
  kernelLogger_->insertEncountedPlayerSet(ChrData::UserIDOnDB);
  
  if(playerName.empty()) 	{ player_ = ChrData::makePlayerPtr(ChrData::UserIDOnDB); }
	else 										{ player_ = ChrData::makePlayerPtr(ChrData::UserIDOnDB, playerName); }
  
  int constexpr InitialMoney = 1000;
  playersMoney_ = std::make_shared<ChrData::Money>(0);
  addMoney(ChrData::Money(InitialMoney));
  
	pushState(std::make_shared<TitleState>());
  loadWorldConfigFromJSON();
}

auto Game::update()
	-> void
{
  if(stateStack_.back().getState()->getStateType() == StateType::EndingState) {
    return;
  }
  
	stateStack_.back().getState()->update();
	
  // ラスボス戦以外に適用
	if(timeManager_->isTimeLimitExceeded()) {
		pushState(std::make_shared<EndingState>(EndCondition::TimeLimitExceeded));
		return;
	}
}