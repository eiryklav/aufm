#pragma once

#include <vector>
#include <memory>
#include <unordered_set>
#include <functional>
#include <boost/optional.hpp>
#include "GameUI.hpp"

#include "../Items/Items.hpp"
#include "../Data/Database.hpp"
#include "../Support/Singleton.hpp"

#define GameMgr Game::Instance()

namespace GameStrategy { class TargetForDefeat; class CodeOfConduct; }
namespace ChrData { class Player; class Money; }
namespace WorldMap { class AreaID; }
namespace TimeManager { class Timer; }

class GameState;

class Game : public Singleton<Game>
{
public:
	using ServantPtr        = std::shared_ptr<ChrData::Player>;
	using Servants          = std::vector<ServantPtr>;
	using Suspicious        = int;
  using WorldName         = std::string;
	using PlayerName        = std::string;
	using MonetaryUnitName  = std::string;
  using FullWorldName     = std::string;
  using ShortWorldName    = std::string;
  
  class GameStateWrapper
  {
  private:
    std::shared_ptr<GameState> state_ = nullptr;
    std::function<void()> callBack_   = []{};
    
  public:
    
    GameStateWrapper(std::shared_ptr<GameState> state)
      : state_(std::move(state)){}
    
    GameStateWrapper(std::shared_ptr<GameState> state, std::function<void()> callBack)
      : state_(std::move(state)), callBack_(std::move(callBack)){}
    
    inline auto getState()    { return state_; }
    inline auto getCallBack() { return callBack_; }
  };

private:
  std::string                         dataResourcesPath_;
  std::shared_ptr<GameUI::Logger>     kernelLogger_;
	std::shared_ptr<DB::Database>       database_;
  std::shared_ptr<TimeManager::Timer> timeManager_;
  std::vector<GameStateWrapper>       stateStack_;
  
  std::unordered_set<std::string> ReservedEventPrefix =
	{
		"ScenarioOnce", "OpenTreasure", "OpenUniqueDrawer", "Shop", "Custom", "CompleteTargetForDefeat"
	};

	std::shared_ptr<GameStrategy::TargetForDefeat> 	targetForDefeat_;
	std::shared_ptr<GameStrategy::CodeOfConduct> 		codeOfConduct_;
	std::shared_ptr<ChrData::Player> 								player_;
	Servants 																				servants_;
	std::shared_ptr<Items::Rucksack> 								rucksack_;
	WorldName 																			worldName_;         // 識別子としての名前

	Suspicious																			currentSuspicious_;
	std::shared_ptr<ChrData::Money>									playersMoney_;
	MonetaryUnitName 																monetaryUnitName_;
  FullWorldName                                   fullWorldName_;
  ShortWorldName                                  shortWorldName_;
  
  std::unordered_map<std::string, bool>						isEventGenerated_;

private:

	auto loadWorldConfigFromJSON();
	
public:
  
  // initialization
	auto open(WorldName const&, std::string) -> void;
  auto initGame(PlayerName const&, std::vector<std::pair<int, int>> const&, int, int, int, int) -> void;
  inline auto getDataResourcesPath() const -> decltype(auto) { return (dataResourcesPath_); }
  
  // update
	auto update() -> void;
  
  // instances
  inline auto getLogger() const   -> decltype(auto) { return (kernelLogger_); }
  inline auto getDatabase() const -> decltype(auto) { return (database_); }
  inline auto getTimeMgr() const  -> decltype(auto) { return (timeManager_); }
  
  // state stack
  auto pushState(std::shared_ptr<GameState>&& s, std::function<void()>&& callBack = []{}) -> void;
  auto changeState(std::shared_ptr<GameState>&& s, std::function<void()>&& callBack = []{}) -> void;
  auto popState() -> void;
  auto getStateStack() const -> std::vector<GameStateWrapper> const&;
	auto clearStateStack() -> void;
	inline auto getTopState() const { return stateStack_.back(); }
  
  // configurations
	inline auto getTargetForDefeat() 	const -> decltype(auto) { return (targetForDefeat_); }
	inline auto getCodeOfConduct() 		const -> decltype(auto) { return (codeOfConduct_); }
  inline auto getMonetaryUnitName()	const -> decltype(auto) { return (monetaryUnitName_); }
  inline auto getWorldName() 				const -> decltype(auto) { return (worldName_); }
  inline auto getFullWorldName()    const -> decltype(auto) { return (fullWorldName_); }
  inline auto getShortWorldName()   const -> decltype(auto) { return (shortWorldName_); }
  
  // current players
	inline auto getPlayer() 					const -> decltype(auto) { return (player_); }
	inline auto getServants() 				const -> decltype(auto) { return (servants_); }
  inline auto getServants()               -> decltype(auto) { return (const_cast<Servants&>(static_cast<const Game*>(this)->getServants())); }
  auto addServant(int id) -> bool;
  auto getAllPlayers() -> std::vector<std::shared_ptr<ChrData::Player>>;
  auto getNumOfServants() -> int;
  auto getNumOfPlayers() -> int;
  
  inline auto getCurrSuspicious() const { return currentSuspicious_; }
  auto addSuspicious(int val) -> void;
  auto getMoney() const -> ChrData::Money const&;
  auto addMoney(ChrData::Money) -> void;
  auto useMoney(ChrData::Money) -> void;
  
  // rucksack
	inline auto getRucksack() 				const -> decltype(auto) { return (rucksack_); }
  
  // try to get equipment for player
  auto tryToGetEquipmentForPlayer(int, Misc::EquipmentType, std::shared_ptr<ChrData::Player> const&, std::string const&, bool) -> bool;
  
  // event
	inline auto isEventGenerated(std::string const& prefix, std::string const& content)
	{
		AUFM_ASSERT(ReservedEventPrefix.count(prefix), "EventGenFlag: Undefined prefix");
    auto const& event = prefix+content;
		return isEventGenerated_[event];
	}

	inline auto setEventGeneratedFlag(std::string const& prefix, std::string const& content)
	{
		AUFM_ASSERT(ReservedEventPrefix.count(prefix), "EventGenFlag: Undefined prefix");
		auto const& event = prefix+content;
		AUFM_ASSERT(isEventGenerated_[event] == false, "EventGenFlag: This event has already been set.");
		isEventGenerated_[event] = true;
	}
	
};