#pragma once

#include <boost/container/flat_map.hpp>

#include <vector>

namespace GameStrategy
{
	using EnemyIDOnDB = int;
  using EnemyNumMap = boost::container::flat_map<EnemyIDOnDB, int>;

	class TargetForDefeat
	{
	private:
		EnemyNumMap remain_;

	public:
		TargetForDefeat(std::vector<std::pair<int, int>> const&);
    auto getRemainOfEnemyID(EnemyIDOnDB) -> int;
		auto getRemainEnemiesList() const -> std::vector<EnemyIDOnDB>;
    auto getCurrentTargetForDefeats() const -> std::vector<std::pair<int, int>>;
		auto defeatEnemy(int id) -> bool;

	};

	class CodeOfConduct
	{
	private:
		using Z100Num 		= int;
		using Probability	= Z100Num;
		using Ratio				= double;
		
	private:
    /*
     attitude_:             人当たり = x in [0, 100]
     changeEquipmentRatio_: 装備交換率
     invitation_:           従者勧誘率
     lootingDrawers_:       窃盗率
     
     未実装
     flattery_:             媚び
     */
		Z100Num attitude_;
		Ratio		changeEquipmentRatio_;
		Ratio 	invitation_;
    Ratio   lootingDrawers_;

	public:
		CodeOfConduct(int attitude, int invitation, int changeEquipmentProb, int lootingDrawers);
		inline auto getAttitude() const { return attitude_; }
		inline auto getChangeEquipmentRatio() const { return changeEquipmentRatio_; }
    inline auto getInvitation() const { return invitation_; }
    inline auto getLootingDrawers() const { return lootingDrawers_; }

	};
}