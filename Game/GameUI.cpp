#include "GameUI.hpp"
#include "../GameState/GameState.hpp"
#include "Game.hpp"

namespace GameUI
{
  
  std::string const Logger::LampMoverPlayerID = "PlayerLampMoverID";
  
#define PUSH_STATUS_METHOD(statusName, statusType) \
  void PlayerStatusLog::push ## statusName(statusType const& val) { statusName ## _.emplace_back(GameMgr->getTimeMgr()->getNowRealTime(), val); }
  
  PUSH_STATUS_METHOD(LV,             std::shared_ptr<ChrData::Level>)
  PUSH_STATUS_METHOD(Exp,            std::shared_ptr<ChrData::Experience>)
  PUSH_STATUS_METHOD(MaxHP,          std::shared_ptr<ChrData::HitPoint>)
  PUSH_STATUS_METHOD(MaxMana,        std::shared_ptr<ChrData::Mana>)
  PUSH_STATUS_METHOD(HP,             std::shared_ptr<ChrData::HitPoint>)
  PUSH_STATUS_METHOD(Mana,           std::shared_ptr<ChrData::Mana>)
  PUSH_STATUS_METHOD(ATK,            std::shared_ptr<ChrData::Attack>)
  PUSH_STATUS_METHOD(DEF,            std::shared_ptr<ChrData::Defense>)
  PUSH_STATUS_METHOD(SP,             std::shared_ptr<ChrData::Speed>)
  PUSH_STATUS_METHOD(Money,          std::shared_ptr<ChrData::Money>)
  PUSH_STATUS_METHOD(ArrivalAtArea,  std::shared_ptr<WorldMap::AreaID>)
  PUSH_STATUS_METHOD(Weapon,         EquipmentID)
  PUSH_STATUS_METHOD(Protector,      EquipmentID)
  PUSH_STATUS_METHOD(Accessory,      EquipmentID)
  PUSH_STATUS_METHOD(Condition,      Condition)
  PUSH_STATUS_METHOD(AIName,         AIName)
  PUSH_STATUS_METHOD(MasteredAction, ActionClassID)
  PUSH_STATUS_METHOD(WayOfUseMoney,  WayOfUseMoney)
  PUSH_STATUS_METHOD(Brainkeep,      Brainkeep)
  
#undef PUSH_STATUS_METHOD
  
  auto Logger::pushMessage(Printer::Message const& msg)
    -> void
  {
    messagesBuffer_.emplace_back(msg);
    modified_.set(Notification::LogUpdated);
  }
  
  auto Logger::pushConversation(Printer::Conversation const& conversation)
    -> void
  {
    conversationBuffer_.emplace_back(conversation);
    modified_.set(Notification::ConversationUpdated);
  }
  
  auto Logger::pushLampMover(LampMoverID id, std::string const& type, std::string const& movingMode, int speed, std::complex<double> const& startPosition, std::string const& scenario)
  -> void
  {
    if(usedLampMovers_.count(id)) { AUFM_ASSERT(false, "Duplicate lampmover generated."); }
    usedLampMovers_.insert(id);
    lampMovers_[id] = LampMover(type, movingMode, speed, startPosition, scenario);
    pushMessage(Printer::Messages::sysLampMoverGenerated(id));
  }
  
  auto Logger::pushLampMover(LampMoverID id, std::string const& type, std::string const& movingMode, int speed, std::complex<double> const& startPosition, picojson::array const& scenarioBlock)
  -> void
  {
    if(usedLampMovers_.count(id)) { AUFM_ASSERT(false, "Duplicate lampmover generated."); }
    usedLampMovers_.insert(id);
    lampMovers_[id] = LampMover(type, movingMode, speed, startPosition, scenarioBlock);
    pushMessage(Printer::Messages::sysLampMoverGenerated(id));
  }

}