#include "GameStrategy.hpp"
#include "../Support/Misc.hpp"

#include <iostream>
#include <vector>

namespace GameStrategy
{
	TargetForDefeat::TargetForDefeat(std::vector<std::pair<int, int>> const& tar)
  {
  	for(auto const& e: tar) {
  		remain_[e.first] = e.second;
  	}
  }
  /*
	{
		for(size_t i=0; i<tar.size; i++) {
			remain_[tar.enemyIDOnDB[i]] = tar.num[i];
		}
	}
   */
  
  auto TargetForDefeat::getRemainOfEnemyID(EnemyIDOnDB eid)
    -> int
  {
    return remain_[eid];
  }

	auto TargetForDefeat::getRemainEnemiesList() const
		-> std::vector<EnemyIDOnDB>
	{
		std::vector<EnemyIDOnDB> enemies;
		for(auto const e: remain_) {
			if(e.second > 0) {
				enemies.push_back(e.first);
			}
		}
    return std::move(enemies);
	}
  
  auto TargetForDefeat::getCurrentTargetForDefeats() const
    -> std::vector<std::pair<int, int>>
  {
    std::vector<std::pair<int, int>> ret;
    for(auto const e: remain_) {
      ret.push_back(e);
    }
    return std::move(ret);
  }

	auto TargetForDefeat::defeatEnemy(int id)
		-> bool
	{
    if(remain_.find(id) == remain_.end()) { return false; }
		if(remain_[id] > 0) {
      remain_[id]--;
      return true;
    }
    return false;
	}

	CodeOfConduct::CodeOfConduct(int attitude, int invitation, int changeEquipmentProb, int lootingDrawers)
	{
		attitude_ = attitude;
		Misc::Z100RangeTest(attitude_);
		invitation_ 					= static_cast<double>(invitation) / 100.0;
		changeEquipmentRatio_ = static_cast<double>(changeEquipmentProb) / 100.0;
    lootingDrawers_       = static_cast<double>(lootingDrawers) / 100.0;
	}
}