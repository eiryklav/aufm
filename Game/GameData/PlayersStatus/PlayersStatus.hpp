#pragma once

#include <vector>
#include "CharacterStatus.hpp"


namespace GameData { namespace Playing { namespace PlayersStatus {

class PlayerStatus {
private:
  Money         money_;
  Level         LV;
  Experience    Exp;  // 総経験値
  HitPoint      HP;
  Mana          Ma;
  Attack        ATK;
  Defense       DEF;
  Speed         SP;
  Luck          LCK;
  Intelligence  INT;
};

class StatusManager {
public:
  auto numberOfPlayers() -> int;
  auto statusOfPlayerWithIndex(int index) -> std::shared_ptr<PlayerStatus>;

private:
  std::vector<std::shared_ptr<PlayerStatus>> playersStatus_;
};



}}}