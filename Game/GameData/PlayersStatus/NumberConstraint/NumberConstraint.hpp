#pragma once

#include "../../../../Support/PrimitiveWrapper.hpp"

#define NUMBER_CONSTRAINT_CTOR_DEF(type) \
  PRIM_WRAP_INHERITED_CTOR_DEF(type, Integer, int)

#define NUMBER_CONSTRAINT_OPERATOR_DEF(type) \
  PRIM_WRAP_INHERITED_OPERATOR_DEF(type, int)

namespace GameData { namespace Playing { namespace NumberConstraint {

class NumberConstraint : public PrimitiveWrapper::Integer {
public:
  NUMBER_CONSTRAINT_CTOR_DEF(NumberConstraint)
  NUMBER_CONSTRAINT_OPERATOR_DEF(NumberConstraint)
  virtual auto assertionWithConstraint() -> void = 0;
};

class PositiveConstraint : public NumberConstraint {
public:
  auto assertionWithConstraint() -> void;
};

  
}}}