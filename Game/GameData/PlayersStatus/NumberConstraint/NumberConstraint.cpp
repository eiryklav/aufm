#include "NumberConstraint.hpp"
#include "../../../../Support/Debugger.hpp"

namespace GameData { namespace Playing { namespace NumberConstraint {


auto PositiveConstraint::assertionWithConstraint()
  -> void
{
  AUFM_ASSERT(get() > 0, "Failure: PositiveConstraint::assertionWithConstraint()");
}


}}}