

#pragma once

//#include "../../../Support/PrimitiveWrapper.hpp"
#include "NumberConstraint/NumberConstraint.hpp"

#define CHR_STATUS_CTOR_DEF(type) \
  PRIM_WRAP_INHERITED_CTOR_DEF(type, Integer, int)

#define CHR_STATUS_OPERATOR_DEF(type) \
  PRIM_WRAP_INHERITED_OPERATOR_DEF(type, int)



namespace GameData { namespace Playing {


class Level : public NumberConstraint::PositiveConstraint
{
public:
  CHR_STATUS_CTOR_DEF(Level)
  CHR_STATUS_OPERATOR_DEF(Level)
};

class Experience : public PrimitiveWrapper::Integer
{
public:
  CHR_STATUS_CTOR_DEF(Experience)
  CHR_STATUS_OPERATOR_DEF(Experience)
};

class HitPoint : public PrimitiveWrapper::Integer
{
public:
  CHR_STATUS_CTOR_DEF(HitPoint)
  CHR_STATUS_OPERATOR_DEF(HitPoint)
};

class Mana : public PrimitiveWrapper::Integer
{
public:
  CHR_STATUS_CTOR_DEF(Mana)
  CHR_STATUS_OPERATOR_DEF(Mana)
};

class Attack : public PrimitiveWrapper::Integer
{
public:
  CHR_STATUS_CTOR_DEF(Attack)
  CHR_STATUS_OPERATOR_DEF(Attack)
};

class Defense : public PrimitiveWrapper::Integer
{
public:
  CHR_STATUS_CTOR_DEF(Defense)
  CHR_STATUS_OPERATOR_DEF(Defense)
};

class Speed : public PrimitiveWrapper::Integer
{
public:
  CHR_STATUS_CTOR_DEF(Speed)
  CHR_STATUS_OPERATOR_DEF(Speed)
};

class Luck : public PrimitiveWrapper::Integer
{
public:
  CHR_STATUS_CTOR_DEF(Luck)
  CHR_STATUS_OPERATOR_DEF(Luck)
};

class Intelligence : public PrimitiveWrapper::Integer
{
public:
  CHR_STATUS_CTOR_DEF(Intelligence)
  CHR_STATUS_OPERATOR_DEF(Intelligence)
};

class Money : public PrimitiveWrapper::Integer
{
public:
  CHR_STATUS_CTOR_DEF(Money)
  CHR_STATUS_OPERATOR_DEF(Money)
};


}}