#include <vector>
#include "PlayersStatus/PlayersStatus.hpp"

namespace GameData { namespace Playing {



class PlayersData {
private:
  PlayersStatus::StatusManager  playersStatus_;
};

/*
  Class:          GameData::Playing::Data
  Responsibility: 一回のプレイに必要となるデータの管理
*/
class IData {
public:

};

class Data : public IData {
private:
  PlayersData playersData_;
};



}}