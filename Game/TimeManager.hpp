#pragma once

#include "../Support/PrimitiveWrapper.hpp"
#include "../Support/Misc.hpp"
#include <iostream>
#include <algorithm>
#include <boost/algorithm/string.hpp>

#define TIME_TYPE_CTOR_DEF(type)  \
  PRIM_WRAP_INHERITED_CTOR_DEF(type, Double, double)

#define TIME_TYPE_OPERATOR_DEF(type)  \
  PRIM_WRAP_INHERITED_OPERATOR_DEF(type, double)

namespace TimeManager
{
  class RealTime : public PrimitiveWrapper::Double
  {
  public:
    TIME_TYPE_CTOR_DEF(RealTime)
    TIME_TYPE_OPERATOR_DEF(RealTime)
  };

  class AufmTime : public PrimitiveWrapper::Double
  {
  public:
    TIME_TYPE_CTOR_DEF(AufmTime)
    TIME_TYPE_OPERATOR_DEF(AufmTime)
  };

  /*
    クラス名: TimeManager
    概要:
      - 時間保持
        1. ゲームプレイ時間を保持
        2. プレイ時間をAufm換算の経過時間に置換

      - 時間経過
        1. Aufm時間経過要求を満たさせる
        2. 5分のタイムリミット判定
  */

  namespace TimeSpan
  {
    double static constexpr
    OneMinute     = 60,
    OneHour       = OneMinute*60,
    OneDay        = 24*OneHour,
    OneYear       = 365*OneDay,
    OneMonth      = 30*OneDay,
    TimePerSpeed  = 20000         // 1SP あたりかかる時間
    ;
  }

  inline auto changeReservedTimeSpanWords(std::string& str)
  {
    boost::algorithm::replace_all(str, "@OneMinute", std::to_string(TimeManager::TimeSpan::OneYear));
    boost::algorithm::replace_all(str, "@OneHour", std::to_string(TimeManager::TimeSpan::OneHour));
    boost::algorithm::replace_all(str, "@OneDay", std::to_string(TimeManager::TimeSpan::OneDay));
    boost::algorithm::replace_all(str, "@OneMonth", std::to_string(TimeManager::TimeSpan::OneMonth));
    boost::algorithm::replace_all(str, "@OneYear", std::to_string(TimeManager::TimeSpan::OneYear));
  }
  
  class TimeStamped
  {
  private:
    RealTime time_;
    
  public:
    TimeStamped(RealTime time): time_(time) {}
    inline auto getRealTime() const -> decltype(auto) { return (time_); }
  };

  class Timer
  {
  private:
    double static constexpr MaxElaspedTime = 5.0*60;
    double static constexpr MaxElaspedAufmTime = TimeSpan::OneMonth;
    constexpr auto static toAufmTimeValue(double realTime) {
      return realTime * MaxElaspedAufmTime / MaxElaspedTime;
    }
    
  private:
    RealTime elaspedTime_;
    AufmTime requestedConsumingAufmTime_;
    AufmTime extraAufmTime_;  // 一時記録用に利用する。Ex) CityState -> ScenarioState(costTime) -> CityState

  private:
    auto consumeAufmTime(AufmTime aufmTime);

  public:
    Timer(): elaspedTime_(0), requestedConsumingAufmTime_(0), extraAufmTime_(0) {}
    auto updateRealTime(RealTime time) -> void;
    inline auto getMaxElaspedRealTime() const { return RealTime(MaxElaspedTime); }
    inline auto getMaxElaspedAufmTime() const { return AufmTime(MaxElaspedAufmTime); }
    inline auto getMaxElaspedAufmTimeDaySec() const { return static_cast<int>(getMaxElaspedAufmTime()); }
    inline auto getNowRealTime() const { return RealTime(elaspedTime_); }
    inline auto getNowAufmTime() const { return AufmTime(toAufmTimeValue(elaspedTime_)); }
    inline auto getNowAufmTimeDaySec() const { return static_cast<int>(getNowAufmTime()); }
    inline auto isFinishedConsumingAufmTime() { return requestedConsumingAufmTime_ <= 0; }
    inline auto requestConsumingAufmTime(AufmTime val) { requestedConsumingAufmTime_ += val; }
    inline auto isTimeLimitExceeded() { return elaspedTime_ >= MaxElaspedTime; }
    inline auto addExtraAufmTime(AufmTime val) { AUFM_ASSERT(val > 0, "In extraAufmTime(): Additional value has to be always positive."); extraAufmTime_ += val; }
    inline auto getExtraAufmTime() { return extraAufmTime_; }
    inline auto clearExtraAufmTime() { extraAufmTime_ = AufmTime(0); }
    
    inline auto depricatedGetRequestedAufmTime() const { return requestedConsumingAufmTime_; }  // for debug
  };
}