#pragma once

#include <iostream>
#include <list>
#include <string>
#include <bitset>
#include <set>
#include <assert.h>
#include <memory>
#include <unordered_map>

#include "../GameState/GameState.hpp"
#include "../Printer.hpp"
#include "TimeManager.hpp"
#include "../Lib/picojson.h"
#include <boost/any.hpp>
#include <boost/optional.hpp>
#include "../Support/AutoIncrementedID.hpp"

enum class StateType;

namespace ChrData {
  class Character;
  class Money;
  class Level;
  class Experience;
  class HitPoint;
  class Mana;
  class Attack;
  class Defense;
  class Speed;
  class Luck;
}

namespace WorldMap {
  class AreaID;
}

namespace GameUI
{
  
  /*
   Notificationは非同期であるのに対しMessageは同期的であるが、両者の区別ははっきりしていない
   少なくとも引数があるメッセージをNotificationとして伝えることはできない。
   */
	enum Notification : int
	{
		LogUpdated,
    ConversationUpdated,
		StateChanged,
    MapChanged,
    BezierPathChanged,
    WaitingForNextEncount,
		CompletedTargetForDefeat,
    AllEncountProcessed,
    AppearanceBlack,
    AppearanceWhite,
		// ---------
		NumOfMaxNotification
	};
  
  // UserInput は 現在のBufferにためるものから、bitsetに変更する
  enum UserInput : int
	{
		NoInput,
		PushOK,
		PushTalk,
		PushOpenDrawer,
		Pause,
    // ---------
    NumOfMaxUserInput
	};

  enum Status: int
  {
    HP,
    Mana,
    LV,
    ATK,
    DEF,
    SP,
    Luck,
    Exp,
    Money,
    EQP,
    Condition,
    NumOfMaxStatus
  };
  
  enum class BrainkeepType
  {
    TrialChangeEquipment,
    TrialInvitation,
    TrialInformationGathering,
  };
  
  struct Brainkeep
  {
    BrainkeepType type;
    std::string   text;
    bool          success;
  };
  
  class PlayerStatusLog
  {
  private:
    using Condition     = int;
    using EquipmentID   = int;
    using AIName        = std::string;
    using ActionClassID = std::string;
    using WayOfUseMoney = std::string;

    template<class T>
    using TimeStampedList = std::list<std::pair<TimeManager::RealTime, T>>;
    
    
#define TIME_STAMPED_LIST_DEF(name, type)  \
  private:  \
    TimeStampedList< type > name ## _ ; \
  public: \
    void push ## name(type const&); \
    inline auto get ## name ## List() const -> decltype(auto) { return (name ## _); }

    TIME_STAMPED_LIST_DEF(LV,                         std::shared_ptr<ChrData::Level>)
    TIME_STAMPED_LIST_DEF(Exp,                        std::shared_ptr<ChrData::Experience>)
    TIME_STAMPED_LIST_DEF(MaxHP,                      std::shared_ptr<ChrData::HitPoint>)
    TIME_STAMPED_LIST_DEF(MaxMana,                    std::shared_ptr<ChrData::Mana>)
    TIME_STAMPED_LIST_DEF(HP,                         std::shared_ptr<ChrData::HitPoint>)
    TIME_STAMPED_LIST_DEF(Mana,                       std::shared_ptr<ChrData::Mana>)
    TIME_STAMPED_LIST_DEF(ATK,                        std::shared_ptr<ChrData::Attack>)
    TIME_STAMPED_LIST_DEF(DEF,                        std::shared_ptr<ChrData::Defense>)
    TIME_STAMPED_LIST_DEF(SP,                         std::shared_ptr<ChrData::Speed>)
    TIME_STAMPED_LIST_DEF(Weapon,                     EquipmentID)
    TIME_STAMPED_LIST_DEF(Protector,                  EquipmentID)
    TIME_STAMPED_LIST_DEF(Accessory,                  EquipmentID)
    TIME_STAMPED_LIST_DEF(Condition,                  Condition)
    TIME_STAMPED_LIST_DEF(AIName,                     AIName)
    TIME_STAMPED_LIST_DEF(MasteredAction,             ActionClassID)
    TIME_STAMPED_LIST_DEF(Money,                      std::shared_ptr<ChrData::Money>)
    TIME_STAMPED_LIST_DEF(WayOfUseMoney,              WayOfUseMoney)
    TIME_STAMPED_LIST_DEF(ArrivalAtArea,              std::shared_ptr<WorldMap::AreaID>)
    TIME_STAMPED_LIST_DEF(Brainkeep,                  Brainkeep)

#undef TIME_STAMPED_LIST_DEF

  };
  
  struct LampMover
  {
    std::string type;
    std::string movingMode;
    double speed = 0;
    std::complex<double> startPosition;
    std::string scenario;
    picojson::array scenarioBlock;
    
    LampMover(){}
    
    LampMover(std::string const& tp, std::string const& mm, double sp, std::complex<double> const& stpos, std::string const& sc)
    : type(tp), movingMode(mm), speed(sp), startPosition(stpos), scenario(sc) {}
    
    LampMover(std::string const& tp, std::string const& mm, double sp, std::complex<double> const& stpos, picojson::array const& scenarioBlock)
    : type(tp), movingMode(mm), speed(sp), startPosition(stpos), scenarioBlock(scenarioBlock) {}
  };
  
	class Logger
	{
	public:
    using PlayerID    = int;
    using LampMoverID = std::string;
    
  public:
    static std::string const LampMoverPlayerID;
    
	private:
    
    std::list<Printer::Message>                   messagesBuffer_;
    std::list<Printer::Conversation>              conversationBuffer_;
//    AutoIncrementedID                             lampMoversIDGenerator_;
    std::set<LampMoverID>                         usedLampMovers_;
    std::unordered_map<LampMoverID, LampMover>    lampMovers_;
    boost::optional<LampMoverID>                  currentEncountLampMoverID_;
		std::bitset<NumOfMaxNotification>             modified_;
    std::bitset<NumOfMaxStatus>                   playerStatusModified_;
    std::bitset<NumOfMaxUserInput>                userInput_;
    
    std::set<PlayerID>                            encountedPlayerSet_;
    std::unordered_map<PlayerID, PlayerStatusLog> playerStatusLog_;

	public:

		inline Logger()
//      : lampMoversIDGenerator_(1)
		{
			modified_.reset();
		}
    
    auto pushMessage(Printer::Message const& msg) -> void;
    auto pushConversation(Printer::Conversation const& conversation) -> void;
    auto pushLampMover(LampMoverID id, std::string const& type, std::string const& movingMode, int speed, std::complex<double> const& startPosition, std::string const& scenario) -> void;
    auto pushLampMover(LampMoverID id, std::string const& type, std::string const& movingMode, int speed, std::complex<double> const& startPosition, picojson::array const& scenarioBlock) -> void;
    auto isGeneratedLampMover(LampMoverID lampMoverID) { return usedLampMovers_.count(lampMoverID); }
    
    inline auto getLampMoverWithID(LampMoverID id) -> decltype(auto)
    {
      return (lampMovers_[id]);
    }
    
    inline auto getCurrentSpecialEncountLampMoverID() const -> decltype(auto)
    {
      return (currentEncountLampMoverID_);
    }
    
    inline auto flushCurrentSpecialEncountLampMoverID()
    {
      currentEncountLampMoverID_ = boost::none;
    }
    
    inline auto notify(int notification)
    {
      modified_.set(notification);
    }
    
    inline auto isNotified(int notification)
    {
      if(modified_.test(notification)) {
        modified_.reset(notification);
        return true;
      }
      return false;
    }
    
    inline auto notifySpecialEncount(LampMoverID id)
    {
      currentEncountLampMoverID_ = boost::make_optional<LampMoverID>(id);
    }
    
    inline auto flushLog()
    {
      messagesBuffer_.clear();
    }
    
    inline auto flushConversation()
    {
      conversationBuffer_.clear();
    }
    
    inline auto isPlayerStatusChanged(Status status)
    {
      if(playerStatusModified_.test(status)) {
        playerStatusModified_.reset(status);
        return true;
      }
      return false;
    }
    
    inline auto notifyPlayerStatusChanged(Status status)
    {
      playerStatusModified_.set(status);
    }

    inline auto getMessagesBuffer() const -> decltype(auto)
    {
      return (messagesBuffer_);
    }
    
    inline auto getConversationBuffer() const -> decltype(auto)
    {
      return (conversationBuffer_);
    }

    inline auto insertEncountedPlayerSet(PlayerID playerID)
    {
      encountedPlayerSet_.insert(playerID);
    }

    inline auto getEncountedPlayerSet() const -> decltype(auto)
    {
      return (encountedPlayerSet_);
    }

    inline auto getPlayerStatusLog(PlayerID playerID) -> decltype(auto)
    {
      AUFM_ASSERT(encountedPlayerSet_.count(playerID), "PlayerID: " << playerID << " has not found in encountedPlayerSet_.");
      return (playerStatusLog_[playerID]);
    }
    
	};
}