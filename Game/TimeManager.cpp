#include "TimeManager.hpp"

namespace TimeManager
{
  auto Timer::consumeAufmTime(AufmTime aufmTime)
  {
    requestedConsumingAufmTime_ -= aufmTime;
    if(requestedConsumingAufmTime_ <= 0) {
      requestedConsumingAufmTime_ = AufmTime(0);
    }
  }

  auto Timer::updateRealTime(RealTime time)
    -> void
  {
    elaspedTime_ += time;
    consumeAufmTime(AufmTime(toAufmTimeValue(time)));
  }
}