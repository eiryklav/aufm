#pragma once

#import <UIKit/UIKit.h>

@interface DelaunayDiagram : NSObject

+ (NSMutableArray*)triangleList;

@end