//
//  MahojinSpark.swift
//  spritekit-cutin-test
//
//  Created by moti on 2015/08/12.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import SpriteKit

public class Mahojin : SpellEffectProtocol {
  
  public weak var delegate: SpellEffectDelegate?
  private let skView:   SKView
  private let skScene:  SKScene
  
  required public init(parentView: UIView) {
    skView = SKView(frame: UIScreen.mainScreen().bounds)
    skView.allowsTransparency = true
    skView.userInteractionEnabled = false
    parentView.addSubview(skView)
    skScene = SKScene(size: skView.bounds.size)
    skScene.backgroundColor = UIColor.clearColor()
    skView.presentScene(skScene)
  }
  
  public func run() {
    
    let mahojinSprite = SKSpriteNode(imageNamed: "mahojin-blurred")
    mahojinSprite.size = CGSizeMake(skView.frame.width * 0.09, skView.frame.width * 0.09)
    mahojinSprite.position = CGPointMake(skView.frame.width / 2, skView.frame.height / 2)
    mahojinSprite.blendMode = .Alpha
    mahojinSprite.colorBlendFactor = 0.5
    mahojinSprite.color = UIColor.cyanColor()
    skScene.addChild(mahojinSprite)
    
    mahojinSprite.runAction(
      SKAction.sequence([
        SKAction.group([
          SKAction.scaleXTo(6.0, duration: 0.5),
          SKAction.scaleYTo(6.0, duration: 0.5)
        ]),
        SKAction.repeatActionForever(SKAction.rotateByAngle(CGFloat(M_PI_4), duration: 1.0))
      ])
    )
    
    NSTimer.scheduledTimerWithTimeInterval(5.0, target: NSBlockOperation(block: {
      mahojinSprite.runAction(SKAction.scaleTo(0, duration: 0.5), completion: {
        mahojinSprite.removeFromParent()
        self.skScene.removeFromParent()
        self.skView.removeFromSuperview()
        self.delegate?.didEndSpellEffect()
      })
    }), selector: "main", userInfo: nil, repeats: false)
    
  }
  
}