//
//  NegaImage.h
//  spritekit-cutin-test
//
//  Created by moti on 2015/08/13.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

#ifndef spritekit_cutin_test_NegaImage_h
#define spritekit_cutin_test_NegaImage_h

#import <UIKit/UIKit.h>

@interface NegaImage : NSObject

+ (UIImage*) createFromImage: (UIImage*)image;

@end

#endif
