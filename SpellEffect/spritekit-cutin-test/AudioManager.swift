//
//  AudioManagerView.swift
//  Aufm
//
//  Created by moti on 2015/06/16.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AVFoundation

class AudioManagerView {
  
  static let sharedInstance = AudioManagerView()
  
  private var bgmPlayer: AVAudioPlayer!
  private var sePlayerDictionary = [String:AVAudioPlayer]()
  
  var bgmVolume:  Float = 0.5
  var seVolume:   Float = 0.5
  
  private var isOnBGM = false//true // default true for release
  private var isOnSE  = true
  
  private init() {}
  
}

protocol AudioManagerViewProtocol {
  func onBGM()
  func offBGM()
  func onSE()
  func offSE()
  func playBGM(fname: String)
  func playSE(fname: String)
  func turnBGMVolumeDown()
  func returnBGMVolumeToOriginal()
}

extension AudioManagerView : AudioManagerViewProtocol {
  
  func onBGM() { isOnBGM = true ; bgmPlayer.play() }
  func offBGM() { isOnBGM = false ; bgmPlayer.stop() }
  func onSE() { isOnSE = true }
  func offSE() { isOnSE = false }
  
  func playBGM(fname: String) {
    let soundFilePath: NSString = NSBundle.mainBundle().pathForResource(fname, ofType: "caf")!
    let fileURL: NSURL = NSURL(fileURLWithPath: soundFilePath as String)!
    bgmPlayer = AVAudioPlayer(contentsOfURL: fileURL, error: nil)
    bgmPlayer.volume = bgmVolume
    bgmPlayer.numberOfLoops = -1
    if isOnBGM { bgmPlayer.play() }
  }
  
  func playSE(fname: String) {
    let soundFilePath: NSString = NSBundle.mainBundle().pathForResource(fname, ofType: "caf")!
    let fileURL = NSURL(fileURLWithPath: soundFilePath as String)!
    if sePlayerDictionary[fname] == nil {
      sePlayerDictionary[fname] = AVAudioPlayer(contentsOfURL: fileURL, error: nil)
    }
    let player = sePlayerDictionary[fname]!
    player.volume = seVolume
    if player.playing {
      player.currentTime = 0.0
    }
    else {
      if isOnSE { player.play() }
    }
  }
  
  func turnBGMVolumeDown() {
    if bgmPlayer.playing {
      bgmVolume = bgmPlayer.volume
      bgmPlayer.volume *= 0.5
    }
  }
  
  func returnBGMVolumeToOriginal() {
    bgmPlayer.volume = bgmVolume
  }
}
