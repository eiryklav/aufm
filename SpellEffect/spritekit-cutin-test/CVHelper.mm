//
//  OpenCVHelper.m
//  spritekit-cutin-test
//
//  Created by moti on 2015/08/12.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CVHelper.h"

@implementation CVHelper : NSObject 

+ (cv::Mat)matWithImage:(UIImage*)image
{
  // 画像の回転を補正する（内蔵カメラで撮影した画像などでおかしな方向にならないようにする）
  UIImage* correctImage = image;
  UIGraphicsBeginImageContext(correctImage.size);
  [correctImage drawInRect:CGRectMake(0, 0, correctImage.size.width, correctImage.size.height)];
  correctImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  
  // UIImage -> cv::Mat
  cv::Mat mat;
  
  UIImageToMat(correctImage, mat);
  return mat;
}

@end