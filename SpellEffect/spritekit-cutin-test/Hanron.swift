//
//  Hanron.swift
//  spritekit-cutin-test
//
//  Created by moti on 2015/08/12.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import SpriteKit
import AufmGameModel

public class Hanron : SpellEffectProtocol {
  
  public weak var delegate: SpellEffectDelegate?
  private let skView:   SKView
  private let skScene:  SKScene
  
  required public init(parentView: UIView) {
    skView = SKView(frame: UIScreen.mainScreen().bounds)
    skView.allowsTransparency = true
    skView.userInteractionEnabled = false
    parentView.addSubview(skView)
    skScene = SKScene(size: skView.bounds.size)
    skScene.backgroundColor = UIColor.clearColor()
    skView.presentScene(skScene)
  }
  
  public func run() {
    
    // 画面ごと動くので元の画面を一時停止
    // 全てのカットインで元の画面を一時停止させるとは限らない。後ろの動きが見える場合は停止させない
    skScene.paused = true
    
    let scene = SKScene(size: skView.bounds.size)
    
    let blackSprite = SKSpriteNode(color: UIColor.blackColor(), size: UIScreen.mainScreen().bounds.size)
    scene.addChild(blackSprite)
    
    let beforeViewSprite = SKSpriteNode.createViewSprite()
    scene.addChild(beforeViewSprite)
    
    let blackHeightSpace = UIScreen.mainScreen().bounds.height * 0.25
    
    let backgroundSprite = SKSpriteNode(texture: SKTexture(image: UIImage.imageFromView()))
    backgroundSprite.size = UIScreen.mainScreen().bounds.size
    backgroundSprite.position = CGPointMake(skView.center.x, -blackHeightSpace)
    scene.addChild(backgroundSprite)
    
    beforeViewSprite.runAction(SKAction.moveTo(CGPointMake(skView.center.x, beforeViewSprite.size.height + blackHeightSpace), duration: 0.15))
    backgroundSprite.runAction(SKAction.moveTo(skView.center, duration: 0.15))
    
    // カットイン描画開始
    skView.presentScene(scene)
    
    let sprite1 = SKSpriteNode(imageNamed: "part1")
    sprite1.size = sprite1.size.aspectFitWithScreen()
    sprite1.size *= 1.05
    sprite1.position = skView.center
    scene.addChild(sprite1)
    
    let sprite2 = SKSpriteNode(imageNamed: "part2")
    sprite2.size = sprite2.size.aspectFitWithScreen()
    sprite2.size *= 1.3
    sprite2.position = skView.center
    sprite2.position -= CGPointMake(SW_MultipledBy(0.2), SH_MultipledBy(0.25))
    scene.addChild(sprite2)
    
    let sprite3 = SKSpriteNode(imageNamed: "part3")
    sprite3.size = sprite3.size.aspectFitWithScreen()
    sprite3.size *= 1.1
    sprite3.position = skView.center
    sprite3.position += CGPointMake(SW_MultipledBy(0.05), -SH_MultipledBy(0.1))
    scene.addChild(sprite3)
    
    sprite1.runAction(SKAction.repeatActionForever(SKAction.shakeAction()))
    sprite2.runAction(SKAction.moveBy(CGVector(angle: -M_PI/8, multipledBy: 30.0), duration: 3.0))
    sprite3.runAction(SKAction.moveBy(CGVector(angle: -M_PI/8, multipledBy: 50.0), duration: 3.0))
    
    NSTimer.scheduledTimerWithTimeInterval(0.5, target: NSBlockOperation(block: {
      Audio.sharedInstance.turnBGMVolumeDown()
      Audio.sharedInstance.playSE("spellcard")
    }), selector: "main", userInfo: nil, repeats: false)
    
    NSTimer.scheduledTimerWithTimeInterval(3.0, target: NSBlockOperation(block: {
      // カットイン終了
      Audio.sharedInstance.returnBGMVolumeToOriginal()
      self.skScene.removeFromParent()
      self.skView.removeFromSuperview()
      self.delegate?.didEndSpellEffect()
    }), selector: "main", userInfo: nil, repeats: false)
  }
  
}


extension SKAction {
  
  class func shakeAction() -> SKAction {
    return SKAction.sequence([
      SKAction.rotateByAngle(CGFloat(+M_PI/512), duration: 0.02),
      SKAction.rotateByAngle(CGFloat(-M_PI/512), duration: 0.02),
      SKAction.rotateByAngle(CGFloat(-M_PI/512), duration: 0.02),
      SKAction.rotateByAngle(CGFloat(+M_PI/512), duration: 0.02)
      ])
  }
  
  class func swayUpAndDown(#sprite: SKSpriteNode) -> SKAction {
    return SKAction.sequence([
      SKAction.moveTo(CGPointMake(sprite.position.x, sprite.position.y + sprite.size.height * 0.01), duration: 0.5),
      SKAction.moveTo(CGPointMake(sprite.position.x, sprite.position.y - sprite.size.height * 0.01), duration: 0.5)
      ])
  }
  
}