//
//  ColorfulBurst.swift
//  spritekit-cutin-test
//
//  Created by moti on 2015/08/12.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import SpriteKit

public class ColorfulBurst : SpellEffectProtocol {
  
  public weak var delegate: SpellEffectDelegate?
  private let skView:   SKView
  private let skScene:  SKScene
  
  required public init(parentView: UIView) {
    skView = SKView(frame: UIScreen.mainScreen().bounds)
    skView.allowsTransparency = true
    skView.userInteractionEnabled = false
    parentView.addSubview(skView)
    skScene = SKScene(size: skView.bounds.size)
    skScene.backgroundColor = UIColor.clearColor()
    skView.presentScene(skScene)
  }
  
  public func run() {
    var blackSheet = SKShapeNode(rect: UIScreen.mainScreen().bounds)
    blackSheet.fillColor = UIColor.blackColor()
    blackSheet.alpha = 0.0
    blackSheet.zPosition = 1
    self.skScene.addChild(blackSheet)
    blackSheet.runAction(SKAction.fadeAlphaTo(0.25, duration: 0.5))
    NSTimer.scheduledTimerWithTimeInterval(5.0, target: NSBlockOperation(block: {
      blackSheet.runAction(SKAction.fadeAlphaTo(0.0, duration: 0.5), completion: {
        blackSheet.removeFromParent()
        self.skScene.removeFromParent()
        self.skView.removeFromSuperview()
        self.delegate?.didEndSpellEffect()
      })
    }), selector: "main", userInfo: nil, repeats: false)
    //////////////////////////////////////////////////////////////////////////
    
    let mahojin = Mahojin(parentView: skView)
    mahojin.run()
    NSTimer.scheduledTimerWithTimeInterval(0.5, target: NSBlockOperation(block: {
      self.createColorfulBurst()
    }), selector: "main", userInfo: nil, repeats: false)
  }
  
}

// MARK: - Private -

extension ColorfulBurst {
  private func createMusoFuinCircles() -> [SKSpriteNode] {
    var ret = [SKSpriteNode]()
    for var index=0; index<5; index++ {
      let sprite =
      SKSpriteNode.createColorfulGlowingCircle(
        radius: skView.frame.width * 0.03,
        position: CGPoint.vertexPointOfPolygon(
          indexOfVertices: index,
          numberOfVertices: 5,
          center: skView.center,
          centerDistance: skView.frame.height * 0)
      )
      sprite.name = index.description
      ret.append(sprite)
      self.skScene.addChild(ret.last!)
    }
    return ret
  }
  
  private func createColorfulBurst() {
    var musoCircles = createMusoFuinCircles()
    var maxCD: CGFloat = skView.frame.height * 0.2
    let D = M_PI * 2.0 / Double(musoCircles.count)
    
    let DelayAddAttraction = 0.3
    
    for var i = 0; i < musoCircles.count; i++ {
      musoCircles[i].runAction(
        SKAction.sequence([
          SKAction.rotateToAngle(CGFloat(-M_PI * 4.0), duration: 2.0),
          SKAction.rotateToAngle(CGFloat(-M_PI * 3.5), duration: 1.0)
          ])
      )
      musoCircles[i].runAction(
        SKAction.sequence([
          
          // 1
          SKAction.group([
            SKAction.sequence([
              SKAction.scaleTo(4.0, duration: 1.0),
              SKAction.scaleTo(7.0, duration: 1.0)
              ]),
            SKAction.customActionWithDuration(
              2.0,
              actionBlock: { (node: SKNode!, elapsedTime: CGFloat) -> () in
                let e = Double(elapsedTime)
                node.position = CGPointMake(
                  self.skView.center.x + CGFloat(sin(e * 2.0 * M_PI - D * Double(node.name!.toInt()! + 1))) * elapsedTime * maxCD,
                  self.skView.center.y + CGFloat(cos(e * 2.0 * M_PI - D * Double(node.name!.toInt()! + 1))) * elapsedTime * maxCD
                )
              }
            )
            ]),
          
          // 3
          SKAction.customActionWithDuration(
            0,
            actionBlock: { (node: SKNode!, elapsedTime: CGFloat) -> () in
              (node as! SKSpriteNode).burstColorfulGlowingCircle()
          })
          ])
      )
      // end for
    }
  }
}