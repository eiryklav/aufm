//
//  MusoFuin.swift
//  spritekit-cutin-test
//
//  Created by moti on 2015/08/12.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import SpriteKit

public class MusoFuin : SpellEffectProtocol {
  
  public var delegate: SpellEffectDelegate?
  private let skView:   SKView
  private let skScene:  SKScene
  
  required public init(parentView: UIView) {
    skView = SKView(frame: UIScreen.mainScreen().bounds)
    skView.allowsTransparency = true
    skView.userInteractionEnabled = false
    parentView.addSubview(skView)
    skScene = SKScene(size: skView.bounds.size)
    skScene.backgroundColor = UIColor.clearColor()
    skView.presentScene(skScene)
  }
  
  private func createMusoFuinCircles() -> [SKSpriteNode] {
    var ret = [SKSpriteNode]()
    for var index=0; index<5; index++ {
      let sprite =
      SKSpriteNode.createColorfulGlowingCircle(
        radius: skView.frame.width * 0.03,
        position: CGPoint.vertexPointOfPolygon(
          indexOfVertices: index,
          numberOfVertices: 5,
          center: skView.center,
          centerDistance: skView.frame.height * 0)
      )
      sprite.name = index.description
      ret.append(sprite)
      self.skScene.addChild(ret.last!)
    }
    return ret
  }
  
  public func run() {
    
    var blackSheet = SKShapeNode(rect: UIScreen.mainScreen().bounds)
    blackSheet.fillColor = UIColor.blackColor()
    blackSheet.alpha = 0.0
    blackSheet.zPosition = 1
    self.skScene.addChild(blackSheet)
    blackSheet.runAction(SKAction.fadeAlphaTo(0.25, duration: 0.5))
    NSTimer.scheduledTimerWithTimeInterval(5.0, target: NSBlockOperation(block: {
      blackSheet.runAction(SKAction.fadeAlphaTo(0.0, duration: 0.5), completion: {
        blackSheet.removeFromParent()
        self.skScene.removeFromParent()
        self.skView.removeFromSuperview()
        self.delegate?.didEndSpellEffect()
      })
    }), selector: "main", userInfo: nil, repeats: false)
    //////////////////////////////////////////////////////////////////////////
    
    var musoCircles = createMusoFuinCircles()
    var maxCD: CGFloat = skView.frame.height * 0.2
    let D = M_PI * 2.0 / Double(musoCircles.count)
    
    let DelayAddAttraction = 0.3
    
    for var i = 0; i < musoCircles.count; i++ {
      musoCircles[i].runAction(SKAction.rotateToAngle(CGFloat(-M_PI * 6.0), duration: 5.0))
      musoCircles[i].runAction(
        SKAction.sequence([
          SKAction.group([
            SKAction.sequence([
              SKAction.scaleTo(6.0, duration: 2.0),
              SKAction.scaleTo(5.0, duration: 1.0)
              ]),
            SKAction.customActionWithDuration(
              3.0 + Double(i) * DelayAddAttraction,
              actionBlock: { (node: SKNode!, elapsedTime: CGFloat) -> () in
                let e = Double(elapsedTime)
                node.position = CGPointMake(
                  self.skView.center.x + CGFloat(sin(e * 1.0 * M_PI - D * Double(node.name!.toInt()! + 1))) * elapsedTime * maxCD,
                  self.skView.center.y + CGFloat(cos(e * 1.0 * M_PI - D * Double(node.name!.toInt()! + 1))) * elapsedTime * maxCD
                )
              }
            )
            ]),
          SKAction.customActionWithDuration(
            0.3,
            actionBlock: { (node: SKNode!, elapsedTime: CGFloat) -> () in
              let E = 3.0 + Double(node.name!.toInt()!) * DelayAddAttraction
              let e = Double(elapsedTime) + E
              
              // 回転力
              node.position = CGPointMake(
                self.skView.center.x + CGFloat(sin(e * 2.0 * M_PI - D * Double(node.name!.toInt()! + 1))) * CGFloat(E) * maxCD,
                self.skView.center.y + CGFloat(cos(e * 2.0 * M_PI - D * Double(node.name!.toInt()! + 1))) * CGFloat(E) * maxCD
              )
              
              // 引力
              node.position -= (node.position - self.skView.center) / (CGFloat(0.6) / elapsedTime)
          }),
          SKAction.customActionWithDuration(
            0.2,
            actionBlock: { (node: SKNode!, elapsedTime: CGFloat) -> () in
              // 引力
              node.position -= (node.position - self.skView.center) / (CGFloat(0.2) / elapsedTime)
          }),
          SKAction.customActionWithDuration(
            0.001,
            actionBlock: { (node: SKNode!, elapsedTime: CGFloat) -> () in
              (node as! SKSpriteNode).burstColorfulGlowingCircle()
          })
          ])
      )
    }
  }
  
}

extension SKSpriteNode {
  
  class func numberOfCirclesOfColorfulGlowingCircle() -> Int {
    return 6
  }
  
  class func createColorfulGlowingCircle(#radius: CGFloat, position: CGPoint) -> SKSpriteNode {
    let ret = SKSpriteNode()
    ret.position = position
    
    let colors = [SKColor(rgba: "#FF0000FF"), SKColor(rgba: "#00FF00FF"), SKColor(rgba: "#0000FFFF"), SKColor(rgba: "#FFFF00FF"), SKColor(rgba: "#00FFFFFF"), SKColor(rgba: "#FF00FFFF")]
    
    let Num = numberOfCirclesOfColorfulGlowingCircle()
    
    for var i=0; i<Num; i++ {
      let circle = SKSpriteNode(imageNamed: "blurred-white-circle")
      circle.size = CGSizeMake(radius * 2.0, radius * 2.0)
      circle.colorBlendFactor = 1.0
      circle.color = colors[i]
      circle.position = CGPoint.vertexPointOfPolygon(indexOfVertices: i, numberOfVertices: Num, center: CGPointZero, centerDistance: radius * 0.2)
      circle.blendMode = SKBlendMode.Screen
      ret.addChild(circle)
    }
    
    ret.zPosition = 2
    
    return ret
  }
  
  func burstColorfulGlowingCircle() {
    
    let Num = SKSpriteNode.numberOfCirclesOfColorfulGlowingCircle()
    
    for var i=0; i<Num; i++ {
      let circle = self.children[i] as! SKSpriteNode
      circle.runAction(
        SKAction.group([
          SKAction.sequence([
            SKAction.scaleTo(0.95, duration: 0.4),
            SKAction.scaleTo(0.25, duration: 0.1)
            ]),
          SKAction.moveBy(
            CGVector(
              dx: UIScreen.mainScreen().bounds.size.width / 15.0 * CGFloat.random(min: -1, max: 1),
              dy: UIScreen.mainScreen().bounds.size.height / 10.0 * CGFloat.random(min: -1, max: 1)
            ),
            duration: 0.5
          ),
          SKAction.fadeAlphaTo(0.0, duration: 0.8)
          ]),
        completion: {
          circle.removeFromParent()
        }
      )
    }
    
    NSTimer.scheduledTimerWithTimeInterval(3.0, target: NSBlockOperation(block: {
      self.removeFromParent()
    }), selector: "main", userInfo: nil, repeats: false)
    
  }
  
  class func createGlowingCenterWhiteCircle(#radius: CGFloat, fillColor: SKColor, position: CGPoint) -> SKSpriteNode {
    let circle = SKSpriteNode(imageNamed: "blurred-white-circle")
    circle.colorBlendFactor = 1.0
    circle.color = fillColor
    circle.size = CGSizeMake(radius * 2.0, radius * 2.0)
    circle.position = position
    let circle2 = SKSpriteNode(imageNamed: "blurred-white-circle")
    circle2.size = CGSizeMake(radius * 1.8, radius * 1.8)
    circle2.position = CGPointZero
    circle.addChild(circle2)
    return circle
  }

}