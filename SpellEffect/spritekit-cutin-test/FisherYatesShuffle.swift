//
//  Shuffle.swift
//  spritekit-cutin-test
//
//  Created by moti on 2015/08/12.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

// http://stackoverflow.com/questions/24026510/how-do-i-shuffle-an-array-in-swift

import UIKit

func shuffle<C: MutableCollectionType where C.Index == Int>(var list: C) -> C {
  let c = count(list)
  if c < 2 { return list }
  for i in 0..<(c - 1) {
    let j = Int(arc4random_uniform(UInt32(c - i))) + i
    swap(&list[i], &list[j])
  }
  return list
}