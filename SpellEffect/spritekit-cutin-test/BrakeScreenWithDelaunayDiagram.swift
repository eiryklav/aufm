//
//  BrakeScreenWithDelaunayDiagram.swift
//  spritekit-cutin-test
//
//  Created by moti on 2015/08/12.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import SpriteKit
import AufmSupport

public class BrakeScreenWithDelaunayDiagram : SpellEffectProtocol {
  
  public weak var delegate:    SpellEffectDelegate?
  private let skView:   SKView!
  private let skScene:  SKScene!
  
  private var pts: [CGPoint]!
  
  required public init(parentView: UIView) {
    skView = SKView(frame: UIScreen.mainScreen().bounds)
    skView.allowsTransparency = true
    skView.userInteractionEnabled = false
    parentView.addSubview(skView)
    skScene = SKScene(size: skView.bounds.size)
    skScene.backgroundColor = UIColor.clearColor()
    skView.presentScene(skScene)
  }
  
  private func delaunayGraphDebug(a: CGPoint, _ b: CGPoint, _ c: CGPoint) {
    let pa = SKShapeNode(circleOfRadius: 2.0)
    pa.fillColor = UIColor.redColor()
    pa.strokeColor = UIColor.clearColor()
    pa.position = a
    pa.zPosition = 2
    skScene.addChild(pa)
    let pb = SKShapeNode(circleOfRadius: 2.0)
    pb.fillColor = UIColor.redColor()
    pb.strokeColor = UIColor.clearColor()
    pb.position = a
    pb.zPosition = 2
    skScene.addChild(pb)
    let pc = SKShapeNode(circleOfRadius: 2.0)
    pc.fillColor = UIColor.redColor()
    pc.strokeColor = UIColor.clearColor()
    pc.position = a
    pc.zPosition = 2
    skScene.addChild(pc)
    
    let bpath = UIBezierPath()
    bpath.moveToPoint(a)
    bpath.addLineToPoint(b)
    bpath.addLineToPoint(c)
    
    let tri = SKShapeNode()
    tri.path = bpath.CGPath
    tri.zPosition = 2
    skScene.addChild(tri)
    
    NSTimer.scheduledTimerWithTimeInterval(5.0, target: NSBlockOperation(block: {
      pa.removeFromParent()
      pb.removeFromParent()
      pc.removeFromParent()
      tri.removeFromParent()
      self.skScene.removeFromParent()
      self.skView.removeFromSuperview()
      self.delegate?.didEndSpellEffect()
    }), selector: "main", userInfo: nil, repeats: false)
  }
  
  public func run() {
    
    let triangles = DelaunayDiagram.triangleList()
    
    let viewImage = UIImage.imageFromView()
    
    for var i=0; i<triangles.count; i++ {
      
      let triangle = triangles[i] as! [NSValue]
      let a = triangle[0].CGPointValue()
      let b = triangle[1].CGPointValue()
      let c = triangle[2].CGPointValue()
      
      // FIXME: クリッピングしたら高速になったが、クリッピングの方法を間違えているので破片が拡大している
      let cropOrigin  = CGPointMake(min(a.x, b.x, c.x) - 1, min(a.y, b.y, c.y) - 1)
      let cropSize    = CGSizeMake(max(a.x, b.x, c.x) - cropOrigin.x + 1, max(a.y, b.y, c.y) - cropOrigin.y + 1)
      let cropCGImageRef = CGImageCreateWithImageInRect(viewImage.CGImage, CGRect(origin: cropOrigin, size: cropSize))
      let cropImage = UIImage(CGImage: cropCGImageRef)!
      let viewNode = SKSpriteNode(texture: SKTexture(image: cropImage))
      
      let clippingPath = CGPathCreateMutable()
      CGPathMoveToPoint   (clippingPath, nil, a.x - cropOrigin.x, a.y - cropOrigin.y)
      CGPathAddLineToPoint(clippingPath, nil, b.x - cropOrigin.x, b.y - cropOrigin.y)
      CGPathAddLineToPoint(clippingPath, nil, c.x - cropOrigin.x, c.y - cropOrigin.y)
      
      //delaunayGraphDebug(a, b, c)
      
      let maskShape = SKShapeNode(path: clippingPath)
      maskShape.fillColor = UIColor.blackColor()
      let mask = SKSpriteNode(texture: skView.textureFromNode(maskShape))
      
      let cropNode = SKCropNode()
      cropNode.addChild(viewNode)
      cropNode.maskNode = mask
      cropNode.position = skView.center
      cropNode.zPosition = 2
      skScene.addChild(cropNode)

      let centABC = (a + b + c) / 3
      let nml = (centABC - skView.center).normalization()
      let speed = Screen.size.width / 2 - (centABC - skView.center).length()
      let vec = (nml * (Screen.size.width / 100) * speed).toCGVector()
      cropNode.runAction(
        SKAction.group([
          SKAction.fadeAlphaTo(0.0, duration: 3.0),
          SKAction.moveBy(vec, duration: 3.0)
        ]),
        completion: {
          cropNode.removeFromParent()
        })
    }
    
    
    var blackSheet = SKShapeNode(rect: UIScreen.mainScreen().bounds)
    blackSheet.fillColor = UIColor.blackColor()
    blackSheet.alpha = 0.0
    blackSheet.zPosition = 1
    self.skScene.addChild(blackSheet)
    blackSheet.runAction(SKAction.fadeAlphaTo(0.25, duration: 0.5))
    NSTimer.scheduledTimerWithTimeInterval(3.0, target: NSBlockOperation(block: {
      blackSheet.runAction(SKAction.fadeAlphaTo(0.0, duration: 0.5), completion: {
        blackSheet.removeFromParent()
        self.skScene.removeFromParent()
        self.skView.removeFromSuperview()
        self.delegate?.didEndSpellEffect()
      })
    }), selector: "main", userInfo: nil, repeats: false)
    //////////////////////////////////////////////////////////////////////////
    
  }
  
}