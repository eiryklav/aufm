//
//  GameViewController.swift
//  spritekit-cutin-test
//
//  Created by moti on 2015/08/10.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import SpriteKit
import SceneKit
import AufmGameModel

extension CGSize {
  func aspectFitWithScreen() -> CGSize {
    let ratio = min(UIScreen.mainScreen().bounds.width / width, UIScreen.mainScreen().bounds.height / height)
    return CGSizeMake(width * ratio, height * ratio)
  }
}

//////////////////////////////////////////////////////////////////////////////////////////

class GameViewController: UIViewController {
  
  private var skView:     SKView!
  private var skScene:    SKScene!
  private var scnView:    SCNView!
  private var scnScene:   SCNScene!
  
  private var enemy: SKSpriteNode!
  
  private var tableView: UITableView!
  
  private let effectNameList = ["Hanron", "MusoFuin", "BrakeScreen", "NegaFlare", "Mahojin", "ColorfulBurst", "RadialGlassEffect"]
  private var effectClassList: [SpellEffectProtocol]!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    
    // BGM
    Audio.sharedInstance.playBGM("seimei")

    // SceneKit
//    scnView = SCNView(frame: skView.bounds)
//    scnView = SC
//    scnScene.
    
    
    // SpriteKit
    skView = self.view as! SKView
    skView.ignoresSiblingOrder = true
    
    skScene = SKScene(size: self.view.bounds.size)
    skScene.scaleMode = .AspectFill
    skView.presentScene(skScene)
    let screenSprite = SKSpriteNode(imageNamed: "main")
    screenSprite.size = UIScreen.mainScreen().bounds.size
    screenSprite.position = skView.center
    skScene.addChild(screenSprite)
    
    setEnemySprite()
    
    
    
    /////////////////////////////////////////////////////////////////
    // table view
    
    // make table view data
    effectClassList = []
    effectClassList.append(Hanron(parentView: skView))
    effectClassList.append(MusoFuin(parentView: skView))
    effectClassList.append(BrakeScreenWithDelaunayDiagram(parentView: skView))
    effectClassList.append(NegaFlare(parentView: skView))
    effectClassList.append(Mahojin(parentView: skView))
    effectClassList.append(ColorfulBurst(parentView: skView))
    effectClassList.append(RadialGlassEffect(parentView: skView))
    
    // show table view
    tableView = UITableView(frame: CGRectMake(view.frame.width * 0.7, view.frame.height * 0.3, view.frame.width * 0.25, view.frame.height * 0.6))
    tableView.alpha = 0.5
    tableView.delegate = self
    tableView.dataSource = self
    self.skView.addSubview(tableView)
  }
  
  private func setEnemySprite() {
    
    let sprite = SKSpriteNode(imageNamed: "sukuna")
    sprite.size = sprite.size.aspectFitWithScreen()
    sprite.size *= 0.75
    sprite.position = skView.center
    sprite.zPosition = 1
    
    let shadow = sprite.createShadowSprite()
    
    skScene.addChild(shadow)
    skScene.addChild(sprite)
    
    shadow.runAction(SKAction.repeatActionForever(
      SKAction.swayUpAndDown(sprite: sprite)
      ))
    sprite.runAction(SKAction.repeatActionForever(
      SKAction.swayUpAndDown(sprite: sprite)
      ))
  }
  
}

extension GameViewController : UITableViewDelegate, UITableViewDataSource {
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    effectClassList[indexPath.row].run()
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return effectClassList.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "Cell")
    cell.textLabel?.text = effectNameList[indexPath.row]
    return cell
  }
}

