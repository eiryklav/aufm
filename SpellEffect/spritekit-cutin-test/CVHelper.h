#pragma once

#import <UIKit/UIKit.h>
#import <opencv2/opencv.hpp>
#import <opencv2/highgui/ios.h>


@interface CVHelper : NSObject

+ (cv::Mat)matWithImage:(UIImage*)image;

@end