//
//  NegaFlare.swift
//  spritekit-cutin-test
//
//  Created by moti on 2015/08/13.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import SpriteKit

public class NegaFlare : SpellEffectProtocol {
  
  public weak var delegate: SpellEffectDelegate?
  private let skView:       SKView
  private let skScene:      SKScene
  
  var mask1: SKSpriteNode!
  var mask2: SKSpriteNode!
  var crop1: SKCropNode!
  var crop2: SKCropNode!
  
  required public init(parentView: UIView) {
    skView = SKView(frame: UIScreen.mainScreen().bounds)
    skView.allowsTransparency = true
    skView.userInteractionEnabled = false
    parentView.addSubview(skView)
    skScene = SKScene(size: skView.bounds.size)
    skScene.backgroundColor = UIColor.clearColor()
    skView.presentScene(skScene)
  }
  
  public func run() {
    
    let negaSprite = SKSpriteNode(texture: SKTexture(image: NegaImage.createFromImage(UIImage.imageFromView())))
    negaSprite.position = skView.center
    mask1 = SKSpriteNode(imageNamed: "gekiha-mask1")
    mask1.position = self.skView.center
    crop1 = SKCropNode()
    crop1.addChild(negaSprite)
    crop1.maskNode = mask1
    crop1.position = CGPointZero
    crop1.zPosition = 1
    self.skScene.addChild(crop1)
    
    mask1.runAction(
      SKAction.sequence([
        SKAction.scaleTo(0, duration: 0),
        SKAction.scaleTo(1.3, duration: 0.7),
      ])
    )
    
    NSTimer.scheduledTimerWithTimeInterval(0.6, target: NSBlockOperation(block: {
      self.skView.setNeedsDisplay()
      let negaSprite = SKSpriteNode(texture: SKTexture(image: NegaImage.createFromImage(UIImage.imageFromView())))
      negaSprite.position = self.skView.center
      let maskShape = SKShapeNode(circleOfRadius: 200.0)
      maskShape.fillColor = UIColor.blackColor()
      maskShape.strokeColor = UIColor.clearColor()
      self.mask2 = SKSpriteNode(texture: self.skView.textureFromNode(maskShape))
//      let mask = SKSpriteNode(imageNamed: "gekiha-mask2")
      self.mask2.position = self.skView.center
      self.crop2 = SKCropNode()
      self.crop2.addChild(negaSprite)
      self.crop2.maskNode = self.mask2
      self.crop2.position = CGPointZero
      self.crop2.zPosition = 1
      
      self.skScene.addChild(self.crop2)
      
      self.mask2.runAction(
        SKAction.sequence([
          SKAction.scaleTo(0, duration: 0),
          SKAction.scaleTo(6.0, duration: 1.0)
        ])
      )
      
      NSTimer.scheduledTimerWithTimeInterval(0.5, target: NSBlockOperation(block: {
        self.crop1.removeFromParent()
        self.crop2.removeFromParent()
        self.skScene.removeFromParent()
        self.skView.removeFromSuperview()
        self.delegate?.didEndSpellEffect()
        
      }), selector: "main", userInfo: nil, repeats: false)
      
    }), selector: "main", userInfo: nil, repeats: false)
    
    /*
    var crops = [SKCropNode]()
    for var i=0; i<4; i++ {
      let negaSprite = SKSpriteNode(texture:------------- NegaImage.createFromImage(UIImage.imageFromView()))
      
      negaSprite.position = skView.center
    
      let maskShape = SKShapeNode(circleOfRadius: 1000.0)
      maskShape.fillColor = UIColor.blackColor()
      maskShape.strokeColor = UIColor.clearColor()
      let mask = SKSpriteNode(texture: skView.textureFromNode(maskShape))
      mask.runAction(SKAction.scaleTo(0.01, duration: 0))
      mask.position = skView.center
      mask.runAction(
        SKAction.sequence([
          SKAction.group([
            SKAction.moveTo(
              skView.center + CGPoint(angle: M_PI_2 * Double(i), multipledBy: 200.0),
              duration: 1.5),
            SKAction.scaleBy(50.0, duration: 1.5),
          ]),
          SKAction.runBlock({
            let negaSprite = SKSpriteNode(texture: SKTexture(image: NegaImage.createFromImage(UIImage.imageFromView())))
            negaSprite.position = self.skView.center
            let maskShape = SKShapeNode(circleOfRadius: 1000.0)
            maskShape.fillColor = UIColor.blackColor()
            maskShape.strokeColor = UIColor.clearColor()
            let mask = SKSpriteNode(texture: self.skView.textureFromNode(maskShape))
            mask.runAction(SKAction.scaleTo(0.01, duration: 0))
            mask.position = self.skView.center
            mask.runAction(
              SKAction.scaleTo(50.0, duration: 2.0)
            )
            
            let crop = SKCropNode()
            crop.addChild(negaSprite)
            crop.maskNode = mask
            crop.position = CGPointZero
            crop.zPosition = 2
            self.skScene.addChild(crop)
          })
        ])
      )

      let crop = SKCropNode()
      crop.addChild(negaSprite)
      crop.maskNode = mask
      crop.position = CGPointZero
      crop.zPosition = 1
      crops.append(crop)
      skScene.addChild(crop)
    }
    */
  }
}
