//
//  GlassEffect.swift
//  spritekit-cutin-test
//
//  Created by moti on 2015/08/25.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import SceneKit
import SpriteKit
import AufmSupport

private class PhysicsScene : SKScene {
  weak var delegate_: PhysicsSceneDelegate?
  override func didSimulatePhysics() {
    delegate_?.didSimulatePhysics()
  }
  
  override init(size: CGSize) {
    super.init(size: size)
  }

  required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}

private protocol PhysicsSceneDelegate: class {
  func didSimulatePhysics()
}

public class RadialGlassEffect : SpellEffectProtocol {
  
  public weak var delegate: SpellEffectDelegate?
  private let skView:       SKView
  private let skScene:      PhysicsScene
  private var cropNodes = [SKCropNode]()
  
  required public init(parentView: UIView) {
    skView = SKView(frame: UIScreen.mainScreen().bounds)
    skView.allowsTransparency = true
    skView.userInteractionEnabled = false
    parentView.addSubview(skView)
    
    skScene = PhysicsScene(size: skView.bounds.size)
    skScene.backgroundColor = SKColor.clearColor()
    skScene.delegate_ = self
  }
    
  public func run() {
    
    skView.presentScene(skScene)  // use protocol extension
    
    let viewImage = UIImage.imageFromView()
    var shapes = [UIBezierPath]()
    var pointsSet = [[CGPoint]]()
//    solve(pc: scnView.center, result: &shapes)
    solve(pc: skView.center, result: &shapes, pointsSet: &pointsSet)
    
    let field = SKFieldNode()
    field.strength = 1.0
    field.falloff = 1.0
    field.position = skView.center
    skScene.addChild(field)
    
    for i in 0..<shapes.count {
      var corner1 = CGPointMake(1e10, 1e10)
      var corner2 = CGPointMake(-1e10, -1e10)
      for point in pointsSet[i] {
        corner1 = CGPointMake(min(corner1.x, point.x), min(corner1.y, point.y))
        corner2 = CGPointMake(max(corner2.x, point.x), max(corner2.y, point.y))
      }
      let cropSize = CGSizeMake((corner2 - corner1).x, (corner2 - corner1).y)
      let cropCGImageRef = CGImageCreateWithImageInRect(viewImage.CGImage, CGRect(origin: corner1, size: cropSize))
      let cropImage = UIImage(CGImage: cropCGImageRef)!
      let viewNode = SKSpriteNode(texture: SKTexture(image: cropImage))
      let maskShape = SKShapeNode(path: shapes[i].CGPath)
      maskShape.fillColor = UIColor.blackColor()
      let mask = SKSpriteNode(texture: skView.textureFromNode(maskShape))
      let cropNode = SKCropNode()
      cropNode.addChild(viewNode)
      cropNode.maskNode = mask
      cropNode.position = (corner1 + corner2) / 2
      cropNode.zPosition = 2
      cropNode.physicsBody = SKPhysicsBody(edgeLoopFromPath: shapes[i].CGPath)
      skScene.addChild(cropNode)
      cropNodes.append(cropNode)
    }
    
    // 応急処置
    NSTimer.scheduledTimerWithTimeInterval(3.0, target: NSBlockOperation(block: {
      self.skScene.removeFromParent()
      self.skView.removeFromSuperview()
      self.delegate?.didEndSpellEffect()
    }), selector: "mai", userInfo: nil, repeats: false)
    
  }
}

// MARK: - Private -

private extension RadialGlassEffect {
  private func linearTransform(#point: CGPoint, angle: CGFloat) -> CGPoint {
    return CGPointMake(
      CGFloat(cos(Double(angle))) * point.x - CGFloat(sin(Double(angle))) * point.y,
      CGFloat(sin(Double(angle))) * point.x + CGFloat(cos(Double(angle))) * point.y
    )
  }
  
  private func outOfRange(#point: CGPoint) -> Bool {
    return !(0 <= point.x && point.x <= Screen.size.width && 0 <= point.y && point.y <= Screen.size.height)
  }
  
  private func rec(#pc: CGPoint, pa: CGPoint, pb: CGPoint, theta1: CGFloat, theta2: CGFloat, r: CGFloat, inout result: [UIBezierPath], inout pointsSet: [[CGPoint]]) {
    if outOfRange(point: pa) && outOfRange(point: pb) { return }
    let nr = r + 100.0 * 50.0 / r // 減衰
    let npa = pc + linearTransform(point: CGPointMake(0, nr), angle: theta1)
    let npb = pc + linearTransform(point: CGPointMake(0, nr), angle: theta2)
    let path = UIBezierPath()
    path.moveToPoint(pb)
    path.addLineToPoint(pa)
    path.addLineToPoint(npa)
    path.addLineToPoint(npb)
    path.closePath()
    result.append(path)
    pointsSet.append([pb,pa,npa,npb])
    rec(pc: pc, pa: npa, pb: npb, theta1: theta1, theta2: theta2, r: nr, result: &result, pointsSet: &pointsSet)
  }
  
  private func solve(#pc: CGPoint, inout result: [UIBezierPath], inout pointsSet: [[CGPoint]]) {
    var thetas = [CGFloat]()
    //    for i in 0..<32 { thetas.append(CGFloat(M_PI / 16))  // random }
    thetas = [
      CGFloat(M_PI/8 + M_PI/16),
      CGFloat(M_PI/8 - M_PI/16),
      CGFloat(M_PI/8 - M_PI/16),
      CGFloat(M_PI/8 + M_PI/16),
      CGFloat(M_PI/8 + M_PI/12),
      CGFloat(M_PI/8 - M_PI/12),
      CGFloat(M_PI/8 + M_PI/16),
      CGFloat(M_PI/8 - M_PI/16),
      CGFloat(M_PI/8 - M_PI/16),
      CGFloat(M_PI/8 + M_PI/16),
      CGFloat(M_PI/8 + M_PI/12),
      CGFloat(M_PI/8 - M_PI/12),
      CGFloat(M_PI/8 + M_PI/16),
      CGFloat(M_PI/8 - M_PI/16),
      CGFloat(M_PI/8 + M_PI/16),
      CGFloat(M_PI/8 - M_PI/16)
    ]
    
    let r: CGFloat = 50.0
    
    var sumTheta: CGFloat = 0
    
    for i in 0..<thetas.count {
      sumTheta += thetas[i]
      
      let theta1 = sumTheta
      let theta2 = sumTheta + thetas[(i+1) % thetas.count]
      
      let pa = pc + linearTransform(point: CGPointMake(0, r), angle: theta1)
      let pb = pc + linearTransform(point: CGPointMake(0, r), angle: theta2)
      
      let path = UIBezierPath()
      path.moveToPoint(pc)
      path.addLineToPoint(pa)
      path.addLineToPoint(pb)
      path.closePath()
      result.append(path)
      pointsSet.append([pc,pa,pb])
      rec(pc: pc, pa: pa, pb: pb, theta1: theta1, theta2: theta2, r: r, result: &result, pointsSet: &pointsSet)
    }
  }
}

extension RadialGlassEffect : PhysicsSceneDelegate {
  private func didSimulatePhysics() {
    var allRemoved = true
    for node in cropNodes {
      if node.position.y < 0 { node.removeFromParent() }
      else { allRemoved = false }
    }
    if allRemoved {
      delegate?.didEndSpellEffect()
    }
  }
}