//
//  SpellEffect.swift
//  spritekit-cutin-test
//
//  Created by moti on 2015/08/12.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import SpriteKit
import SceneKit

public protocol SpellEffectProtocol {
  weak var delegate: SpellEffectDelegate? { get }
  init(parentView: UIView)
  func run()
}

//protocol SpellEffect2DProtocol: SpellEffectProtocol {}
//protocol SpellEffect3DProtocol: SpellEffectProtocol {}

public protocol SpellEffectDelegate: class {
  func didEndSpellEffect()
}