//
//  DelaunayDiagram.m
//  spritekit-cutin-test
//
//  Created by moti on 2015/08/12.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

#import "DelaunayDiagram.h"
#import <Foundation/Foundation.h>
#import <algorithm>
#import <random>
#import <opencv2/opencv.hpp>
#import <opencv2/highgui/ios.h>
//#import "../../Aufm/Support/Misc.hpp"

@implementation DelaunayDiagram

+ (NSMutableArray*)triangleList
{
  int const width   = static_cast<int>([[UIScreen mainScreen] bounds].size.width);
  int const height  = static_cast<int>([[UIScreen mainScreen] bounds].size.height);
  
  static std::random_device device;
  static std::mt19937       generator(device());
  std::uniform_int_distribution<int> distribution_x(2, width-2);
  std::uniform_int_distribution<int> distribution_y(2, height-2);
  std::vector<cv::Point2f> points;
  for(int i = 0; i < 30; i++) {
    int x = distribution_x(generator);
    int y = distribution_y(generator);
    
    x = std::min(width - 2, x + int((width - 2 - x) * 0.1));
    y = std::max(0,         y - int(y * 0.1));
    
    points.push_back(cv::Point2f(x, y));
  }
  
  cv::Subdiv2D subdiv;
  subdiv.initDelaunay(cv::Rect(0, 0, width, height));
  subdiv.insert(points);
  
  std::vector<cv::Vec6f> triangles;
  subdiv.getTriangleList(triangles);
  
  NSMutableArray* ret = [NSMutableArray array];
  
  auto inRange = [&](int x, int y) -> bool {
    return 0<=x && x<=width && 0<=y && y<=height;
  };
  
  for(auto it = triangles.begin(); it != triangles.end(); it++) {
    cv::Vec6f &vec = *it;
    NSMutableArray* triangle = [NSMutableArray array];
    bool ng = false;
    for(int i=0; i<6; i+=2) {
      if(!inRange(vec[i], vec[i+1])) { ng = true; break; }
      [triangle addObject: [NSValue valueWithCGPoint: CGPointMake(vec[i], vec[i+1])]];
    }
    if(ng) { continue; }
    [ret addObject: triangle];
  }
  return ret;
}

@end