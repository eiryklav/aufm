
import SpriteKit

//////////////////////////////////////////////////////////////////////////////
// Predefined Sprite
//////////////////////////////////////////////////////////////////////////////

extension SKSpriteNode {
  
  class func createViewSprite() -> SKSpriteNode {
    let ret = SKSpriteNode(texture: SKTexture(image: UIImage.imageFromView()))
    ret.size = UIScreen.mainScreen().bounds.size
    ret.position = CGPointMake(ret.size.width / 2, ret.size.height / 2)
    return ret
  }
  
  func createShadowSprite() -> SKSpriteNode {
    let shadow = self.copy() as! SKSpriteNode
    shadow.size *= 1.05
    shadow.blendMode = .Alpha
    shadow.colorBlendFactor = 1
    shadow.color = SKColor.blackColor()
    shadow.alpha = 0.25  // make shadow partly transparent
    return shadow
  }
    
}

// CGPoint

extension CGPoint {
  static func vertexPointOfPolygon(#indexOfVertices: Int, numberOfVertices: Int, center: CGPoint, centerDistance: CGFloat) -> CGPoint {
    let angle = CGFloat(indexOfVertices) / CGFloat(numberOfVertices) * CGFloat(M_PI * 2.0)
    return center + CGPointMake(centerDistance * sin(angle), -centerDistance * cos(angle))
  }
  
  func linearTransform(angle: Double) -> CGPoint {
    return CGPointMake(
      x * CGFloat(cos(angle)) + y * CGFloat(-sin(angle)),
      x * CGFloat(sin(angle)) + y * CGFloat(cos(angle)))
  }
}