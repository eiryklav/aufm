//
//  NegaImage.m
//  spritekit-cutin-test
//
//  Created by moti on 2015/08/13.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

#import "NegaImage.h"
#import <Foundation/Foundation.h>
#import <opencv2/opencv.hpp>
#import <opencv2/highgui/ios.h>
#import "CVHelper.h"

@implementation NegaImage

+ (UIImage*) createFromImage: (UIImage*)image
{
  auto negaMat = ~[CVHelper matWithImage: image];
  return MatToUIImage(negaMat);
}

@end