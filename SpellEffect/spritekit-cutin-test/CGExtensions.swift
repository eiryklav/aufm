//
//  CGOperators.swift
//  spritekit-cutin-test
//
//  Created by moti on 2015/08/12.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

func * (lhs: CGSize, rhs: CGFloat) -> CGSize {
  return CGSizeMake(lhs.width * rhs, lhs.height * rhs)
}

func / (lhs: CGSize, rhs: CGFloat) -> CGSize {
  return CGSizeMake(lhs.width / rhs, lhs.height / rhs)
}

func *= (inout lhs: CGSize, rhs: CGFloat) {
  lhs = CGSizeMake(lhs.width * rhs, lhs.height * rhs)
}

func /= (inout lhs: CGSize, rhs: CGFloat) {
  lhs = CGSizeMake(lhs.width / rhs, lhs.height / rhs)
}

func + (lhs: CGPoint, rhs: CGPoint) -> CGPoint {
  return CGPointMake(lhs.x + rhs.x, lhs.y + rhs.y)
}

func - (lhs: CGPoint, rhs: CGPoint) -> CGPoint {
  return CGPointMake(lhs.x - rhs.x, lhs.y - rhs.y)
}

func * (lhs: CGPoint, rhs: CGFloat) -> CGPoint {
  return CGPointMake(lhs.x * rhs, lhs.y * rhs)
}

func / (lhs: CGPoint, rhs: CGFloat) -> CGPoint {
  return CGPointMake(lhs.x / rhs, lhs.y / rhs)
}

func += (inout lhs: CGPoint, rhs: CGPoint) {
  lhs = lhs + rhs
}

func -= (inout lhs: CGPoint, rhs: CGPoint) {
  lhs = lhs - rhs
}



func SW_MultipledBy(val: CGFloat) -> CGFloat {
  return UIScreen.mainScreen().bounds.width * val
}

func SH_MultipledBy(val: CGFloat) -> CGFloat {
  return UIScreen.mainScreen().bounds.height * val
}

extension CGPoint {
  
  init(angle: Double, multipledBy: Double) {
    x = CGFloat(-1.0 * sin(angle) * multipledBy)
    y = CGFloat(+1.0 * cos(angle) * multipledBy)
  }
  
  func toCGVector() -> CGVector {
    return CGVector(dx: x, dy: y)
  }
  
  func length() -> CGFloat {
    return sqrt(x * x + y * y)
  }
  
  func normalization() -> CGPoint {
    let l = length()
    return CGPointMake(x / l, y / l)
  }
}

extension CGVector {
  init(angle: Double, multipledBy: Double) {
    dx = CGFloat(-1.0 * sin(angle) * multipledBy)
    dy = CGFloat(+1.0 * cos(angle) * multipledBy)
  }
  
  func normalization() -> CGVector {
    let dl = sqrt(dx * dx + dy * dy)
    return CGVectorMake(dx / dl, dy / dl)
  }
  
}