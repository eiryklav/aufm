#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <set>
#include <iomanip>
#include <unordered_set>
#include <unordered_map>
#include <fstream>
#include <assert.h>
#include <memory>

#include <graphviz/gvc.h>
#include <graphviz/cgraph.h>

#include "../Lib/picojson.h"

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

namespace graph {

  int constexpr MaxTarget = 26;

  typedef std::vector<std::pair<int, int>> EnemyGroup;  // 敵cがn体のvector = EnemyGroup
  typedef std::vector<std::pair<EnemyGroup, double>> ECInfo;

  struct Edge {
    int to;
    int reqEC;
    ECInfo info;
    std::vector<double> expSameEnemyNum;  // 1EC辺りidxの敵と遭遇する体数の期待値
    Edge(int to, int ec, ECInfo info, std::vector<double> const& expected): to(to), reqEC(ec), info(info), expSameEnemyNum(expected) {}
  };

  typedef std::vector<Edge> Edges;
  typedef std::vector<Edges> Graph;

  int V, E, source;
  Graph G;

    auto getJSON() {
    std::cout << "グラフの接続情報(WorldMapGraph.json or D-x-y.json): ";
    std::string fname; std::cin >> fname;
    std::ifstream ifs(fname.c_str());
    std::string json, s;
    while(ifs >> s) { json += s; }
    return json;
  }

  std::unordered_map<std::string, int> vmap;
  std::unordered_map<int, int> enemap;

  auto makeGraph(std::string const& json) {

    using namespace picojson;
    value v; assert(parse(v, json).empty());

    // source = "StartPosition"
    auto sourceStr = v.get<object>()["StartPosition"].get<std::string>();
    assert(vmap.find(sourceStr) != vmap.end());
    source = vmap[sourceStr];

    auto& arr = v.get<object>()["Graph"].get<picojson::array>();
    G.resize(1000);
    V = 0;
    for(auto& vv: arr) {
      auto e = vv.get<object>();
      auto ss = e["From"].get<std::string>(); if(vmap.find(ss) == vmap.end()) vmap[ss] = V++;
      auto tt = e["To"].get<std::string>();   if(vmap.find(tt) == vmap.end()) vmap[tt] = V++;
      auto reqEC = static_cast<int>(e["RequiredEC"].get<double>());

      int Enemies = 0;
      auto ecenemies = e["EncountEnemies"].get<picojson::array>();
      ECInfo info;
      std::vector<double> expected(MaxTarget);
      for(auto& vv: ecenemies) {
        auto e = vv.get<object>();
        auto prob = e["Probability"].get<double>() / 100.0;
        auto eneg = e["EnemyGroup"].get<picojson::array>();
        std::vector<int> num(MaxTarget);
        for(auto& vv: eneg) {
          auto eid = static_cast<int>(vv.get<double>());  // 各敵種別
          if(enemap.find(eid) == enemap.end()) enemap[eid] = Enemies ++;
          num[enemap[eid]] ++;
        }
        EnemyGroup group;
        rep(i, MaxTarget) if(num[i]) {
          expected[i] += num[i] * prob;
          group.emplace_back(i, num[i]);
        }
        info.emplace_back(group, prob);
      }
      G[vmap[ss]].emplace_back(vmap[tt], reqEC, info, expected);
    }

    G.resize(V);
    /*
    // 標準入力
    G.resize(V);
    rep(_, E) {
      int a, b, ec; cin >> a >> b >> ec;
      int n; cin >> n;
      ECInfo info;
      std::vector<double> expected(MaxTarget);
      rep(_, n) { // nグループ
        int m; double prob;
        std::cin >> m >> prob; prob /= 100;
        EnemyGroup group;
        rep(_, m) { // m種
          int c, num; std::cin >> c >> num;
          group.emplace_back(c, num);
          expected[c] += num * prob;
        }
        info.emplace_back(group, prob);
      }
      G[a].emplace_back(b, ec, info, expected);
    }
    */

  }
}

namespace expectedEC {

  int target[graph::MaxTarget];
  int targetBits;

  struct elem {
    int pos;
    int use; // bit
    double expBattleCnt;
    elem(int pos, int use, double p): pos(p), use(use), expBattleCnt(p) {}
  };

  auto calcExpectedEC(graph::Edge const& edge, int tarEnemies) {
    double maxEC = edge.reqEC;
    rep(i, graph::MaxTarget) {
      if(tarEnemies >> i & 1) {
        maxEC = std::max(maxEC, 1.0 * target[i] / edge.expSameEnemyNum[i]);
      }
    }
    return maxEC;
  }

  // calc expected number list
  auto bfs() {

    std::vector<double> ret(graph::V);

    std::queue<elem> q;
    q.emplace(graph::source, targetBits, 0.0);
    while(!q.empty()) {
      auto const p = q.front(); q.pop();
      auto const& curr = p.pos;
      auto const& use = p.use;
      auto const& expected = p.expBattleCnt;

      int typeMax = -1, reqEC = -1;

      std::vector<graph::Edge> nextEdge;
      std::vector<int> nextUse;
      rep(i, graph::G[curr].size()) {
        int contain = 0;
        // 討伐目標と重複する敵の種類数を数える。ただし、一度処理した敵は含めない
        for(auto const& prob_pair: graph::G[curr][i].info)
          for(auto const& c: prob_pair.first)
            if(use >> c.first & 1) contain &= 1 << c.first;

        int setSize = __builtin_popcount(contain);
        if(typeMax < setSize || (typeMax == setSize && reqEC > graph::G[curr][i].reqEC)) {
          typeMax = setSize;
          reqEC = graph::G[curr][i].reqEC;
          nextEdge.clear();
          nextEdge.push_back(graph::G[curr][i]);
          nextUse.clear();
          nextUse.push_back(use ^ contain);
        }
        else if(typeMax == setSize && reqEC == graph::G[curr][i].reqEC) {
          typeMax = setSize;
          reqEC = graph::G[curr][i].reqEC;
          nextEdge.push_back(graph::G[curr][i]);
          nextUse.push_back(use ^ contain);
        }
      }

      rep(i, nextEdge.size()) {
        // 複数選択肢がある場合、その中空「ランダムで選択する」とした場合（決め打ちではない！）
        int contain = use ^ nextUse[i];
        q.emplace(nextEdge[i].to, nextUse[i], (expected + calcExpectedEC(nextEdge[i], contain)) / nextEdge.size());
      }

    }

    return ret;
  }

  auto inputTarget() {
    std::cout << "敵の数最大 = " << graph::MaxTarget << std::endl;
    std::cout << "討伐目標の敵の数: "; int N; std::cin >> N;
    std::cout << "討伐目標: [敵種別(0〜" << graph::MaxTarget - 1 << ") 数]\n";
    rep(i, N) {
      int c, num;
      for(;std::cin >> c >> num;) {
        std::cout << i+1 << "種目: ";
        if(c < 0 || c >= graph::MaxTarget) { std::cout << "敵種別は[0, " << graph::MaxTarget - 1 << "]の範囲で指定してください\n"; }
        else { break; }
      }
      target[c] = num;
      targetBits |= 1 << c;
    }
  }

}

namespace gv {
  namespace detail {

    char* to_char_ptr(std::string const& rval) { return const_cast<char*>(rval.c_str()); }

    typedef std::unique_ptr<GVC_t, decltype(&gvFreeContext)> gvc_ptr;
    inline gvc_ptr create_context() {
      return { gvContext(), gvFreeContext };
    }

    typedef std::unique_ptr<Agraph_t, decltype(&agclose)> agraph_ptr;
    inline agraph_ptr create_agraph(std::string&& name) {
      return { agopen(const_cast<char*>(name.c_str()), Agdirected, NULL), agclose };
    }

    gvc_ptr     ctx     = create_context();
    agraph_ptr  agraph  = create_agraph("graph");

    typedef std::shared_ptr<Agnode_t> agnode_ptr;
    std::unordered_map<std::string, agnode_ptr> nodemap;

    int edge_num = 0;
  }

  auto init() {
    std::unique_ptr<Agsym_t> sym(
      agnodeattr(detail::agraph.get(), detail::to_char_ptr("shape"), detail::to_char_ptr("ellipse"))
    );
  }

  auto make_node(std::string const& s) {
    auto nnode = detail::agnode_ptr(agnode(detail::agraph.get(), detail::to_char_ptr(s), 1));
    detail::nodemap.emplace(s, std::move(nnode));
  }

  auto make_edge(std::string const& s1, std::string const& s2) {
    assert(detail::nodemap.find(s1) != detail::nodemap.end());
    assert(detail::nodemap.find(s2) != detail::nodemap.end());
    std::unique_ptr<Agedge_t> e(
      agedge(detail::agraph.get(), detail::nodemap[s1].get(), detail::nodemap[s2].get(),
        detail::to_char_ptr("E"+std::to_string(detail::edge_num++)), 1)
    );
  }

  auto layout() {
    gvLayout(detail::ctx.get(), detail::agraph.get(), "dot");
    gvRenderFilename(detail::ctx.get(), detail::agraph.get(), detail::to_char_ptr("png"), detail::to_char_ptr("graph.png"));
    gvFreeLayout(detail::ctx.get(), detail::agraph.get());
  }

}


int main() {

  graph::makeGraph(graph::getJSON());

  expectedEC::inputTarget();

  auto ret = expectedEC::bfs();
  std::cout << std::setprecision(8);
  rep(i, ret.size()) {
    std::cout << "node " << i << ": " << ret[i] << std::endl;
  }

  return 0;
}