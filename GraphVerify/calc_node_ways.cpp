#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
#include <complex>
#include <queue>
#include <set>
#include <iomanip>
#include <unordered_set>
#include <unordered_map>
#include <fstream>
#include <assert.h>
#include <memory>
#include <cstdlib>
#include <sstream>

#include <graphviz/gvc.h>

#include "../Lib/picojson.h"

#define REP(i,a,b) for(int i=a;i<(int)b;i++)
#define rep(i,n) REP(i,0,n)

typedef long long ll;

namespace gv {
  namespace detail {
    std::ofstream ofs;
    std::string name;
  }

  auto init(std::string const& name) {
    detail::name = name;
    detail::ofs = std::ofstream(name + ".dot");
    detail::ofs << "digraph {\n";
  }

  auto set_source(std::string const& source) {
    detail::ofs << "node [shape = doublecircle]; " << source << '\n'
                << "node [shape = circle];\n";
  }

  auto add_edge(std::string const& s, std::string const& t, std::string const& label = "") {
    detail::ofs << s << " -> " << t << "[label = \"" << label << "\"];\n";
  }

  auto set_node_label(std::string const& n, std::string const& label) {
    detail::ofs << n << " [label = \"" << label << "\"];\n";
  }

  auto close() {
    detail::ofs << "}\n";
    detail::ofs.close();

    std::stringstream ss; ss << "dot -Kdot -Tpng " << detail::name << ".dot -o " << detail::name << ".png";
    auto ret = std::system(ss.str().c_str());
    if(!WIFEXITED(ret)) {
      assert(false && "UnexpectedError");
    }
    std::cout << "Created \"" << detail::name << ".png\"\n";
  }
}

namespace misc {
  template<class T> std::string print(std::vector<T> const& v) {
    std::string ret = "[\n";
    rep(i, v.size()) {
      if(i) ret += ",\n"; ret += "  " + v[i].print();
    }
    ret += "\n]";
    return ret;
  }
}

namespace graph {

  int constexpr MaxTarget = 26;

  std::unordered_map<std::string, int> vmap;
  std::unordered_map<int, std::string> vmapString;

  struct EnemyInfo {
    int enemy, count;
    EnemyInfo(){}
    EnemyInfo(int enemy, int count): enemy(enemy), count(count){}

    std::string print() const {
      return "id:" + std::to_string(enemy+1) + ", num:" + std::to_string(count);
    }
  };

  typedef std::vector<EnemyInfo> EnemyGroup;  // 敵cがn体のvector = EnemyGroup

  struct EneGroupFreq {
    EnemyGroup enemyGroup; double prob;
    EneGroupFreq() : prob(0.0) {}
    EneGroupFreq(EnemyGroup const& group, double prob): enemyGroup(group), prob(prob) {}

    std::string print() const {
      return misc::print<EnemyInfo>(enemyGroup) + std::string(", ") + "prob: " + std::to_string(prob);
    }
  };

  typedef std::vector<EneGroupFreq> EneGroupFreqList;

  struct Edge {
    int to;
    int reqEC;
    EneGroupFreqList eneGroupFreqList;
    std::vector<double> expSameEnemyNum;  // 1EC辺りidxの敵と遭遇する体数の期待値
    Edge(int to, int ec, EneGroupFreqList egfreqlist, std::vector<double> const& expected)
    : to(to), reqEC(ec), eneGroupFreqList(egfreqlist), expSameEnemyNum(expected) {}
  };

  typedef std::vector<Edge> Edges;
  typedef std::vector<Edges> Graph;

  int MaxVertices, source;
  Graph G;

  auto getJSON() {
    std::cerr << "Input json file name. (usually WorldMapGraph.json or D-x-y.json): ";
    std::string fname; std::cin >> fname;
    std::ifstream ifs(fname.c_str());
    std::string json, s;
    while(ifs >> s) { json += s; }
    return json;
  }

  auto makeGraph(std::string const& json) {

    gv::init("graph-num-of-case");

    picojson::value v;
    assert(parse(v, json).empty());
    auto& o = v.get<picojson::object>();

    auto& sourceStr = o["StartPosition"].get<std::string>();
    source = vmap[sourceStr], vmapString[source] = sourceStr;
    gv::set_source(sourceStr);

    MaxVertices = 1;  // only source
    G.resize(1000);

    auto& arr = o["Graph"].get<picojson::array>();
    for(auto& v: arr) {
      auto& o = v.get<picojson::object>();

      auto& ss = o["From"].get<std::string>();
      auto& tt = o["To"].get<std::string>();
      if(vmap.find(ss) == vmap.end()) vmap[ss] = MaxVertices++, vmapString[vmap[ss]] = ss;
      if(vmap.find(tt) == vmap.end()) vmap[tt] = MaxVertices++, vmapString[vmap[tt]] = tt;

      auto reqEC  = static_cast<int>(o["RequiredEC"].get<double>());
      auto isBidi = o["IsBidirectional"].get<bool>();
      EneGroupFreqList freqList;
      std::vector<double> expected(MaxTarget);

      auto& arr = o["EncountEnemies"].get<picojson::array>();
      for(auto& v: arr) {
        auto& o     = v.get<picojson::object>();
        auto  prob  = o["Probability"].get<double>() / 100.0; // 小数点指定可能
        auto& arr   = o["EnemyGroup"].get<picojson::array>();

        std::vector<int> num(MaxTarget);

        for(auto& v: arr) {
          auto eid = static_cast<int>(v.get<double>());  // 各敵種別
          num[eid-1] ++;
        }

        EnemyGroup group;
        rep(i, MaxTarget) if(num[i]) {
          expected[i] += num[i] * prob;
          group.emplace_back(i, num[i]);
        }
        freqList.emplace_back(group, prob);
      }

      G[vmap[ss]].emplace_back(vmap[tt], reqEC, freqList, expected);
      gv::add_edge(ss, tt, "EC:"+std::to_string(reqEC) + ",\n" + misc::print(freqList));
      if(isBidi) {
        G[vmap[tt]].emplace_back(vmap[ss], reqEC, freqList, expected);
        gv::add_edge(tt, ss, "EC:"+std::to_string(reqEC) + ",\n" + misc::print(freqList));
      }

    }

    G.resize(MaxVertices);
  }
}

auto showBits(int x) {
  std::vector<int> bits;
  rep(_, graph::MaxTarget) {
    bits.push_back(x & 1);
    x >>= 1;
  }
  std::reverse(bits.begin(), bits.end());
  rep(i, bits.size()) std::cerr << bits[i];
  std::cerr << "\n";
}

namespace expectedEC {

  int target[graph::MaxTarget];
  int targetBits;

  struct elem {
    int pos = 0;
    int use = 0; // bit
    double expBattleCnt = 0;
    elem(int pos, int use, double exp): pos(pos), use(use), expBattleCnt(exp) {}
  };
  /*
  auto calcExpectedEC(graph::Edge const& edge, int tarEnemies) {
    double maxEC = edge.reqEC;
    rep(i, graph::MaxTarget) {
      if(tarEnemies >> i & 1) {
        maxEC = std::max(maxEC, 1.0 * target[i] / edge.expSameEnemyNum[i]);
      }
    }
    return maxEC;
  }
  */

  // calc expected number list
  auto bfs() {

    std::vector<double> ret(graph::MaxVertices, -1.0);
    ret[graph::source] = 1;
    std::deque<elem> q;
    q.emplace_back(graph::source, targetBits, 1);
    std::unordered_map<int, int> viscnt;
    while(!q.empty()) {
      auto const  p = q.front(); q.pop_front();
      auto const& curr      = p.pos;
      auto const& use       = p.use;
      auto const& expected  = p.expBattleCnt;

      if(viscnt[curr] >= 1000) { continue; }
      viscnt[curr] ++;

//      std::cerr << "tarBits: "; showBits(use);

      // 最適の辺情報を保持
      int typeMax = -1, reqEC = -1;
      std::vector<graph::Edge> nextEdge;
      std::vector<int> nextUse;

//      std::cerr << "curr: " << curr << ", G[" << curr << "].size(): " << graph::G[curr].size() << std::endl;

      rep(i, graph::G[curr].size()) {
        int contain = 0;
        for(auto const& pair: graph::G[curr][i].eneGroupFreqList) {
          for(auto const& c: pair.enemyGroup) {
//            std::cerr << "c.enemy: " << c.enemy << " ";
            if(use >> c.enemy & 1) contain |= 1 << c.enemy;
          }
//          std::cerr << "\n";
        }

//        std::cerr << "contain: "; showBits(contain);

        int containSize = __builtin_popcount(contain);
        if((typeMax < containSize) || (typeMax == containSize && reqEC > graph::G[curr][i].reqEC)) {
          typeMax = containSize;
          reqEC = graph::G[curr][i].reqEC;
          nextEdge.clear();
          nextEdge.push_back(graph::G[curr][i]);
          nextUse.clear();
          nextUse.push_back(use ^ contain);
        }
        else if(typeMax == containSize && reqEC == graph::G[curr][i].reqEC) {
          typeMax = containSize;
          reqEC = graph::G[curr][i].reqEC;
          nextEdge.push_back(graph::G[curr][i]);
          nextUse.push_back(use ^ contain);
        }
      }

      rep(i, nextEdge.size()) {
        // 複数選択肢がある場合、その中から「等確率で選択する」とした場合（決め打ちではない！）
        int contain = use ^ nextUse[i];
        auto nexp = expected; //(expected + calcExpectedEC(nextEdge[i], contain)) / nextEdge.size();
        if(ret[nextEdge[i].to] < 0) ret[nextEdge[i].to] = 0;
        ret[nextEdge[i].to] += nexp;
        q.emplace_back(nextEdge[i].to, nextUse[i], nexp);
      }

    }

    return ret;
  }

  auto inputTarget() {
    std::cerr << "入力グラフ内の敵の種別数の最大値 = " << graph::MaxTarget << "\n\n";
    std::cerr << "討伐目標にある敵の種別数: "; int N; std::cin >> N;
    std::cerr << "討伐目標: [敵種別(1〜" << graph::MaxTarget << ") 数]\n";

    targetBits = 0;
    rep(i, N) {
      int c, num;
      for(;;) { // infinite loop for input validation
        std::cerr << i+1 << "種目: "; std::cin >> c >> num; c--;  // c = [0, MaxTarget-1]
        if(c < 0 || c >= graph::MaxTarget) { std::cerr << "敵種別は[1, " << graph::MaxTarget << "]の範囲で指定してください\n"; }
        else { break; }
      }
      target[c] = num;
      targetBits |= 1 << c;
//      std::cerr << "enemy: " << c+1 << ", num: " << num << std::endl;
//      std::cerr << "targetBits: "; showBits(targetBits);
    }
    std::cerr << "\n";
  }

}

int main() {

  graph::makeGraph(graph::getJSON());

  expectedEC::inputTarget();

  auto ret = expectedEC::bfs();
  rep(i, ret.size()) {
    std::cerr << "node " << i << ": " << std::setprecision(8) << ret[i] << std::endl;
    gv::set_node_label(graph::vmapString[i], graph::vmapString[i] + ": " + std::to_string(static_cast<int>(ret[i])) + " ways");
  }
  gv::close();

  return 0;
}