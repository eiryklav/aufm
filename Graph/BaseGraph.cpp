#include "BaseGraph.hpp"

namespace BaseGraph
{
  auto parseItem(picojson::object& o)
    -> std::vector<ECItemsPair>
  {
    std::vector<ECItemsPair> ret;

    auto& ec_item_array = o["Windfalls"].get<picojson::array>();

    for(auto& item_val: ec_item_array) {
      auto& item_o = item_val.get<picojson::object>();
      int ec = item_o["EC"].get<double>();
      auto const& item_array = item_o["ItemList"].get<picojson::array>();
      
      ItemList itemList;
      for(auto const& item: item_array) {
        itemList.emplace_back(static_cast<int>(item.get<double>()));
      }
      ret.emplace_back(ec, itemList);
    }
    return ret;
  }

  auto parseEnemyGroup(picojson::object& o)
    -> std::vector<EnemyProbGroupPair>
  {
    std::vector<EnemyProbGroupPair> ret;
    
    auto& enemy_group_arr = o["EncountEnemies"].get<picojson::array>();
    Misc::ProbRangeTester tester;
    for(auto& val: enemy_group_arr) {
      auto& enemy_o = val.get<picojson::object>();
      int prob = enemy_o["Probability"].get<double>();
      auto const& group_arr = enemy_o["EnemyGroup"].get<picojson::array>();
      tester.add(prob);
      EnemyGroup enemyGroup;
      for(auto const& enemy_id: group_arr) {
        enemyGroup.emplace_back(static_cast<int>(enemy_id.get<double>()));
      }
      ret.emplace_back(prob, enemyGroup);
    }
    std::sort(ret.begin(), ret.end());
    tester.validateSum();
    return ret;
  }

}