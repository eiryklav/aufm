#pragma once

#include <string>
#include <sstream>
#include <memory>
#include <boost/functional/hash.hpp>

#include "BaseGraph.hpp"
#include "../Printer.hpp"
#include "../Lib/picojson.h"
#include "../Support/Misc.hpp"

namespace DungeonMap
{
  class ForkPoint : public Printer::Printable
  {
  private:

    int forkPointID_;

  public:
    inline auto getID() const { return forkPointID_; }

  public:
    
    inline ForkPoint(int r=-1) : forkPointID_(r) {}
    inline ForkPoint(std::string const& s) { forkPointID_ = std::stoi(s); }

    inline bool operator == (ForkPoint rhs) const { return forkPointID_ == rhs.getID();  }
    inline auto print() const -> std::string { std::stringstream ss; ss << forkPointID_; return ss.str(); }

  public:
    friend inline auto hash_value(ForkPoint const& forkPoint)
    {
      std::size_t seed = 0;
      boost::hash_combine(seed, forkPoint.getID());
      return seed;
    }
  };

  class Graph
  {
  private:
    
    std::unordered_map<ForkPoint, BaseGraph::Edges<ForkPoint>, boost::hash<ForkPoint>> graph;
    
  public:

    inline Graph(picojson::array& graph_o) { BaseGraph::makeGraph<ForkPoint>(graph_o, graph); }
    inline auto getLinkedEdges(ForkPoint const& id) -> decltype(auto) { return (graph[id]); }

  };
}