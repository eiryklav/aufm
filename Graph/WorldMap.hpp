#pragma once

#include <string>
#include <sstream>
#include <memory>
#include <boost/algorithm/string.hpp>
#include <boost/functional/hash.hpp>

#include "BaseGraph.hpp"
#include "../Printer.hpp"
#include "../Lib/picojson.h"
#include "../Support/Misc.hpp"

namespace WorldMap
{
	using VerticalID = int;
	using HorizontalID = int;

	enum class AreaType { City, Dungeon };

	class AreaID : public Printer::Printable
	{
	private:
		VerticalID vid_;
		HorizontalID hid_;
		AreaType type_;

	public:

		inline auto getVID() 	const { return vid_; 	}
		inline auto getHID() 	const { return hid_;	}
		inline auto getType() const { return type_; }

	public:
		
		inline AreaID() : vid_(-99999), hid_(-99999), type_(AreaType::City) {}
		inline AreaID(VerticalID vid, HorizontalID hid, AreaType type) : vid_(vid), hid_(hid), type_(type) {}
		AreaID(std::string const& areaIDString);
		
		inline bool operator == 		(AreaID const& rhs) const { return vid_ == rhs.getVID() && hid_ == rhs.getHID(); 	}
		inline bool isEqualVertical	(AreaID const& rhs) const { return vid_ == rhs.getVID(); }

		auto print() const -> std::string;

	public:
    friend inline auto hash_value(AreaID const& areaID)
  	{
  		std::size_t seed = 0;
  		boost::hash_combine(seed, areaID.getVID());
  		boost::hash_combine(seed, areaID.getHID());
  		boost::hash_combine(seed, areaID.getType());
  		return seed;
   	}
	};

	class Graph
	{
	private:
		
		std::unordered_map<AreaID, BaseGraph::Edges<AreaID>, boost::hash<AreaID>> graph;
		
	public:

    inline Graph(picojson::array& graph_o) { BaseGraph::makeGraph<AreaID>(graph_o, graph); }
		inline auto getLinkedEdges(AreaID const& id) -> decltype(auto) { return (graph[id]); }

	};
}