#pragma once

#include <iostream>
#include <set>
#include <memory>
#include "BaseGraph.hpp"
#include "WorldMap.hpp"
#include "DungeonMap.hpp"
#include <boost/optional.hpp>
#include "../Lib/picojson.h"
#include "../Support/Singleton.hpp"
//#include <boost/type_erasure/any_cast.hpp>
//#include <boost/type_erasure/member.hpp>
//#include <boost/preprocessor.hpp>

/*
  Name: PathSolver
  Desc: グラフ保持と経路変更。エンカウント処理とエンカウントチャンスの増加は請け負わない。
 */
namespace PathSolver
{
  enum class PathOf { World, Dungeon, AtCity };
  
  namespace detail { extern PathOf currentOnPath_; }
  
  inline auto onPath(PathOf path)   { detail::currentOnPath_ = path; }
  inline auto isOnPath(PathOf path) { return detail::currentOnPath_ == path; }
  inline auto getCurrentOnPath()    { return detail::currentOnPath_; }
  
  #define WorldPath   PathSolver::World::Instance()
  #define DungeonPath PathSolver::Dungeon::Instance()
  
#define PATH_SOLVER_SWITCH(block) \
  switch (PathSolver::getCurrentOnPath()) block
  
#define CASE_PATH(which, block)  \
  case PathSolver::PathOf::which: block break;
  
#define PATH_SOLVER_ASSERTION  {AUFM_ASSERT(false, "PATH_SOLVER_SWITCH : Undefined path");}

  /*
    PathSolver::World
    PathSolver::Dungeon
    
    tarEdge_  が　設定されていれば、solveCurrentRoute()　でノード到達判定
              　　　そうでなければ、findNextRoute()　で次のエッジを設定
    ...Route という名前は ...Edge に書き換えるのが適切
   */
  
  class World : public Singleton<World>
  {
  private:
    std::shared_ptr<WorldMap::AreaID>                   position_         = nullptr;
    std::shared_ptr<WorldMap::Graph>                    worldMapGraph_    = nullptr;
    boost::optional<BaseGraph::Edge<WorldMap::AreaID>>  tarEdge_          = boost::none;
    int                                                 totalEC_          = 0;
  
  public:
    auto init(std::shared_ptr<WorldMap::AreaID> const& startPosition,
              std::shared_ptr<WorldMap::Graph>  const& graph) -> void;
    
    auto getPosition() const -> decltype(auto)                      { return (position_); }
    auto setPosition(std::shared_ptr<WorldMap::AreaID> const& pos)  { position_ = pos;    }
    auto getCurrentEdge() const -> decltype(auto)                   { return (tarEdge_);  }
    auto isExistCurrentRoute() const  -> bool;
    auto findNextRoute()              -> bool;
    auto solveCurrentRoute()          -> boost::optional<WorldMap::AreaID>;
    auto incrementTotalEC()           -> void                       { totalEC_++;         }
    auto getTotalEC() const           -> int                        { return totalEC_;    }
  };
  
  class Dungeon : public Singleton<Dungeon>
  {
  private:
    std::shared_ptr<WorldMap::AreaID>                             dungeonID_          = nullptr;
    std::shared_ptr<DungeonMap::ForkPoint>                        position_           = nullptr;
    std::shared_ptr<DungeonMap::Graph>                            dungeonMapGraph_    = nullptr;
    boost::optional<BaseGraph::Edge<DungeonMap::ForkPoint>>       tarEdge_            = boost::none;
    int                                                           totalEC_            = 0;
    
  public:
    
    auto init(std::shared_ptr<WorldMap::AreaID>       const& dungeonID,
              std::shared_ptr<DungeonMap::ForkPoint>  const& startPosition,
              std::shared_ptr<DungeonMap::Graph>      const& graph) -> void;
    
    auto getDungeonID() const -> decltype(auto)                         { return (dungeonID_);  }
    auto getPosition()  const -> decltype(auto)                         { return (position_);   }
    auto setPosition(std::shared_ptr<DungeonMap::ForkPoint> const& pos) { position_ = pos;      }
    auto getCurrentEdge() const -> decltype(auto)                       { return (tarEdge_);    }
    auto isExistCurrentRoute() const  -> bool;
    auto findNextRoute()              -> bool;
    auto solveCurrentRoute()          -> boost::optional<DungeonMap::ForkPoint>;
    auto incrementTotalEC()           -> void                           { totalEC_++;           }
    auto getTotalEC() const           -> int                            { return totalEC_;      }
  };
  
};