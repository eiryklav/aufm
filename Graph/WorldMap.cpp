#include "WorldMap.hpp"

namespace WorldMap
{
	AreaID::AreaID(std::string const& areaIDString)
	{
		std::vector<std::string> vecAreaID;
		boost::algorithm::split(vecAreaID, areaIDString, std::bind2nd(std::equal_to<char>(), '-'));
		AUFM_ASSERT(vecAreaID.size() == 3, "Area name is descripted such as 'C-1-1'");		// Ex) C-1-1

		AUFM_ASSERT(vecAreaID[0].size() == 1, "Descripted C or D");
		type_ = [&]{
			switch(vecAreaID[0][0]){
				case 'C': return AreaType::City;
				case 'D': return AreaType::Dungeon;
				default: AUFM_ASSERT(false, "Descripted C or D");
			}
		}();

		vid_ = std::stoi(vecAreaID[1]);
		hid_ = std::stoi(vecAreaID[2]);
	}

	auto AreaID::print() const
		-> std::string
	{
		std::stringstream ss;
		if(type_ == AreaType::City) { ss << "C"; }
		else if(type_ == AreaType::Dungeon) { ss << "D"; }
		else { AUFM_ASSERT(false, "Descripted C or D"); }
		ss << "-" << vid_ << "-" << hid_;
		return ss.str();
	}
}