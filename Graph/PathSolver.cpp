//
//  PathSolver.cpp
//  Aufm
//
//  Created by moti on 2015/07/24.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

#include "PathSolver.hpp"
#include "../Game/Game.hpp"
#include "../Game/GameStrategy.hpp"
#include "../Support/Misc.hpp"
#include "../Lib/picojson.h"
#include "../Graph/BaseGraph.hpp"
#include "../Graph/WorldMap.hpp"
#include "../Graph/DungeonMap.hpp"

#include <iostream>
#include <set>
#include <memory>
#include <boost/optional.hpp>

namespace PathSolver { namespace detail
{
  PathOf currentOnPath_;
  
  template<class GraphT, class PositionT>
  auto findNextEdge(std::shared_ptr<GraphT> const& graph,
                    std::shared_ptr<PositionT> const& currPos,
                    boost::optional<BaseGraph::Edge<PositionT>>& tarEdge,
                    int& totalEC)
    -> bool
  {
    auto const targetingEnemies = GameMgr->getTargetForDefeat()->getRemainEnemiesList();
    auto edges = graph->getLinkedEdges(*currPos); // !!! COPY !!!
    
    // ゼロ接続
    if(edges.size() == 1 && edges[0].getRequiredEC() == 0) {
      tarEdge = boost::make_optional(edges[0]);
      return true;
    }
    else {
      for(auto const& e: edges) {
        AUFM_ASSERT(e.getRequiredEC() > 0, "RequiredECがおかしいです。");
      }
    }
    
    if(edges.empty()) {
      return false; // 行き止まり
    }
    
    std::vector<BaseGraph::Edge<PositionT>> candidateEdges;
    
    // 討伐グラフの残りの敵の種数が多いもの, エッジのECが少ないもの　で貪欲
    int maxTypeOfEnemy = 0, encountChance = 1<<29;
    
    for(auto const& e: edges) {
      int cnt = 0;
      for(int tar: targetingEnemies) {
        if(e.isExistEnemyID(tar)) {
          cnt++;
        }
      }
      
      // 種類数が同列ならECの小さい方を選択
      if(maxTypeOfEnemy < cnt) {
        maxTypeOfEnemy = cnt;
        encountChance = e.getRequiredEC();
        candidateEdges.clear();
        candidateEdges.push_back(e);
        PRINT("PathSolver::detail: selected edge: maxTypeOfEnemy < cnt")
      }
      else if(maxTypeOfEnemy == cnt && encountChance > e.getRequiredEC()) {
        encountChance = e.getRequiredEC();
        candidateEdges.clear();
        candidateEdges.push_back(e);
        PRINT("PathSolver::detail: selected edge: maxTypeOfEnemy == cnt && encountChance > e.getRequiredEC()")
      }
      else if(maxTypeOfEnemy == cnt && encountChance == e.getRequiredEC()) {
        candidateEdges.push_back(e);
      }
    }
    
    
    AUFM_ASSERT(!candidateEdges.empty(), "エッジ選択のアルゴリズムが間違っています。");
    
    // 遷移可能な辺から等確率で一つ選ぶ
    tarEdge = boost::make_optional(candidateEdges[Misc::randomRange(0, static_cast<int>(candidateEdges.size())-1)]);
    PRINT("PathSolver::detail: selected edge: random")
    GameMgr->getLogger()->notify(GameUI::Notification::BezierPathChanged);
    
    return true;
  }

  template<class PositionT>
  auto solveTargetForDefeat(boost::optional<BaseGraph::Edge<PositionT>>& tarEdge,
                               int& totalEC)
    -> boost::optional<PositionT>
  {
    bool resolvedTarget = true;
    auto const& remainEnemies = GameMgr->getTargetForDefeat()->getRemainEnemiesList();
    
    for(int eid: remainEnemies) {
      if(tarEdge->isExistEnemyID(eid)) {
        resolvedTarget = false;
        break;
      }
    }
    
    if(resolvedTarget && totalEC >= tarEdge->getRequiredEC()) {
      auto ret = boost::make_optional(*(tarEdge->getTo()));
      totalEC = 0;
      tarEdge = boost::none;
      return ret;
    }
    return boost::none;
  }
  
}}

namespace PathSolver
{
  
  // World
  auto World::init(std::shared_ptr<WorldMap::AreaID> const& startPosition,
                   std::shared_ptr<WorldMap::Graph> const& graph)
    -> void
  {
    position_       = startPosition;
    worldMapGraph_  = graph;
    tarEdge_        = BaseGraph::Edge<WorldMap::AreaID>(*startPosition);  // エッジを設定すれば、はじめに街に入る
    totalEC_        = 0;
  }
  
  auto World::findNextRoute()
    -> bool
  {
    return detail::findNextEdge(worldMapGraph_, position_, tarEdge_, totalEC_);
  }
  
  auto World::isExistCurrentRoute() const
    -> bool
  {
    return !!tarEdge_;
  }
  
  auto World::solveCurrentRoute()
    -> boost::optional<WorldMap::AreaID>
  {
    return detail::solveTargetForDefeat<WorldMap::AreaID>(tarEdge_, totalEC_);
  }
  
  
  // Dungeon
  auto Dungeon::init(std::shared_ptr<WorldMap::AreaID>      const& dungeonID,
                     std::shared_ptr<DungeonMap::ForkPoint> const& startPosition,
                     std::shared_ptr<DungeonMap::Graph>     const& graph)
    -> void
  {
    dungeonID_        = dungeonID;
    position_         = startPosition;
    dungeonMapGraph_  = graph;
    tarEdge_          = boost::none;
    totalEC_          = 0;
  }
  
  auto Dungeon::findNextRoute()
    -> bool
  {
    return detail::findNextEdge(dungeonMapGraph_, position_, tarEdge_, totalEC_);
  }
  
  auto Dungeon::isExistCurrentRoute() const
    -> bool
  {
    return !!tarEdge_;
  }
  
  auto Dungeon::solveCurrentRoute()
    -> boost::optional<DungeonMap::ForkPoint>
  {
    return detail::solveTargetForDefeat(tarEdge_, totalEC_);
  }
}