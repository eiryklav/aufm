#pragma once

#include <vector>
#include <complex>
#include <set>
#include <unordered_map>
#include <boost/functional/hash.hpp>
#include <memory>

#include "../Lib/picojson.h"
#include "../Support/Misc.hpp"

namespace BaseGraph
{
  using ItemOnDB            = int;
  using EncountChance       = int;
  using EdgeID              = int;
  using RequiredEC          = EncountChance;
  using Point               = std::complex<double>;
  using BezierPath          = std::vector<Point>;
  using MapID               = int;
  using ItemList            = std::vector<ItemOnDB>;
  using ECItemsPair         = std::pair<EncountChance, ItemList>;
  using EnemyIDOnDB         = int;
  using EnemyGroup          = std::vector<EnemyIDOnDB>;
  using Probability         = int;
  using EnemyProbGroupPair  = std::pair<Probability, EnemyGroup>;
  
  template<class NodeIDType> class Edge
  {
  private:
    EdgeID                          edgeID_;
    std::shared_ptr<NodeIDType>     to_;
    RequiredEC                      reqEC_;
    BezierPath                      bezierPath_;
    MapID                           mapID_;
    std::vector<ECItemsPair>        ecItemsList_;
    std::vector<EnemyProbGroupPair> encountEnemies_;
    std::set<EnemyIDOnDB>           allEnemiesSet_;
    
  public:
    inline Edge(): edgeID_(-1), reqEC_(-1){}
    inline Edge(NodeIDType const& to): edgeID_(-1), to_(std::make_shared<NodeIDType>(to)), reqEC_(0){}
    
    inline Edge(
                EdgeID edgeID,
                NodeIDType const& to,
                RequiredEC reqEC,
                std::vector<ECItemsPair> const& ecItemsList,
                std::vector<EnemyProbGroupPair> enemyPGList,
                BezierPath bezierPath,
                MapID mapID
                )
    : edgeID_(edgeID),
    to_(std::make_shared<NodeIDType>(to)),
    reqEC_(reqEC),
    ecItemsList_(ecItemsList),
    encountEnemies_(enemyPGList),
    bezierPath_(bezierPath),
    mapID_(mapID)
    {
      for(auto const& pair: encountEnemies_)
        for(EnemyIDOnDB eid: pair.second)
          allEnemiesSet_.insert(eid);
    }
    
    inline auto getEdgeID()           const                   { return edgeID_;           }
    inline auto getTo()               const -> decltype(auto) { return (to_);             }
    inline auto getRequiredEC()       const                   { return reqEC_;            }
    inline auto getBezierPath()       const -> decltype(auto) { return (bezierPath_);     }
    inline auto getECItemsPairList()  const -> decltype(auto) { return (ecItemsList_);    }
    inline auto getEncountEnemies()   const -> decltype(auto) { return (encountEnemies_); }
    
    inline auto isExistEnemyID(EnemyIDOnDB eid) const { return allEnemiesSet_.count(eid); }
  };
  
  auto parseItem(picojson::object& o) -> std::vector<ECItemsPair>;
  auto parseEnemyGroup(picojson::object& o) -> std::vector<EnemyProbGroupPair>;
  
  template<class NodeType>
  using Edges = std::vector<Edge<NodeType>>;
  
  template<class NodeType>
  using GraphT = std::unordered_map<NodeType, BaseGraph::Edges<NodeType>, boost::hash<NodeType>>;
  
  template<class NodeType>
  auto makeGraph(picojson::array& graph_array, GraphT<NodeType>& graph)
  {
    for(auto& ite: graph_array) {
      auto& o = ite.get<picojson::object>();
      auto edgeID     = static_cast<int>(o["ID"].get<double>());
      auto areaIDFrom = NodeType(o["From"].get<std::string>());
      auto areaIDTo   = NodeType(o["To"].get<std::string>());
      auto reqEC      = static_cast<int>(o["RequiredEC"].get<double>());
      auto isBidirectional = o["IsBidirectional"].get<bool>();

      auto const& bezierPathArray = o["BezierPath"].get<picojson::array>();
      BezierPath path;
      for(auto const& v: bezierPathArray)
        path.push_back(Misc::getDoublePointFromJSON(v.get<picojson::array>()));
      
      int mapID = -1;
      if(o.find("MapID") != o.end()) {
        mapID = static_cast<int>(o["MapID"].get<double>());
      }
      
      graph[areaIDFrom].emplace_back(
        edgeID,
        areaIDTo,
        reqEC,
        BaseGraph::parseItem(o),
        BaseGraph::parseEnemyGroup(o),
        path,
        mapID
      );

      if(isBidirectional) {

        auto const& bezierPathReverseArray = o["BezierPathReverse"].get<picojson::array>();
        BezierPath pathReverse;
        for(auto const& v: bezierPathReverseArray)
          pathReverse.push_back(Misc::getDoublePointFromJSON(v.get<picojson::array>()));

        graph[areaIDTo].emplace_back(
          edgeID,
          areaIDFrom,
          reqEC,
          BaseGraph::parseItem(o),
          BaseGraph::parseEnemyGroup(o),
          pathReverse,
          mapID
        );
      }

    }
  }

}