#include "CityState.hpp"
#include "ScenarioState.hpp"
#include "../Game/Game.hpp"
#include "../Game/GameStrategy.hpp"
#include "../Game/TimeManager.hpp"
#include "../Character/ChrData.hpp"
#include "../Graph/WorldMap.hpp"
#include "../Support/Misc.hpp"
#include "../Support/Debugger.hpp"
#include "../Graph/PathSolver.hpp"

#include <fstream>
#include <tuple>
#include <complex>
#include <algorithm>

auto CityState::loadDrawersData(picojson::array& drawerArray)
{
  Misc::ProbRangeTester tester;
  Tag tag = 1;
  for(auto& drawer: drawerArray) {
    auto& o = drawer.get<picojson::object>();
    auto position     = Misc::getIntPointFromJSON( o["Position"].get<picojson::array>() );
    int itemIDOnDB    = o["Content"].get<double>();
    int probability   = o["Probability"].get<double>();
    int suspicious    = o["Suspicious"].get<double>();
    auto isUnique     = o["IsUnique"].get<bool>();
    drawers_.emplace_back(probability, std::make_tuple(tag++, position, itemIDOnDB, suspicious, isUnique));
    tester.add(probability);
  }
  tester.validateSum();
  std::sort(drawers_.begin(), drawers_.end());
}

auto CityState::loadInhabitantsData(picojson::array& inhabitantArray)
{
  for(auto& inhabitant: inhabitantArray) {
    auto const& personalityArray = inhabitant.get<picojson::object>()["Personality"].get<picojson::array>();
    Personality personality;
    for(auto const& interval: personalityArray) {
      personality.emplace_back(
        static_cast<int>(interval.get<picojson::array>()[0].get<double>()),
        static_cast<int>(interval.get<picojson::array>()[1].get<double>())
      );
    }
    
    inhabitants_.emplace_back(personality, inhabitant.get<picojson::object>()["Scenario"].get<std::string>());
  }
}

auto CityState::loadCityDataFromJSON()
{
  picojson::value v;
  std::ifstream fs(GameMgr->getDataResourcesPath()+"Worlds/" + GameMgr->getWorldName() + "/Area/" + cityID_.print() + ".json");
  if(fs.fail()) { AUFM_ASSERT(false, GameMgr->getDataResourcesPath()+"Worlds/" + GameMgr->getWorldName() + "/Area/" + cityID_.print() + ".json" << " not found."); }
  
  std::string jsonString, str;
  while(fs >> str) { jsonString += str; }
  auto err = picojson::parse(v, jsonString);
  if(!err.empty()) { AUFM_ASSERT(false, "JSON Parse error!"); }

  auto& o = v.get<picojson::object>();
  
  cityName_ = o["CityName"].get<std::string>();
  enterCityText_ = o["EnterCityText"].get<std::string>();
  auto minLengthOfStayString = o["MinLengthOfStay"].get<std::string>();
  TimeManager::changeReservedTimeSpanWords(minLengthOfStayString);
  minLengthOfStay_ = std::stod(minLengthOfStayString);  // !!! 四則計算はしていない
  wealthRatio_ = o["Wealth"].get<double>() / 100.0;
  loadDrawersData(o["Drawers"].get<picojson::array>());
  loadInhabitantsData(o["Inhabitants"].get<picojson::array>());
  
}

CityState::CityState(WorldMap::AreaID const& cityID)
  : cityID_(cityID), updateNum_(0), additionalLengthOfStay_(0)
{
  loadCityDataFromJSON();
  startAufmTimeOfStaying_ = GameMgr->getTimeMgr()->getNowAufmTime();
  
  // Record arrival time
  GameMgr->getLogger()->getPlayerStatusLog(ChrData::UserIDOnDB).pushArrivalAtArea(std::make_shared<WorldMap::AreaID>(cityID_));
  
  GameMgr->getLogger()->pushMessage(Printer::Message(Printer::MessageType::EnterCity, enterCityText_));
  
  PathSolver::onPath(PathSolver::PathOf::AtCity);
  GameMgr->getLogger()->notify(GameUI::Notification::MapChanged);
}

auto CityState::isMatchPersonality(Personality const& personality)
{
  auto num = GameMgr->getCodeOfConduct()->getAttitude();
  for(auto const& interval: personality) {
    if(interval.first <= num && num <= interval.second) return true;
  }
  return false;
}

auto CityState::contactInhabitant()
{

  double const wanderingTime  = TimeManager::TimeSpan::OneHour;   // 最低消費時間

  GameMgr->getTimeMgr()->requestConsumingAufmTime(TimeManager::AufmTime(wanderingTime));

  using Iter = std::vector<Inhabitant>::iterator;
  std::vector<Iter> vec;
  for(auto iter=inhabitants_.begin(); iter!=inhabitants_.end(); iter++) {
    auto const& personality = std::get<0>(*iter);
    if(isMatchPersonality(personality)) {
      vec.emplace_back(iter);
    }
  }
  
  if(vec.empty()) { /* 性格が誰とも合わない */ return;  }

  int ridx = Misc::randomRange(0, static_cast<int>(vec.size())-1);
  GameMgr->pushState(std::make_shared<ScenarioState>(std::get<1>(*vec[ridx]), true));
}

auto CityState::lootDrawer()
{
  /*
   箪笥の引き出しは、ひとつの街に無数にある。
   そう考えると、引き出しのデータは開けたら終わり、ではなくて再利用するのが妥当かと思う。
   ただし、isUnique であるタンスは一度開けたら空（ハズレ）になるようにしておく。
   ↑追加機能のPositionによって、同じ家に何度も入り込む現象が出来てしまうのはどうすべきか。
   */
  if(!Misc::randomBernoulli(GameMgr->getCodeOfConduct()->getLootingDrawers())) { return; }
  if(drawers_.empty()) { return; }
  int ridx = Misc::getRandomIndexAtProbTVec<decltype(drawers_[0].second)>(drawers_);
  auto const& dr = drawers_[ridx];
  auto const& tag         = std::get<0>(dr.second);
  auto const& position    = std::get<1>(dr.second);
  auto const& itemIDOnDB  = std::get<2>(dr.second);
  auto const& suspicious  = std::get<3>(dr.second);
  auto const& isUnique    = std::get<4>(dr.second);

  currentPosition_ = position;
  GameMgr->addSuspicious(suspicious);
  GameMgr->getTimeMgr()->requestConsumingAufmTime(TimeManager::AufmTime(TimeManager::TimeSpan::OneHour));
  
  namespace MSG = Printer::Messages;
  GameMgr->getLogger()->pushMessage(MSG::openDrawer(GameMgr->getPlayer()->getName()));
  
  if(isUnique) {
    if(GameMgr->isEventGenerated(
      "OpenUniqueDrawer",
      cityID_.print() + ":" + std::to_string(tag))) {
      GameMgr->getLogger()->pushMessage(MSG::foundItEmpty());
      return;
    }
    else {
      GameMgr->setEventGeneratedFlag(
        "OpenUniqueDrawer",
        cityID_.print() + ":" + std::to_string(tag)
      );
    }
  }

  if(itemIDOnDB < 0) {
    GameMgr->getLogger()->pushMessage(MSG::foundItEmpty());
    return;
  }

  GameMgr->getRucksack()->pushItem(itemIDOnDB);
}

auto CityState::enterShop(Misc::EquipmentType shopType)
{
  // 交換比率
  if(!Misc::randomBernoulli(GameMgr->getCodeOfConduct()->getChangeEquipmentRatio())) { return; }

  auto const shopTypeStr = [&]() {
    switch(shopType) {
      case Misc::EquipmentType::Weapon:     return "Weapon";
      case Misc::EquipmentType::Protector:  return "Protector";
      case Misc::EquipmentType::Accessory:  return "Accessory";
    }
  }();
  
  // 二度以上の来訪はない
  if(GameMgr->isEventGenerated("Shop", cityID_.print()+":"+shopTypeStr)) { return; }
  GameMgr->setEventGeneratedFlag("Shop", cityID_.print()+":"+shopTypeStr);

  using ChrData::Money;
  namespace MSG = Printer::Messages;
  
  GameMgr->getLogger()->pushMessage([&]() -> decltype(auto) {
    switch (shopType) {
      case Misc::EquipmentType::Weapon:     return (MSG::enterWeaponShop());
      case Misc::EquipmentType::Protector:  return (MSG::enterProtectorShop());
      case Misc::EquipmentType::Accessory:  return (MSG::enterAccessoryShop());
      default: AUFM_ASSERT(false, "No match EquipmentType");
    }
  }());
  
  std::ifstream fs(GameMgr->getDataResourcesPath()+"Worlds/"+GameMgr->getWorldName()+"/Area/"+cityID_.print()+".json");
  
  if(fs.fail()) {
    AUFM_ASSERT(false, GameMgr->getDataResourcesPath()+"Worlds/"+GameMgr->getWorldName()+"/"+cityID_.print()+".json" << " Not found json file");
  }
  
  picojson::value v;
  std::string s, jsonString;
  while(fs >> s) { jsonString += s; }
  auto err = picojson::parse(v, jsonString);
  
  auto& o = v.get<picojson::object>()["Shops"].get<picojson::object>()[shopTypeStr].get<picojson::object>();
  
  // ナビマップ上の位置指定
  currentPosition_ = Misc::getIntPointFromJSON(o["Position"].get<picojson::array>());
  
  // ログにリストアップ
  auto const& eqpArray = o["Goods"].get<picojson::array>();
  auto const allPlayers = GameMgr->getAllPlayers();
  for(auto const& e: eqpArray) {
    auto eqpID  = e.get<picojson::array>()[0].get<double>();
    auto price  = Money(static_cast<int>(e.get<picojson::array>()[1].get<double>()));
    auto rec = GameMgr->getDatabase()->getEquipmentRecord(shopType, eqpID);
    
    GameMgr->getLogger()->pushMessage(MSG::listUpGoods(
      Misc::getStringCorrespondToWorld(rec["Name"], GameMgr->getWorldName()),
      price,
      GameMgr->getMonetaryUnitName()
    ));
  }
  
  auto const BudgetPerOne = ChrData::Money(GameMgr->getMoney() / GameMgr->getNumOfPlayers());
  
  for(auto player: allPlayers) {
    
    auto currID = player->getBasicStat()->getEquipment(shopType);
    auto currRec = currID != -1 ? GameMgr->getDatabase()->getEquipmentRecord(shopType, currID) : DB::RecType();
    auto currRank = std::stoi(currRec["Rank"]);
    
    auto candidateID = -1;
    auto candidatePrice = BudgetPerOne;
    auto candidateRank = currRank;
    std::string candidateEqpJSONName;
    
    for(auto const& e: eqpArray) {
      auto newID    = e.get<picojson::array>()[0].get<double>();
      auto newRec   = GameMgr->getDatabase()->getEquipmentRecord(shopType, newID);
      auto newPrice = Money(static_cast<int>(e.get<picojson::array>()[1].get<double>()));
      
      if(newPrice <= BudgetPerOne) {
        auto newRank  = std::stoi(newRec["Rank"]);
        if(currRank >= newRank) { continue; } // 購入対象が現在のランクより高いのが前提条件
        
        if((candidateRank <  newRank) ||
           (candidateRank == newRank && newPrice < candidatePrice)) // 同ランクから最安
        {
          candidateID = newID;
          candidatePrice = newPrice;
          candidateEqpJSONName = newRec["Name"];
        }
      }
    }
    
    if(candidateID == -1) { continue; }
    
    auto candidateEqpName = Misc::getStringCorrespondToWorld(candidateEqpJSONName, GameMgr->getWorldName());
    
    if(candidateID != -1) {
      GameMgr->useMoney(candidatePrice);
      GameMgr->getLogger()->pushMessage(Printer::Messages::buyGoods(candidateEqpName, candidatePrice, GameMgr->getMonetaryUnitName()));
      GameMgr->tryToGetEquipmentForPlayer(candidateID, shopType, player, cityName_, true);
    }
    
  }
  
  GameMgr->getLogger()->pushMessage([&]() -> decltype(auto) {
    switch (shopType) {
      case Misc::EquipmentType::Weapon:     return (MSG::exitWeaponShop());
      case Misc::EquipmentType::Protector:  return (MSG::exitProtectorShop());
      case Misc::EquipmentType::Accessory:  return (MSG::exitAccessoryShop());
      default: AUFM_ASSERT(false, "No match EquipmentType");
    }
  }());
  
  // 時間経過
  GameMgr->getTimeMgr()->requestConsumingAufmTime(TimeManager::AufmTime(TimeManager::TimeSpan::OneHour/2));
}

auto CityState::enterInn()
{
  double k = 0.15;  // wealthRatio_ = 1.0 であれば、1.0 / k 回休めば必ず完全回復する
  
  GameMgr->getPlayer()->getCurrStat()
    ->increaseHP(ChrData::HitPoint(static_cast<int>(GameMgr->getPlayer()->getBasicStat()
                                                    ->getHP() * k * wealthRatio_)));
  GameMgr->getPlayer()->getCurrStat()
    ->increaseMana(ChrData::Mana(static_cast<int>(GameMgr->getPlayer()->getBasicStat()
                                                  ->getMana() * k * wealthRatio_)));
  
  auto servants = GameMgr->getServants();
  
  for(auto const& s: servants) {
    s->getCurrStat()->increaseHP(ChrData::HitPoint(static_cast<int>(s->getBasicStat()
                                                                    ->getHP() * k * wealthRatio_)));
    s->getCurrStat()->increaseMana(ChrData::Mana(static_cast<int>(s->getBasicStat()
                                                                  ->getMana() * k * wealthRatio_)));
  }
}

auto CityState::update()
	-> void
{
  
  PathSolver::onPath(PathSolver::PathOf::AtCity);
  
  if(GameMgr->getTimeMgr()->getNowAufmTime() - startAufmTimeOfStaying_ >= minLengthOfStay_ + GameMgr->getTimeMgr()->getExtraAufmTime()) {
    GameMgr->getTimeMgr()->clearExtraAufmTime();
    PRINT("滞在期間を過ぎました。街を出ます。")
    GameMgr->popState();
  }
  else {
    // 関数内で pushState(), popState() が行われうるので、同時に2つ以上の行動を取れないようにする
    switch(updateNum_) {
      case 0: contactInhabitant();                        break;
      case 1: lootDrawer();                               break;
      case 2: enterShop(Misc::EquipmentType::Weapon);     break;
      case 3: enterShop(Misc::EquipmentType::Protector);  break;
      case 4: enterShop(Misc::EquipmentType::Accessory);  break;
      case 5: enterInn();                                 break;
      default:
        AUFM_ASSERT(false, "No match updateNum_ in CityUpdate.");
    }
    (updateNum_ += 1) %= 6;
  }
}