#include "ScenarioState.hpp"
#include "BattleState.hpp"
#include "../Game/Game.hpp"
#include "../Game/TimeManager.hpp"
#include "../Support/Misc.hpp"
#include "../Data/BaseParser.hpp"
#include "../Printer.hpp"

#include <boost/algorithm/string.hpp>
#include <fstream>
#include <memory>

ScenarioState::ScenarioState(std::string const& fname, bool once)
  : scenarioID_(fname), currentIdx_(0), talkerName_("＊"), scenarioOnce_(once)
{
  std::string str, jsonString;
  std::ifstream fs(GameMgr->getDataResourcesPath()+"Scenario/" + GameMgr->getWorldName() + "/" + fname + ".json");
  if(fs.fail()) { AUFM_ASSERT(false, GameMgr->getDataResourcesPath()+"Scenario/" + GameMgr->getWorldName() + "/" + fname + ".json" << "not found"); }
  
  while(fs >> str) { jsonString += str; }
  picojson::value v;
  auto err = picojson::parse(v, jsonString);
  scenarioArray_ = v.get<picojson::array>();
}

ScenarioState::ScenarioState(picojson::array const& scenarioArray, std::string blockScenarioID, bool once)
: scenarioID_("BlockScenario::" + blockScenarioID), scenarioArray_(scenarioArray), currentIdx_(0)
{
}

auto ScenarioState::changeReservedWords(std::string& str)
{
  boost::algorithm::replace_all(str, "@Player", GameMgr->getPlayer()->getName());
  boost::algorithm::replace_all(str, "@Money", std::to_string(GameMgr->getMoney()) + GameMgr->getMonetaryUnitName());
  boost::algorithm::replace_all(str, "\\n", "\n");
  TimeManager::changeReservedTimeSpanWords(str);
}

auto ScenarioState::runStatement(picojson::array& statement)
  -> int
{
  // 一行文のステートメントのみ実行する。連続した文は再帰、複数行実行したい場合は新たにpushState(ScenarioState)とする
  auto const& c = statement[0].get<std::string>();
  if(c == "Text")
  {
    auto text = statement[1].get<std::string>();
    changeReservedWords(text);
    GameMgr->getLogger()->pushMessage(Printer::Message(Printer::MessageType::Default, text));
    return SCENARIO_OK;
  }

  else if(c == "SetTalkerName")
  {
    talkerName_ = statement[1].get<std::string>();
    return SCENARIO_OK;
  }

  else if(c == "Talk")
  {
    auto text = statement[1].get<std::string>();
    changeReservedWords(text);
    GameMgr->getLogger()->pushMessage(Printer::Message(Printer::MessageType::Default, talkerName_ + "「" + text + "」"));
    return SCENARIO_OK;
  }

  else if(c == "AddServant")
  {
    return GameMgr->addServant(static_cast<int>(statement[1].get<double>())) ? SCENARIO_OK : SCENARIO_NG;
  }

  else if(c == "CostTime")
  {
    using TimeManager::AufmTime;
    auto time = statement[1].get<std::string>();
    changeReservedWords(time);
    ExpressionParser pa(time);
    auto t = AufmTime(static_cast<int>(pa.expression()));
    GameMgr->getTimeMgr()->requestConsumingAufmTime(t);
    GameMgr->getTimeMgr()->addExtraAufmTime(t);
    return SCENARIO_OK;
  }
  
  else if(c == "SetConverser")
  {
    // CharacterAnimationViewのキャラ表示を変更
    AUFM_ASSERT(false, "SetConverser has not been implemented.");
    return SCENARIO_OK;
  }
  
  else if(c == "Conversation")
  {
    // ScenarioConversation
    
    // FIXME: pushConversation
    
    AUFM_ASSERT(false, "Conversation has not been implemented.");
    return SCENARIO_OK;
  }

  else if(c == "SetFlag")
  {
    auto const& flag = statement[1].get<std::string>();
    GameMgr->setEventGeneratedFlag("Custom", flag);
    return SCENARIO_OK;
  }

  else if(c == "CheckFlag")
  {
    auto const& flag = statement[1].get<std::string>();
    return GameMgr->isEventGenerated("Custom", flag) ? SCENARIO_OK : SCENARIO_NG;
  }
  
  else if(c == "Condition")
  {
    /*
     TODO:
     条件判定のみ。StatusReplacementを使う
     Return SCENARIO_OK or SCENARIO_NG
     */
    AUFM_ASSERT(false, "Condition command has not been implemetend.");
  }

  else if(c == "If")
  {
    // ブロック化したい場合は、goSubを使う。
    auto& condition = statement[1].get<picojson::array>();
    auto r = runStatement(condition);
    if(r == SCENARIO_OK) {
      auto& then = statement[2].get<picojson::array>();
      return runStatement(then);
    }
    else {
      if(statement.size() == 4) {
        auto& else_ = statement[3].get<picojson::array>();
        return runStatement(else_);
      }
    }
    return SCENARIO_OK;
  }

  else if(c == "GetItem")
  {
    GameMgr->getRucksack()->pushItem(static_cast<int>(statement[1].get<double>()));
    return SCENARIO_OK;
  }

  else if(c == "BattleWithGroup")
  {
    auto const& group = statement[1].get<picojson::array>();
    std::vector<int> enemies;
    for(auto const& enemy: group) {
      enemies.push_back(static_cast<int>(enemy.get<double>()));
    }
    GameMgr->pushState(std::make_shared<BattleState>(enemies));
    // 戦闘に勝利していた場合、そのままシナリオを続行する。
    return SCENARIO_OK;
  }

  else if(c == "Bernoulli")
  {
    ExpressionParser pa(statement[1].get<std::string>());
    return Misc::randomBernoulli(
      pa.expression() / 100.0
    );
  }

  else if(c == "Exit")
  {
    GameMgr->popState();
    GameMgr->setEventGeneratedFlag("ScenarioOnce", scenarioID_);  // ステートpop時にScenarioOnceを登録する
    return SCENARIO_OK;
  }

  else if(c == "Goto")
  {
    GameMgr->changeState(std::make_shared<ScenarioState>(statement[1].get<std::string>()));
    return SCENARIO_OK;
  }

  else if(c == "GoSub")
  {
    GameMgr->pushState(std::make_shared<ScenarioState>(statement[1].get<std::string>()));
    return SCENARIO_OK;
  }
  
  else if(c == "ChangeAI")
  {
    AUFM_ASSERT(false, "ChangeAI command has not been implemented.");
    return SCENARIO_OK;
  }
  
  else if(c == "GetInformation")
  {
    AUFM_ASSERT(false, "GetInformation command has not been implemented.");
    return SCENARIO_OK;
  }

  else
  {
    AUFM_ASSERT(false, "Undefined scenario command.");
  }
}

auto ScenarioState::update()
  -> void
{
  if(scenarioOnce_) {
    if(GameMgr->isEventGenerated("ScenarioOnce", scenarioID_)) {
      PRINT("ScenarioState: SenarioOnce")
      GameMgr->popState();
      return;
    }
  }
  
  if(currentIdx_ == scenarioArray_.size()) {
    GameMgr->popState();
    GameMgr->setEventGeneratedFlag("ScenarioOnce", scenarioID_);  // ステートpop時にScenarioOnceを登録する
  }
  else {
    PRINT("Scenario statement currentIdx_: " << currentIdx_)
    runStatement(scenarioArray_[currentIdx_++].get<picojson::array>());
  }
}