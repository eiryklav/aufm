#pragma once

#include "../Game/Game.hpp"
#include "../Graph/WorldMap.hpp"

#include <boost/optional.hpp>
#include <memory>

class GameWorldState : public GameState
{
private:
  auto loadGraphFromJSON();
  
  bool firstResponderFlag = false;
  auto firstResponderWithState() -> bool;

public:

	GameWorldState();
	auto update() -> void;
  auto getStateType() const -> StateType { return StateType::GameWorldState; }

};