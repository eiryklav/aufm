#pragma once

#include "GameState.hpp"
#include <iostream>
#include <string>
#include <array>

#include "../Character/ChrData.hpp"

namespace BattleSystem { class Battle; }
enum class EncountType { Normal, Special };

class BattleState : public GameState
{
private:
	std::shared_ptr<BattleSystem::Battle> battle_;
  EncountType encountType_;
  
  bool firstResponderFlag = false;
  auto firstResponderWithState() -> void;

public:
  BattleState(std::vector<int> const&, EncountType encountType_ = EncountType::Normal);
	auto update() -> void;
	
	inline auto getStateType() const -> StateType { return StateType::BattleState; }
};