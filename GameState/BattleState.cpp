#include "BattleState.hpp"
#include "../BattleSystem/BattleSystem.hpp"
#include "../GameState/EndingState.hpp"
#include "../Printer.hpp"
#include "../Game/GameStrategy.hpp"

#include "../Data/Database.hpp"
#include "../Lib/picojson.h"
#include "../Support/Misc.hpp"
#include "../Graph/PathSolver.hpp"

#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <list>

BattleState::BattleState(std::vector<int> const& enemyIDList, EncountType encountType)
: encountType_(encountType)
{
	battle_ = std::make_shared<BattleSystem::Battle>(enemyIDList);
}

auto BattleState::firstResponderWithState()
  -> void
{
  if(firstResponderFlag) { return; }
  firstResponderFlag = true;
  
  auto encountTypePrefix  = [&](){
    switch (encountType_) {
      case EncountType::Normal:   return "通常";
      case EncountType::Special:  return "特殊";
      default: AUFM_ASSERT(false, "Undefined encount type");
    }}();
  
  int numOfEnemies = 0;
  
  auto const& c = battle_->getContainer();
  for(auto const& p: *c) {
    if(p->getChrType() == ChrData::Character::ChrType::Enemy) {
      numOfEnemies ++;
    }
  }
  
  std::string position;
  
  PATH_SOLVER_SWITCH({
    CASE_PATH(World, {
      position = WorldPath->getPosition()->print() + std::to_string(WorldPath->getCurrentEdge()->getEdgeID());
    })
    CASE_PATH(Dungeon, {
      position = DungeonPath->getPosition()->print() + std::to_string(DungeonPath->getCurrentEdge()->getEdgeID());
    })
    CASE_PATH(AtCity, PATH_SOLVER_ASSERTION)
  })
  
  // 戦闘開始非開示ログ生成
  GameMgr->getLogger()->pushMessage(
    Printer::Messages::sysBattleStart(encountTypePrefix, numOfEnemies, position)
  );
  
  // 「〜が現れた」を表示
  for(auto const& p: *c) {
    if(p->getChrType() == ChrData::Character::ChrType::Enemy) {
      GameMgr->getLogger()->pushMessage(Printer::Messages::encountEnemy(p->getName()));
    }
  }
}

auto BattleState::update()
	-> void
{
  firstResponderWithState();
  
	using BattleSystem::Result;
	namespace MSG = Printer::Messages;
	
	auto res = battle_->resume();

	if(res == Result::Win) {
		// ラスボスは特別処理する
		GameMgr->popState();
	}
  else if(res == Result::Lose) {
		GameMgr->pushState(std::make_shared<EndingState>(EndCondition::PlayerHasDied));
	}
	else if(res == Result::Running) {
		// continue battle
	}
	else {
		AUFM_ASSERT(false, "Undefined result in BattleState");
	}
}
