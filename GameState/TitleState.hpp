#pragma once

#include "GameState.hpp"

#include <iostream>
#include <string>
#include <array>

class TitleState : public GameState
{
public:
	TitleState();

	auto update() -> void;
	
	inline auto getStateType() const -> StateType { return StateType::TitleState; }
};