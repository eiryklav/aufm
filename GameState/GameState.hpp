#pragma once

#include <iostream>
#include <memory>
#include <unordered_map>
/*
	GameState: virtual class
*/

namespace WorldMap { class WorldMapGraph; }

enum class StateType
{
	TitleState,
	EndingState,
	GameWorldState,
	BattleState,
	CityState,
	DungeonState,
	ScenarioState,
};

static std::unordered_map<StateType, std::string> StateTypeString = {
	{StateType::TitleState, "TitleState"},
	{StateType::EndingState, "EndingState"},
	{StateType::GameWorldState, "GameWorldState"},
	{StateType::BattleState, "BattleState"},
	{StateType::CityState, "CityState"},
	{StateType::DungeonState, "DungeonState"},
	{StateType::ScenarioState, "ScenarioState"}
};

class GameState
{
public:

	/*
		GameMgr に pushState() されるたびに、std::make_shared<>() で派生クラスのインスタンス（＝ステート）が作成される。
		ステートの初期処理をコンストラクタで行う。この際、コンストラクタ内で新しいステートをpushState()してはならない。（現在のpushState()が完了していないため）
    また、多くの場合、pushState() の呼び出し元が、派生クラスのインスタンスの引数を生成するので、コンストラクタは引数を受け取るようにする。
	*/
	
	inline GameState() {}
	virtual auto update() -> void = 0;
	virtual auto getStateType() const -> StateType = 0;
};