#include "DungeonState.hpp"
#include "ScenarioState.hpp"
#include "../Game/GameUI.hpp"
#include "../Game/TimeManager.hpp"
#include "../Graph/DungeonMap.hpp"
#include "../Support/Misc.hpp"
#include "../Lib/picojson.h"
#include "EndingState.hpp"
#include "BattleState.hpp"
#include "CityState.hpp"
#include "GameWorldState.hpp"
#include "../Graph/PathSolver.hpp"
#include "DungeonMap.hpp"
#include "../Printer.hpp"

#include <iostream>
#include <fstream>
#include <vector>

auto DungeonState::loadDungeonDataFromJSON()
{
	picojson::value v;
	std::ifstream fs(
		GameMgr->getDataResourcesPath()+"Worlds/"
		+ GameMgr->getWorldName()
		+ "/Area/" + dungeonID_.print()
		+ ".json");
  
  if(fs.fail()) {
    AUFM_ASSERT(false, GameMgr->getDataResourcesPath()+"Worlds/"
                + GameMgr->getWorldName()
                + "/Area/" + dungeonID_.print()
                + ".json" << " not found");
  }

	std::string jsonString, str; while(fs >> str) { jsonString += str; }
	if(!picojson::parse(v, jsonString).empty()) { AUFM_ASSERT(false, "JSON parse error!"); }
  
	auto& o = v.get<picojson::object>();
  
  enterDungeonText_ = o["EnterDungeonText"].get<std::string>();
  
  // ForkPointのJSONでの型は、AreaIDと同様にstring
  DungeonPath->init(std::make_shared<WorldMap::AreaID>(dungeonID_),
                    std::make_shared<DungeonMap::ForkPoint>(o["StartPosition"].get<std::string>()),
                    std::make_shared<DungeonMap::Graph>(o["Graph"].get<picojson::array>()));
  
	auto& triggerActionArray = o["TriggerActions"].get<picojson::array>();
  
  for(auto& val: triggerActionArray) {
    auto& o = val.get<picojson::object>();
    auto const forkPointID = std::stoi(o["ForkPointID"].get<std::string>());
    triggerCommandMap_[forkPointID] = o["Command"].get<picojson::array>();
  }
}

auto DungeonState::arriveAtForkPoint(std::shared_ptr<DungeonMap::ForkPoint> const& forkPoint)
{

  DungeonPath->setPosition(forkPoint);

	auto const iter = triggerCommandMap_.find(forkPoint->getID());
	if(iter == triggerCommandMap_.end()) { return; }
  
  auto& command = iter->second;
  auto const& commandType = command[0].get<std::string>();
  auto& args = command[1];
  
  if(commandType == "Scenario") {
    GameMgr->pushState(std::make_shared<ScenarioState>(args.get<std::string>()));
  }
  else if(commandType == "Treasure") {
    GameMgr->getLogger()->pushMessage(Printer::Messages::openTreasureDuringJourney(GameMgr->getPlayer()->getName()));
    auto& array = args.get<picojson::array>();
    for(auto const& e: array) {
      GameMgr->getRucksack()->pushItem(static_cast<int>(e.get<double>()));
    }
  }
  else if(commandType == "GenLampMovers") {
    
    auto& array = args.get<picojson::array>();
    for(auto& val: array) {
      auto& o = val.get<picojson::object>();
      auto const& id = o["ID"].get<std::string>();
      
      if(GameMgr->getLogger()->isGeneratedLampMover(id)) { /* 一度生成したLampMoverは二度と生成されない */ return; }
      
      auto const& type  = o["Type"].get<std::string>();
      auto const& mode  = o["MovingMode"].get<std::string>();
      auto speed = o["Speed"].get<double>();
      auto startPosition = Misc::getDoublePointFromJSON(o["StartPosition"].get<picojson::array>());
      
      if(o.find("ScenarioBlock") != o.end()) {
        GameMgr->getLogger()->pushLampMover(id, type, mode, speed, startPosition, o["ScenarioBlock"].get<picojson::array>());
      }
      else {
        GameMgr->getLogger()->pushLampMover(id, type, mode, speed, startPosition, o["Scenario"].get<std::string>());
      }
    }
    
  }
  else if(commandType == "Exit") {
    if(command[1].get<std::string>() != "Entrance") {
      // 入り口とは別の出口に出現
      WorldPath->setPosition(std::make_shared<WorldMap::AreaID>(command[1].get<std::string>()));
    }
    
    // Record arrival at exit time
    GameMgr->getLogger()->getPlayerStatusLog(ChrData::UserIDOnDB).pushArrivalAtArea(DungeonPath->getDungeonID());
    
    GameMgr->popState();
  }
  else {
    AUFM_ASSERT(false, "Not found goIntoForkPoint command");
  }
  
  // DO NOTHING HERE
}

DungeonState::DungeonState(WorldMap::AreaID const& dungeonID)
	: dungeonID_(dungeonID)
{
	loadDungeonDataFromJSON();
  
  // Record arrival time
  GameMgr->getLogger()->getPlayerStatusLog(ChrData::UserIDOnDB).pushArrivalAtArea(std::make_shared<WorldMap::AreaID>(dungeonID_));
  
  GameMgr->getLogger()->pushMessage(Printer::Message(Printer::MessageType::EnterDungeon, enterDungeonText_));
  
  PathSolver::onPath(PathSolver::PathOf::Dungeon);
  GameMgr->getLogger()->notify(GameUI::Notification::MapChanged);
}

// ------------------------------------------------------------------------------------
// MARK: UPDATE

auto existRoute() {
  return DungeonPath->isExistCurrentRoute() || DungeonPath->findNextRoute();
}

auto useEncountChance() {
  
#define ENCOUNT_ENEMIES_LIST DungeonPath->getCurrentEdge()->getEncountEnemies()
  
  auto index = Misc::getRandomIndexAtProbTVec<BaseGraph::EnemyGroup>(ENCOUNT_ENEMIES_LIST);
  auto const& enemiesIDOnDB = ENCOUNT_ENEMIES_LIST[index].second;
  
  DungeonPath->incrementTotalEC();
  
  if(enemiesIDOnDB.empty()) {
    GameMgr->getLogger()->pushMessage(Printer::Messages::sysNoEncount());
  }
  else {
    GameMgr->pushState(std::make_shared<BattleState>(enemiesIDOnDB));
  }
  
#undef ENCOUNT_ENEMIES_LIST
}

auto DungeonState::update()
	-> void
{
#define NOTIFIED(n) (GameMgr->getLogger()->isNotified(GameUI::Notification::n))
  
  PathSolver::onPath(PathSolver::PathOf::Dungeon);
  
  if(auto const& lampID_ = GameMgr->getLogger()->getCurrentSpecialEncountLampMoverID()) {
    auto lampID = *lampID_; GameMgr->getLogger()->flushCurrentSpecialEncountLampMoverID();
    auto const& lampMover = GameMgr->getLogger()->getLampMoverWithID(lampID);
    if(!lampMover.scenario.empty()) {
      GameMgr->pushState(
        std::make_shared<ScenarioState>(lampMover.scenario, true),
        [lampID]() {
          // ScenarioState終了時にLampMoverの削除を通達する
          GameMgr->getLogger()->pushMessage(Printer::Messages::sysLampMoverKilled(lampID));
        }
      );
    }
    else {
      GameMgr->pushState(std::make_shared<ScenarioState>(lampMover.scenarioBlock, "SpecialEncountWithLampMoverID-" + lampID, true));
    }
    
    return;
  }
  
  if(!existRoute()) {
    if(!NOTIFIED(WaitingForNextEncount)) { return; }
    GameMgr->pushState(std::make_shared<EndingState>(EndCondition::DeadEndInDungeon));
    return;
	}
  
  if(!NOTIFIED(WaitingForNextEncount)) { return; }
	if(auto forkpoint = DungeonPath->solveCurrentRoute()) {
    arriveAtForkPoint(std::make_shared<DungeonMap::ForkPoint>(*forkpoint));
	}
  else {
    useEncountChance();
	}
  
#undef NOTIFIED
}
