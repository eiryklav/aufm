#pragma once

#include <iostream>
#include <memory>

#include "GameState.hpp"
#include "../Lib/picojson.h"

class ScenarioState : public GameState
{
private:

  static int constexpr
  SCENARIO_OK  = 1,
  SCENARIO_NG  = 0
  ;

  std::string     scenarioID_;
  picojson::array scenarioArray_;
  int             currentIdx_;
  std::string     talkerName_;
  bool            scenarioOnce_;

  auto changeReservedWords(std::string&);
  auto runStatement(picojson::array& statement) -> int;
  
public:

  ScenarioState(std::string const& fname, bool once = false);
  ScenarioState(picojson::array const& scenarioArray, std::string blockScenarioID, bool once = false);
  auto update() -> void;
  inline auto getStateType() const -> StateType { return StateType::ScenarioState; }
};