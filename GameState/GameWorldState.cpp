#include "GameWorldState.hpp"
#include "CityState.hpp"
#include "DungeonState.hpp"
#include "EndingState.hpp"
#include "../Support/Misc.hpp"
#include "../Lib/picojson.h"
#include "../Printer.hpp"
#include "../Game/GameUI.hpp"
#include "../Game/TimeManager.hpp"
#include "../Graph/PathSolver.hpp"
#include "../GameState/BattleState.hpp"

#include "../Graph/BaseGraph.hpp"

#include <iostream>
#include <set>
#include <fstream>

auto GameWorldState::loadGraphFromJSON()
{
	picojson::value v;
  std::ifstream fs(GameMgr->getDataResourcesPath()+"Worlds/" + GameMgr->getWorldName() + "/WorldMap.json");
  if(fs.fail()) { AUFM_ASSERT(false, GameMgr->getDataResourcesPath()+"Worlds/" + GameMgr->getWorldName() + "/WorldMap.json"); }
  
  std::string json_string, str; while(fs >> str) { json_string += str; }
	if(!picojson::parse(v, json_string).empty()) { std::runtime_error("JSON parse error!"); }
  
	auto& o = v.get<picojson::object>();

	auto const& startPos = o["StartPosition"].get<std::string>();
	auto& graphArray = o["Graph"].get<picojson::array>();
	WorldPath->init(std::make_shared<WorldMap::AreaID>(startPos),
                  std::make_shared<WorldMap::Graph>(graphArray));
}

GameWorldState::GameWorldState()
{
  loadGraphFromJSON();
  PathSolver::onPath(PathSolver::PathOf::World);
  GameMgr->getLogger()->notify(GameUI::MapChanged);
}

auto GameWorldState::firstResponderWithState()
  -> bool
{
  if(!firstResponderFlag) {
    firstResponderFlag = true;
    return true;
  }
  return false;
}

auto GameWorldState::update()
	-> void
{
  auto firstResponse = firstResponderWithState();
  
  PathSolver::onPath(PathSolver::PathOf::World);
  
  if(!WorldPath->isExistCurrentRoute() && !WorldPath->findNextRoute()) {
    
    // WaitingForNextEncountといえるのか？
    if(!GameMgr->getLogger()->isNotified(GameUI::WaitingForNextEncount)) { return; }
    
    GameMgr->pushState(std::make_shared<EndingState>(EndCondition::DeadEndOfRoad));
    return;
	}
  
	if(auto wrapped = WorldPath->solveCurrentRoute()) {
    
    // ノードに入るときはWaitingForNextEncountではないので正確な記述ではない
    if(!firstResponse && !GameMgr->getLogger()->isNotified(GameUI::WaitingForNextEncount)) { return; }
    
    auto arrivalArea = *wrapped;
    WorldPath->setPosition(std::make_shared<WorldMap::AreaID>(arrivalArea));
		if(arrivalArea.getType() == WorldMap::AreaType::City) {
			GameMgr->pushState(std::make_shared<CityState>(arrivalArea));
		}
		else if(arrivalArea.getType() == WorldMap::AreaType::Dungeon) {
      GameMgr->pushState(std::make_shared<DungeonState>(arrivalArea));
		}
		else {
			AUFM_ASSERT(false, "Undefined area.");
		}
  }
	else {
    
    if(!GameMgr->getLogger()->isNotified(GameUI::WaitingForNextEncount)) { return; }
    
    // エンカウント処理
    auto index = Misc::getRandomIndexAtProbTVec<BaseGraph::EnemyGroup>(WorldPath->getCurrentEdge()->getEncountEnemies());
    auto const& enemiesIDOnDB = WorldPath->getCurrentEdge()->getEncountEnemies()[index].second;
    WorldPath->incrementTotalEC();
    if(!enemiesIDOnDB.empty()) {
      // エンカウントが起きる場合。敵グループの配列が空なら、エンカウントは起きない。
      GameMgr->pushState(std::make_shared<BattleState>(enemiesIDOnDB));
    }
    else {
      GameMgr->getLogger()->pushMessage(Printer::Messages::sysNoEncount());
    }
	}
}
