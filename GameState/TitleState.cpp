#include "../Game/Game.hpp"
#include "../Game/GameUI.hpp"
#include "../Character/ChrData.hpp"
#include "TitleState.hpp"
#include "GameWorldState.hpp"
#include "../Game/TimeManager.hpp"
#include "../Printer.hpp"

TitleState::TitleState() {}

auto TitleState::update()
	-> void
{
  GameMgr->getLogger()->pushMessage(Printer::Messages::startRPG(GameMgr->getFullWorldName()));
  GameMgr->changeState(std::make_shared<GameWorldState>());
  /*
  ユーザー入力確認用
  if(GameMgr->getLogger()->isNotificationFromSwift()) {
    auto& notificationFromSwiftBuffer = GameMgr->getLogger()->getNotificationFromSwiftBuffer();
    for(auto const& input: notificationFromSwiftBuffer) {
      if(input == GameUI::NotificationFromSwift::PushOK) {
        GameMgr->changeState(std::make_shared<GameWorldState>());
      }
      else {
        notificationFromSwiftBuffer.pop_front();
      }
    }
  }
  */
}
