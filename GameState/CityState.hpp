#pragma once

#include <iostream>
#include <memory>
#include <tuple>
#include <complex>

#include "GameState.hpp"
#include "../Graph/WorldMap.hpp"
#include "../Lib/picojson.h"
#include "../Game/TimeManager.hpp"

namespace std {
  constexpr bool operator < (std::complex<int> const& lhs, std::complex<int> const& rhs) { return lhs.real() == rhs.real() ? lhs.imag() < rhs.imag() : lhs.real() < rhs.real(); }
}

class CityState : public GameState
{
private:

  using Tag           = int;
  using Position      = std::complex<int>;
  using ItemIDOnDB    = int;
  using Probability   = int;
  using Suspicious    = int;
  using IsUnique      = bool;
  using Ratio         = double;
  using Drawer        = std::tuple<Tag, Position, ItemIDOnDB, Suspicious, IsUnique>;
  using Personality   = std::vector<std::pair<int, int>>;
  using ScenarioName  = std::string;
  using Inhabitant    = std::pair<Personality, ScenarioName>;

private:

  WorldMap::AreaID        cityID_;
  std::vector<Inhabitant> inhabitants_;
  TimeManager::AufmTime   startAufmTimeOfStaying_;
  double                  minLengthOfStay_;
  Ratio                   wealthRatio_;
  double                  additionalLengthOfStay_;
  std::string             cityName_;
  std::string             enterCityText_;
  int                     updateNum_;
  std::vector<std::pair<Probability, Drawer>> drawers_;
  Position                currentPosition_;

private:

  auto loadDrawersData(picojson::array&);
  auto loadInhabitantsData(picojson::array&);
  auto loadCityDataFromJSON();
  auto contactInhabitant();
  auto lootDrawer();
  auto enterShop(Misc::EquipmentType);
  auto enterInn();
  
  auto isMatchPersonality(Personality const&);

public:

	CityState(WorldMap::AreaID const&);
	inline auto getStateType() const -> StateType { return StateType::CityState; }
  inline auto getCurrentPosition() const { return currentPosition_; }
	
	auto update() -> void;
};