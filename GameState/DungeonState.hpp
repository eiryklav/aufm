#pragma once

#include <iostream>
#include <memory>
#include <unordered_map>
#include <boost/optional.hpp>
#include "../Lib/picojson.h"

#include "WorldMap.hpp"

namespace DungeonMap {
  class ForkPoint;
  using ForkPointID = int;
}

class DungeonState : public GameState
{
private:
  WorldMap::AreaID                                              dungeonID_;
  std::string                                                   enterDungeonText_   = "default";
  std::unordered_map<DungeonMap::ForkPointID, picojson::array>  triggerCommandMap_;

private:
  auto loadDungeonDataFromJSON();
  auto arriveAtForkPoint(std::shared_ptr<DungeonMap::ForkPoint> const&);
public:
  DungeonState(WorldMap::AreaID const&);
	auto update() -> void;
	auto getStateType() const -> StateType { return StateType::DungeonState; }
};