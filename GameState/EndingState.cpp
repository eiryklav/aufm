#include "EndingState.hpp"
#include "ChrData.hpp"

EndingState::EndingState(EndCondition condition)
{
		switch(condition) {
      case EndCondition::EndOfScenario:
        GameMgr->getLogger()->pushMessage(Printer::Messages::endConditionEndOfScenarioSuccess());
        break;
        
      case EndCondition::PlayerHasDied:
        GameMgr->getLogger()->pushMessage(Printer::Messages::endConditionPlayerHasDied());
        break;
        
      case EndCondition::TimeLimitExceeded:
        GameMgr->getLogger()->pushMessage(Printer::Messages::endConditionTimeLimitExceeded());
        break;
        
      case EndCondition::DeadEndOfRoad:
        GameMgr->getLogger()->pushMessage(Printer::Messages::endConditionDeadEndOfRoad(GameMgr->getPlayer()->getName()));
        break;
        
      case EndCondition::DeadEndInDungeon:
        GameMgr->getLogger()->pushMessage(Printer::Messages::endConditionDeadEndInDungeon(GameMgr->getPlayer()->getName()));
        break;
    }
}

auto EndingState::update()
  -> void
{
  GameMgr->clearStateStack();
}