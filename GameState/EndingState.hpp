#pragma once

#include "../Game/Game.hpp"
#include "GameState.hpp"

#include <iostream>

enum class EndCondition
{
	EndOfScenario,
	PlayerHasDied,
  DeadEndOfRoad,
  DeadEndInDungeon,
	TimeLimitExceeded
};

class EndingState : public GameState
{
public:
  EndingState(EndCondition condition);

  auto update() -> void;
	auto getStateType() const -> StateType { return StateType::EndingState; }
};