//
//  Character.swift
//  RelationalController
//
//  Created by moti on 2015/09/29.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import SpriteKit

struct CharacterIdentifiers {
  static let Yosuga = "Yosuga"
}

public class Character {
  
  let id:                         Int
  let node:                       SKNode
  weak var relationalController:  RelationalController!
  private let ai:                 AIProtocol
  var influencesOf:               [Int:[Influence]]
  public var intimacyWithID:      [Int:Intimacy]
  
  private func AIWithCharacterIdentifier(characterIdentifier name: String) -> AIProtocol {
    switch name {
    case CharacterIdentifiers.Yosuga: return YosugaAI()
    default:
      fatalError("No such character identifier: \(name)")
    }
  }
  
  private func NodeWithCharacterIdentifier(characterIdentifier name: String) -> SKNode {
    switch name {
    case CharacterIdentifiers.Yosuga:  return SKShapeNode(ellipseOfSize: CGSizeMake(8, 8))
    default:
      fatalError("No such character identifier: \(name)")
    }
  }
  
  public init(id: Int, characterIdentifier chrName: String) {
    self.id = id
    self.node = NodeWithCharacterIdentifier(characterIdentifier: chrName)
    self.node.constraints = []
    self.ai = AIWithCharacterIdentifier(characterIdentifier: chrName)
    self.intimacyWithID     = [:]
    self.influencesOf = [:]
  }
  
  func update() {
    for (id, _) in relationalController.characters {
      for influence in influencesOf[id]! {
        intimacyWithID[id]?.influencedBy(influence)
      }
      intimacyWithID[id]?.updateBonds()
    }
    ai.update()
  }
  
  func reactTo(utterance: Utterance) {
    ai.reactToUtterance(utterance)
  }
  
}
