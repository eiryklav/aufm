//
//  CharacterManager.swift
//  RelationalController
//
//  Created by moti on 2015/09/29.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import SpriteKit

class AutoIncrementedID {
  
  private static var nextID_: Int = 0
  static var nextID: Int {
    get {
      return nextID_++
    }
  }
  
}

public class RelationalController {
  
  var characters: [Int:Character]
  
  weak var delegate: RelationalControllerDelegate?
  
  public init() {
    characters = [:]
  }
  
  func lowerLimitWithFamiliarity(familiarity: Double) -> CGFloat {
    return CGFloat(min(0.0, 100 * (1.0 - familiarity) - 10.0))
  }
  
  func upperLimitWithFamiliarity(familiarity: Double) -> CGFloat {
    var upperLimit = 100000.0
    if familiarity < 0.5 {
      upperLimit = max(10.0, 100.0 * familiarity)
    }
    return CGFloat(upperLimit)
  }
  
  public func addCharacter(characterIdentifier chrName: String, initialIntimacyStrength: Double) {
    
    let newCharacter = Character(id: AutoIncrementedID.nextID, characterIdentifier: chrName)
    
    newCharacter.relationalController = self
    
    // 他社との初期関係を設定
    for (_, character) in characters {
      newCharacter.intimacyWithID[character.id] = Intimacy(
        owner: newCharacter,
        target: character,
        initialStrength: initialIntimacyStrength
      )
    }
    
  }
  
  public func update() {
    
    for (_, character) in characters {
      character.update()
    }
    
  }
  
}

public protocol RelationalControllerDelegate: class {
  func currentTime() -> Double
}