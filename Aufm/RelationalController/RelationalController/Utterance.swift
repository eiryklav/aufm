//
//  Language.swift
//  RelationalController
//
//  Created by moti on 2015/10/10.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit

public class Utterance {
  
  public enum Side { case Leftside, Upside, Rightside, Downside }
  
  let speakerID: Int
  let side: Side
  let position: CGPoint
  
  init(speakerID: Int, side: Side, position: CGPoint) {
    self.speakerID = speakerID
    self.side = side
    self.position = position
  }
  
}
