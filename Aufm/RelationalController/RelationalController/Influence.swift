//
//  Influence.swift
//  RelationalController
//
//  Created by moti on 2015/10/10.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit

protocol Influence {
  weak var owner: Character? { get set }
  weak var target: Character? { get set }
  func value() -> Double  // strength に追加する値
}

class GoodInfluenceExample : Influence {
  
  weak var owner: Character?
  weak var target: Character?
  
  init(owner: Character, target: Character) {
    self.owner = owner
  }
  
  func value() -> Double {
    return +1.0
  }
  
}

class BadInfluenceExample : Influence {
  
  weak var owner: Character?
  weak var target: Character?
  
  init(owner: Character, target: Character) {
    self.owner = owner
    self.target = target
  }
  
  func value() -> Double {
    return -1.0
  }
  
}