//
//  ViewController.swift
//  RelationalController
//
//  Created by moti on 2015/09/29.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit
import SpriteKit

func + (lhs: CGPoint, rhs: CGPoint) -> CGPoint { return CGPointMake(lhs.x + rhs.x, lhs.y + rhs.y) }
func - (lhs: CGPoint, rhs: CGPoint) -> CGPoint { return CGPointMake(lhs.x - rhs.x, lhs.y - rhs.y) }
func * (lhs: CGPoint, rhs: CGFloat) -> CGPoint { return CGPointMake(lhs.x * rhs, lhs.y * rhs) }
func / (lhs: CGPoint, rhs: CGFloat) -> CGPoint { return CGPointMake(lhs.x / rhs, lhs.y / rhs) }

class ViewController: UIViewController {
  
  let relationalController = RelationalController()
  let PlayerID = 1
  
  override func loadView() {
    view = RCView(frame: UIScreen.mainScreen().bounds)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let skview  = view as! RCView
    let skscene = SKScene(size: view.frame.size)
    
    skview.presentScene(skscene)
    
    let heroNode = SKShapeNode(ellipseOfSize: CGSizeMake(10, 10))
    relationalController.addCharacter(character: Character(id: 1, node: heroNode), initialFamiliarity: 0.5)
     heroNode
    heroNode.position = skview.center + CGPointMake(-10, 0)
    heroNode.fillColor = UIColor.redColor()
    skscene.addChild(heroNode)
    
    let yosugaNode = SKShapeNode(ellipseOfSize: CGSizeMake(8, 8))
    relationalController.addCharacter(character: Character(id: 2, node: yosugaNode), initialFamiliarity: 0.5)
    yosugaNode.position = skview.center + CGPointMake(+10, 0)
    skscene.addChild(yosugaNode)
    
    skview.heroNode = heroNode
    
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }


}

