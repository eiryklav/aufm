//
//  RCView.swift
//  RelationalController
//
//  Created by moti on 2015/10/09.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit
import SpriteKit

class RCView : SKView {
  
  private var startPosition: CGPoint!
  weak var heroNode: SKNode!
  
  required init(coder: NSCoder) { fatalError() }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
    startPosition = touches.first!.locationInView(self)
  }
  
  override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
    let pt  = touches.first!.locationInView(self)
    let v   = pt - startPosition
    
    heroNode.position = heroNode.position + CGPointMake(v.x, -v.y)
    
    startPosition = pt
  }
  
  override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
    
  }
  
}
