//
//  AIProtocol.swift
//  RelationalController
//
//  Created by moti on 2015/10/12.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit

protocol AIProtocol {
  weak var owner: Character? { get set }
  func update()
  func reactToUtterance(utterance: Utterance)
}