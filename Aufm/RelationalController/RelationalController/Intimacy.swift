//
//  Intimacy.swift
//  RelationalController
//
//  Created by moti on 2015/10/10.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit

public class Intimacy {
  
  weak var owner:  Character?
  weak var target: Character?
  
  private var strength_:  Double  = 0.0
  public var strength:    Double {
    get {
      return strength_
    }
  }
  
  private var diversity: Int {
    get {
      return owner!.influencesOf[target!.id]!.count * target!.influencesOf[owner!.id]!.count
    }
  }
  
  public var duration: Double {
    get {
      return owner!.relationalController.delegate!.currentTime()
    }
  }
  
  /*
    瞬時的な親密度（3要素を引数とする関数）
  */
  public var momentaryIntimacy: Double {
    get {
      return strength * Double(diversity) // × 比例定数
    }
  }
  
  /*
    絆：親密度の累積和
  */
  private var bonds_: Double = 0.0
  public var bonds: Double {
    get {
      return bonds_
    }
  }
  
  func updateBonds() {
    bonds_ += momentaryIntimacy / duration // × 比例定数
  }
  
  func influencedBy(influence: Influence) {
    strength_ += influence.value()
  }
  
  init(owner: Character, target: Character, initialStrength: Double) {
    self.owner = owner
    self.target = target
    // はじめの状態で相手からどれだけ良い影響を受けるか
    self.strength_ = initialStrength
  }
  
}
