//
//  ViewController.swift
//  UpperTabButtonView
//
//  Created by moti on 2015/09/12.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit
import UpperTabButtonView

class ViewController: UIViewController {
  
  private var upperTabButtonView: UpperTabButtonView!
  private let data = ["MainStatus", "StatusGraph", "Item", "Money"]
  private var views = [UIView]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    view.backgroundColor = UIColor.whiteColor()
    
    upperTabButtonView = UpperTabButtonView(frame: view.frame)
    upperTabButtonView.dataSource = self
    upperTabButtonView.delegate   = self
    upperTabButtonView.reload()
    view.addSubview(upperTabButtonView)
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

}

extension ViewController : UpperTabButtonViewDataSource {
  func numberOfTabButtonsInUpperTabButtonView() -> Int {
    return data.count
  }
  func upperTabButtonView(uppetTabButtonView: UpperTabButtonView, titleForTabButtonWithIndex index: Int) -> String {
    return data[index]
  }
  func upperTabButtonView(uppetTabButtonView: UpperTabButtonView, subViewForIndex index: Int) -> UIView {
    
    if views.count == 0 {
      var v = UIView(frame: upperTabButtonView.subViewFrame)
      v.backgroundColor = UIColor(red: 1.0, green: 0.0, blue: 0.0, alpha: 0.2)
      views.append(v)
      v = UIView(frame: upperTabButtonView.subViewFrame)
      v.backgroundColor = UIColor(red: 0.0, green: 1.0, blue: 0.0, alpha: 0.2)
      views.append(v)
      v = UIView(frame: upperTabButtonView.subViewFrame)
      v.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 1.0, alpha: 0.2)
      views.append(v)
      v = UIView(frame: upperTabButtonView.subViewFrame)
      v.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 0.0, alpha: 0.2)
      views.append(v)
    }
    
    return views[index]
  }

}

extension ViewController : UpperTabButtonViewDelegate {
  
  func backgroundViewInUpperTabButtonView() -> UIView {
    let bgImgView = UIImageView(image: UIImage(named: "background")!)
    bgImgView.frame.size = UIScreen.mainScreen().bounds.size
    return bgImgView
  }
  
  func upperTabButtonView(upperTabButtonView: UpperTabButtonView, didPushTabButtonWithIndex index: Int) {
    print(data[index])
  }
}