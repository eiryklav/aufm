//
//  UpperTabButtonView.swift
//  AufmData
//
//  Created by moti on 2015/09/06.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit

class TransparentTabButton : UIButton {
  required init?(coder aDecoder: NSCoder) { fatalError() }
  override init(frame: CGRect) {
    super.init(frame: frame)
  }
}

public class UpperTabButtonView : UIView {
  
  weak public var dataSource: UpperTabButtonViewDataSource?
  weak public var delegate:   UpperTabButtonViewDelegate?
  
  public var subViewFrame:    CGRect!
  public var selectedIndex:   Int = 0 // 外からimmutableにするにはどうすればよいか
  public var hasCloseButton:  Bool  = true
  
  public var numberOfTabButtons: Int {
    get {
      guard let dataSource = dataSource else { print("dataSource is unset") ; return 0 }
      return dataSource.numberOfTabButtonsInUpperTabButtonView()
    }
  }
  
  required public init?(coder aDecoder: NSCoder) { fatalError() }
  
  override public init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  public func reload() {
    
    guard let delegate = delegate else { print("delegate is unset") ; return }
    
    //
    closeButtonWidth = hasCloseButton ? frame.width * 0.1 : 0
    
    // MARK: Menubar
    
    // MARK: Menubar Background
    backgroundView = delegate.backgroundViewInUpperTabButtonView()
    addSubview(backgroundView)
    
    let barRawImage = UIImage(named: "menubar")!
      .scaleAspectFitResize(CGSizeMake(UIScreen.mainScreen().bounds.width, 10000))
    
    let barRawBgImage = UIImage.imageFromView(
      backgroundView,
      rect: CGRectMake(0, 0, barRawImage.size.width, barRawImage.size.height)
    )
    
    let barImage = UIImage.drawImage(
      barRawImage,
      inImage: barRawBgImage,
      atPoint: CGPointZero
    )
    
    barBackgroundImageView = UIImageView(image: barImage)
    backgroundView.addSubview(barBackgroundImageView)
    
    
    // MARK: Menubar Foreground SelectImage
    
    let selectRawImage = UIImage(named: "select")!
      .scaleAspectFitResize(CGSizeMake(1000, barBackgroundImageView.frame.size.height))
    
    buttonWidth = selectRawImage.size.width
    buttonHeight = barBackgroundImageView.frame.height
    
    leftSpaceWidth = (frame.width - buttonWidth * CGFloat(numberOfTabButtons)) / 2
    
    if leftSpaceWidth <= 0 {
      print("UpperTabButtonView - Warning: leftSpaceWidth <= 0\nSelection image's width should be shorter")
    }
    
    guard numberOfTabButtons > 0 else {
      print("UpperTabButtonView - Error: numberOfTabButtons = 0")
      return
    }
    
    selectBackgroundImageViews = []
    for index in 0..<numberOfTabButtons {
      
      let selectRawBgImage = UIImage.imageFromView(
        barBackgroundImageView,
        rect: CGRectMake(
          leftSpaceWidth + buttonWidth * CGFloat(index),
          0,
          buttonWidth,
          buttonHeight)
      )
      
      let selectImage = UIImage.drawImage(
        selectRawImage,
        inImage: selectRawBgImage,
        atPoint: CGPointZero
      )
      
      let view = UIImageView(image: selectImage)
      view.frame.origin.x = leftSpaceWidth + buttonWidth * CGFloat(index)
      selectBackgroundImageViews.append(view)
      barBackgroundImageView.addSubview(view)
      view.hidden = true
      
      let label = UILabel()
      label.font = UIFont(name: "LucidaGrande", size: 36.0)
      label.text = titleForTabButtonWithIndex(index)
      label.textColor = UIColor.whiteColor()
      label.shadowOffset = CGSizeMake(1, 1)
      label.shadowColor = UIColor.blackColor()
      label.frame = view.frame
      label.textAlignment = .Center
      barBackgroundImageView.addSubview(label)
      
      let button = TransparentTabButton(frame: view.frame)
      button.tag = index
      button.addTarget(self, action: "didPushTabButton:", forControlEvents: .TouchUpInside)
      addSubview(button)
    }
    
    subViewFrame = CGRectMake(0, barBackgroundImageView.frame.height, frame.width, frame.height - barBackgroundImageView.frame.height)
    subView = subViewForIndex(selectedIndex)

    backgroundView.addSubview(subView)
    
    selectBackgroundImageViews[selectedIndex].hidden = false
    didPushTabButtonWithIndex(selectedIndex)
  }
  
  func didPushTabButton(sender: TransparentTabButton) {
    selectBackgroundImageViews[selectedIndex].hidden = true
    selectedIndex = sender.tag
    selectBackgroundImageViews[selectedIndex].hidden = false
    didPushTabButtonWithIndex(sender.tag)
    subView = subViewForIndex(selectedIndex)
    backgroundView.addSubview(subView)
  }
  
  public func subViewForIndex(index: Int) -> UIView {
    return dataSource?.upperTabButtonView(self, subViewForIndex: index) ?? UIView()
  }
  
  public func titleForTabButtonWithIndex(index: Int) -> String {
    return dataSource?.upperTabButtonView(self, titleForTabButtonWithIndex: index) ?? ""
  }
  
  public func didPushTabButtonWithIndex(index: Int) {
    delegate?.upperTabButtonView(self, didPushTabButtonWithIndex: index)
  }
  
  // MARK: - private -
  private var backgroundView:             UIView!
  private var barBackgroundImageView:     UIImageView!
  private var tabView:                    UIView!
  private var subView:                    UIView!
  private var buttonWidth:                CGFloat!
  private var buttonHeight:               CGFloat!
  private var leftSpaceWidth:             CGFloat!
  private var closeButtonWidth:           CGFloat!
  private var selectBackgroundImageViews: [UIImageView]!
}

public protocol UpperTabButtonViewDataSource: class {
  func numberOfTabButtonsInUpperTabButtonView() -> Int
  func upperTabButtonView(uppetTabButtonView: UpperTabButtonView, titleForTabButtonWithIndex index: Int) -> String
  func upperTabButtonView(uppetTabButtonView: UpperTabButtonView, subViewForIndex index: Int) -> UIView
}

public protocol UpperTabButtonViewDelegate: class {
  func upperTabButtonView(upperTabButtonView: UpperTabButtonView, didPushTabButtonWithIndex index: Int)
  func backgroundViewInUpperTabButtonView() -> UIView
}