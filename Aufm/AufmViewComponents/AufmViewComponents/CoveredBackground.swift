//
//  CoveredBackground.swift
//  AufmCommonComponentViews
//
//  Created by moti on 2015/09/01.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmVisualSoundEffect

public protocol CoveredBackgroundDelegate: class {
  func didTouch()
}

public class CoveredBackground : UIImageView {
  
  public weak var delegate: CoveredBackgroundDelegate?
  private var popWindowFrame: CGRect
  
  required public init?(coder aDecoder: NSCoder) { fatalError() }
  
  public init(darkenImage: UIImage, popWindowFrame: CGRect) {
    self.popWindowFrame = popWindowFrame
    super.init(frame: UIScreen.mainScreen().bounds)
    image = darkenImage
  }
  
  override public func hitTest(point: CGPoint, withEvent event: UIEvent?) -> UIView? {
    guard   CGRectContainsPoint(bounds, point)          else { return nil }
    guard  !CGRectContainsPoint(popWindowFrame, point)  else { return nil }
    return self
  }
  
  override public func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
    Audio.sharedInstance.playSE("click")
    delegate?.didTouch()
  }
}
