//
//  RadioImageButton.swift
//  AufmCommonComponentViews
//
//  Created by moti on 2015/09/30.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit

public class RadioImageButton: UIButton, BTProtocol,
  BGNone, BTLabelAboveSwitchableLayer, SEButtonTouchClick, BTTouchActionNoEffect {
  
  public var label:           UILabel!
  public var layer_:          CALayer!
  public var selectedLayer:   CALayer!
  public let classID:         String
  public let buttonID:        Int
  
  weak public var delegate: RadioButtonDelegate?
  
  required public init(coder: NSCoder) { fatalError() }
  
  public init(
    frame: CGRect,
    classID: String,
    buttonID: Int,
    unselectedImage: UIImage,
    selectedImage: UIImage,
    text: String = "",
    selectedText: String = ""
  ) {
    self.classID      = classID
    self.buttonID     = buttonID
    self.text         = text
    self.selectedText = selectedText
    super.init(frame: frame)
    addTarget(self, action: "didPushButton:", forControlEvents: .TouchUpInside)
  }
  
  func didPushButton(sender: RadioImageButton) {
    delegate?.didPushButtonWithClassID(sender.classID, buttonID: sender.buttonID)
  }
  
  // MARK: - private -
  private var text: String
  private var selectedText: String
  
}

public protocol RadioButtonDelegate: class {
  func didPushButtonWithClassID(classID: String, buttonID: Int)
}