//
//  UIColorExtension.swift
//  RSBarcodesSample
//
//  Created by R0CKSTAR on 6/13/14.
//  Copyright (c) 2014 P.D.Q. All rights reserved.
//

import UIKit

public extension UIColor {
    
  public class func blackStyleBorderColor() -> UIColor {
    return UIColor(
      red:    151.0/255.0,
      green:  151.0/255.0,
      blue:   151.0/255.0,
      alpha:  180.0/255.0)
  }
  
  public class func blackStyleTerminalBackgroundColor() -> UIColor {
    return UIColor(red: 15.0/255.0, green: 15.0/255.0, blue: 15.0/255.0, alpha: 0.3)
  }
  
  public class func blackStyleBackgroundColor() -> UIColor {
    return UIColor.clearColor()
  }
  
  public class func whiteStyleBorderColor() -> UIColor {
    return UIColor(
      red:    151.0/255.0,
      green:  151.0/255.0,
      blue:   151.0/255.0,
      alpha:  180.0/255.0)
  }
  
  public class func whiteStyleBackgroundColor() -> UIColor {
    return UIColor(red: 216.0/255.0, green: 216.0/255.0, blue: 216.0/255.0, alpha: 0.5)
  }
  
  public class func deepWhiteStyleBackgroundColor() -> UIColor {
    return UIColor(red: 216.0/255.0, green: 216.0/255.0, blue: 216.0/255.0, alpha: 0.8)
  }
  
  public class func whiteStyleSelectedTableCellColor() -> UIColor {
    return UIColor(rgba: "#aa8888ff")
  }
  
  public class func whiteStyleSelectedBarButtonColor() -> UIColor {
    return UIColor(red: 167.0/255.0, green: 157.0/255.0, blue: 146.0/255.0, alpha: 0.4)
  }
  
  public class func whiteStyleUnselectedBarButtonColor() -> UIColor {
    return UIColor(red: 155.0/255.0, green: 155.0/255.0, blue: 155.0/255.0, alpha: 0.9)
  }
  
  public class func whiteStyleSelectedRadioButtonColor() -> UIColor {
    return UIColor(red: 167.0/255.0, green: 157.0/255.0, blue: 146.0/255.0, alpha: 0.4)
  }
  
  public class func whiteStyleUnselectedRadioButtonColor() -> UIColor {
    return UIColor(red: 216.0/255.0, green: 216.0/255.0, blue: 216.0/255.0, alpha: 0.8)
  }
  
  public class func whiteStyleHighlightedBarButtonColor() -> UIColor {
    return UIColor(red: 167.0/255.0, green: 157.0/255.0, blue: 146.0/255.0, alpha: 1.0)
  }
  
  public class func whiteStyleTimelineTimeBackgroundColor() -> UIColor {
    return UIColor(red: 216.0/255.0, green: 216.0/255.0, blue: 216.0/255.0, alpha: 0.42)
  }
  
  public class func whiteStyleTimelineWayOfUseBackgroundColor() -> UIColor {
    return UIColor(red: 216.0/255.0, green: 216.0/255.0, blue: 216.0/255.0, alpha: 0.5)
  }
  
  public convenience init(rgba: String) {
    var red:   CGFloat = 0.0
    var green: CGFloat = 0.0
    var blue:  CGFloat = 0.0
    var alpha: CGFloat = 1.0
    
    if rgba.hasPrefix("#") {
      let index   = rgba.startIndex.advancedBy(1)
      let hex     = rgba.substringFromIndex(index)
      let scanner = NSScanner(string: hex)
      var hexValue: CUnsignedLongLong = 0
      if scanner.scanHexLongLong(&hexValue) {
        switch (hex.characters.count) {
        case 3:
          red   = CGFloat((hexValue & 0xF00) >> 8)       / 15.0
          green = CGFloat((hexValue & 0x0F0) >> 4)       / 15.0
          blue  = CGFloat(hexValue & 0x00F)              / 15.0
        case 4:
          red   = CGFloat((hexValue & 0xF000) >> 12)     / 15.0
          green = CGFloat((hexValue & 0x0F00) >> 8)      / 15.0
          blue  = CGFloat((hexValue & 0x00F0) >> 4)      / 15.0
          alpha = CGFloat(hexValue & 0x000F)             / 15.0
        case 6:
          red   = CGFloat((hexValue & 0xFF0000) >> 16)   / 255.0
          green = CGFloat((hexValue & 0x00FF00) >> 8)    / 255.0
          blue  = CGFloat(hexValue & 0x0000FF)           / 255.0
        case 8:
          red   = CGFloat((hexValue & 0xFF000000) >> 24) / 255.0
          green = CGFloat((hexValue & 0x00FF0000) >> 16) / 255.0
          blue  = CGFloat((hexValue & 0x0000FF00) >> 8)  / 255.0
          alpha = CGFloat(hexValue & 0x000000FF)         / 255.0
        default:
          print("Invalid RGB string, number of characters after '#' should be either 3, 4, 6 or 8", terminator: "")
        }
      } else {
        print("Scan hex error")
      }
    } else {
      print("Invalid RGB string, missing '#' as prefix", terminator: "")
    }
    self.init(red:red, green:green, blue:blue, alpha:alpha)
  }
}