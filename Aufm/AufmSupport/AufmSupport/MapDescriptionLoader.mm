//
//  MapDescriptionLoader.m
//  Aufm
//
//  Created by moti on 2015/06/23.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

#import "MapDescriptionLoader.h"
#import <UIKit/UIKit.h>
#import "../../../Lib/picojson.h"
#import "../../../Support/Misc.hpp"

@implementation MapDescriptionLoader : NSObject

-(id)init {
  return [super init];
}

-(void)loadWorldConfigFromJSON:(NSString*)jsonString {
  picojson::value v;
  auto err = picojson::parse(v, [jsonString UTF8String]);
  if(!err.empty()) { AUFM_ASSERT(false, "Can't load jsonString in MapDescriptionLoader"); }
  
  auto& o = v.get<picojson::object>();
  
  _fullName = [NSString stringWithUTF8String: o["FullName"].get<std::string>().c_str()];
  _shortName = [NSString stringWithUTF8String: o["ShortName"].get<std::string>().c_str()];
  _subName = [NSString stringWithUTF8String: o["SubName"].get<std::string>().c_str()];
  _flavor = [NSString stringWithUTF8String: o["Flavor"].get<std::string>().c_str()];
  _areaButtonIDOriginArray = [NSMutableArray array];
  auto& arr = o["AreaButton"].get<picojson::array>();
  for(auto& val: arr) {
    auto& o = val.get<picojson::object>();
    auto& farr = o["Origin"].get<picojson::array>();
    CGPoint origin = CGPointMake(farr[0].get<double>(), farr[1].get<double>()); // 実際の入力はint
    [_areaButtonIDOriginArray addObject: [NSArray arrayWithObjects: [NSString stringWithUTF8String: o["Area"].get<std::string>().c_str()], [NSValue valueWithCGPoint: origin], nil]];
  }
}

// WorldMap & Dungeon 共用
-(void)loadEnemiesInEdgeFromJSON:(NSString*)jsonString {
  picojson::value v;
  auto err = picojson::parse(v, [jsonString UTF8String]);
  if(!err.empty()) { AUFM_ASSERT(false, "Can't load jsonString in MapDescriptionLoader"); }
  
  
  auto& o = v.get<picojson::object>();
  
  // TargetEnemies
  _targetEnemies = [NSMutableArray array];
  auto& tArr = o["TargetEnemies"].get<picojson::array>();
  for(auto& val: tArr) {
    auto& o = val.get<picojson::object>();
    auto ID = static_cast<int>(o["ID"].get<double>());
    auto isUnwrapped = o["IsUnwrapped"].get<bool>();
    [_targetEnemies addObject: [NSArray arrayWithObjects: [NSNumber numberWithInt: ID], [NSNumber numberWithBool: isUnwrapped], nil]];
  }
  
  auto& arr = o["Graph"].get<picojson::array>();
  
  // [fromAreaID : [(toAreaID, enemiesSet)]]
  _enemiesGraph = [NSMutableDictionary dictionary];
  
  // Make graph 'from'
  for(auto& val: arr) {
    auto& o = val.get<picojson::object>();
    NSString* from = [NSString stringWithUTF8String: o["From"].get<std::string>().c_str()];
    NSString* to = [NSString stringWithUTF8String: o["To"].get<std::string>().c_str()];
    auto isBidir = o["IsBidirectional"].get<bool>();
    if([_enemiesGraph objectForKey: from] == nil) {
      [_enemiesGraph setObject: [NSMutableArray array] forKey: from];
      if(isBidir) {
        if([_enemiesGraph objectForKey: to] == nil) {
          [_enemiesGraph setObject: [NSMutableArray array] forKey: to];
        }
      }
    }
  }
  
  
  for(auto& val: arr) {
    auto& o = val.get<picojson::object>();
    NSString* from = [NSString stringWithUTF8String: o["From"].get<std::string>().c_str()];
    NSString* to = [NSString stringWithUTF8String: o["To"].get<std::string>().c_str()];
    auto isBidir = o["IsBidirectional"].get<bool>();
    auto& arr = o["EncountEnemies"].get<picojson::array>();
    NSMutableSet* st = [NSMutableSet set];
    for(auto& val: arr) {
      auto& o = val.get<picojson::object>();
      auto& arr = o["EnemyGroup"].get<picojson::array>();
      for(auto& val: arr) {
        auto item = static_cast<int>(val.get<double>());
        [st addObject: [NSNumber numberWithInt: item]];
      }
    }
    [[_enemiesGraph objectForKey: from] addObject: [NSArray arrayWithObjects: to, st, nil]];
    if(isBidir) {
      [[_enemiesGraph objectForKey: to] addObject: [NSArray arrayWithObjects: from, st, nil]];
    }
  }
}

-(void)loadCityDataFromJSON:(NSString*)jsonString {
  picojson::value v;
  auto err = picojson::parse(v, [jsonString UTF8String]);
  if(!err.empty()) { AUFM_ASSERT(false, "Can't load jsonString in MapDescriptionLoader"); }
  
  auto& o = v.get<picojson::object>();
  
  _cityName = [NSString stringWithUTF8String: o["CityName"].get<std::string>().c_str()];
  _cityFlavor = [NSString stringWithUTF8String: o["Flavor"].get<std::string>().c_str()];
}

@end