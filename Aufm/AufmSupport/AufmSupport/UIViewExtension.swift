//
//  UIViewExtension.swift
//  Aufm
//
//  Created by moti on 2015/07/05.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

extension UIView {
  
  func blurEffect(frame: CGRect, style: UIBlurEffectStyle) {
    let blueEffect = UIBlurEffect(style: style)
    let effectView = UIVisualEffectView(effect: blueEffect)
    effectView.frame = frame
    self.addSubview(effectView)
  }

}
