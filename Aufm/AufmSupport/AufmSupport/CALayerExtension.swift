//
//  CALayerExtension.swift
//  Aufm
//
//  Created by moti on 2015/07/30.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import Foundation
import UIKit

extension CALayer {
  public func setShadowForButton() {
    self.shadowOpacity  = 0.8
    self.shadowRadius   = 15.0
  }
  
  public func setDeepShadowForButton() {
    self.shadowOpacity  = 0.9
    self.shadowRadius   = 15.0
  }
  
  public func setShadowWithOffset(rawSize: CGSize) {
    self.shadowOpacity  = 0.9
    self.shadowRadius   = 3.0
    self.shadowOffset   = CGSizeMake(rawSize.width * 0.025, rawSize.height * 0.025)
  }
  
  public func setShadowForView() {
    self.shadowOpacity  = 0.3
    self.shadowRadius   = 15.0
  }
}
