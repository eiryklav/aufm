//
//  Audio.swift
//  Aufm
//
//  Created by moti on 2015/06/16.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AVFoundation

public class Audio {
  
  public static let sharedInstance = Audio()
  
  private var bgmPlayer: AVAudioPlayer!
  private var sePlayerDictionary = [String:AVAudioPlayer]()
  
  var bgmVolume:  Float = 0.5
  var seVolume:   Float = 0.5
  
  private var isOnBGM = false // default true for release
  private var isOnSE  = true
  
  private init() {}
  
}

protocol AudioProtocol {
  func onBGM()
  func offBGM()
  func onSE()
  func offSE()
  func playBGM(fname: String)
  func playSE(fname: String)
  func turnBGMVolumeDown()
  func returnBGMVolumeToOriginal()
}

extension Audio : AudioProtocol {
  
  public func onBGM() { isOnBGM = true ; bgmPlayer.play() }
  public func offBGM() { isOnBGM = false ; bgmPlayer.stop() }
  public func onSE() { isOnSE = true }
  public func offSE() { isOnSE = false }
  
  public func playBGM(fname: String) {
    let soundFilePath: NSString = NSBundle.mainBundle().pathForResource(fname, ofType: "caf")!
    let fileURL: NSURL = NSURL(fileURLWithPath: soundFilePath as String)
    bgmPlayer = try! AVAudioPlayer(contentsOfURL: fileURL)
    bgmPlayer.volume = bgmVolume
    bgmPlayer.numberOfLoops = -1
    if isOnBGM { bgmPlayer.play() }
  }
  
  public func playSE(fname: String) {
    // 効果音ミュートかどうかの判定があるため、インスタンスメソッドとしている
    let soundFilePath: NSString = NSBundle.mainBundle().pathForResource(fname, ofType: "caf")!
    let fileURL = NSURL(fileURLWithPath: soundFilePath as String)
    if sePlayerDictionary[fname] == nil {
      sePlayerDictionary[fname] = try! AVAudioPlayer(contentsOfURL: fileURL)
    }
    let player = sePlayerDictionary[fname]!
    player.volume = seVolume
    if player.playing {
      player.currentTime = 0.0
    }
    else {
      if isOnSE { player.play() }
    }
  }
  
  public func turnBGMVolumeDown() {
    if bgmPlayer.playing {
      bgmVolume = bgmPlayer.volume
      bgmPlayer.volume *= 0.5
    }
  }
  
  public func returnBGMVolumeToOriginal() {
    bgmPlayer.volume = bgmVolume
  }
}
