//
//  SKNode.swift
//  Aufm
//
//  Created by moti on 2015/07/20.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import SpriteKit

extension SKNode {
  class func unarchiveFromFile(file : NSString) -> SKNode? {
    if let myPath = NSBundle.mainBundle().pathForResource(file as String, ofType: "sks") {
      let sceneData = try? NSData(contentsOfFile: myPath, options: .DataReadingMappedIfSafe)
      let myArchiver = NSKeyedUnarchiver(forReadingWithData: sceneData!)
      
      myArchiver.setClass(self.classForKeyedUnarchiver(), forClassName: "SKScene")
      let myScene = myArchiver.decodeObjectForKey(NSKeyedArchiveRootObjectKey) as! SKScene
      myArchiver.finishDecoding()
      return myScene
    } else {
      return nil
    }
  }
}