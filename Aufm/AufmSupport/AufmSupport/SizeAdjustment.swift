//
//  SizeAdjustment.swift
//  Aufm
//
//  Created by moti on 2015/06/17.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

struct iPhone6 {
  static let size: CGSize = CGSizeMake(667.0, 375.0)
}

public struct Screen {
  public static let size: CGSize = CGSizeMake(UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height)
}

public func adjustX(x: CGFloat) -> CGFloat {
  let widthRatio:  CGFloat = Screen.size.width / iPhone6.size.width
  return x * widthRatio
}

public func adjustY(y: CGFloat) -> CGFloat {
  let heightRatio: CGFloat = Screen.size.height / iPhone6.size.height
  return y * heightRatio
}

public func adjustPoint(point: CGPoint) -> CGPoint { return CGPointMake(adjustX(point.x), adjustY(point.y)) }

public extension CGPoint {
  public func adjustedPoint() -> CGPoint { return CGPointMake(adjustX(x), adjustY(y)) }
}

public func adjustSize(size: CGSize) -> CGSize { return CGSizeMake(adjustX(size.width), adjustY(size.height)) }

public extension CGSize {
  public func adjustedSize() -> CGSize { return CGSizeMake(adjustX(width), adjustY(height)) }
}

public func adjustFontSize(size: CGFloat) -> CGFloat {
  return size * Screen.size.width / iPhone6.size.width
}

public func centerOf(selfLength selfLength: CGFloat, superLength: CGFloat) -> CGFloat {
  return CGFloat((superLength-selfLength)/2.0)
}

public func centerOf(selfSize selfSize: CGSize, superSize: CGSize) -> CGPoint {
  return CGPointMake(centerOf(selfLength: selfSize.width, superLength: superSize.width), centerOf(selfLength: selfSize.height, superLength: superSize.height))
}

public func xPosition(leftSideView leftSideView: UIView, spaceSCRatio: CGFloat) -> CGFloat {
  return leftSideView.frame.origin.x+leftSideView.frame.size.width+Screen.size.width*spaceSCRatio
}

public func yPosition(aboveView aboveView: UIView, spaceSCRatio: CGFloat) -> CGFloat {
  return aboveView.frame.origin.y+aboveView.frame.size.height+Screen.size.height*spaceSCRatio
}

public func CGRectMakeSizeAdjusted(x: CGFloat, _ y: CGFloat, _ width: CGFloat, _ height: CGFloat, sizeRatio: CGPoint) -> CGRect {
  return CGRectMake(x*sizeRatio.x, y*sizeRatio.y, width*sizeRatio.x, height*sizeRatio.y)
}