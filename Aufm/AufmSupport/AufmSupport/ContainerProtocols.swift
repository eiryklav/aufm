//
//  ContainerGatewayProtocol
//  AufmSupport
//
//  Created by moti on 2015/08/28.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit

public protocol HasStartPointOfViewController {
  func startPointOfViewController() -> UIViewController
}

public protocol ViewControllersContainerDelegate: class {
  func endPointOfViewController(sender: UIViewController)
}

public protocol ViewControllersContainerType {}

public let kContainerFlowControlNone        = "kContainerFlowControlNone"
public let kContainerFlowControlGameToData  = "kContainerFlowControlGameToData"
public let kContainerFlowControlDataToGame  = "kContainerFlowControlDataToGame"
