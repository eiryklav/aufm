//
//  SubViewControllerType.swift
//  AufmSupport
//
//  Created by moti on 2015/09/07.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit

public protocol SubViewControllerType {}
extension SubViewControllerType {
  public func setViewFrame(frame: CGRect) {
    let o = self as! UIViewController
    o.view.frame = frame
  }
}