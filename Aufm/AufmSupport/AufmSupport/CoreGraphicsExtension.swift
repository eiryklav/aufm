//
//  CoreGraphicsExtension.swift
//  Aufm
//
//  Created by moti on 2015/07/31.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

public func + (left: CGPoint, right: CGPoint) -> CGPoint {
  return CGPoint(x: left.x + right.x, y: left.y + right.y)
}

public func += (inout left: CGPoint, right: CGPoint) {
  left = left + right
}

public func - (left: CGPoint, right: CGPoint) -> CGPoint {
  return CGPoint(x: left.x - right.x, y: left.y - right.y)
}

public func -= (inout left: CGPoint, right: CGPoint) {
  left = left - right
}

public func * (left: CGPoint, right: CGFloat) -> CGPoint {
  return CGPointMake(left.x * right, left.y * right)
}

extension CGPoint {
  
  public func distance() -> CGFloat {
    return CGFloat(sqrt(Double(x) * Double(x) + Double(y) * Double(y)))
  }
  
  public func normalizedPoint() -> CGPoint {
    let dist = distance()
    return CGPointMake(x / dist, y / dist)
  }
  
}