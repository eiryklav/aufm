//
//  UIImageExtension.swift
//  Aufm
//
//  Created by moti on 2015/06/17.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

public extension UIImage {
  public class func imageWithColor(color: UIColor) -> UIImage {
    let rect: CGRect = CGRectMake(0, 0, 1, 1)
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(1, 1), false, 0)
    color.setFill()
    UIRectFill(rect)
    let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return image
  }
  
  public func resize(size: CGSize) -> UIImage {
    UIGraphicsBeginImageContext(size)
    self.drawInRect(CGRectMake(0, 0, size.width, size.height))
    let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return resizedImage
  }
  
  public func pixelColorAtPoint(point: CGPoint) -> UIColor {
    // ref: http://swift-salaryman.com/pixcelcolor.php
    
    // ピクセルデータ取得してバイナリ化
    let pixelData = CGDataProviderCopyData(CGImageGetDataProvider(self.CGImage))
    let data: UnsafePointer = CFDataGetBytePtr(pixelData)
    
    // RGBおよびアルファ値を取得
    let pixelInfo: Int = ((Int(self.size.width) * Int(point.y)) + Int(point.x)) * 4
    let r: CGFloat = CGFloat(data[pixelInfo]) / CGFloat(255.0)
    let g: CGFloat = CGFloat(data[pixelInfo+1]) / CGFloat(255.0)
    let b: CGFloat = CGFloat(data[pixelInfo+2]) / CGFloat(255.0)
    let a: CGFloat = CGFloat(data[pixelInfo+3]) / CGFloat(255.0)
    
    return UIColor(red: r, green: g, blue: b, alpha: a)
  }
  
  public func lightedImageWithColor(color: UIColor = UIColor(rgba: "#00000055")) -> UIImage {
    let rect = CGRectMake(0, 0, self.size.width, self.size.height)
    
    UIGraphicsBeginImageContext(self.size)
    
    let ctx = UIGraphicsGetCurrentContext()
    let maskImage = self.CGImage
    CGContextClipToMask(ctx, rect, maskImage)
    CGContextDrawImage(ctx, rect, self.CGImage)
    
    self.drawAtPoint(CGPointZero)
    let light = color
    light.setFill()
    UIRectFillUsingBlendMode(rect, CGBlendMode.Multiply)
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return newImage
  }
  
  public class func imageFromView() -> UIImage {
    let screen = UIApplication.sharedApplication().keyWindow
    let layer = screen!.layer;
    
    let scale = UIScreen.mainScreen().scale;
    UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale);
    screen!.drawViewHierarchyInRect(screen!.bounds, afterScreenUpdates: true)
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return newImage
  }
  
  public class func imageFromLayer(layer: CALayer) -> UIImage {
    UIGraphicsBeginImageContext(layer.frame.size)
    layer.renderInContext(UIGraphicsGetCurrentContext()!)
    let result = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return result
  }
  
  class func drawImage(fgImage: UIImage, inImage bgImage: UIImage, atPoint point: CGPoint, blendMode: CGBlendMode) -> UIImage {
    UIGraphicsBeginImageContextWithOptions(bgImage.size, false, 0)
    bgImage.drawInRect(CGRectMake(0, 0, bgImage.size.width, bgImage.size.height))
    fgImage.drawInRect(CGRectMake(point.x, point.y, fgImage.size.width, fgImage.size.height), blendMode: blendMode, alpha: 1.0)
    
    let ret = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return ret
  }
  
  class func imageFromView(view: UIView) -> UIImage {
    UIGraphicsBeginImageContextWithOptions(view.frame.size, false, UIScreen.mainScreen().scale)
    view.drawViewHierarchyInRect(view.bounds, afterScreenUpdates: true)
    let ret = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return ret
  }
  
  class func imageFromView(view: UIView, rect: CGRect) -> UIImage {
    UIGraphicsBeginImageContextWithOptions(rect.size, false, UIScreen.mainScreen().scale)
    let ctx = UIGraphicsGetCurrentContext()
    CGContextTranslateCTM(ctx, -rect.origin.x, -rect.origin.y)
    if(!view.drawViewHierarchyInRect(view.bounds, afterScreenUpdates: true)) {
      print("notice: 'renderInContext:' worked. (alternative)")
      view.layer.renderInContext(ctx!)
    }
    let ret = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return ret
  }
  
  func scaleAspectFitResize(size: CGSize) -> UIImage {
    let widthRatio = size.width / self.size.width
    let heightRatio = size.height / self.size.height
    let ratio = (widthRatio < heightRatio) ? widthRatio : heightRatio
    let resizedSize = CGSize(width: (self.size.width * ratio), height: (self.size.height * ratio))
    UIGraphicsBeginImageContext(resizedSize)
    drawInRect(CGRect(x: 0, y: 0, width: resizedSize.width, height: resizedSize.height))
    let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return resizedImage
  }
  
  // 比率だけ指定する場合
  func scaleAspectFitResize(ratio ratio: CGFloat) -> UIImage {
    let resizedSize = CGSize(width: Int(self.size.width * ratio), height: Int(self.size.height * ratio))
    UIGraphicsBeginImageContext(resizedSize)
    drawInRect(CGRect(x: 0, y: 0, width: resizedSize.width, height: resizedSize.height))
    let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return resizedImage
  }
  
  func cropImageWithRect(rect: CGRect) -> UIImage {
    let scale: CGFloat = 1.0//UIScreen.mainScreen().scale
    let cropCGImage = CGImageCreateWithImageInRect(
      self.CGImage,
      CGRectMake(
        rect.origin.x * scale,
        rect.origin.y * scale,
        rect.width    * scale,
        rect.height   * scale
      )
      )!
    let cropImage = UIImage(CGImage: cropCGImage, scale: scale, orientation: .Up)
    return cropImage
  }
}