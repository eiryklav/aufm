//
//  UIFontExtension.swift
//  AufmSupport
//
//  Created by moti on 2015/08/27.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit

public extension UIFont {
  
  public enum SizeType {
    
    case XLarge
    case Large
    case Medium
    case SemiMedium
    case XWideSmall
    case WideSmall
    case Small
    case XSmall
    case XXSmall
    
    public func value() -> CGFloat {
      
      switch self {
      case .XLarge:     return adjustFontSize(27.0)
      case .Large:      return adjustFontSize(22.0)
      case .Medium:     return adjustFontSize(18.0)
      case .SemiMedium: return adjustFontSize(16.0)
      case .XWideSmall: return adjustFontSize(15.0)
      case .WideSmall:  return adjustFontSize(14.0)
      case .Small:      return adjustFontSize(12.0)
      case .XSmall:     return adjustFontSize(8.0)
      case .XXSmall:    return adjustFontSize(6.0)
      }
      
    }
    
  }
  
  public convenience init?(sizeType: SizeType) {
    self.init(name: "AnonymousPro", sizeType: sizeType)
  }
  public convenience init?(name: String, sizeType: SizeType) {
    self.init(name: name, size: sizeType.value())
  }
  /*
  public var sizeType: SizeType {
    get {
      //fatalError("cannot use 'get' yet")
      return .XXSmall
    }
    set {
      switch sizeType {
      case .XLarge:     fontWithSize(adjustFontSize(27.0))
      case .Large:      fontWithSize(adjustFontSize(22.0))
      case .Medium:     fontWithSize(adjustFontSize(18.0))
      case .SemiMedium: fontWithSize(adjustFontSize(16.0))
      case .WideSmall:  fontWithSize(adjustFontSize(13.0))
      case .Small:      fontWithSize(adjustFontSize(12.0))
      case .XSmall:     fontWithSize(adjustFontSize(8.0))
      case .XXSmall:    fontWithSize(adjustFontSize(6.0))
      }
    }
  }
  */
}
