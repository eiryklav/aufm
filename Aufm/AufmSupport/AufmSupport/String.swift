//
//  String.swift
//  AufmSupport
//
//  Created by moti on 2015/08/23.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import Foundation

public extension String {
  public static func getJSONStringFromPath(path: String) -> String {
    var jsonString: String!
    if let filePath = NSBundle.mainBundle().pathForResource(path, ofType: "json"){
      jsonString = try! String(contentsOfFile: filePath, encoding: NSUTF8StringEncoding)
      if jsonString.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) == 0 {
        assert(false, "Can't load json data.")
      }
    }
    else {
      assert(false, "json data has not found.")
    }
    return jsonString
  }
}