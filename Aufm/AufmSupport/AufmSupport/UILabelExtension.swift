//
//  UILabelExtension.swift
//  AufmSupport
//
//  Created by moti on 2015/08/27.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import Foundation

public extension UILabel {
  func centeringInView(view: UIView) {
    frame = CGRectMake(0, frame.origin.y, view.frame.width, frame.size.height)
    textAlignment = .Center
  }
}