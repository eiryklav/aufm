//
//  ParentViewController.swift
//  AufmViewControllers
//
//  Created by moti on 2015/08/27.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmTitle
import AufmSetting
import AufmSupport
import AufmGame
import AufmData

public class ModuleNavigator {
  init() {
    gameContainer.delegate = self
    statusContainer.delegate = self
  }
}

public let moduleNavigator = ModuleNavigator()

extension ModuleNavigator : HasStartPointOfViewController {
  public func startPointOfViewController() -> UIViewController {
    return gameContainer.startPointOfViewController()
//    return titleContainer.startPointOfViewController()
  }
}

extension ModuleNavigator : CanNavigateFromGame {
  public func instanceOfStatusViewController() -> UIViewController {
    return statusContainer.startPointOfViewController()
  }
}

extension ModuleNavigator : CanNavigateFromStatus {
  public func instanceOfGameViewController() -> UIViewController {
    return gameContainer.startPointOfViewController()
  }
}