//
//  StatusUpperTabViewController.swift
//  AufmData
//
//  Created by moti on 2015/09/06.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmSupport
import UpperTabButtonView
import AufmVisualSoundEffect

public class StatusTabViewController : UIViewController {
  
  private var backgroundView: UIView!
  private var backgroundImageView: UIImageView!
  private let tabs = ["MainStatus", "StatusGraph", "Item", "Money"]
  
  private var mainView: UpperTabButtonView!
  
  override public func viewDidLoad() {
    super.viewDidLoad()
    
    backgroundView = UIView(frame: view.bounds)
    backgroundImageView  = UIImageView(image: UIImage(named: "background")!)
    backgroundImageView.frame = view.bounds
    backgroundView.addSubview(backgroundImageView)
    
    mainView = UpperTabButtonView(frame: view.bounds)
    
    let mainStatus = MainStatusViewController(viewFrame: mainView.contentViewFrame)
    addChildViewController(mainStatus)
    mainStatus.didMoveToParentViewController(self)
    
    let statusGraph = StatusGraphViewController(viewFrame: mainView.contentViewFrame)
    statusGraph.delegate = self
    addChildViewController(statusGraph)
    statusGraph.didMoveToParentViewController(self)
    
    let item = MainStatusViewController(viewFrame: mainView.contentViewFrame)
    addChildViewController(item)
    item.didMoveToParentViewController(self)
    
    let money = MainStatusViewController(viewFrame: mainView.contentViewFrame)
    addChildViewController(money)
    money.didMoveToParentViewController(self)
    
    mainView.dataSource = self
    mainView.delegate = self
    mainView.reloadData()
    view.addSubview(mainView)
    
  }
  
}

extension StatusTabViewController : UpperTabButtonViewDataSource {
  
  public func numberOfTabButtonsInUpperTabButtonView() -> Int {
    return tabs.count
  }
  
  public func upperTabButtonView(upperTabButtonView: UpperTabButtonView, titleForTabButtonWithIndex index: Int) -> String {
    return tabs[index]
  }
  
  public func upperTabButtonView(upperTabButtonView: UpperTabButtonView, contentViewForIndex index: Int) -> UIView {
    return childViewControllers[index].view
  }
  
}

extension StatusTabViewController : UpperTabButtonViewDelegate {
  
  public func upperTabButtonView(upperTabButtonView: UpperTabButtonView, didPushTabButtonWithIndex index: Int, forControlEvents controlEvents: UIControlEvents) {
    
    if controlEvents == .TouchUpInside {
      Audio.sharedInstance.playSE("click")
    }
    else {
      fatalError("Unimplemented")
    }
  }
  
  public func upperTabButtonView(upperTabButtonView: UpperTabButtonView, didPushCloseButtonForControlEvents controlEvents: UIControlEvents) {
    if controlEvents == .TouchDown {
      Audio.sharedInstance.playSE("click")
    }
    else if controlEvents == .TouchUpInside {
      navigationController!.popToViewController(
        statusContainer.delegate!.instanceOfGameViewController(),
        animated: false
      )
    }
    else {
      fatalError("Unimplemented")
    }
  }
  
  public func backgroundViewInUpperTabButtonView(upperTabButtonView: UpperTabButtonView) -> UIView {
    return backgroundView
  }
  
}

extension StatusTabViewController : StatusGraphViewControllerDelegate {
  func backgroundImageInSuperView() -> UIImage {
    return backgroundImageView.image!.resize(UIScreen.mainScreen().bounds.size)
      .cropImageWithRect(mainView.contentViewFrame)
  }
}