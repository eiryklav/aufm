//
//  MainStatusSubTitleView.swift
//  AufmData
//
//  Created by moti on 2015/09/18.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmObjCppWrapper

class MainStatusSubTitleViewController : UIViewController {
  
  init(subTitleIndex: Int, viewFrame: CGRect) {
    self.subTitleIndex = subTitleIndex
    self.viewFrame = viewFrame
    super.init(nibName: nil, bundle: nil)
  }
  
  override func loadView() {
    super.loadView()
    
    view.frame = viewFrame

    let frame = viewFrame
    
    nameLabel = UILabel()
    nameLabel.text = "　,　"
    nameLabel.textColor = UIColor.whiteColor()
    nameLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", sizeType: .WideSmall)
    nameLabel.sizeToFit()
    nameLabel.frame.origin =
      CGPointMake(
        frame.width * 0.05,
        (frame.height - nameLabel.frame.height) / 2
    )
    view.addSubview(nameLabel)
    
    let statusView = UIView(frame:
      CGRectMake(
        frame.width   * 0.475,
        frame.height  * 3 / 10,
        frame.width   * 0.45,
        frame.height  * 3 / 5
      )
    )
    view.addSubview(statusView)
    
    lvLabel = UILabel()
    lvLabel.text = "LV100"
    lvLabel.textColor = UIColor.whiteColor()
    lvLabel.font = UIFont(name: "Anonymous Pro", sizeType: .Small)
    lvLabel.sizeToFit()
    lvLabel.frame.origin = CGPointMake(0, 0)
    statusView.addSubview(lvLabel)
    
    hpLabel = UILabel()
    hpLabel.text = "HP100/100"
    hpLabel.textColor = UIColor.whiteColor()
    hpLabel.font = UIFont(name: "Anonymous Pro", sizeType: .Small)
    hpLabel.sizeToFit()
    hpLabel.frame.origin = CGPointMake(statusView.frame.width / 3, 0)
    statusView.addSubview(hpLabel)
    
    tpLabel = UILabel()
    tpLabel.text = "TP100/100"
    tpLabel.textColor = UIColor.whiteColor()
    tpLabel.font = UIFont(name: "Anonymous Pro", sizeType: .Small)
    tpLabel.sizeToFit()
    tpLabel.frame.origin = CGPointMake(statusView.frame.width * 2 / 3, 0)
    statusView.addSubview(tpLabel)
    
    akLabel = UILabel()
    akLabel.text = "AK100/100"
    akLabel.textColor = UIColor.whiteColor()
    akLabel.font = UIFont(name: "Anonymous Pro", sizeType: .Small)
    akLabel.sizeToFit()
    akLabel.frame.origin = CGPointMake(0, lvLabel.frame.maxY)
    statusView.addSubview(akLabel)
    
    dfLabel = UILabel()
    dfLabel.text = "DF100/100"
    dfLabel.textColor = UIColor.whiteColor()
    dfLabel.font = UIFont(name: "Anonymous Pro", sizeType: .Small)
    dfLabel.sizeToFit()
    dfLabel.frame.origin = CGPointMake(hpLabel.frame.minX, hpLabel.frame.maxY)
    statusView.addSubview(dfLabel)
    
    spLabel = UILabel()
    spLabel.text = "SP100/100"
    spLabel.textColor = UIColor.whiteColor()
    spLabel.font = UIFont(name: "Anonymous Pro", sizeType: .Small)
    spLabel.sizeToFit()
    spLabel.frame.origin = CGPointMake(tpLabel.frame.minX, tpLabel.frame.maxY)
    statusView.addSubview(spLabel)
  }
  
  override func viewDidLoad() {
    
    super.viewDidLoad()
    
    if subTitleIndex == 0 {
      nameLabel.text  = "AIの名前, \(cppCode.getPlayerName())"
      lvLabel.text    = "LV\(cppCode.getPlayerStatusLV())"
      hpLabel.text    = "HP\(cppCode.getPlayerStatusHP())/\(cppCode.getPlayerStatusMaxHP())"
      tpLabel.text    = "TP\(cppCode.getPlayerStatusMana())/\(cppCode.getPlayerStatusMaxMana())"
      akLabel.text    = "AK\(cppCode.getPlayerStatusATK())"
      dfLabel.text    = "DF\(cppCode.getPlayerStatusDEF())"
      spLabel.text    = "SP\(cppCode.getPlayerStatusSP())"
    }
    else if subTitleIndex < cppCode.getNumOfPlayers() {
      let servantIndex = subTitleIndex - 1
      nameLabel.text  = "AIの名前, \(cppCode.getServantNameWithIndex(servantIndex))"
      lvLabel.text    = "LV\(cppCode.getServantStatusLVWithIndex(servantIndex))"
      hpLabel.text    = "HP\(cppCode.getServantStatusHPWithIndex(servantIndex))/\(cppCode.getServantStatusMaxHPWithIndex(servantIndex))"
      tpLabel.text    = "TP\(cppCode.getServantStatusManaWithIndex(servantIndex))/\(cppCode.getServantStatusMaxManaWithIndex(servantIndex))"
      akLabel.text    = "AK\(cppCode.getServantStatusATKWithIndex(servantIndex))"
      dfLabel.text    = "DF\(cppCode.getServantStatusDEFWithIndex(servantIndex))"
      spLabel.text    = "SP\(cppCode.getServantStatusSPWithIndex(servantIndex))"
    }
    else {
      print("Warning: MainStatusSubTitleViewController: 死亡帳はまだ実装されていない")
    }
    
    nameLabel.sizeToFit()
    lvLabel.sizeToFit()
    hpLabel.sizeToFit()
    tpLabel.sizeToFit()
    akLabel.sizeToFit()
    dfLabel.sizeToFit()
    spLabel.sizeToFit()
    
  }
  
  required init?(coder aDecoder: NSCoder) { fatalError() }

  // MARK: private
  private var subTitleIndex:  Int!    // 0...(プレイヤー数 + 死亡帳) - 1
  private var viewFrame:      CGRect!

  private var nameLabel: UILabel!
  private var lvLabel:   UILabel!
  private var hpLabel:   UILabel!
  private var tpLabel:   UILabel!
  private var akLabel:   UILabel!
  private var dfLabel:   UILabel!
  private var spLabel:   UILabel!
}
