//
//  StatusZoomGraphViewController.swift
//  AufmData
//
//  Created by moti on 2015/10/02.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmCommonComponentViews
import CorePlot

class StatusZoomGraphViewController : UIViewController {
  
  var prevScreenImage:        UIImage?
  private var backgroundView: CoveredBackground!
  var zoomGraphView:          ZoomGraphView!
  
  private weak var linePlotDataSource: CPTScatterPlotDataSource?
  private weak var zoomGraphViewDelegate: ZoomGraphViewDelegate?
  
  private let scatterPlot1ID: String
  private let scatterPlot2ID: String
  
  required init?(coder aDecoder: NSCoder) { fatalError() }
  
  init(
    linePlotDataSource: CPTScatterPlotDataSource,
    delegate: ZoomGraphViewDelegate,
    scatterPlot1ID plot1: String,
    scatterPlot2ID plot2: String
  ) {
    self.scatterPlot1ID = plot1
    self.scatterPlot2ID = plot2
    self.linePlotDataSource = linePlotDataSource
    self.zoomGraphViewDelegate = delegate
    super.init(nibName: nil, bundle: nil)
  }
  
  override func loadView() {
    super.loadView()
    
    let kSCWidth  = UIScreen.mainScreen().bounds.width
    let kSCHeight = UIScreen.mainScreen().bounds.height
    zoomGraphView = ZoomGraphView(
      frame: CGRectMake(
        0,
        kSCHeight * 0.225,
        kSCWidth,
        kSCHeight * 0.7
      ),
      linePlotDataSource: linePlotDataSource!,
      delegate: zoomGraphViewDelegate!,
      scatterPlot1ID: scatterPlot1ID,
      scatterPlot2ID: scatterPlot2ID
    )
    zoomGraphView.layer.zPosition = 2
    view.addSubview(zoomGraphView)
  }
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    
    guard let image = prevScreenImage?.lightedImageWithColor() else { fatalError("Unset previous screen image") }
    
    if backgroundView != nil {
      backgroundView.image = image
    }
    else {
      backgroundView = CoveredBackground(darkenImage: image, popWindowFrame: zoomGraphView.frame)
      backgroundView.layer.zPosition = 1
      backgroundView.delegate = self
      view.addSubview(backgroundView)
    }
    
    zoomGraphView.reload()
    
  }
  
  override func viewDidDisappear(animated: Bool) {
    super.viewDidDisappear(animated)
    prevScreenImage = nil
  }
  
}

extension StatusZoomGraphViewController : CoveredBackgroundDelegate {
  func didTouch() {
    navigationController!.popViewControllerAnimated(false)
  }
}