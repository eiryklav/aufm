//
//  ZoomGraphView.swift
//  AufmData
//
//  Created by moti on 2015/10/02.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmSupport
import CorePlot

class ZoomGraphView : UIView {
  
  required init?(coder: NSCoder) { fatalError() }
  
  init(
    frame: CGRect,
    linePlotDataSource: CPTScatterPlotDataSource,
    delegate: ZoomGraphViewDelegate,
    scatterPlot1ID plot1: String,
    scatterPlot2ID plot2: String
  ) {
    self.kScatterPlot1 = plot1
    self.kScatterPlot2 = plot2
    super.init(frame: frame)
    self.linePlotDataSource = linePlotDataSource
    self.delegate = delegate
    
    // ブラー背景
    let blurEffect = UIBlurEffect(style: .Light)
    let visualEffectView = UIVisualEffectView(effect: blurEffect)
    visualEffectView.frame = CGRectMake(bounds.width * 0.1, -bounds.height * 0.125, bounds.width * 0.8, bounds.height * 0.1)
    visualEffectView.layer.cornerRadius = 2.0
    visualEffectView.clipsToBounds = true
    
    // 名前、ステータス表示
    let label = UILabel()
    label.font = UIFont(name: "Hiragino Kaku Gothic ProN", sizeType: .Medium)
    label.text = "\(delegate.characterNameInZoomGraphView(self)) - \(selectedID)"
    label.textColor = UIColor.whiteColor()
    label.sizeToFit()
    label.frame.origin = CGPointMake((visualEffectView.frame.width - label.frame.width) / 2, visualEffectView.frame.height * 0.1)
    visualEffectView.addSubview(label)
    
    addSubview(visualEffectView)
    
    makePlotArea()
  }
  
  private func makePlotArea() {
    
    graphFrame = bounds
    graphFrame.origin.y += graphFrame.height * 0.025
    graphFrame.size.height -= graphFrame.height * 0.025
    
    // グラフの生成
    graph = CPTXYGraph(frame: CGRectZero)
    let theme = CPTTheme(named: kCPTDarkGradientTheme)
    graph.applyTheme(theme)
    let hostingView = CPTGraphHostingView(frame: graphFrame)
    addSubview(hostingView)
    hostingView.collapsesLayers = false
    hostingView.hostedGraph = graph
    
    // ボーダー
    graph.plotAreaFrame.cornerRadius  = 2.0
    
    // パディング
    graph.paddingLeft   = 0.0
    graph.paddingTop    = 0.0
    graph.paddingRight  = 0.0
    graph.paddingBottom = 0.0

    graph.plotAreaFrame.paddingLeft   = 30.0
    graph.plotAreaFrame.paddingBottom = 30.0
    
    // プロット間隔の設定
    let plotSpace = graph.defaultPlotSpace as! CPTXYPlotSpace
    plotSpace.allowsUserInteraction = false
    plotSpace.delegate = self
    plotSpace.xRange = CPTPlotRange(location: CPTDecimalFromDouble(-0.25), length: CPTDecimalFromDouble(2.0))
    plotSpace.yRange = CPTPlotRange(location: CPTDecimalFromDouble(0.0), length: CPTDecimalFromDouble(100.0))
    
    // テキストスタイル
    let textStyle = CPTMutableTextStyle()
    textStyle.fontSize = adjustFontSize(8.0)
    textStyle.color = CPTColor.whiteColor()
    
    // X軸の設定
    let axisSet = graph.axisSet as! CPTXYAxisSet
    let x = axisSet.xAxis
    x.titleTextStyle = textStyle
    x.majorIntervalLength = CPTDecimalFromDouble(0.5)
    x.minorTicksPerInterval = 59
    
    // Y軸の設定
    axisSet.yAxis.titleTextStyle = textStyle
    let formatter = NSNumberFormatter()
    axisSet.yAxis.labelFormatter = formatter
    
    // 第１線の生成
    scatterPlot1 = CPTScatterPlot()
    scatterPlot1.identifier     = kScatterPlot1
    scatterPlot1.dataSource     = linePlotDataSource
    scatterPlot1.delegate       = self
    
    // スタイル設定
    let lineStyle = CPTMutableLineStyle()
    lineStyle.miterLimit        = 1.0
    lineStyle.lineWidth         = 3.0
    lineStyle.lineColor         = CPTColor.blueColor()
    scatterPlot1.dataLineStyle  = lineStyle
    
    // プロットシンボル
    let plotSymbol = CPTPlotSymbol()
    plotSymbol.fill = CPTFill(color: CPTColor.blueColor())
    plotSymbol.size = adjustSize(CGSizeMake(30.0, 30.0))
    scatterPlot1.plotSymbol = plotSymbol
    
    // グラデーション設定
    let areaColor               = CPTColor(componentRed: 0.3, green: 1.0, blue:0.3, alpha: 0.8)
    let areaGradient            = CPTGradient(beginningColor:areaColor, endingColor:CPTColor.clearColor())
    areaGradient.angle          = -90.0
    let areaGradientFill        = CPTFill(gradient: areaGradient)
    scatterPlot1.areaFill       = areaGradientFill
    scatterPlot1.areaBaseValue  = CPTDecimalFromDouble(1.75)

    graph.addPlot(scatterPlot1)
    
    // 第２線
    scatterPlot2 = CPTScatterPlot()
    scatterPlot2.identifier = kScatterPlot2
    scatterPlot2.dataSource = linePlotDataSource
    scatterPlot2.delegate = self
    
    lineStyle.lineColor = CPTColor.redColor()
    scatterPlot2.dataLineStyle = lineStyle
    scatterPlot2.plotSymbol = plotSymbol
    
    graph.addPlot(scatterPlot2)
    
    // アノテーションの追加
    makeAnnotation()

    // タッチ領域の背景色を薄くする
    let leftTouchView = UIView(frame: CGRectMake(0, graphFrame.origin.y, graphFrame.width * 0.1, graphFrame.height))
    leftTouchView.backgroundColor = UIColor(white: 0.8, alpha: 0.1)
    addSubview(leftTouchView)
    let rightTouchView = UIView(frame: CGRectMake(graphFrame.width * 0.9, graphFrame.origin.y, graphFrame.width * 0.1, graphFrame.height))
    rightTouchView.backgroundColor = UIColor(white: 0.8, alpha: 0.1)
    addSubview(rightTouchView)
  }

  private var timerForLongPress = NSTimer()
  
  override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
    
    let touch = touches.first!
    let point = touch.locationInView(self)
    
    let plotSpace = graph.defaultPlotSpace as! CPTXYPlotSpace
    
    if timerForLongPress.valid { timerForLongPress.invalidate() }
    
    if point.x < graphFrame.width * 0.1 {
      timerForLongPress = NSTimer.scheduledTimerWithTimeInterval(
        0.01,
        target: NSBlockOperation(block: {
          plotSpace.xRange = CPTPlotRange(
            location: CPTDecimalFromDouble(plotSpace.xRange.locationDouble - 0.1),
            length: plotSpace.xRange.length
          )
          (self.graph.plotAreaFrame.axisSet as! CPTXYAxisSet).yAxis.orthogonalCoordinateDecimal = CPTDecimalFromDouble(plotSpace.xRange.locationDouble + self.kXAxisOffsetDouble)
        }),
        selector: "main",
        userInfo: nil,
        repeats: true
      )
    }
    
    if point.x > graphFrame.width * 0.9 {
      timerForLongPress = NSTimer.scheduledTimerWithTimeInterval(
        0.01,
        target: NSBlockOperation(block: {
          plotSpace.xRange = CPTPlotRange(
            location: CPTDecimalFromDouble(plotSpace.xRange.locationDouble + 0.1),
            length: plotSpace.xRange.length
          )
          (self.graph.plotAreaFrame.axisSet as! CPTXYAxisSet).yAxis.orthogonalCoordinateDecimal = CPTDecimalFromDouble(plotSpace.xRange.locationDouble + self.kXAxisOffsetDouble)
        }),
        selector: "main",
        userInfo: nil,
        repeats: true
      )
    }
    
  }
  
  override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
    if timerForLongPress.valid { timerForLongPress.invalidate() }
  }
  
  private func makeAnnotation() {
    
    /*
    let image = UIImage(named: "status-graph-annotation")
    let fillImage = CPTFill(image: CPTImage(nativeImage: image))
    annotationTextLayer.fill = fillImage
    annotationTextLayer.sizeToFit()
    let annotationLayer = CPTLayer(frame: CGRectMake(0, 0, adjustX(60), adjustY(40)))
    annotationLayer.addSublayer(annotationTextLayer)
    */
    
    let lineStyle = CPTMutableLineStyle()
    lineStyle.lineWidth = 2.0
    lineStyle.lineColor = CPTColor.whiteColor()
    
    let textStyle = CPTMutableTextStyle()
    textStyle.fontSize = adjustFontSize(15.0)
    textStyle.fontName = "Anonymous Pro"
//    textStyle.textAlignment = .Center
    textStyle.color = CPTColor.whiteColor()

    annotationTextLayer = CPTTextLayer(text: "     ", style: textStyle)
    annotationTextLayer.cornerRadius = adjustY(5.0)
    annotationTextLayer.borderLineStyle = lineStyle
    annotationTextLayer.hidden = true
    
    
    /*
    let bgLayer = CPTLayer()
    bgLayer.borderColor = UIColor.whiteColor().CGColor
    bgLayer.backgroundColor = UIColor(rgba: "#34495eff").CGColor
    bgLayer.cornerRadius = adjustY(5.0)
    annotationTextLayer.insertSublayer(bgLayer, atIndex: 0)
    */
/*
    CPTMutableLineStyle lineStyle = [CPTMutableLineStyle lineStyle];
    lineStyle.lineWidth = 2.0;
    lineStyle.lineColor = [CPTColor whiteColor];
    
    CPTTextLayer *textLayer = [[CPTTextLayer alloc] initWithText:valueToDisplayString style:style];
    textLayer.fill = [CPTFill fillWithColor:[CPTColor blueColor]];
    textLayer.cornerRadius = 10.0;
    textLayer.borderLineStyle = lineStyle;
    */
    
    // annotation 作成
    annotation = CPTPlotSpaceAnnotation(
      plotSpace: graph.defaultPlotSpace,
      anchorPlotPoint: [NSNumber(CGFloat: 0), NSNumber(CGFloat: 0)]
    )
    annotation.contentLayer = annotationTextLayer
//    annotation.contentLayer = annotationLayer
    annotation.displacement = CGPointMake(0.0, adjustY(20.0))  // 設定されたアンカーからのズレ. Y軸は下から上に伸びている.
    graph.plotAreaFrame.plotArea.addAnnotation(annotation)
    
  }
  
  func reload() {
    scatterPlot1.reloadData()
    adjustYRangeInGraph(graph)
  }
  
  // delegate function
  private func valueForPlotXAtIndex(index: Int, inPlot plot: CPTPlot) -> CGFloat {
    return delegate!.zoomGraphView(self, plot: plot, valueForPlotXAtIndex: index)
  }
  
  private func valueForPlotYAtIndex(index: Int, inPlot plot: CPTPlot) -> Int {
    return delegate!.zoomGraphView(self, plot: plot, valueForPlotYAtIndex: index)
  }
  
  private func adjustYRangeInGraph(graph: CPTXYGraph) {
    delegate?.zoomGraphView(self, adjustYRangeInGraph: graph)
  }
  
  // MARK: - private -
  
  private var scatterPlot1: CPTScatterPlot!
  private var scatterPlot2: CPTScatterPlot!
  private var graph: CPTXYGraph!
  private var graphFrame: CGRect!
  
  private weak var linePlotDataSource:  CPTScatterPlotDataSource?
  private weak var delegate:            ZoomGraphViewDelegate?
  
  private let kScatterPlot1: String
  private let kScatterPlot2: String
  
  private var annotationTextLayer: CPTTextLayer!
  private var annotation: CPTPlotSpaceAnnotation!
  
  private var selectedID: String {
    get {
      return delegate!.selectedIDInZoomGraphView(self)
    }
  }
  
  private let kXAxisOffsetDouble = 0.25
  
}

extension ZoomGraphView : CPTPlotSpaceDelegate {
  
  func plotSpace(space: CPTPlotSpace!, willDisplaceBy displacement: CGPoint) -> CGPoint {
    return CGPointMake(displacement.x, 0)
  }
  
  func plotSpace(space: CPTPlotSpace!, willChangePlotRangeTo newRange: CPTPlotRange!, forCoordinate coordinate: CPTCoordinate) -> CPTPlotRange! {
    if coordinate == .X {
      if newRange.locationDouble + 1.75 > 5.0 {
        let range = CPTMutablePlotRange()
        range.length = newRange.length
        range.location = CPTDecimalFromDouble(3.25)
        return range
      }
      if newRange.locationDouble + kXAxisOffsetDouble < 0 {
        let range = CPTMutablePlotRange()
        range.length = newRange.length
        range.location = CPTDecimalFromDouble(-kXAxisOffsetDouble)
        return range
      }
    }
    return newRange
  }
  
}

extension ZoomGraphView : CPTScatterPlotDelegate {

  func scatterPlot(plot: CPTScatterPlot!, plotSymbolWasSelectedAtRecordIndex idx: UInt) {
    let pointX = NSNumber(CGFloat: valueForPlotXAtIndex(Int(idx), inPlot: plot))
    let pointY = NSNumber(integer: valueForPlotYAtIndex(Int(idx), inPlot: plot))
    
    let anchorPoint = [pointX, pointY]
    
    let selID = plot.identifier as! String == kScatterPlot1 ? selectedID : delegate!.maxIDForSelectedIDInZoomGraphView(self)
    
    // annotation
    annotationTextLayer.hidden = false
    annotationTextLayer.text = " time: \(Double(Int(floor(pointX.doubleValue * 1000.0))) / 1000.0)sec, \(selID): \(pointY) "
    annotation.anchorPlotPoint = anchorPoint

  }
  
}

protocol ZoomGraphViewDelegate: class {
  func zoomGraphView(zoomGraphView: ZoomGraphView, plot: CPTPlot, valueForPlotXAtIndex index: Int) -> CGFloat
  func zoomGraphView(zoomGraphView: ZoomGraphView, plot: CPTPlot, valueForPlotYAtIndex index: Int) -> Int
  func zoomGraphView(zoomGraphView: ZoomGraphView, adjustYRangeInGraph graph: CPTXYGraph)
  func characterNameInZoomGraphView(zoomGraphView: ZoomGraphView) -> String
  func selectedIDInZoomGraphView(zoomGraphView: ZoomGraphView) -> String
  func maxIDForSelectedIDInZoomGraphView(zoomGraphView: ZoomGraphView) -> String
}