//
//  CharacterButton.swift
//  AufmData
//
//  Created by moti on 2015/09/27.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import Foundation
import AufmCommonComponentViews
import AufmSupport

class CharacterButton : UIButton, BTProtocol,
  BGNone, BTLabelAboveLayer, SEButtonTouchClick, BTTouchActionNoEffect {
  
  var label: UILabel!
  var layer_: CALayer!
  var highlightedLayer: CALayer!
  
  let buttonNames = [
    "status-graph-red-button",
    "status-graph-yellow-button",
    "status-graph-blue-button",
    "status-graph-purple-button"
  ]
  
  init(frame: CGRect, text: String, playerID: Int) {
    
    super.init(frame: frame)
    
    label = UILabel()
    label.frame.origin.x = frame.width * 0.25
    label.text = text
    label.textColor = UIColor.whiteColor()
    label.font = UIFont(name: "Hiragino Kaku Gothic ProN", sizeType: .SemiMedium)!
    label.sizeToFit()
    label.frame.origin = CGPointMake(
      (frame.width - label.frame.size.width) / 2,
      frame.height * 0.5
    )
    
    layer_ = CALayer()
    layer_.contents = UIImage(named: buttonNames[playerID])!.resize(frame.size).CGImage
    layer_.frame = frame
    highlightedLayer = CALayer()
    highlightedLayer.contents = UIImage(named: buttonNames[playerID])!.resize(frame.size).lightedImageWithColor(UIColor(white: 0.5, alpha: 0.8)).CGImage
    highlightedLayer.frame = frame
    
    applyBackground()
    applyBTActionTouch()
    applySEButtonTouchForControlEvents(.TouchUpInside)
    applyTitle()
  }
  
  required init?(coder aDecoder: NSCoder) { fatalError() }
  
  
}