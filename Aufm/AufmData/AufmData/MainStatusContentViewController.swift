//
//  MainStatusContentView.swift
//  AufmData
//
//  Created by moti on 2015/09/18.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit
import ExPickerView
import AufmObjCppWrapper

class MainStatusContentViewController : UIViewController {
    
  weak var delegate: MainStatusContentViewDelegate?

  init(contentIndex: Int, viewFrame: CGRect) {
    self.contentIndex = contentIndex
    self.viewFrame = viewFrame
    super.init(nibName: nil, bundle: nil)
  }
  
  override func loadView() {
    super.loadView()
    
    view.frame = viewFrame
    
    weaponLabel = UILabel(frame:
      CGRectMake(
        viewFrame.width   * 0.05,
        viewFrame.height  * 0.075,
        viewFrame.width   * 0.5,
        viewFrame.height  * 0.1
      )
    )
    weaponLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", sizeType: .XWideSmall)
    view.addSubview(weaponLabel)
    let weaponImageView = UIImageView(image: UIImage(named: "ability-icon-weapon"))
    weaponImageView.frame =
      CGRectMake(
        viewFrame.width   * 0.475,
        weaponLabel.frame.minY,
        viewFrame.height  * 0.125,
        viewFrame.height  * 0.125
    )
    view.addSubview(weaponImageView)
    
    protectorLabel = UILabel(frame:
      CGRectMake(
        viewFrame.width   * 0.05,
        weaponLabel.frame.maxY + viewFrame.height * 0.025,
        viewFrame.width   * 0.5,
        viewFrame.height  * 0.1
      )
    )
    protectorLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", sizeType: .XWideSmall)
    view.addSubview(protectorLabel)
    let protectorImageView = UIImageView(image: UIImage(named: "ability-icon-protector"))
    protectorImageView.frame =
      CGRectMake(
        weaponImageView.frame.minX,
        protectorLabel.frame.minY,
        viewFrame.height  * 0.125,
        viewFrame.height  * 0.125
    )
    view.addSubview(protectorImageView)
    
    accessoryLabel = UILabel(frame:
      CGRectMake(
        viewFrame.width   * 0.05,
        protectorLabel.frame.maxY + viewFrame.height * 0.025,
        viewFrame.width   * 0.5,
        viewFrame.height  * 0.1
      )
    )
    accessoryLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", sizeType: .XWideSmall)
    view.addSubview(accessoryLabel)
    let accessoryImageView = UIImageView(image: UIImage(named: "ability-icon-accessory"))
    accessoryImageView.frame =
      CGRectMake(
        weaponImageView.frame.minX,
        accessoryLabel.frame.minY,
        viewFrame.height  * 0.125,
        viewFrame.height  * 0.125
    )
    view.addSubview(accessoryImageView)
    
    abilityPickerView = ExPickerView(frame:
      CGRectMake(
        viewFrame.width   * 0.05,
        viewFrame.height  * 0.5,
        viewFrame.width   * 0.475,
        viewFrame.height  * 0.4
      )
    )
    abilityPickerView.dataSource  = self
    abilityPickerView.delegate    = self
    view.addSubview(abilityPickerView)
    
    abilityNameLabel = UILabel(frame:
      CGRectMake(
        viewFrame.width   * 0.55,
        viewFrame.height  * 0.1,
        viewFrame.width   * 0.45,
        viewFrame.height  * 0.1
      )
    )
    abilityNameLabel.textColor = UIColor.whiteColor()
    abilityNameLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", sizeType: .Medium)
    view.addSubview(abilityNameLabel)
    
    abilityBriefLabel = UILabel(frame:
      CGRectMake(
        abilityNameLabel.frame.minX,
        abilityNameLabel.frame.maxY + viewFrame.height * 0.025,
        abilityNameLabel.frame.width,
        abilityNameLabel.frame.height
      )
    )
    abilityBriefLabel.numberOfLines = 0
    abilityBriefLabel.textColor = UIColor.whiteColor()
    abilityBriefLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", sizeType: .Small)
    view.addSubview(abilityBriefLabel)
    
    abilityFlavorLabel = UILabel(frame:
      CGRectMake(
        abilityBriefLabel.frame.minX,
        abilityBriefLabel.frame.maxY + viewFrame.height * 0.05,
        abilityBriefLabel.frame.width,
        viewFrame.height * 0.7
      )
    )
    abilityFlavorLabel.numberOfLines = 0
    abilityFlavorLabel.textColor = UIColor.whiteColor()
    abilityFlavorLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", sizeType: .WideSmall)
    view.addSubview(abilityFlavorLabel)
  }
  
  override func viewDidLoad() {
    
    super.viewDidLoad()
    
    guard contentIndex < cppCode.getNumOfPlayers() else { print("Warning: 死亡帳のviewDidLoad()は実装されていない。"); return }
    weaponLabel.textColor = UIColor.whiteColor()
    protectorLabel.textColor = UIColor.whiteColor()
    accessoryLabel.textColor = UIColor.whiteColor()
    
    weaponLabel.text = cppCode.getPlayerStatusWeapon()
    protectorLabel.text = cppCode.getPlayerStatusProtector()
    accessoryLabel.text = cppCode.getPlayerStatusAccessory()
    
    abilityViewsInPickerView = []
    abilityNames    = []
    abilityTP       = []
    abilityBriefs   = []
    abilityFlavors  = []
    
    let playerID = contentIndex == 0 ? PlayerID : cppCode.getEncountedPlayersList()[contentIndex - 1] as! Int
    let masteredActions = cppCode.getTimeStampedMasteredActionList(playerID) as [AnyObject]
    for array in masteredActions {
      let timeStampedActionID = array as! [AnyObject]
      let actionClassID = timeStampedActionID[1] as! String
      let name = cppCode.getActionNameWithID(actionClassID)
      let type = actionClassID.substringToIndex(actionClassID.startIndex.advancedBy(2)) // 前提:2文字限定 SA, SB, SC, ..., PA, PB, PC, ...
      let selView = SelectionView(frame: CGRectMake(0, 0, abilityPickerView.frame.width, abilityPickerView.frame.height / 3))
      selView.text = name
      selView.label.textColor = UIColor.whiteColor()
      selView.label.font = UIFont(name: "Hiragino Kaku Gothic ProN", sizeType: .Small)
      switch type {
      case "SA":  selView.image = UIImage(named: "ability-icon-skull")
      case "SB":  selView.image = UIImage(named: "ability-icon-skull")
      case "SC":  selView.image = UIImage(named: "ability-icon-skull")
      case "PA":  selView.image = UIImage(named: "ability-icon-skull")
      case "PB":  selView.image = UIImage(named: "ability-icon-skull")
      case "PC":  selView.image = UIImage(named: "ability-icon-skull")
      default:
        selView.image = UIImage(named: "ability-icon-weapon")
      }
      abilityViewsInPickerView.append(selView)
      abilityNames.append(name)
      abilityTP.append(cppCode.getActionManaWithID(actionClassID))
      abilityBriefs.append(cppCode.getActionDescriptionWithID(actionClassID))
      abilityFlavors.append(cppCode.getActionFlavorWithID(actionClassID))
    }
    
    abilityPickerView.reload()
    abilityPickerView.selectRow(0, animated: false)
  }
  
  
  required init?(coder aDecoder: NSCoder) { fatalError() }
  
  // MARK: private
  private var contentIndex:       Int!    // 0...(プレイヤー数 + 死亡帳) - 1
  private var viewFrame:          CGRect!
  private var weaponLabel:        UILabel!
  private var protectorLabel:     UILabel!
  private var accessoryLabel:     UILabel!
  private var abilityPickerView:  ExPickerView!
  private var abilityNameLabel:   UILabel!
  private var abilityBriefLabel:  UILabel!
  private var abilityFlavorLabel: UILabel!
  
  // dataSource
  private var abilityViewsInPickerView: [SelectionView]!
  
  // data
  private var abilityNames:   [String]!
  private var abilityTP:      [Int]!
  private var abilityBriefs:  [String]!
  private var abilityFlavors: [String]!
  
}

extension MainStatusContentViewController : ExPickerViewDataSource {
  
  func numberOfRowsInExPickerView(exPickerView: ExPickerView) -> Int {
    return abilityViewsInPickerView.count
  }
  
}

extension MainStatusContentViewController : ExPickerViewDelegate {
  
  func exPickerView(exPickerView: ExPickerView, viewForRow row: Int) -> SelectionView {
    return abilityViewsInPickerView[row]
  }
  
  func exPickerView(exPickerView: ExPickerView, didSelectRow row: Int) {
    abilityNameLabel.text   = abilityNames[row]
    abilityNameLabel.sizeToFit()
    abilityBriefLabel.text  = "TP\(abilityTP[row])　\(abilityBriefs[row])"
    abilityBriefLabel.frame.size = CGSizeMake(viewFrame.width * 0.4, viewFrame.height * 0.3)
    abilityBriefLabel.sizeToFit()
    abilityFlavorLabel.text = abilityFlavors[row]
    abilityFlavorLabel.frame.size = CGSizeMake(viewFrame.width * 0.4, viewFrame.height * 0.7)
    abilityFlavorLabel.sizeToFit()
  }
  
  func widthInExPickerView(exPickerView: ExPickerView) -> CGFloat {
    return abilityPickerView.frame.width
  }
  
  func rowHeightInExPickerView(exPickerView: ExPickerView) -> CGFloat {
    return abilityPickerView.frame.height / 3
  }
  
}

protocol MainStatusContentViewDelegate : class {
  
}