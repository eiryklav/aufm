//
//  DataContainer.swift
//  AufmData
//
//  Created by moti on 2015/09/16.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmSupport

public class StatusViewControllersContainer : ViewControllersContainerType {
  public weak var delegate: CanNavigateFromStatus?
  
  let statusTabViewController = StatusTabViewController()
  
  init() {}
}

extension StatusViewControllersContainer : HasStartPointOfViewController {
  public func startPointOfViewController() -> UIViewController {
    return statusTabViewController
  }
}

public protocol CanNavigateFromStatus: class {
  func instanceOfGameViewController() -> UIViewController
}

public let statusContainer = StatusViewControllersContainer()