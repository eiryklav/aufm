//
//  DataStatusViewController.swift
//  Aufm
//
//  Created by moti on 2015/06/30.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import CorePlot
import AufmCommonComponentViews
import AufmSupport
import AufmObjCppWrapper
import ExPickerView

class StatusGraphViewController : UIViewController {
  
  weak var delegate: StatusGraphViewControllerDelegate?
  
  required init?(coder aDecoder: NSCoder) { fatalError() }
  
  init(viewFrame: CGRect) {
    // self.view を変更しないこと
    self.viewFrame = viewFrame
    self.selectedButtonID = 0
    super.init(nibName: nil, bundle: nil)
  }
  
  override func loadView() {
    
    super.loadView()
    view.frame = viewFrame
    
    let background = UIImageView(
      frame: CGRectMake(
        view.frame.width  * 0.010,
        view.frame.height * 0.010,
        view.frame.width  * 0.980,
        view.frame.height * 0.980
      )
    )
    let image = UIImage.drawImage(
      UIImage(named: "status-graph-background")!.resize(background.frame.size),
      inImage: delegate!.backgroundImageInSuperView().cropImageWithRect(background.frame),
      atPoint: background.frame.origin,
      blendMode: .ColorDodge
    )
    background.image = image
    background.userInteractionEnabled = true
    view.addSubview(background)
    
    buttons = []
    
    buttons.append(CharacterButton(
      frame: CGRectMake(
        background.frame.width   * 0.02,
        background.frame.height  * 0.03,
        background.frame.width   * 0.40,
        background.frame.height  * 0.175
      ),
      text:     cppCode.getPlayerName(),
      playerID: 0
    ))
    
    let numberOfServants = cppCode.getNumOfServants()
    for var i=0; i<numberOfServants; i++ {
      buttons.append(CharacterButton(
        frame: CGRectMake(
          buttons[0].frame.minX,
          buttons[0].frame.maxY + background.frame.height * 0.22 * CGFloat(i),
          buttons[0].frame.width,
          buttons[0].frame.height
        ),
        text:     cppCode.getServantNameWithIndex(i),
        playerID: i+1
      ))
    }
    
    for button in buttons {
      background.addSubview(button)
    }
    
    graphBackground = UIImageView(image: UIImage(named: "status-graph-graph-background"))
    graphBackground.frame = CGRectMake(
      buttons[0].frame.maxX + background.frame.width * 0.04,
      background.frame.height * 0.075,
      background.frame.width  * 0.4,
      background.frame.height * 0.8
    )
    graphBackground.userInteractionEnabled = true
    
    background.addSubview(graphBackground)
    
    // Graph
    graph = CPTXYGraph(frame: CGRectZero)
    let theme = CPTTheme(named: kCPTDarkGradientTheme)
    graph.applyTheme(theme)
    
    let hostingView = CPTGraphHostingView(frame: graphBackground.bounds)
    graphBackground.addSubview(hostingView)
    hostingView.collapsesLayers = false
    hostingView.hostedGraph = graph
    
    graph.paddingLeft   = -5.0
    graph.paddingTop    = -5.0
    graph.paddingRight  = -5.0
    graph.paddingBottom = -5.0
    
    graph.cornerRadius = 0.0
    
    graph.plotAreaFrame.cornerRadius  = 2.0
    graph.plotAreaFrame.paddingLeft   = 30.0
    graph.plotAreaFrame.paddingBottom = 30.0
    
    let plotAreaLineStyle = CPTMutableLineStyle()
    plotAreaLineStyle.lineWidth = 2.0
    plotAreaLineStyle.lineColor = CPTColor(genericGray: 0.3)
    graph.plotAreaFrame.borderLineStyle = plotAreaLineStyle
    graph.fill = CPTFill(color: CPTColor.clearColor())
    graph.plotAreaFrame.fill = CPTFill(color: CPTColor(componentRed: 0.2, green: 0.2, blue: 0.2, alpha: 0.8))
    
    // Setup plot space
    let plotSpace = graph.defaultPlotSpace as! CPTXYPlotSpace
    plotSpace.xRange = CPTPlotRange(location: CPTDecimalFromDouble(0.0), length: CPTDecimalFromDouble(5.5))
    
    let textStyle = CPTMutableTextStyle()
    textStyle.fontSize = adjustFontSize(8.0)
    
    // XAxis
    let axisSet = graph.axisSet as! CPTXYAxisSet
    let x = axisSet.xAxis
    x.titleTextStyle = textStyle
    x.majorIntervalLength = CPTDecimalFromDouble(1.0)
    x.minorTicksPerInterval = 2
    let formatter = NSNumberFormatter()
    formatter.maximumFractionDigits = 0
    x.labelFormatter = formatter
    
    axisSet.yAxis.titleTextStyle = textStyle
    axisSet.yAxis.labelFormatter = formatter
    
    // Create plot area
    
    // 第１線
    let linePlot1 = CPTScatterPlot()
    let lineStyle = CPTMutableLineStyle()
    lineStyle.miterLimit = 1.0
    lineStyle.lineWidth = 1.0
    lineStyle.lineColor = CPTColor.blueColor()
    linePlot1.dataLineStyle = lineStyle
    linePlot1.identifier = kScatterPlot1
    linePlot1.dataSource = self
    
    let areaColor = CPTColor(componentRed: 0.3, green: 1.0, blue:0.3, alpha: 0.8)
    let areaGradient = CPTGradient(beginningColor:areaColor, endingColor:CPTColor.clearColor())
    areaGradient.angle = -90.0
    let areaGradientFill = CPTFill(gradient: areaGradient)
    
    linePlot1.areaFill = areaGradientFill
    linePlot1.areaBaseValue = CPTDecimalFromDouble(1.75)
    
    graph.addPlot(linePlot1)
    
    // 第２線(MAX値)
    let linePlot2 = CPTScatterPlot()
    lineStyle.lineColor = CPTColor.redColor()
    linePlot2.dataLineStyle = lineStyle
    linePlot2.identifier = kScatterPlot2
    linePlot2.dataSource = self
    
    graph.addPlot(linePlot2)
    
    // transparent button in graph
    let graphButton = UIButton(frame: graph.frame)
    graphBackground.addSubview(graphButton)
    graphButton.setTitle("", forState: .Normal)
    graphButton.addTarget(self, action: "didPushGraphButton", forControlEvents: .TouchUpInside)
    
    // SwipeRadio
    swipeRadio = SwipeRadioButtonView(
      origin: CGPointMake(
        graphBackground.frame.maxX + background.frame.width * 0.025,
        graphBackground.frame.minY - background.frame.height * 0.05
      ),
      icons:  [
        Icon(image: UIImage(named: "status-icon-lv"), id: "LV"),
        Icon(image: UIImage(named: "status-icon-hp"), id: "HP"),
        Icon(image: UIImage(named: "status-icon-tp"), id: "TP"),
        Icon(image: UIImage(named: "status-icon-ak"), id: "AK"),
        Icon(image: UIImage(named: "status-icon-df"), id: "DF"),
        Icon(image: UIImage(named: "status-icon-sp"), id: "SP")
      ],
      bigIconSize:      CGSizeMake(background.frame.height / 5, background.frame.height / 5),
      smallIconSize:    CGSizeMake(background.frame.height / 8, background.frame.height / 8),
      selectedSpace:    0,
      unselectedSpace:  0
    )
    swipeRadio.delegate = self
    
    background.addSubview(swipeRadio)
    
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    loadTimeStampedLists()
  }
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    
    loadTimeStampedLists()
    adjustYRangeInGraph(graph)
    graph.reloadData()
  }
  
  private func loadTimeStampedLists() {
    timeStampedListWithPlayerIndex = [:]
    
    let numOfPlayers = cppCode.getNumOfPlayers()
    
    for i in 0..<numOfPlayers {
      let playerID = i == 0 ? PlayerID : cppCode.getServantIDWithIndex(i-1)
      timeStampedListWithPlayerIndex[i] = [:]
      timeStampedListWithPlayerIndex[i]!["HP"]    = cppCode.getTimeStampedHPList(playerID)
      timeStampedListWithPlayerIndex[i]!["TP"]    = cppCode.getTimeStampedManaList(playerID)
      timeStampedListWithPlayerIndex[i]!["MaxHP"] = cppCode.getTimeStampedMaxHPList(playerID)
      timeStampedListWithPlayerIndex[i]!["MaxTP"] = cppCode.getTimeStampedMaxManaList(playerID)
      timeStampedListWithPlayerIndex[i]!["LV"]    = cppCode.getTimeStampedLVList(playerID)
      timeStampedListWithPlayerIndex[i]!["AK"]    = cppCode.getTimeStampedATKList(playerID)
      timeStampedListWithPlayerIndex[i]!["DF"]    = cppCode.getTimeStampedDEFList(playerID)
      timeStampedListWithPlayerIndex[i]!["SP"]    = cppCode.getTimeStampedSPList(playerID)
    }
  }
  
  func didPushGraphButton() {
    let zoomvc = StatusZoomGraphViewController(
      linePlotDataSource: self,
      delegate: self,
      scatterPlot1ID: kScatterPlot1,
      scatterPlot2ID: kScatterPlot2
    )
    addChildViewController(zoomvc)
    zoomvc.didMoveToParentViewController(self)
    zoomvc.prevScreenImage = UIImage.imageFromView()
    navigationController!.pushViewController(zoomvc, animated: false)
  }

  private func timeStampWithPlayerIndex(playerIndex: Int, forStatus status: String, forRow row: Int) -> (time: Double, value: Int) {
    let array = timeStampedListWithPlayerIndex[playerIndex]![status]![row] as! NSArray
    return (array[0] as! Double, array[1] as! Int)
  }
  
  private func valueForPlayerStatus(status: String) -> Int {
    switch status {
      case "HP":    return cppCode.getPlayerStatusHP()
      case "TP":    return cppCode.getPlayerStatusMana()
      case "MaxHP": return cppCode.getPlayerStatusMaxHP()
      case "MaxTP": return cppCode.getPlayerStatusMaxMana()
      case "LV":    return cppCode.getPlayerStatusLV()
      case "AK":    return cppCode.getPlayerStatusATK()
      case "DF":    return cppCode.getPlayerStatusDEF()
      case "SP":    return cppCode.getPlayerStatusSP()
    default: fatalError()
    }
  }
  
  private func valueForStatus(status: String, servantIndex index: Int) -> Int {
    switch status {
    case "HP":    return cppCode.getServantStatusHPWithIndex(index)
    case "TP":    return cppCode.getServantStatusManaWithIndex(index)
    case "MaxHP": return cppCode.getServantStatusMaxHPWithIndex(index)
    case "MaxTP": return cppCode.getServantStatusManaWithIndex(index)
    case "LV":    return cppCode.getServantStatusLVWithIndex(index)
    case "AK":    return cppCode.getServantStatusLVWithIndex(index)
    case "DF":    return cppCode.getServantStatusDEFWithIndex(index)
    case "SP":    return cppCode.getServantStatusSPWithIndex(index)
    default:      fatalError()
    }
  }
  
  private func adjustYRangeInGraph(graph: CPTXYGraph) {
    let y = (graph.axisSet as! CPTXYAxisSet).yAxis
    let plotSpace = graph.defaultPlotSpace as! CPTXYPlotSpace
    
    switch swipeRadio.selectedIconID {
    case "MaxHP":
      fallthrough
    case "HP":
      y.majorIntervalLength         = CPTDecimalFromInt(200)
      y.minorTicksPerInterval       = 4
      plotSpace.yRange = CPTPlotRange(location: CPTDecimalFromInt(0), length: CPTDecimalFromInt(1100))
      
    case "MaxTP":
      fallthrough
    case "TP":
      y.majorIntervalLength         = CPTDecimalFromInt(200)
      y.minorTicksPerInterval       = 4
      plotSpace.yRange = CPTPlotRange(location: CPTDecimalFromInt(0), length: CPTDecimalFromInt(1100))
      
    case "LV":
      y.majorIntervalLength         = CPTDecimalFromDouble(20)
      y.minorTicksPerInterval       = 4
      plotSpace.yRange = CPTPlotRange(location: CPTDecimalFromInt(1), length: CPTDecimalFromInt(109))
      
    case "AK":
      y.majorIntervalLength         = CPTDecimalFromInt(200)
      y.minorTicksPerInterval       = 4
      plotSpace.yRange = CPTPlotRange(location: CPTDecimalFromInt(0), length: CPTDecimalFromInt(1100))
      
    case "DF":
      y.majorIntervalLength         = CPTDecimalFromInt(200)
      y.minorTicksPerInterval       = 4
      plotSpace.yRange = CPTPlotRange(location: CPTDecimalFromInt(0), length: CPTDecimalFromInt(1100))
      
    case "SP":
      y.majorIntervalLength         = CPTDecimalFromInt(200)
      y.minorTicksPerInterval       = 4
      plotSpace.yRange = CPTPlotRange(location: CPTDecimalFromInt(0), length: CPTDecimalFromInt(1100))
      
    default:
      fatalError()
    }
  }
  
  private func maxIDForIconID(iconID: String) -> String {
    switch iconID {
    case "HP":  return "MaxHP"
    case "TP":  return "MaxTP"
    default:    fatalError()
    }
  }
  
  // MARK: - private -
  private var viewFrame: CGRect

  private var background:       UIImageView!
  private var selectedButtonID: Int
  private var buttons:          [UIButton]!
  
  private var graphBackground:  UIImageView!
  private var graph:            CPTXYGraph! //BEMSimpleLineGraphView! X軸が詳細に指定できないので使えない
  let kScatterPlot1 = "StatusPlot1"
  let kScatterPlot2 = "StatusPlot2"
  private var swipeRadio:       SwipeRadioButtonView!
  
  private var timeStampedListWithPlayerIndex: [Int:[String:NSMutableArray]]!
  
}

protocol StatusGraphViewControllerDelegate: class {
  func backgroundImageInSuperView() -> UIImage
}

extension StatusGraphViewController : SwipeRadioButtonViewDelegate {
  
  func didSelectIcon(iconID: String) {
    print("\(iconID) selected")
    adjustYRangeInGraph(graph)
    graph.reloadData()
  }
  
}

extension StatusGraphViewController : CPTScatterPlotDataSource {
  
  func numberOfRecordsForPlot(plot: CPTPlot!) -> UInt {
    
    let selID = swipeRadio.selectedIconID
    
    if plot.identifier as! String == kScatterPlot1 {
      return UInt(timeStampedListWithPlayerIndex[selectedButtonID]![selID]!.count + 1)  // 直近の分を忘れないこと
    }
    else if plot.identifier as! String == kScatterPlot2 {
      if selID == "HP" {
        return UInt(timeStampedListWithPlayerIndex[selectedButtonID]!["MaxHP"]!.count + 1)
      }
      else if selID == "TP" {
        return UInt(timeStampedListWithPlayerIndex[selectedButtonID]!["MaxTP"]!.count + 1)
      }
      else {
        return 0
      }
    }
    else {
      fatalError()
    }
    
  }
  
  func numberForPlot(plot: CPTPlot!, field fieldEnum: UInt, recordIndex idx: UInt) -> AnyObject! {
    
    let selectedIconID = plot.identifier as! String == kScatterPlot1 ? swipeRadio.selectedIconID : maxIDForIconID(swipeRadio.selectedIconID)
    let data = timeStampedListWithPlayerIndex[selectedButtonID]![selectedIconID]!
    
    switch fieldEnum {
      
    case UInt(CPTScatterPlotField.X.rawValue):
      
      if Int(idx) == data.count {
        return NSNumber(double: cppCode.getCurrentRealTime() / 60.0)
      }
      
      return NSNumber(double: (data[Int(idx)] as! NSArray)[0] as! Double / 60.0)
      
      
    case UInt(CPTScatterPlotField.Y.rawValue):
      
      if Int(idx) == data.count {
        if selectedButtonID == 0 {
          return NSNumber(integer: valueForPlayerStatus(selectedIconID))
        }
        else {
          return NSNumber(integer: valueForStatus(selectedIconID, servantIndex: selectedButtonID - 1))
        }
      }
      
      return NSNumber(integer: (data[Int(idx)] as! NSArray)[1] as! Int)
      
      
    default:
      fatalError()
    }
    
  }
  
}

extension StatusGraphViewController : CPTScatterPlotDelegate {
}

extension StatusGraphViewController : ZoomGraphViewDelegate {
  
  func characterNameInZoomGraphView(zoomGraphView: ZoomGraphView) -> String {
    if selectedButtonID == 0 {
      return cppCode.getPlayerName()
    }
    else {
      return cppCode.getServantNameWithIndex(selectedButtonID)
    }
  }
  
  func zoomGraphView(zoomGraphView: ZoomGraphView, plot: CPTPlot, valueForPlotXAtIndex index: Int) -> CGFloat {
    
    let selectedIconID = plot.identifier as! String == kScatterPlot1 ? swipeRadio.selectedIconID : maxIDForIconID(swipeRadio.selectedIconID)
    
    let data = timeStampedListWithPlayerIndex[selectedButtonID]![selectedIconID]!
    
    if Int(index) == data.count {
      return CGFloat(cppCode.getCurrentRealTime() / 60.0)
    }
    else {
      return (data[Int(index)] as! NSArray)[0] as! CGFloat / 60.0
    }
  }
  
  func zoomGraphView(zoomGraphView: ZoomGraphView, plot: CPTPlot, valueForPlotYAtIndex index: Int) -> Int {
    
    let selectedIconID = plot.identifier as! String == kScatterPlot1 ? swipeRadio.selectedIconID : maxIDForIconID(swipeRadio.selectedIconID)
    let data = timeStampedListWithPlayerIndex[selectedButtonID]![selectedIconID]!
    
    if Int(index) == data.count {
      if selectedButtonID == 0 {
        return valueForPlayerStatus(selectedIconID)
      }
      else {
        return valueForStatus(selectedIconID, servantIndex: selectedButtonID - 1)
      }
    }
    
    return (data[Int(index)] as! NSArray)[1] as! Int
  }
  
  func zoomGraphView(zoomGraphView: ZoomGraphView, adjustYRangeInGraph graph: CPTXYGraph) {
    adjustYRangeInGraph(graph)
  }
  
  func selectedIDInZoomGraphView(zoomGraphView: ZoomGraphView) -> String {
    return swipeRadio.selectedIconID
  }
  
  func maxIDForSelectedIDInZoomGraphView(zoomGraphView: ZoomGraphView) -> String {
    return maxIDForIconID(swipeRadio.selectedIconID)
  }
  
}