//
//  DataBrainkeepViewController.swift
//  Aufm
//
//  Created by moti on 2015/07/19.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//
/*

import UIKit

class DataBrainkeepViewController : UIViewController {
  
  private var commonView: DataCommonView!
  private var brainkeepView: DataBrainkeepView!
  private var statusView: DataBrainkeepStatusView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    commonView = DataCommonView(selected: .Brainkeep, notificationPrefix: Notifications.DataBrainkeepPrefix, isSecondScene: true)
    super.view.addSubview(commonView)
    let UpMost = commonView.bottomOfBar
    let Rect = CGRectMake(0, UpMost, Screen.size.width, Screen.size.height - UpMost)
    
    let Left  = Rect.width * 0.025
    let Top   = Rect.height * 0.05 + UpMost
    
    brainkeepView = DataBrainkeepView(frame: CGRectMake(Left, Top, Rect.width * 0.4, Rect.height * 0.75))
    super.view.addSubview(brainkeepView)
    statusView = DataBrainkeepStatusView(frame: CGRectMake(brainkeepView.frame.maxX + Rect.width * 0.025, Top, Rect.width * 0.5, Rect.height * 0.875))
    super.view.addSubview(statusView)
    
    /*
      NSNotificationCenter
    */
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "backButtonPushed", name: Notifications.DataBrainkeepPrefix + Notifications.PushedBackButtonSuffix, object: nil)
  }
  
  override func viewDidAppear(animated: Bool) {
    brainkeepView.setData()
    statusView.setData()
  }
  
  func backButtonPushed() {
    self.navigationController?.popToRootViewControllerAnimated(false)
  }
}

*/