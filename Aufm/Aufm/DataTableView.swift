//
//  DataTableView.swift
//  Aufm
//
//  Created by moti on 2015/07/12.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

class DataTableView : UITableView {
  
  override init(frame: CGRect) {
    super.init(frame: frame, style: .Plain)
    
    self.backgroundColor = UIColor.whiteStyleBackgroundColor()
    self.rowHeight = frame.height * 0.12
    self.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
  }
  
  required init(coder aDecoder: NSCoder) { fatalError("NSCoder x") }
    
}

extension DataTableView : UITableViewDelegate {
  func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    if indexPath.row % 2 == 0 {
      cell.backgroundColor = UIColor.whiteStyleBackgroundColor()
    }
    else {
      cell.backgroundColor = UIColor(
        hue:        0.61,
        saturation: 0.09,
        brightness: 0.99,
        alpha:      0.9)
    }
    cell.selectedBackgroundView = UIView(frame: cell.frame)
    cell.selectedBackgroundView!.backgroundColor = UIColor.whiteStyleSelectedTableCellColor()
  }
}