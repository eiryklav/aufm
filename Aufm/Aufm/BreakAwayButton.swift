//
//  BreakAwayButton.swift
//  AufmGame
//
//  Created by moti on 2015/09/05.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmCommonComponentViews

class BreakAwayButton : UIButton, BTProtocol,
  BGNone, SEButtonTouchClick, BTLabelAboveLayer, BTTouchActionNoEffect {
  
  var label:            UILabel!
  var layer_:           CALayer!
  var highlightedLayer: CALayer!
  
  required init?(coder aDecoder: NSCoder) { fatalError() }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    label = UILabel()
    label.text = "離脱"
    label.font = UIFont(sizeType: .Medium)!
    label.textColor = UIColor.whiteColor()
    label.sizeToFit()
    label.textAlignment = .Center
    label.frame.origin = CGPointMake(
      (frame.width - label.frame.size.width) / 2,
      (frame.height - label.frame.size.height) / 2
    )
    
    // Normal
    let gradient = CAGradientLayer()
    gradient.colors = [UIColor(rgba: "#FF5E3AFF").CGColor, UIColor(rgba: "#FF2A68FF").CGColor]
    gradient.frame = bounds
    gradient.cornerRadius = 4.0
    
    let borderLayer = CALayer()
    borderLayer.cornerRadius = 4.0
    borderLayer.borderColor = UIColor.blackColor().CGColor
    borderLayer.frame = bounds
    borderLayer.borderWidth = 1.0
    
    layer_ = CALayer()
    layer_.insertSublayer(gradient, atIndex: 0)
    layer_.insertSublayer(borderLayer, atIndex: 1)
    
    // Highlighted
    let highlightedGradient = CAGradientLayer()
    highlightedGradient.colors = [UIColor(rgba: "#DF3E1AFF").CGColor, UIColor(rgba: "#DF0A48FF").CGColor]
    highlightedGradient.frame = bounds
    highlightedGradient.cornerRadius = 4.0
    
    let highlightedBorder = CALayer()
    highlightedBorder.cornerRadius = 4.0
    highlightedBorder.borderColor = UIColor.blackColor().CGColor
    highlightedBorder.frame = bounds
    highlightedBorder.borderWidth = 1.0
    
    highlightedLayer = CALayer()
    highlightedLayer.insertSublayer(highlightedGradient, atIndex: 0)
    highlightedLayer.insertSublayer(highlightedBorder, atIndex: 1)
    
    applyBackground()
    applyTitle()
    applySEButtonTouchForControlEvents(.TouchUpInside)
  }
  
  
}

