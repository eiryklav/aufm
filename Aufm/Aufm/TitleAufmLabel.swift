//
//  TitleAufmLabel.swift
//  Aufm
//
//  Created by moti on 2015/06/17.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

class TitleAufmLabel : UILabel {
  
  let FontSize: CGFloat = adjustFontSize(52.0)
  
  required init(coder aDecoder: NSCoder) {
    fatalError("NSCoder not supported")
  }
  
  init() {
    super.init(frame: CGRectZero)
    
    self.text = "Aufm"
    self.font = UIFont(name: "Simsun", size: FontSize)
    self.sizeToFit()
  }
}
