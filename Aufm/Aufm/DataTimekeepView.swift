//
//  DataTimekeepView.swift
//  Aufm
//
//  Created by moti on 2015/07/18.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

class DataTimekeepView : UIView {
  
  private var label: BaseLabel!
  
  required init(coder: NSCoder) { fatalError("NSCoder x") }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    self.backgroundColor = UIColor.blackStyleTerminalBackgroundColor()
    
    label = BaseLabel(origin: CGPointMake(frame.width * 0.025, frame.height * 0.025), text: "", sizeType: .Small, color: UIColor.greenColor())
    self.addSubview(label)
  }
}

protocol DataTimekeepViewProtocol {
  func setData()
}

extension DataTimekeepView : DataTimekeepViewProtocol {
  func setData() {
    // ゲームを終えて一度タイトル画面に行った後２回めのプレイであれば、初期化が呼び出されないためinitだけで済まさない
    var text = "./timekeep.afm\n\n"
    var i = 1;
    for item in Global.settingInfo.targetOfBeatsWithEnemyID {
      text += "No\(i++). \(cppCode.getChrStatusNameWithID(item.0)) = \(item.1)\n"
    }
    label.text = text
    label.sizeToFit()
  }
}