//
//  BackgroundView.swift
//  AufmGame
//
//  Created by moti on 2015/09/01.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit

class BackgroundView : UIView {
  
  required init(coder: NSCoder) { fatalError() }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    let imageView = UIImageView(frame: bounds)
    imageView.image = UIImage(named: "background")
    addSubview(imageView)
  }
  
}
