//
//  DLTableViewCellForBattle.swift
//  Aufm
//
//  Created by moti on 2015/07/16.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

protocol DLTableViewCellForBattleProtocol {
  func detailButtonPushed()
}

class DLTableViewCellForBattle : DataTimelineTableViewCell, DLTableViewCellForBattleProtocol {
  
  private var detailButton : UIButton!
  var indexPath: NSIndexPath!
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String!) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    
    self.selectionStyle = .None
    
    timeView.backgroundColor = UIColor.whiteStyleTimelineTimeBackgroundColor()
    mainView.backgroundColor = UIColor.whiteStyleTimelineWayOfUseBackgroundColor()
    
    detailButton = UIButton()
    detailButton.setImage(UIImage(named: "detail-button")!, forState: .Normal)
    detailButton.addTarget(self, action: "detailButtonPushed", forControlEvents: .TouchUpInside)
    self.addSubview(detailButton)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    detailButton.frame.size = CGSizeMake(frame.width * 0.2, frame.height * 0.65)
    detailButton.frame.origin = CGPointMake(frame.width * 0.75, centerOf(selfLength: frame.height * 0.65, superLength: frame.height))
  }
  
  required init(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
  
  func detailButtonPushed() {
    AudioManagerView.sharedInstance.playSE("click")
    Global.beforeViewImage = UIImage.imageFromView()
    let mapEdgeID = Global.passedMapEdges[self.indexPath.row]
    app.dlBattleDetailViewController.mapEdgeID = mapEdgeID
    app.gameNavigationController.pushViewController(app.dlBattleDetailViewController, animated: false)
  }
  
}
