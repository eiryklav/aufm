//
//  DLBattleTableView.swift
//  Aufm
//
//  Created by moti on 2015/08/07.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

protocol DLBattleTableViewInit {
  init(frame: CGRect, mapEdgeID: MapEdge)
}

class DLBattleTableView : DLTableView, DLBattleTableViewInit {
  
  var mapEdgeID:    MapEdge
  var battleIndex:  Int = 0
  
  required init(frame: CGRect, mapEdgeID: MapEdge) {
    self.mapEdgeID = mapEdgeID
    super.init(frame: frame, style: .Plain)
    reloadData()
  }

  required init(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }

  required init(frame: CGRect, style: UITableViewStyle) {
    fatalError("init(frame:style:) has not been implemented")
  }

  required init(frame: CGRect) {
    fatalError("init(frame:) has not been implemented")
  }
  
  override func reloadData() {
    attrStrings = Global.battleMessages[mapEdgeID]![battleIndex].extractMessages(.All)
    super.superReloadData()
  }
}

extension DLBattleTableView : UITableViewDataSource {
  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return Global.battleMessages[mapEdgeID]![battleIndex].messages.count
  }
  
  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("TimelineCell", forIndexPath: indexPath) as! DLTableViewCell
    cell.frame.size = CGSizeMake(self.frame.width, cell.frame.height)
    cell.backgroundColor = UIColor.clearColor()
    cell.setData(attributedText: attrStrings[indexPath.row], timesec: Global.battleMessages[mapEdgeID]![battleIndex].messages[indexPath.row].timeStamp)
    cell.accessoryType = .None
    return cell
  }
}
