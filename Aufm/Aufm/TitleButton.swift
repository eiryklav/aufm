//
//  TitleButton.swift
//  Aufm
//
//  Created by moti on 2015/06/17.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

class TitleButton : BaseButton {
  
  let FontSize: CGFloat = adjustFontSize(23.0)
  
  required init(coder: NSCoder) {
    fatalError("NSCoding not supported")
  }
  
  init() {
    super.init(size: CGSizeZero)
  }
  
  convenience init(text: String) {
    self.init()
    
    self.titleLabel!.font = UIFont(name: "Simsun", size: FontSize)
    self.setTitle(text, forState: .Normal)
    self.frame.size = CGSizeMake(Screen.size.width/3.0, Screen.size.height/8.0)
  }

  required init(size: CGSize) {
      fatalError("init(size:) has not been implemented")
  }
}
