//
//  ConversationView.swift
//  Aufm
//
//  Created by moti on 2015/06/16.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmSupport

class ConversationView : UIView {
  private var conversationLabel: ConversationLabel!
  private var borderImageView: UIImageView!
  
  func presentText(text: String) {
    conversationLabel.presentText(text)
  }
  
  required init?(coder aDecoder: NSCoder) { fatalError() }
  
  override func hitTest(point: CGPoint, withEvent event: UIEvent?) -> UIView? {
    return nil
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    layer.zPosition = 3
    backgroundColor = UIColor(rgba: "#33333388")
    layer.setShadowForView()
    conversationLabel = ConversationLabel(margin: CGRectMake(frame.width * 0.03, frame.height * 0.2, frame.width, 5000))
    addSubview(conversationLabel)
    borderImageView = UIImageView(image: UIImage(named: "conversation-blink-border"))
    borderImageView.frame = CGRectMake(0, 0, frame.size.width, frame.size.height)
    borderImageView.contentMode = UIViewContentMode.ScaleToFill
    borderImageView.alpha = 0.0
    addSubview(borderImageView)
  }
  
  func presentAlertText(text: String) {
    conversationLabel.presentText(text, duration: 3.0)
    UIView.animateWithDuration(
      0.05,
      delay: 0.0,
      options: .CurveLinear,
      animations: {
        self.borderImageView.alpha = 1.0
      }, completion: { _ in
        UIView.animateWithDuration(0.8, delay: 0.0, options: .CurveEaseOut,
          animations: {
            self.borderImageView.alpha = 0.0
          }, completion: { _ in
            self.borderImageView.alpha = 0.0
          }
        )
      }
    )
  }
  
}
