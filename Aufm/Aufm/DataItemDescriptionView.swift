//
//  DataItemDescriptionView.swift
//  Aufm
//
//  Created by moti on 2015/07/11.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

class DataItemDescriptionView : DataBriefFlavorDescriptionView {
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "setData:", name: Notifications.DataItemIDSet, object: nil)
  }
  
  required init(coder: NSCoder) { fatalError("NSCoder x") }
  
}

protocol DataItemDescriptionViewProtocol {
  func setData(notification: NSNotification)
}

extension DataItemDescriptionView : DataItemDescriptionViewProtocol {
  func setData(notification: NSNotification) {
    let id = notification.userInfo!["ID"]! as! Int
    nameLabel.text = "\(cppCode.getItemNameWithID(id))"
    nameLabel.frame.size = CGSizeMake(5000, nameLabel.frame.height)
    nameLabel.sizeToFit()
    briefDescLabel.text = "\(cppCode.getItemDescriptionWithID(id))"
    briefDescLabel.frame.size = CGSizeMake(5000, briefDescLabel.frame.height)
    briefDescLabel.sizeToFit()
    flavorDescLabel.frame.size = CGSizeMake(flavorDescScrollView.frame.width, 5000)
    flavorDescLabel.text = "\(cppCode.getItemFlavorWithID(id))"
    flavorDescLabel.sizeToFit()
    flavorDescScrollView.contentSize = flavorDescLabel.frame.size
  }
}