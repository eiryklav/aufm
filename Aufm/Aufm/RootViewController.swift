//
//  RootViewController.swift
//  Aufm
//
//  Created by moti on 2015/08/23.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmSetting

class RootViewController : UIViewController {
  let titleViewController = TitleViewController()
  // ...
  
  let gameViewController                = GameViewController()
  let dataStatusViewController          = DataStatusViewController()
  let dataItemViewController            = DataItemViewController()
  let dataAbilityViewController         = DataAbilityViewController()
  let dataMoneyViewController           = DataMoneyViewController()
  let dataLogViewController             = DataLogViewController()
  let dlBattleDetailViewController      = DLBattleDetailViewController()
  let dataTimekeepViewController        = DataTimekeepViewController()
  let dataBrainkeepViewController       = DataBrainkeepViewController()
  let optionWindowViewController        = OptionWindowViewController()
  let naviMapDescriptionViewController  = NaviMapDescriptionViewController()
}
