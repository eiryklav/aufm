//
//  ConversationLabel.swift
//  Aufm
//
//  Created by moti on 2015/06/16.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmSupport

class ConversationLabel : UILabel {
  
  var LabelMarginLeft:  CGFloat = 0.0
  var LabelMarginTop:   CGFloat = 0.0
  var LabelWidth:       CGFloat = 0.0
  var LabelHeight:      CGFloat = 0.0
  
  required convenience init(margin: CGRect) {
    self.init()
    
    LabelMarginLeft = margin.origin.x
    LabelMarginTop  = margin.origin.y
    LabelWidth      = margin.width
    LabelHeight     = margin.height
    
    font = UIFont(name: "AndaleMono", sizeType: .Medium)
    textColor = UIColor(rgba: "#DDDDDDAA")
    layer.shadowOpacity = 0.5
    numberOfLines = 0
  }
  
  func presentText(text: String, duration: Double = 10.0) {
    alpha = 1.0
    frame = CGRectMake(LabelMarginLeft, LabelMarginTop, LabelWidth, LabelHeight)
    self.text = text
    sizeToFit()
    NSTimer.scheduledTimerWithTimeInterval(duration, target: NSBlockOperation(block: {
      UIView.animateWithDuration(0.25, animations: {
        self.alpha = 0.0
      })
    }), selector: "main", userInfo: nil, repeats: false)
  }
}