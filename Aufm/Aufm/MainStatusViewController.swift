//
//  DataStatusViewController.swift
//  Aufm
//
//  Created by moti on 2015/06/30.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmCommonComponentViews
import AufmObjCppWrapper
import AufmVisualSoundEffect
import TabDialogView
import ExPickerView

class MainStatusViewController : UIViewController {
  
  required init?(coder aDecoder: NSCoder) { fatalError() }
  
  init(viewFrame: CGRect) {
    // self.view を変更しないこと
    self.viewFrame = viewFrame
    tabDialogView = TabDialogView(frame:
      CGRectMake(
        viewFrame.width * 0.05,
        viewFrame.height * 0.05,
        viewFrame.width * 0.9,
        viewFrame.height * 0.9
      )
    )
    super.init(nibName: nil, bundle: nil)
  }
  
  override func loadView() {
    view = UIView(frame: viewFrame)
    view.addSubview(tabDialogView)
    
    tabDialogView.dataSource  = self
    tabDialogView.delegate    = self
    tabDialogView.loadSubviews()
    
    let numberOfPlayers = cppCode.getNumOfPlayers()
    for index in 0..<4+1 {
      
      let subTitle = MainStatusSubTitleViewController(subTitleIndex: index, viewFrame: tabDialogView.subTitleViewFrame)
      addChildViewController(subTitle)
      subTitle.didMoveToParentViewController(self)
      childViewControllers[index * 2].loadView()
      
      let content = MainStatusContentViewController(contentIndex: index, viewFrame: tabDialogView.contentViewFrame)
      addChildViewController(content)
      content.didMoveToParentViewController(self)
      childViewControllers[index * 2 + 1].loadView()
      
      if numberOfPlayers <= index && index < 4 {
        tabDialogView.hideTabButtonWithIndex(index)
      }
    }
    
    tabDialogView.reload()
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    
    childViewControllers[tabDialogView.selectedIndex * 2].viewDidLoad()
    childViewControllers[tabDialogView.selectedIndex * 2 + 1].viewDidLoad()
  }
  
  // MARK: private
  
  private var viewFrame:            CGRect
  private var tabDialogView:        TabDialogView
  
  private var playersSubTitleViews: [UIView]!
  private var playersContentViews:  [UIView]!
  
  private func reloadViewsInTabWithIndex(index: Int) {
    childViewControllers[index * 2 + 1].viewDidLoad()
  }

}

extension MainStatusViewController : TabDialogViewDataSource {
  
  func numberOfTabButtonsInTabDialogView() -> Int {
    return 4 + 1  // 最大プレイヤー数 + 死亡者帳簿
  }
  
  func tabDialogView(tabDialogView: TabDialogView, subTitleViewForIndex index: Int) -> UIView {
    return childViewControllers[index * 2].view
  }
  
  func tabDialogView(tabDialogView: TabDialogView, contentViewForIndex index: Int) -> UIView {
    return childViewControllers[index * 2 + 1].view
  }
  
}

extension MainStatusViewController : TabDialogViewDelegate {
  func tabDialogView(tabDialogView: TabDialogView, didPushTabButtonWithIndex index: Int) {
    Audio.sharedInstance.playSE("click")
    reloadViewsInTabWithIndex(index)
  }
}
