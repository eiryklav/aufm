//
//  DLTableView.swift
//  Aufm
//
//  Created by moti on 2015/07/15.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

protocol DLTableViewInit {
  init(frame: CGRect, style: UITableViewStyle)
  init(frame: CGRect)
}

class DLTableView : UITableView, DLTableViewInit {
  
  var messageClassification: Messages.MessageClassification = .All
  var attrStrings = [NSAttributedString]()
    
  required override init(frame: CGRect, style: UITableViewStyle) {
    super.init(frame: frame, style: style)
    self.estimatedRowHeight = 5
    self.rowHeight = UITableViewAutomaticDimension
    self.dataSource = self
    self.separatorColor = UIColor.clearColor()
    self.backgroundColor = UIColor.whiteStyleBackgroundColor()
    self.registerClass(DLTableViewCell.self, forCellReuseIdentifier: "TimelineCell")
    self.registerClass(DLTableViewCellForBattle.self, forCellReuseIdentifier: "BattleTimelineCell")
  }
  
  required override init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  required init(coder aDecoder: NSCoder) { fatalError("NSCoder x") }
  
}

protocol DLTableViewProtocol {
  func reloadData()
  func superReloadData()
}

extension DLTableView : DLTableViewProtocol {
  override func reloadData() {
    switch messageClassification {
    case .Battle:
      attrStrings = []
      for mapEdge in Global.passedMapEdges {
        let maparr = mapEdge.mapID.componentsSeparatedByString("-Map")
        let mapID = maparr[1].toInt()!
        let mapIDChar = Character(UnicodeScalar(65 + mapID - 1))
        attrStrings.append(NSAttributedString(string: "マップ \(mapIDChar)  ルート \(mapEdge.edgeID)"));
      }
      
    default:
      attrStrings = Global.allMessages.extractMessages(messageClassification)
    }
    super.reloadData()
    self.setContentOffset(CGPointMake(0, self.contentSize.height - self.frame.size.height), animated: false)
  }
  
  func superReloadData() {
    super.reloadData()
  }
}

extension DLTableView : UITableViewDataSource {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return attrStrings.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    if messageClassification == .Battle {
      let cell = tableView.dequeueReusableCellWithIdentifier("BattleTimelineCell", forIndexPath: indexPath) as! DLTableViewCellForBattle
      cell.indexPath = indexPath
      cell.frame.size = CGSizeMake(self.frame.width, cell.frame.height)
      cell.backgroundColor = UIColor.clearColor()
      cell.setData(attributedText: attrStrings[indexPath.row], timesec: Global.allMessages.messages[indexPath.row].timeStamp)
      cell.accessoryType = .None
      return cell
    }
    else {
      let cell = tableView.dequeueReusableCellWithIdentifier("TimelineCell", forIndexPath: indexPath) as! DLTableViewCell
      cell.frame.size = CGSizeMake(self.frame.width, cell.frame.height)
      cell.backgroundColor = UIColor.clearColor()
      cell.setData(attributedText: attrStrings[indexPath.row], timesec: Global.allMessages.messages[indexPath.row].timeStamp)
      cell.accessoryType = .None
      return cell
    }
  }
}