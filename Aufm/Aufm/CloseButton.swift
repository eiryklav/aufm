//
//  CloseButton.swift
//  AufmData
//
//  Created by moti on 2015/09/06.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmCommonComponentViews

class CloseButton : UIButton, BTProtocol,
  BGNone, SEButtonTouchClick, BTLabelAboveLayer, BTTouchActionNoEffect {
  
  let text      = "×"
  let font_     = UIFont(name: "Lucida Grande", sizeType: .Medium)!
  let textColor = UIColor.whiteColor()
  var layer_: CALayer!
  var highlightedLayer: CALayer!
  
  required init?(coder aDecoder: NSCoder) { fatalError() }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    layer_ = CALayer()
    highlightedLayer = CALayer()
    highlightedLayer.contents = UIImage(named: "select-menu-bar-button")!.CGImage!
    highlightedLayer.position = CGPointZero
    
    applyBackground()
    applyTitle()
    applySEButtonTouchForControlEvents(.TouchUpInside)
  }
  
}
