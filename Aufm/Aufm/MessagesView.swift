//
//  MessageView.swift
//  Aufm
//
//  Created by moti on 2015/06/11.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmVisualSoundEffect

class MessagesView : UIView {
  
  weak var dataSource:  MessagesViewDataSource?
  weak var delegate:    MessagesViewDelegate?
  var messagesLabel:    MessagesLabel!
  var characterView:    CharacterAnimationView!
  
  required init?(coder aDecoder: NSCoder) { fatalError() }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    backgroundColor = UIColor(rgba: "#00000033")
    
    // --------------------------------------
    // CharacterView
    
    let kSCWidth  = UIScreen.mainScreen().bounds.width
    let kSCHeight = UIScreen.mainScreen().bounds.height
    characterView = CharacterAnimationView(frame: CGRectMake(kSCWidth * 0.7 - frame.minX, kSCHeight * 0.2, kSCWidth * 0.3, kSCHeight * 0.8))
    characterView.characterName = "agent"
    characterView.hidden = true // MARK: FOR DEBUG
    addSubview(characterView)
    
    // --------------------------------------
    // MessagesLabel

    messagesLabel = MessagesLabel(margin: CGRectMake(frame.width * 0.05, frame.height * 0.075, frame.width * 0.85, 5000))
    addSubview(messagesLabel)
    
  }
  
  func reload() {
    if let dataSource = dataSource {
      messagesLabel.setAttributedStrings(dataSource.attributedStringsForView())
    }
  }
  
  override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
    Audio.sharedInstance.playSE("click")
    delegate?.didTouchMessagesView()
  }
  
}

protocol MessagesViewDataSource: class {
  func attributedStringsForView() -> [NSAttributedString]
}

protocol MessagesViewDelegate: class {
  func didTouchMessagesView()
}