//
//  DataTimelineTableViewCell.swift
//  Aufm
//
//  Created by moti on 2015/07/15.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

class DataTimelineTableViewCell : UITableViewCell {
  
  var timeView: UIView!
  var timeLabel: BaseLabel!
  var mainView: UIView!
  var mainLabel: BaseLabel!
  
  override func layoutSubviews() {
    super.layoutSubviews()
    timeView.frame = CGRectMake(0, 0, frame.width * 0.2, timeView.frame.height)
    mainView.frame = CGRectMake(frame.width * 0.2, 0, frame.width * 0.8, mainView.frame.height)
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String!) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    timeView = UIView(frame: CGRectMake(0, 0, frame.width * 0.2, frame.height))
    timeView.backgroundColor = UIColor.whiteStyleTimelineTimeBackgroundColor()
    self.addSubview(timeView)
    
    timeLabel = BaseLabel(text: "00:00", sizeType: .Small, color: UIColor.blackColor())
    timeView.addSubview(timeLabel)
    
    mainView = UIView(frame: CGRectMake(frame.width * 0.2, 0, frame.width * 0.8, frame.height))
    mainView.backgroundColor = UIColor.whiteStyleTimelineWayOfUseBackgroundColor()
    self.addSubview(mainView)
    
    mainLabel = BaseLabel(origin: CGPointMake(mainView.frame.width * 0.05, timeLabel.frame.minY), text: "", sizeType: .Small, color: UIColor.blackColor())
    mainView.addSubview(mainLabel)
  }
  
  required init(coder: NSCoder) { fatalError("NSCoder x") }
}

protocol DataTimelineTableViewCellProtocol {
  func setData(#attributedText: NSAttributedString, timesec: Double)
}

extension DataTimelineTableViewCell : DataTimelineTableViewCellProtocol {
  func setData(#attributedText: NSAttributedString, timesec: Double) {
    let min = Int(floor(timesec / 60.0))
    let sec = Int(floor(timesec)) - min * 60
    timeLabel.text = NSString(format: "%02d:%02d", min, sec) as String
    timeLabel.sizeToFit()
    timeLabel.frame.origin = centerOf(selfSize: timeLabel.frame.size, superSize: timeView.frame.size)
    mainLabel.frame.size = CGSizeMake(mainView.frame.width * 0.95, mainLabel.frame.height)
    mainLabel.attributedText = attributedText
    mainLabel.sizeToFit()
    mainLabel.frame.origin = CGPointMake(mainLabel.frame.minX, centerOf(selfLength: mainLabel.frame.height, superLength: mainView.frame.height))
  }
}