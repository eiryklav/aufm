//
//  DataStatusTransitionView.swift
//  Aufm
//
//  Created by moti on 2015/07/09.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import CorePlot

protocol DataStatusTransitionViewInit {
  init(frame: CGRect, firstPlayerID: Int)
}

class DataStatusTransitionView : CPTGraphHostingView, DataStatusTransitionViewInit {
  
  private var currentPlayerID: Int!
  private var currentStatus = Global.StatusForDisp.LV
  private var graph: DataXYGraph!
  
  let FirstScatterID  = "FirstScatterID"
  let SecondScatterID = "SecondScatterID"
  
  required init(frame: CGRect, firstPlayerID: Int) {
    
    super.init(frame: frame)
    currentPlayerID = firstPlayerID

    graph = DataXYGraph(frame: frame)
    self.hostedGraph = graph
    let plot1 = CPTScatterPlot(frame: frame)
    plot1.dataSource = self
    plot1.dataLineStyle = graph.lineStyleForScatter
    plot1.identifier = FirstScatterID
    //plot1.plotSymbol = plotSymbol
    graph.addPlot(plot1)
    
    let plot2 = CPTScatterPlot(frame: frame)
    plot2.dataSource = self
    graph.lineStyleForScatter.lineColor = CPTColor(componentRed: 0.531, green: 0.50, blue: 0.88, alpha: 0.80)
    plot2.dataLineStyle = graph.lineStyleForScatter
    plot2.identifier = SecondScatterID
    graph.addPlot(plot2)
    
    /*
      NSNotificationCenter
    */
    
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "plotStatusSegmentSelected:", name: Notifications.ValueChangedPlotStatus, object: nil)
  }
  
  required init(coder: NSCoder) { fatalError("NSCoder x") }
  
  private func checkHasBeenDead() -> Bool {
    // 対象のキャラクタが死亡している場合はグラフを止める
    // ※最後に記録されたConditionがdeadであるかどうかで判断しているので、これが適用できない場合は変更が必要。
    let condition = Global.statusOfTimeStamp[currentPlayerID]![StatusIDForDisp(.Condition)]!.last!.1 as! Int
    if condition & ConditionDead > 0 { return true }
    return false
  }
  
}

protocol DataStatusTransitionViewProtocol {
  func plotStatusSegmentSelected(notification: NSNotification)
  func setPlotGraph(currentPlayerID: Int)
}

extension DataStatusTransitionView : DataStatusTransitionViewProtocol {
  
  func plotStatusSegmentSelected(notification: NSNotification) {
    
    currentStatus = Global.StatusForDisp(rawValue: notification.userInfo!["Index"]! as! Int)!
    
    var yIntervalLength: Int32 = 20
    var location: Int32 = 0
    var length: Int32   = 100
    
    switch currentStatus {
    case .LV:       (yIntervalLength, location, length) = (20, 1, 100)
    case .CurrHP:   fallthrough
    case .CurrMana: fallthrough
    case .ATK:      fallthrough
    case .DEF:      fallthrough
    case .SP:       (yIntervalLength, location, length) = (100, 0, 999)
    default:
      break
    }
    
    (graph.axisSet as! CPTXYAxisSet).yAxis.majorIntervalLength = CPTDecimalFromInt(yIntervalLength)
    
    let plotSpace = graph.defaultPlotSpace as! CPTXYPlotSpace
    plotSpace.yRange = CPTPlotRange(location: CPTDecimalFromInt(location), length: CPTDecimalFromInt(length))
    graph.reloadData()
  }
  
  func setPlotGraph(currentPlayerID: Int) {
    self.currentPlayerID = currentPlayerID
    graph.reloadData()
  }
}

extension DataStatusTransitionView : CPTPlotDataSource {
  
  func numberOfRecordsForPlot(plot: CPTPlot!) -> UInt {
    
    var status = currentStatus
    
    switch currentStatus {
    case .CurrHP:   if plot.identifier as! String == SecondScatterID { status = .MaxHP }
    case .CurrMana: if plot.identifier as! String == SecondScatterID { status = .MaxMana }
    default:
      if plot.identifier as! String == SecondScatterID { return 0 }
    }
    
    return UInt(Global.statusOfTimeStamp[currentPlayerID]![StatusIDForDisp(status)]!.count + 1)
  }
  
  func numberForPlot(plot: CPTPlot!, field fieldEnum: UInt, recordIndex index: UInt) -> AnyObject! {
    
    let data: [(Double, Any)] = Global.statusOfTimeStamp[currentPlayerID]![StatusIDForDisp(currentStatus)]!
    
    switch fieldEnum {
    case UInt(CPTScatterPlotField.X.rawValue):
      
      if plot.identifier as! String == FirstScatterID {
        
        if Int(index) >= data.count {
          if checkHasBeenDead() { return (data.last!.0 / 60.0) as AnyObject! }
          return cppCode.getCurrentRealTime() / 60.0 as AnyObject!
        }
        return (data[Int(index)].0 / 60.0) as AnyObject!
      }
      else {
        var data2 = [(Double, Any)]()
        switch currentStatus {
        case .CurrHP:   data2 = Global.statusOfTimeStamp[currentPlayerID]![StatusIDForDisp(.MaxHP)]!
        case .CurrMana: data2 = Global.statusOfTimeStamp[currentPlayerID]![StatusIDForDisp(.MaxMana)]!
        default:
          assert(false, "ここには来ません")
        }
        if Int(index) >= data2.count {
          if checkHasBeenDead() { return (data.last!.0 / 60.0) as AnyObject! }
          return cppCode.getCurrentRealTime() / 60.0 as AnyObject!
        }
        return (data2[Int(index)].0 / 60.0) as AnyObject!
      }
      
    case UInt(CPTScatterPlotField.Y.rawValue):
      
      if plot.identifier as! String == FirstScatterID {
        if Int(index) >= data.count {
          if checkHasBeenDead() { return (data.last!.1 as! Int) as AnyObject! }
          return (data.last!.1 as! Int) as AnyObject!
        }
        return (data[Int(index)].1 as! Int) as AnyObject!
      }
      else {
        var data2 = [(Double, Any)]()
        switch currentStatus {
        case .CurrHP:   data2 = Global.statusOfTimeStamp[currentPlayerID]![StatusIDForDisp(.MaxHP)]!
        case .CurrMana: data2 = Global.statusOfTimeStamp[currentPlayerID]![StatusIDForDisp(.MaxMana)]!
        default:
          assert(false, "ここには来ません")
        }
        if checkHasBeenDead() { return (data2.last!.1 as! Int) as AnyObject! }
        if Int(index) >= data2.count {
          return (data2.last!.1 as! Int) as AnyObject!
        }
        return (data2[Int(index)].1 as! Int) as AnyObject!
      }
      
    default:
      break;
    }
    
    return 0 as AnyObject!
  }
}