//
//  DataItemViewController.swift
//  Aufm
//
//  Created by moti on 2015/07/11.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

/*

import UIKit

class DataItemViewController : UIViewController {
  
  private var commonView: DataCommonView!
  private var itemTableView: DataItemTableView!
  private var itemDescriptionView: DataItemDescriptionView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    commonView = DataCommonView(selected: .Item, notificationPrefix: Notifications.DataItemPrefix)
    self.view.addSubview(commonView)
    
    let UpMost = commonView.bottomOfBar
    let Rect = CGRectMake(0,UpMost,Screen.size.width,Screen.size.height-UpMost)
    
    itemTableView = DataItemTableView(frame: CGRectMake(Rect.width * 0.025, UpMost + Rect.height * 0.05, Rect.width * 0.5, Rect.height * 0.75))
    self.view.addSubview(itemTableView)
    
    itemDescriptionView = DataItemDescriptionView(frame: CGRectMake(itemTableView.frame.maxX + Rect.width * 0.025, itemTableView.frame.minY, Rect.width * 0.425, Rect.height * 0.875))
    self.view.addSubview(itemDescriptionView)
    
    /*
      NSNotificationCenter
    */
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "backButtonPushed", name: Notifications.DataItemPrefix + Notifications.PushedBackButtonSuffix, object: nil)
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    itemTableView.setItems()
    itemDescriptionView.unsetData()
  }
  
  func backButtonPushed() {
    self.navigationController?.popToRootViewControllerAnimated(false)
  }
  
  
}

*/
