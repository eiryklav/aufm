//
//  DataMoneyViewController.swift
//  Aufm
//
//  Created by moti on 2015/07/14.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

/*

import UIKit

class DataMoneyViewController : UIViewController {
  
  private var commonView: DataCommonView!
  private var moneyView: UIView!
  private var possessMoneyView: DataPossessMoneyView!
  private var wayOfUseLabelView: DataWayOfUseMoneyLabelView!
  private var timelineTableView: DataWayOfUseTimelineTableView!
  private var moneyTransitionView: DataMoneyTransitionView!
  
  override func viewDidLoad() {
    commonView = DataCommonView(selected: .Money, notificationPrefix: Notifications.DataMoneyPrefix)
    self.view.addSubview(commonView)
    
    let UpMost = commonView.bottomOfBar
    let Rect = CGRectMake(0, UpMost, Screen.size.width, Screen.size.height - UpMost)
    let Top = UpMost + Rect.height * 0.05
    let Left = Screen.size.width * 0.025
    
    possessMoneyView = DataPossessMoneyView(frame: CGRectMake(Left, Top, Rect.width * 0.5, Rect.height * 0.125))
    self.view.addSubview(possessMoneyView)
    
    wayOfUseLabelView = DataWayOfUseMoneyLabelView(frame: CGRectMake(Left, possessMoneyView.frame.maxY, Rect.width * 0.5, Rect.height * 0.085))
    self.view.addSubview(wayOfUseLabelView)
    
    timelineTableView = DataWayOfUseTimelineTableView(frame: CGRectMake(Left, wayOfUseLabelView.frame.maxY, possessMoneyView.frame.width, Rect.height * 0.55))
    self.view.addSubview(timelineTableView)
    
    moneyTransitionView = DataMoneyTransitionView(frame: CGRectMake(timelineTableView.frame.maxX + Rect.width * 0.025, possessMoneyView.frame.minY, Rect.width * 0.425, Rect.height * 0.9))
    self.view.addSubview(moneyTransitionView)
    
    /*
      NSNotificationCenter
    */
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "backButtonPushed", name: Notifications.DataMoneyPrefix + Notifications.PushedBackButtonSuffix, object: nil)
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    possessMoneyView.setData()
    moneyTransitionView.setData()
    timelineTableView.reloadData()
  }
  
  func backButtonPushed() {
    self.navigationController?.popToRootViewControllerAnimated(false)
  }
}
*/