//
//  DataPossessMoneyView.swift
//  Aufm
//
//  Created by moti on 2015/07/14.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

class DataPossessMoneyView : UIView {
  
  private var leftDeadSpace : UIView!
  private var moneyView: UIView!
  private var moneyLabel: BaseLabel!

  private var wayOfUseLabel: BaseLabel!
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    leftDeadSpace = UIView(frame: CGRectMake(0, 0, frame.width * 0.2, frame.height))
    leftDeadSpace.backgroundColor = UIColor.deepWhiteStyleBackgroundColor()
    moneyView = UIView(frame: CGRectMake(frame.width * 0.2, 0, frame.width * 0.8, frame.height))
    moneyView.backgroundColor = UIColor.deepWhiteStyleBackgroundColor()
    moneyLabel = BaseLabel(origin: CGPointMake(moneyView.frame.width * 0.05, moneyView.frame.height * 0.3), text: "", sizeType: .SemiMedium, color: UIColor.blackColor())
    moneyView.addSubview(moneyLabel)
    self.addSubview(leftDeadSpace)
    self.addSubview(moneyView)
    setData()
  }
  
  required init(coder: NSCoder) { fatalError("NSCoder x") }
  
}

protocol DataProssessMoneyViewProtocol {
  func setData()
}

extension DataPossessMoneyView : DataProssessMoneyViewProtocol {
  func setData() {
    moneyLabel.text = "所持金 #\(cppCode.getPlayersMoney())"
    moneyLabel.sizeToFit()
  }
}
