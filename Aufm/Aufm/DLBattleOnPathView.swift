//
//  DLBattleOnPathView.swift
//  Aufm
//
//  Created by moti on 2015/08/07.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import SpriteKit

protocol DLBattleOnPathViewInit {
  init(frame: CGRect, mapEdgeID: MapEdge)
}

private var sizeRatio: CGSize!

extension CGSize {
  private static func adjustLocal(width width: CGFloat, height: CGFloat) -> CGSize {
    return CGSizeMake(width * sizeRatio.width, height * sizeRatio.height)
  }
  private func adjustedToLocal() -> CGSize {
    return CGSizeMake(width * sizeRatio.width, height * sizeRatio.height)
  }
}

extension CGPoint {
  private static func adjustLocal(x x: CGFloat, y: CGFloat) -> CGPoint {
    return CGPointMake(x * sizeRatio.width, y * sizeRatio.height)
  }
  private func adjustedToLocal() -> CGPoint {
    return CGPointMake(x * sizeRatio.width, y * sizeRatio.height)
  }
}

class DLBattleOnPathView : SKView, DLBattleOnPathViewInit {
  
  var sizeOfRawImage: CGSize! // マップ全体表示
  
  required init(frame: CGRect, mapEdgeID: MapEdge) {
    super.init(frame: frame)
    
    self.allowsTransparency = true
    self.layer.borderColor = UIColor.whiteStyleBorderColor().CGColor
    self.layer.borderWidth = 1.0
    
    // Main scene
    let scene = SKScene(size: frame.size)
    scene.backgroundColor = SKColor.clearColor()
    self.presentScene(scene)
    
    // Map sprite
    let mapSprite = SKSpriteNode(imageNamed: Global.settingInfo.worldName + "-" + mapEdgeID.mapID)
    
    // Set sizeRatio
    sizeOfRawImage = mapSprite.size
    sizeRatio = CGSizeMake(self.bounds.width / sizeOfRawImage.width, self.bounds.height / sizeOfRawImage.height)
    
    mapSprite.size = mapSprite.size.adjustedToLocal()
    mapSprite.position = self.center
    scene.addChild(mapSprite)
    
    // Get bezier path
    let pathArray = (cppCode.getBezierPathWithMapID(mapEdgeID.mapID, edgeID: mapEdgeID.edgeID) as [AnyObject]) as! [NSValue]
    
    let bezierPath = UIBezierPath()
    var point = pathArray[0].CGPointValue().adjustedToLocal()
    point.y = self.bounds.height-point.y
    bezierPath.moveToPoint(point);
    
    for var i=1; i<pathArray.count; i+=3 {
      var termpoint   = point + pathArray[i].CGPointValue().adjustedToLocal()
      var ctrlpoint1  = point + pathArray[i+1].CGPointValue().adjustedToLocal();
      var ctrlpoint2  = point + pathArray[i+2].CGPointValue().adjustedToLocal()
      bezierPath.addCurveToPoint(termpoint, controlPoint1: ctrlpoint1, controlPoint2: ctrlpoint2);
    }
    
    let pathNode = SKShapeNode()
    pathNode.path = bezierPath.CGPath
    pathNode.glowWidth = 1.0
    pathNode.strokeColor = SKColor(rgba: "#e74c3caa")
    pathNode.lineWidth = 2.0
    
    scene.addChild(pathNode)
  }

  required init(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}