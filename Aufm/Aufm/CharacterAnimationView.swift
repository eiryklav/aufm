//
//  CharacterAnimationView.swift
//  Aufm
//
//  Created by moti on 2015/06/16.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmSupport
import AufmVisualSoundEffect

class CharacterAnimationView: UIImageView {
  
  weak var    delegate: CharacterAnimationViewDelegate?
  var         characterName: String?
  
  private var bodyImageView: UIImageView!
  private var compImageViews = [UIImageView]()
  
  // Arrayの添字とOrderInLayerは同じではないが、どちらも昇順になっている
  private var stDicArray: [[String:AnyObject]]!
  private var stTimeIntervals: [Double]!
  private var stCompsPicCount: [Int]!
  
  private var bodyRawSize: CGSize!
  private var sizeRatio: CGSize!
  
  
  required init(coder aDecoder: NSCoder) { fatalError() }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    backgroundColor = UIColor.clearColor()
    userInteractionEnabled = true
  }
  
  
  private func loadConfigAndSortByOrderInLayer() {
    
    guard let characterName = characterName else { return }
    
    let stcompsloader = StandingComponentsLoader(JSONString: String.getJSONStringFromPath("\(characterName)-standing-config"))
    
    let r = stcompsloader.dicarray as [AnyObject]
    stDicArray = r as! [[String:AnyObject]]
    stDicArray.sortInPlace { return ($0["OrderInLayer"] as! Int) < ($1["OrderInLayer"] as! Int) }
    stTimeIntervals = Array(count: stDicArray.count, repeatedValue: 0.0)
    stCompsPicCount = Array(count: stDicArray.count, repeatedValue: 1)
  }
  
  private func makeBodyComponent() {
    
    guard let characterName = characterName else { return }
    
    for var i=0; i<stDicArray.count; i++ {
      
      guard stDicArray[i]["OrderInLayer"] as! Int == 0 else { continue }
      
      bodyImageView = UIImageView()
      let image = UIImage(named: "\(characterName)-body-1")
      bodyRawSize = image!.size
      bodyImageView.image = image
      bodyImageView.contentMode = UIViewContentMode.ScaleAspectFit  // 縦を基準に縮める
      bodyImageView.layer.zPosition = 0
      bodyImageView.frame.size = frame.size
      //bodyImageView.backgroundColor = UIColor.redColor()  // MARK: FOR DEBUG
      sizeRatio = CGSizeMake(frame.size.width / bodyRawSize.width, frame.size.height / bodyRawSize.height)
      bodyImageView.frame.origin = (stDicArray[i]["Position"] as! NSValue).CGPointValue()
      bodyImageView.frame.origin.x *= sizeRatio.width
      bodyImageView.frame.origin.y *= sizeRatio.height
      frame.origin.y = Screen.size.height - frame.size.height * 0.99  // タップした時の沈みを考慮した1％
      
      bodyImageView.animationDuration = (stDicArray[i]["TimeInterval"] as! NSNumber).doubleValue
      bodyImageView.animationRepeatCount = 0
      
      let componentName = stDicArray[i]["ComponentName"] as! String
      
      let MaxCount = (stDicArray[i]["MaxCount"] as! NSNumber).integerValue
      var picArray = [UIImage]()
      for var j=1; j<=MaxCount; j++ {
        picArray.append(UIImage(named: "\(characterName)-\(componentName)-\(j)")!)
      }
      bodyImageView.animationImages = picArray
      addSubview(bodyImageView)
      bodyImageView.layer.setShadowForButton()
      bodyImageView.startAnimating()
      
    }
    
  }
  
  private func makeSubComponents() {
    
    guard let characterName = characterName else { return }
    
    for var i=0; i<stDicArray.count; i++ {
      
      guard (stDicArray[i]["OrderInLayer"] as! NSNumber).integerValue != 0 else { continue }
      
      let compImageView = UIImageView()
      let componentName = stDicArray[i]["ComponentName"] as! String
      let image = UIImage(named: "\(characterName)-\(componentName)-1")
      compImageView.contentMode = UIViewContentMode.ScaleAspectFit  // サイズの長い方基準に縮める
      compImageView.image = image
      compImageView.layer.zPosition = CGFloat((stDicArray[i]["OrderInLayer"] as! NSNumber).integerValue)
      
      // 位置設定
      compImageView.frame.origin = (stDicArray[i]["Position"] as! NSValue).CGPointValue()
      compImageView.frame.origin.x *= sizeRatio.width
      compImageView.frame.origin.y *= sizeRatio.height
      compImageView.frame.size = CGSizeMake(image!.size.width * sizeRatio.width, image!.size.height * sizeRatio.height)
      
      // コマ間隔設定
      let timeInterval = (stDicArray[i]["TimeInterval"] as! NSNumber).doubleValue
      compImageView.animationDuration = timeInterval
      
      // 各コマの画像設定
      let maxCount = (stDicArray[i]["MaxCount"] as! NSNumber).integerValue
      var picArray = [UIImage]()
      for var j=1; j<=maxCount; j++ {
        picArray.append(UIImage(named: "\(characterName)-\(componentName)-\(j)")!)
      }
      
      // AutoReverse
      if let rev: AnyObject = stDicArray[i]["AutoReverse"] {
        if rev as! Bool {
          for var j=maxCount; j>=1; j-- {
            picArray.append(UIImage(named: "\(characterName)-\(componentName)-\(j)")!)
          }
        }
      }
      
      // 表示
      compImageView.animationImages = picArray
      compImageViews.append(compImageView)
      self.addSubview(compImageViews.last!)
      
      // 繰り返しするアニメーションの開始遅延設定
      if let timeDelay: AnyObject = stDicArray[i]["ScheduledWithTimeInterval"] {
        compImageView.animationRepeatCount = 1
        if stDicArray[i]["ScheduledWithTimeIntervalFineAdjustment"] != nil {
          animateOnceFineAdjustedDelayWithImageView(compImageView, index: i)
        }
        else {
          NSTimer.scheduledTimerWithTimeInterval((timeDelay as! Double) + Double(maxCount - 1) * timeInterval, target: NSBlockOperation(block: {
          compImageView.startAnimating()
          }), selector: "main", userInfo: nil, repeats: true)
        }
      }
      else {
        compImageView.animationRepeatCount = 0
        compImageViews.last!.startAnimating()
      }
      
    }
    
  }
  
  private func animateOnceFineAdjustedDelayWithImageView(imageView: UIImageView, index: Int) {
    let maxCount        = stDicArray[index]["MaxCount"] as! Int
    let timeInterval    = stDicArray[index]["TimeInterval"] as! Double
    let timeDelay       = stDicArray[index]["ScheduledWithTimeInterval"] as! Double
    let fineAdjustment  = stDicArray[index]["ScheduledWithTimeIntervalFineAdjustment"] as! Double
    
    NSTimer.scheduledTimerWithTimeInterval(timeDelay + Double.random(min: 0.0, max: fineAdjustment) + Double(maxCount - 1) * timeInterval, target: NSBlockOperation(block: {
      imageView.startAnimating()
      self.animateOnceFineAdjustedDelayWithImageView(imageView, index: index)
    }), selector: "main", userInfo: nil, repeats: false)
  }
  
  private func animateComponent() {
    if self.subviews.count > 1 { for subview in self.subviews { subview.removeFromSuperview() } }
    
    loadConfigAndSortByOrderInLayer()
    makeBodyComponent()
    makeSubComponents()
  }

  override func hitTest(point: CGPoint, withEvent event: UIEvent?) -> UIView? {
    
    guard CGRectContainsPoint(bounds, point) else { return nil }
    
    // userInteractionEnabledよりhitTestが優先されるのでフラグとして利用する
    guard userInteractionEnabled else { return nil }

    
    // hitTestの判定が怪しい？
    let color = bodyImageView.image!.pixelColorAtPoint(
      CGPointMake(
        point.x * bodyImageView!.image!.size.width  / frame.size.width,
        point.y * bodyImageView!.image!.size.height / frame.size.height
      )
    )
    var r: CGFloat = 0.0
    var g: CGFloat = 0.0
    var b: CGFloat = 0.0
    var a: CGFloat = 0.0
    if color.getRed(&r, green: &g, blue: &b, alpha: &a) {
      let check = Int(a * 255.0)
      if check == 0 {
        return nil
      }
      else {
        return self
      }
    }
    return nil
  }
  
  override func startAnimating() {
    guard characterName != nil else { fatalError("Error: characterName is nil") }
    animateComponent()
  }
  
  override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
    // 拡大縮小を設定
    let animation = CAKeyframeAnimation(keyPath: "transform.scale")
    animation.duration  = 0.7
    animation.keyTimes = [0.0 as NSNumber, 0.2 as NSNumber, 0.35 as NSNumber, 0.5 as NSNumber, 0.7 as NSNumber]
    animation.values   = [1.0 as AnyObject, 0.985 as AnyObject, 1.0 as AnyObject, 1.05 as AnyObject, 1.0 as AnyObject]
    
    layer.addAnimation(animation, forKey: "scale-layer")
    
    Audio.sharedInstance.playSE("play")
    delegate?.didTouchCharacterAnimationView()
  }
}

protocol CharacterAnimationViewDelegate: class {
  func didTouchCharacterAnimationView()
}