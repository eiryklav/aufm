//
//  DataWayOfUseTimelineTableView.swift
//  Aufm
//
//  Created by moti on 2015/07/14.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

class DataWayOfUseTimelineTableView : UITableView {
  
  override init(frame: CGRect, style: UITableViewStyle) {
    super.init(frame: frame, style: style)
    self.rowHeight = UITableViewAutomaticDimension
    self.dataSource = self
    self.separatorColor = UIColor.clearColor()
    self.backgroundColor = UIColor.whiteStyleBackgroundColor()
    self.registerClass(DataTimelineTableViewCell.self, forCellReuseIdentifier: "TimelineCell")
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  required init(coder aDecoder: NSCoder) { fatalError("NSCoder x") }

}

extension DataWayOfUseTimelineTableView : UITableViewDataSource {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return Global.statusOfTimeStamp[PlayerID]!["WayOfUseMoney"]!.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("TimelineCell", forIndexPath: indexPath) as! DataTimelineTableViewCell
    cell.frame.size = CGSizeMake(self.frame.width, cell.frame.height)
    cell.backgroundColor = UIColor.clearColor()
    cell.timeView.backgroundColor = UIColor.clearColor()
    cell.mainView.backgroundColor = UIColor.clearColor()
    cell.setData(attributedText: NSAttributedString(string: Global.statusOfTimeStamp[PlayerID]!["WayOfUseMoney"]![indexPath.row].1 as! String), timesec: Global.allMessages.messages[indexPath.row].timeStamp)
    cell.accessoryType = .None
    return cell
  }
  
}