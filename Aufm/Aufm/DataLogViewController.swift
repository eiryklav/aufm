//
//  DataLogViewController.swift
//  Aufm
//
//  Created by moti on 2015/07/15.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//
/*

import UIKit

class DataLogViewController : UIViewController {
  
  private var commonView: DataCommonView!
  private var radioSortButtons = [DataRadioSortButton]()
  private var timelineTableView: DLTableView!
  
  override func viewDidLoad() {
    
    super.viewDidLoad()
    commonView = DataCommonView(selected: .Log, notificationPrefix: Notifications.DataLogPrefix, isSecondScene: true)
    let UpMost = commonView.bottomOfBar
    let Rect = CGRectMake(0, UpMost, Screen.size.width, Screen.size.height - UpMost)
    let Top = UpMost + Rect.height * 0.05
    self.view.addSubview(commonView)
    
    radioSortButtons.append(DataRadioSortButton(frame: CGRectMake(0, UpMost + Rect.height * 0.05, Rect.width * 0.25, Rect.height * 0.15), uniqueID: "All", text: "All"))
    self.view.addSubview(radioSortButtons.last!)
    radioSortButtons.append(DataRadioSortButton(frame: CGRectMake(0, radioSortButtons.last!.frame.maxY + Rect.height * 0.025, radioSortButtons.last!.frame.width, radioSortButtons.last!.frame.height), uniqueID: "Battle", text: "Battle"))
    self.view.addSubview(radioSortButtons.last!)
    radioSortButtons.append(DataRadioSortButton(frame: CGRectMake(0, radioSortButtons.last!.frame.maxY + Rect.height * 0.025, radioSortButtons.last!.frame.width, radioSortButtons.last!.frame.height), uniqueID: "Event", text: "Event"))
    self.view.addSubview(radioSortButtons.last!)
    radioSortButtons.append(DataRadioSortButton(frame: CGRectMake(0, radioSortButtons.last!.frame.maxY + Rect.height * 0.025, radioSortButtons.last!.frame.width, radioSortButtons.last!.frame.height), uniqueID: "Talk", text: "Talk"))
    self.view.addSubview(radioSortButtons.last!)
    
    radioSortButtons[0].beChecked(radioSortButtons[0])
    
    timelineTableView = DLTableView(frame: CGRectMake(radioSortButtons[0].frame.maxX + Rect.width * 0.025, radioSortButtons[0].frame.minY, Rect.width * 0.975 - (radioSortButtons[0].frame.maxX + Rect.width * 0.025), Rect.height * 0.9), style: .Plain)
    self.view.addSubview(timelineTableView)
    /*
      NSNotificationCenter
    */
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "sortRadioButtonPushed:", name: Notifications.DataLogPrefix + Notifications.RadioButtonSuffix, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "backButtonPushed", name: Notifications.DataLogPrefix + Notifications.PushedBackButtonSuffix, object: nil)
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    timelineTableView.reloadData()
  }
  
  func sortRadioButtonPushed(notification: NSNotification) {
    let uniqueID = notification.userInfo!["CheckedID"]! as! String
    
    AudioManagerView.sharedInstance.playSE("click")
    
    switch uniqueID {
    case "All":
      timelineTableView.messageClassification = .All
    case "Battle":
      timelineTableView.messageClassification = .Battle
    case "Event":
      timelineTableView.messageClassification = .Event
    case "Talk":
      timelineTableView.messageClassification = .Talk
    default:
      assert(false);
    }
    timelineTableView.reloadData()
  }
  
  func backButtonPushed() {
    self.navigationController?.popToRootViewControllerAnimated(false)
  }
}

*/
