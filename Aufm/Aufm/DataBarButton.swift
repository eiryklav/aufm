//
//  DataBarButton.swift
//  Aufm
//
//  Created by moti on 2015/06/30.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

protocol DataBarButtonInit {
  init(frame: CGRect, item: DataBarView.ItemType)
}

class DataBarButton : UIButton, DataBarButtonInit {
  
  required init(coder aDecoder: NSCoder) { fatalError("NSCoder x") }
  
  var isSelectedButton = false
  var item: DataBarView.ItemType!
  private let FontName = "Baskerville"
  
  required init(frame: CGRect, item: DataBarView.ItemType) {
    super.init(frame: frame)
    
    self.item = item
    
    self.setTitleColor(UIColor.whiteColor(), forState: .Normal)
    self.backgroundColor = UIColor.whiteStyleUnselectedBarButtonColor()
    self.setBackgroundImage(UIImage.imageWithColor(UIColor.whiteStyleHighlightedBarButtonColor()), forState: .Highlighted)
    
    
    self.titleLabel!.font = UIFont(name: FontName, size: adjustFontSize(30.0))
    
    switch item {
    case .Status:     self.setTitle("Status", forState: .Normal)
    case .Item:       self.setTitle("Item", forState: .Normal)
    case .Ability:    self.setTitle("Ability", forState: .Normal)
    case .Money:      self.setTitle("Money", forState: .Normal)
    case .Log:        self.setTitle("Log", forState: .Normal)
    case .Timekeep:   self.setTitle("Timekeep", forState: .Normal)
    case .Speed:      self.setTitle("Speed", forState: .Normal)
    case .Brainkeep:  self.setTitle("Brainkeep", forState: .Normal)
    
    case .Back:
      self.setImage(UIImage(named: "back-tab-button"), forState: .Normal)
      self.alpha = 0.2
      self.backgroundColor = UIColor.clearColor()
      
    case .Next:
      self.setImage(UIImage(named: "next-tab-button"), forState: .Normal)
      self.alpha = 0.2
      self.backgroundColor = UIColor.clearColor()
      
    default:
      assert(false)
    }
    
    self.addTarget(self, action: "touchDown", forControlEvents: .TouchDown)
    self.addTarget(self, action: "touchUp", forControlEvents: .TouchUpInside)
    self.addTarget(self, action: "touchUp", forControlEvents: .TouchUpOutside)
  }
  
  func touchUp() {
    if isSelectedButton {
      self.layer.setShadowForButton()
    }
  }
  
  func touchDown() {
    
    if isSelectedButton {
      self.layer.setDeepShadowForButton()
      return
    }
    
    AudioManagerView.sharedInstance.playSE("click")
    app.gameNavigationController.popToRootViewControllerAnimated(false)
    
    switch self.item! {
    case .Status:
      app.gameNavigationController!.pushViewController(app.dataStatusViewController, animated: false)
    case .Item:
      app.gameNavigationController!.pushViewController(app.dataItemViewController, animated: false)
    case .Ability:
      app.gameNavigationController!.pushViewController(app.dataAbilityViewController, animated: false)
    case .Money:
      app.gameNavigationController!.pushViewController(app.dataMoneyViewController, animated: false)
    case .Next:
      app.gameNavigationController!.pushViewController(app.dataLogViewController, animated: false)
    case .Back:
      app.gameNavigationController!.pushViewController(app.dataStatusViewController, animated: false)
    case .Log:
      app.gameNavigationController!.pushViewController(app.dataLogViewController, animated: false)
    case .Timekeep:
      app.gameNavigationController!.pushViewController(app.dataTimekeepViewController, animated: false)
    case .Brainkeep:
      app.gameNavigationController!.pushViewController(app.dataBrainkeepViewController, animated: false)
    default:
      break;
    }
  }
  
}

protocol DataBarButtonProtocol {
  func beSelected()
  func beUnselected()
}

extension DataBarButton : DataBarButtonProtocol {
  func beSelected() {
    isSelectedButton = true
    self.backgroundColor = UIColor.whiteStyleSelectedBarButtonColor()
    self.layer.setShadowForButton()
  }
  
  func beUnselected() {
    isSelectedButton = false
    self.backgroundColor = UIColor.whiteStyleUnselectedBarButtonColor()
  }
}