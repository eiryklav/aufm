//
//  PickerEnterButton.swift
//  Aufm
//
//  Created by moti on 2015/06/25.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

class PickerEnterButton : UIButton {
  
  required init(coder: NSCoder) {
    fatalError("NSCoder not supported")
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    self.addTarget(self, action: "touchedDown", forControlEvents: .TouchDown)
    self.addTarget(self, action: "touchedUpOutside", forControlEvents: .TouchUpOutside)
    self.addTarget(self, action: "touchedUpInside", forControlEvents: .TouchUpInside)
    self.layer.borderColor = UIColor(
      red:    151.0/255.0,
      green:  151.0/255.0,
      blue:   151.0/255.0,
      alpha:  180.0/255.0).CGColor!
    self.layer.borderWidth = 1.0
  }
  
  override func drawRect(rect: CGRect) {
    var ctx = UIGraphicsGetCurrentContext();
    
    CGContextBeginPath        (ctx)
    CGContextMoveToPoint      (ctx, rect.width*0.25, rect.height*0.35)
    CGContextAddLineToPoint   (ctx, rect.width*0.80, rect.height*0.50)
    CGContextAddLineToPoint   (ctx, rect.width*0.25, rect.height*0.65)
    CGContextClosePath        (ctx)
    
    CGContextSetRGBFillColor  (ctx, 0.2, 0.2, 0.2, 1)
    CGContextFillPath         (ctx)
    
    CGContextBeginPath        (ctx)
    CGContextMoveToPoint      (ctx, rect.width*0.25, rect.height*0.35)
    CGContextAddLineToPoint   (ctx, rect.width*0.80, rect.height*0.50)
    CGContextAddLineToPoint   (ctx, rect.width*0.25, rect.height*0.65)
    CGContextClosePath        (ctx)
    
    CGContextSetRGBStrokeColor(ctx, 0.3, 0.3, 0.3, 1)
    CGContextStrokePath       (ctx)
  }
  
  func touchedDown() {
    self.frame.origin = CGPointMake(
      self.frame.origin.x+self.frame.size.width*0.03,
      self.frame.origin.y//+self.frame.size.height*0.01
    )
  }
  
  func touchedUpOutside() {
    self.frame.origin = CGPointMake(
      self.frame.origin.x-self.frame.size.width*0.03,
      self.frame.origin.y//-self.frame.size.height*0.01
    )
  }
  
  func touchedUpInside() {
    AudioManagerView.sharedInstance.playSE("click")
    self.frame.origin = CGPointMake(
      self.frame.origin.x-self.frame.size.width*0.03,
      self.frame.origin.y//-self.frame.size.height*0.01
    )
    NSNotificationCenter.defaultCenter().postNotificationName(Notifications.PushedPickerEnterButton, object: nil)
  }
}
