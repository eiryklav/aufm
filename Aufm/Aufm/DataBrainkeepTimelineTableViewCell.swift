//
//  DataBrainkeepTimelineTableViewCell.swift
//  Aufm
//
//  Created by moti on 2015/07/19.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

class DataBrainkeepTimelineTableViewCell : DataTimelineTableViewCell {
  override init(style: UITableViewCellStyle, reuseIdentifier: String!) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    timeView.backgroundColor = UIColor.clearColor()
    mainView.backgroundColor = UIColor.clearColor()
  }
  
  required init(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
}