//
//  StatusView.swift
//  Aufm
//
//  Created by moti on 2015/06/12.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmSupport
import AufmVisualSoundEffect

class StatusView : UIView {
  
  weak var delegate:    StatusViewDelegate?
  weak var dataSource:  StatusViewDataSource?
  
  var statusScrollView: UIScrollView!
  
  private var lvLabel: UILabel!
  private var hpLabel: UILabel!
  private var maLabel: UILabel!
  private var akLabel: UILabel!
  private var dfLabel: UILabel!
  private var spLabel: UILabel!
  
  private var namePlayersLabel = [UILabel]()
  private var lvPlayersLabel = [UILabel]()
  private var hpPlayersLabel = [UILabel]()
  private var maPlayersLabel = [UILabel]()
  private var akPlayersLabel = [UILabel]()
  private var dfPlayersLabel = [UILabel]()
  private var spPlayersLabel = [UILabel]()
  
  var partitionLine:  UIImageView!
  var statusButton:   StatusButton!
  
  required init?(coder aDecoder: NSCoder) { fatalError() }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    backgroundColor   = UIColor(rgba: "#2c3e5088")
    layer.borderColor = UIColor(rgba: "#2c3e5088").CGColor
    layer.borderWidth = 1.0
    
    let STW: CGFloat = frame.width
    let STH: CGFloat = frame.height
    statusScrollView = UIScrollView(frame: CGRectMake(STW*0.05, STH*0.05, STW*0.95, STH*0.7))
    statusScrollView.delegate = self
    
    let SVW: CGFloat = statusScrollView.bounds.width   // ScrollViewの幅であることに注意
    let SVH: CGFloat = statusScrollView.bounds.height
    
    let LBLH: CGFloat = SVH/10.0
    let LMOST_LBLW: CGFloat = SVW/6.0
    let LBLW: CGFloat = SVW/2.5
    
    var top: CGFloat!
    var left: CGFloat!
    // 1列目
    top   = LBLH
    left  = CGFloat(0)
    lvLabel = UILabel(frame: CGRectMake(left, top, LMOST_LBLW, LBLH)); top = top + LBLH
    hpLabel = UILabel(frame: CGRectMake(left, top, LMOST_LBLW, LBLH*CGFloat(2.0))); top = top + LBLH*CGFloat(2.0)
    maLabel = UILabel(frame: CGRectMake(left, top, LMOST_LBLW, LBLH)); top = top + LBLH
    akLabel = UILabel(frame: CGRectMake(left, top, LMOST_LBLW, LBLH)); top = top + LBLH
    dfLabel = UILabel(frame: CGRectMake(left, top, LMOST_LBLW, LBLH)); top = top + LBLH
    spLabel = UILabel(frame: CGRectMake(left, top, LMOST_LBLW, LBLH));
    
    lvLabel.text = "Lv"
    hpLabel.text = "HP"
    maLabel.text = "TP"
    akLabel.text = "AK"
    dfLabel.text = "DF"
    spLabel.text = "SP"
    
    lvLabel.font = UIFont(name: "AndaleMono", sizeType: .Small)
    hpLabel.font = UIFont(name: "AndaleMono", sizeType: .Small)
    maLabel.font = UIFont(name: "AndaleMono", sizeType: .Small)
    akLabel.font = UIFont(name: "AndaleMono", sizeType: .Small)
    dfLabel.font = UIFont(name: "AndaleMono", sizeType: .Small)
    spLabel.font = UIFont(name: "AndaleMono", sizeType: .Small)
    
    lvLabel.textColor = UIColor.whiteColor()
    hpLabel.textColor = UIColor.whiteColor()
    maLabel.textColor = UIColor.whiteColor()
    akLabel.textColor = UIColor.whiteColor()
    dfLabel.textColor = UIColor.whiteColor()
    spLabel.textColor = UIColor.whiteColor()
    
    statusScrollView.addSubview(lvLabel)
    statusScrollView.addSubview(hpLabel)
    statusScrollView.addSubview(maLabel)
    statusScrollView.addSubview(akLabel)
    statusScrollView.addSubview(dfLabel)
    statusScrollView.addSubview(spLabel)
    
    let colors = [UIColor.redColor(), UIColor.yellowColor(), UIColor.blueColor(), UIColor.purpleColor()]
    
    // 2〜5列目
    for var i=0; i<4; i++ {
      top   = CGFloat(0)
      left  = LMOST_LBLW+LBLW*CGFloat(i)
      namePlayersLabel.append(UILabel(frame: CGRectMake(left, top, LBLW, LBLH))); top = top + LBLH
      lvPlayersLabel.append(UILabel(frame: CGRectMake(left, top, LBLW, LBLH))); top = top + LBLH
      hpPlayersLabel.append(UILabel(frame: CGRectMake(left, top, LBLW, LBLH*CGFloat(2.0)))); top = top + LBLH*CGFloat(2.0)
      maPlayersLabel.append(UILabel(frame: CGRectMake(left, top, LBLW, LBLH))); top = top + LBLH
      akPlayersLabel.append(UILabel(frame: CGRectMake(left, top, LBLW, LBLH))); top = top + LBLH
      dfPlayersLabel.append(UILabel(frame: CGRectMake(left, top, LBLW, LBLH))); top = top + LBLH
      spPlayersLabel.append(UILabel(frame: CGRectMake(left, top, LBLW, LBLH)));
      
      namePlayersLabel.last!.font = UIFont(name: "AndaleMono", sizeType: .Small)
      lvPlayersLabel.last!.font   = UIFont(name: "AndaleMono", sizeType: .Small)
      hpPlayersLabel.last!.font   = UIFont(name: "AndaleMono", sizeType: .Small)
      maPlayersLabel.last!.font   = UIFont(name: "AndaleMono", sizeType: .Small)
      akPlayersLabel.last!.font   = UIFont(name: "AndaleMono", sizeType: .Small)
      dfPlayersLabel.last!.font   = UIFont(name: "AndaleMono", sizeType: .Small)
      spPlayersLabel.last!.font   = UIFont(name: "AndaleMono", sizeType: .Small)
      
      namePlayersLabel.last!.textColor  = colors[i]
      lvPlayersLabel.last!.textColor    = colors[i]
      hpPlayersLabel.last!.textColor    = colors[i]
      maPlayersLabel.last!.textColor    = colors[i]
      akPlayersLabel.last!.textColor    = colors[i]
      dfPlayersLabel.last!.textColor    = colors[i]
      spPlayersLabel.last!.textColor    = colors[i]
      
      hpPlayersLabel.last!.numberOfLines = 2
      
      statusScrollView.addSubview(namePlayersLabel.last!)
      statusScrollView.addSubview(lvPlayersLabel.last!)
      statusScrollView.addSubview(hpPlayersLabel.last!)
      statusScrollView.addSubview(maPlayersLabel.last!)
      statusScrollView.addSubview(akPlayersLabel.last!)
      statusScrollView.addSubview(dfPlayersLabel.last!)
      statusScrollView.addSubview(spPlayersLabel.last!)
    }
    
    addSubview(statusScrollView)
    
    var contentRect = CGRectZero
    for view in statusScrollView.subviews {
      contentRect = CGRectUnion(contentRect, view.frame)
    }
    statusScrollView.contentSize = contentRect.size
    
    partitionLine = UIImageView(frame: CGRectMake(0, STH * 0.75, STW, STH * 0.004))
    partitionLine.alpha = 0.5
    partitionLine.image = UIImage(named: "partition-line")
    addSubview(partitionLine)
    
    statusButton = StatusButton(frame: CGRectMake(0, STH * 0.76, STW, STH * 0.24))
    statusButton.addTarget(self, action: "didPushStatusButton", forControlEvents: .TouchUpInside)
    addSubview(statusButton)
  }
  
  func reload() {
    
    guard let dataSource = dataSource else { fatalError("Status dataSource undefined")}
    
    let NumOfPlayers = dataSource.numberOfColumn()
    for i in 0..<NumOfPlayers {
      namePlayersLabel[i].text  = dataSource.titleInStatusName("Name",  column: i)
      lvPlayersLabel  [i].text  = dataSource.titleInStatusName("LV",    column: i)
      hpPlayersLabel  [i].text  = dataSource.titleInStatusName("HP",    column: i)
      maPlayersLabel  [i].text  = dataSource.titleInStatusName("Mana",  column: i)
      akPlayersLabel  [i].text  = dataSource.titleInStatusName("ATK",   column: i)
      dfPlayersLabel  [i].text  = dataSource.titleInStatusName("DEF",   column: i)
      spPlayersLabel  [i].text  = dataSource.titleInStatusName("SP",    column: i)
    }
  }
  
  func didPushStatusButton() {
    delegate?.didPushStatusButton()
  }
  
}

extension StatusView : UIScrollViewDelegate {
  func scrollViewDidScroll(scrollView: UIScrollView) {
    scrollView.contentOffset.y = 0  // 縦方向のスクロールをキャンセル
  }
}

protocol StatusViewDelegate: class {
  func didPushStatusButton()
}

protocol StatusViewDataSource: class {
  func numberOfColumn() -> Int
  func titleInStatusName(statusName: String, column: Int) -> String
}

