//
//  PauseButton.swift
//  AufmGame
//
//  Created by moti on 2015/08/28.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmCommonComponentViews

class PauseButton : UIButton, BTProtocol,
  BGNone, SEButtonTouchClick, BTImage, BTTouchActionNoEffect {
  let image = UIImage(named: "gear")! // 端末によって複数サイズ用意する？
  
  let kSCWidth  = UIScreen.mainScreen().bounds.width
  let kSCHeight = UIScreen.mainScreen().bounds.height
  
  init() {
    let frame = CGRectMake(kSCWidth * 0.875, kSCHeight * 0.05, kSCHeight * 0.125, kSCHeight * 0.125)
    super.init(frame: frame)
    applyBackground()
    applyTitle()
    applySEButtonTouchForControlEvents(.TouchUpInside)
    imageView!.contentMode = .ScaleAspectFit
  }
  
  required init?(coder aDecoder: NSCoder) { fatalError() }

}