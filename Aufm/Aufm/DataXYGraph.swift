//
//  DataXYGraph.swift
//  Aufm
//
//  Created by moti on 2015/07/14.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import CorePlot

class DataXYGraph : CPTXYGraph {
  
  let lineStyle = CPTMutableLineStyle()
  let textStyle = CPTMutableTextStyle()
  let lineStyleForScatter = CPTMutableLineStyle()
  var plotSpace = CPTXYPlotSpace()
  
  override init(frame newFrame: CGRect, xScaleType newXScaleType: CPTScaleType, yScaleType newYScaleType: CPTScaleType) {
    super.init(frame: newFrame, xScaleType: newXScaleType, yScaleType: newYScaleType)
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    self.masksToBorder = false
    self.applyTheme(CPTTheme(named: kCPTPlainWhiteTheme))
    self.backgroundColor = UIColor.clearColor().CGColor
    
    // Paddings
    self.paddingLeft   = 0 //frame.width * 0.1
    self.paddingTop    = 0 //frame.height * 0.1
    self.paddingRight  = 0 //frame.width * 0.1
    self.paddingBottom = 0 //frame.height * 0.1
    
    self.plotAreaFrame.paddingLeft   = frame.width * 0.125
    self.plotAreaFrame.paddingTop    = frame.height * 0.1
    self.plotAreaFrame.paddingRight  = frame.width * 0.1
    self.plotAreaFrame.paddingBottom = frame.height * 0.1
    
    // Background
    self.plotAreaFrame.fill = CPTFill(
      color: CPTColor(
        componentRed: 216.0/255.0,
        green:        216.0/255.0,
        blue:         216.0/255.0,
        alpha:        0.5))
    
    // Text style
    textStyle.color = CPTColor.blackColor()
    textStyle.fontName = "AnonymousPro"
    textStyle.fontSize = adjustFontSize(13.0)
    textStyle.textAlignment = CPTTextAlignment.Right
    self.titleTextStyle = textStyle
    
    // Axis line style
    lineStyle.lineColor = CPTColor(
      componentRed: 151.0/255.0,
      green:        151.0/255.0,
      blue:         151.0/255.0,
      alpha:        180.0/255.0)
    lineStyle.lineWidth = 1.0
    
    // Set XY axis set
    
    let formatter = NSNumberFormatter()
    formatter.maximumFractionDigits = 0
    
    let axisSet = self.axisSet as! CPTXYAxisSet
    let x = axisSet.xAxis
    //    x.title = "Time"
    x.labelTextStyle = textStyle
    x.axisLineStyle = lineStyle
    x.labelingPolicy = CPTAxisLabelingPolicy.FixedInterval
    x.minorTicksPerInterval = 0
    x.majorIntervalLength = CPTDecimalFromInt(1)
    x.labelFormatter = formatter
    
    let y = axisSet.yAxis
    y.labelTextStyle = textStyle
    y.axisLineStyle = lineStyle
    y.labelingPolicy = CPTAxisLabelingPolicy.FixedInterval
    y.minorTicksPerInterval = 0
    y.majorIntervalLength = CPTDecimalFromInt(20)
    y.labelFormatter = formatter
    
    // Border
    self.plotAreaFrame.borderLineStyle = lineStyle
    self.plotAreaFrame.cornerRadius = 0.0
    
    // Line style for scatter
    lineStyleForScatter.lineColor = CPTColor(componentRed: 0.88, green: 0.50, blue: 0.531, alpha: 0.80)
    lineStyleForScatter.lineWidth = 1.0
    
    // Plot space
    plotSpace = self.defaultPlotSpace as! CPTXYPlotSpace
    plotSpace.xRange = CPTPlotRange(location: CPTDecimalFromInt(0), length: CPTDecimalFromInt(5))
    plotSpace.yRange = CPTPlotRange(location: CPTDecimalFromInt(0), length: CPTDecimalFromInt(100))
  }
  
  required init(coder: NSCoder) { fatalError("NSCoder x") }
}
