//
//  PlotStatusSegmentedControl.swift
//  Aufm
//
//  Created by moti on 2015/07/09.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import NYSegmentedControl

protocol PlotStatusSegmentedControlInit {
  init(frame: CGRect)
  init(rect: CGRect)
}

class PlotStatusSegmentedControl : NYSegmentedControl, PlotStatusSegmentedControlInit {
  
  var notificationName: String = "NoNotification"
  
  required init(coder: NSCoder) {
    fatalError("NSCoder not supported")
  }
  
  required override init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  required init(rect: CGRect) {
    super.init(items: Global.StatusNameList)
    self.addTarget(self, action: "segmentSelected:", forControlEvents: .ValueChanged)
    self.selectedSegmentIndex = 0
    
    // Customize and size the control
    self.titleFont = UIFont(name: "AnonymousPro", size: adjustFontSize(14.0))
    self.selectedTitleFont = UIFont(name: "AnonymousPro", size: adjustFontSize(14.0))
    self.selectedTitleTextColor = UIColor(rgba: "#0000aaff")
    self.borderWidth = 1.0
    self.borderColor = UIColor.whiteStyleBorderColor()
    self.drawsGradientBackground = false
    self.segmentIndicatorInset = 2.0
    self.cornerRadius = 0.0
    self.backgroundColor = UIColor.whiteStyleBackgroundColor()
    self.drawsSegmentIndicatorGradientBackground = true
    self.segmentIndicatorGradientTopColor = UIColor(red: 0.3, green: 0.5, blue: 0.75, alpha: 1.0)
    self.segmentIndicatorGradientBottomColor = UIColor(red: 0.2, green: 0.35, blue: 0.35, alpha: 1.0)
    self.segmentIndicatorAnimationDuration = 0.3
    self.segmentIndicatorBorderWidth = 0.0
    self.sizeToFit()
    self.frame = rect
  }
  
  func segmentSelected(sender: NYSegmentedControl) {
    NSNotificationCenter.defaultCenter().postNotificationName(Notifications.ValueChangedPlotStatus, object: nil, userInfo: ["Index":self.selectedSegmentIndex])
  }

}
