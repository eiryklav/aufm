//
//  DataNameTextLabel.swift
//  Aufm
//
//  Created by moti on 2015/07/12.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

class DataNameTextLabel : BaseLabel {
  
  required init(frame: CGRect) {
    super.init(frame: frame, text: "", sizeType: .Medium, color: UIColor.whiteColor())
    self.backgroundColor = UIColor.whiteStyleBackgroundColor()
    self.layer.borderColor = UIColor.whiteStyleBorderColor().CGColor!
    self.layer.borderWidth = 1.0
  }
  
  required init(coder: NSCoder) {
    fatalError("NSCoder x")
  }

  required init(frame: CGRect, text: String, sizeType: FontSizeType, color: UIColor, fontName: String) {
      fatalError("init(frame:text:sizeType:color:fontName:) has not been implemented")
  }

  required init(origin: CGPoint, text: String, sizeType: FontSizeType, color: UIColor, fontName: String) {
      fatalError("init(origin:text:sizeType:color:fontName:) has not been implemented")
  }
  
}

protocol DataNameTextLabelProtocol {
  func setName(characterID: Int)
}

extension DataNameTextLabel : DataNameTextLabelProtocol {
  func setName(characterID: Int) {
    let AIName = Global.statusOfTimeStamp[characterID]!["AI"]!.last!.1 as! String
    if cppCode.isPlayerID(characterID) {
      self.text = "\(AIName), \(cppCode.getPlayerName())"
    }
    else {
      self.text = "\(AIName), \(cppCode.getChrStatusNameWithID(characterID))"
    }
  }
}