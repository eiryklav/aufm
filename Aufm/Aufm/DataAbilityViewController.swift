//
//  DataAbilityViewController.swift
//  Aufm
//
//  Created by moti on 2015/07/12.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

/*

import UIKit

class DataAbilityViewController : UIViewController {
  
  private var commonView:             DataCommonView!
  
  private var upArrowButton:          UpArrowButton!
  private var downArrowButton:        DownArrowButton!
  private var nameTextLabel:          DataAbilityNameTextLabel!
  private var abilityTableView:       DataAbilityTableView!
  private var abilityDescriptionView: DataAbilityDescriptionView!

  private var currentEncountedPlayerIndex: Int = 0
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    commonView = DataCommonView(selected: .Ability, notificationPrefix: Notifications.DataAbilityPrefix)
    self.view.addSubview(commonView)
    
    let UpMost = commonView.bottomOfBar
    let Rect = CGRectMake(0,UpMost,Screen.size.width,Screen.size.height-UpMost)
    
    upArrowButton = UpArrowButton(frame: CGRectMake(Rect.width * 0.025, UpMost + Rect.height * 0.05, Rect.width * 0.075, Rect.height * 0.075), notificationPrefix: Notifications.DataAbilityPrefix, style: .White)
    self.view.addSubview(upArrowButton)
    
    downArrowButton = DownArrowButton(frame: CGRectMake(upArrowButton.frame.minX, upArrowButton.frame.maxY + Rect.height * 0.020, upArrowButton.frame.width, upArrowButton.frame.height), notificationPrefix: Notifications.DataAbilityPrefix, style: .White)
    self.view.addSubview(downArrowButton)
    
    nameTextLabel = DataAbilityNameTextLabel(
      frame: CGRectMake(
        upArrowButton.frame.maxX + Rect.width * 0.02,
        (upArrowButton.frame.minY + downArrowButton.frame.maxY - Rect.height * 0.175) / 2.0,
        Rect.width * 0.405,
        Rect.height * 0.175
      )
    )
    self.view.addSubview(nameTextLabel)
    
    abilityTableView = DataAbilityTableView(frame: CGRectMake(
      upArrowButton.frame.minX,
      downArrowButton.frame.maxY + Rect.height * 0.025,
      nameTextLabel.frame.maxX - upArrowButton.frame.minX,
      Rect.height * 0.553)
    )
    self.view.addSubview(abilityTableView)
    
    abilityDescriptionView = DataAbilityDescriptionView(frame: CGRectMake(nameTextLabel.frame.maxX + Rect.width * 0.025, upArrowButton.frame.minY, Rect.width * 0.425, Rect.height * 0.875))
    self.view.addSubview(abilityDescriptionView)
    
    /*
      NSNotificationCenter
    */
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "backButtonPushed", name: Notifications.DataAbilityPrefix + Notifications.PushedBackButtonSuffix, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "upArrowButtonPushed", name: Notifications.DataAbilityPrefix + Notifications.UpArrowButtonPushedSuffix, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "downArrowButtonPushed", name: Notifications.DataAbilityPrefix + Notifications.DownArrowButtonPushedSuffix, object: nil)
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    
    if currentEncountedPlayerIndex == 0 {
      upArrowButton.beUnabled()
    }
    else {
      upArrowButton.beEnabled()
    }
    if currentEncountedPlayerIndex + 1 >= Global.encountedPlayers.count {
      downArrowButton.beUnabled()
    }
    else {
      downArrowButton.beEnabled()
    }
    
    setNameAndAbilityList()
  }
  
  func setNameAndAbilityList() {
    let playerID = Global.encountedPlayers[currentEncountedPlayerIndex];
    nameTextLabel.setName(playerID)
    abilityTableView.setAbilitiesWithPlayerID(playerID)
    abilityDescriptionView.unsetData()
  }
  
  func backButtonPushed() {
    self.navigationController?.popToRootViewControllerAnimated(false)
  }
  
  func upArrowButtonPushed() {
    currentEncountedPlayerIndex--
    if currentEncountedPlayerIndex == 0 {
      upArrowButton.beUnabled()
    }
    if currentEncountedPlayerIndex + 2 == Global.encountedPlayers.count {
      downArrowButton.beEnabled()
    }
    setNameAndAbilityList()
  }
  
  func downArrowButtonPushed() {
    currentEncountedPlayerIndex++
    if currentEncountedPlayerIndex + 1 >= Global.encountedPlayers.count {
      downArrowButton.beUnabled()
    }
    if currentEncountedPlayerIndex == 1 {
      upArrowButton.beEnabled()
    }
    setNameAndAbilityList()
  }
  
}

*/