//
//  DataRadioSortButton.swift
//  Aufm
//
//  Created by moti on 2015/07/15.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

class DataRadioSortButton : UIButton {
  
  var uniqueID: String!
  var notificationString: String!
  
  required init(coder: NSCoder) {
    fatalError("NSCoder not required")
  }
  
  init(frame: CGRect, uniqueID: String, text: String) {
    super.init(frame: frame)
    
    self.backgroundColor = UIColor.whiteStyleUnselectedRadioButtonColor()
    
    self.uniqueID = uniqueID
    self.notificationString = Notifications.DataLogPrefix + Notifications.RadioButtonSuffix
    self.titleLabel!.font = UIFont(name: "Baskerville", size: adjustFontSize(28.0))
    self.titleLabel!.textColor = UIColor.whiteColor()
    self.setTitle(text, forState: .Normal)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "beUnchecked:", name: notificationString, object: nil)
    self.addTarget(self, action: "beChecked:", forControlEvents: .TouchDown)
  }
  
}

protocol DataRadioSortButtonProtocol {
  func beUnchecked(notification: NSNotification)
  func beChecked(sender: DataRadioSortButton)
}

extension DataRadioSortButton : DataRadioSortButtonProtocol {
  func beUnchecked(notification: NSNotification) {
    if notification.userInfo!["CheckedID"] as! String == self.uniqueID { return }
    self.selected = false
    self.backgroundColor = UIColor.whiteStyleUnselectedRadioButtonColor()
  }
  
  func beChecked(sender: DataRadioSortButton) {
    self.selected = true
    self.backgroundColor = UIColor.whiteStyleSelectedRadioButtonColor()
    NSNotificationCenter.defaultCenter().postNotificationName(sender.notificationString, object: nil, userInfo: ["CheckedID":sender.uniqueID])
  }
}