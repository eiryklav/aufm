//
//  StatusButton.swift
//  AufmGame
//
//  Created by moti on 2015/09/01.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmCommonComponentViews

class StatusButton : UIButton, BTProtocol,
  BGNone, SEButtonTouchClick, BTImage, BTTouchActionNoEffect {
  
  let image = UIImage(named: "status-button")!
  
  required init?(coder aDecoder: NSCoder) { fatalError() }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    applyBackground()
    applyTitle()
    applySEButtonTouchForControlEvents(.TouchDown)
  }
  
}
