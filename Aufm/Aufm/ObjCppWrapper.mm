//
//  ObjCppWrapper.m
//  Aufm
//
//  Created by moti on 2015/04/26.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <string>
#import <fstream>
#import "ObjCppWrapper.h"
#import "../../Game/Game.hpp"
#import "../../Character/ChrData.hpp"
#import "../../Game/GameUI.hpp"
#import "../../Game/GameStrategy.hpp"
#import "../../Game/TimeManager.hpp"
#import "../../Data/Database.hpp"
#import "../../GameState/GameState.hpp"
#import "../../Support/Misc.hpp"
#import "../../Lib/picojson.h"
#import "../../BattleSystem/BattleAI.hpp"
#import "../../Graph/PathSolver.hpp"

@implementation ObjCppWrapper

NSInteger const ConditionPoison = ChrData::ChrCurrStat::poison;
NSInteger const ConditionPalsy = ChrData::ChrCurrStat::palsy;
NSInteger const ConditionDead = ChrData::ChrCurrStat::dead;

NSInteger const PlayerID = ChrData::UserIDOnDB;

-(id)init {
  self = [super init];
  return self;
}

-(void)dealloc {
}

-(void)initGameWithPlayerName:(NSString*)playerName targetForDefeat:(NSArray*)tForD attitude:(NSInteger)atti invitation:(NSInteger)invi changeEqp:(NSInteger)chEqp lootingDrawers:(NSInteger)loot
{
  std::vector<std::pair<int, int>> tForDVec;
  for(unsigned int i=0; i<[tForD count]; i++) {
    NSArray* tuple = [tForD objectAtIndex: i];
    tForDVec.emplace_back([[tuple objectAtIndex: 0] intValue], [[tuple objectAtIndex: 1] intValue]);
  }
  std::string playerNameString = [playerName UTF8String];
  
  GameMgr->initGame(playerNameString,
                    tForDVec,
                    static_cast<int>(atti),
                    static_cast<int>(invi),
                    static_cast<int>(chEqp),
                    static_cast<int>(loot));
}

-(void)open:(NSString*)worldName
{
  std::string worldNameString = [worldName UTF8String];
  NSBundle* bundle = [NSBundle mainBundle];
  NSString* resourceDirectoryPath = [bundle bundlePath];
  NSString* path = [resourceDirectoryPath stringByAppendingString: @"/DataResources/"];
  GameMgr->open(worldNameString, [path UTF8String]);
}

-(BOOL)updateWithTimeSinceLast:(double)timeSinceLast
{
//  timeSinceLast *= 10;  // 早回し（いかにも問題が起こりそう）
	GameMgr->getTimeMgr()->updateRealTime(TimeManager::RealTime(timeSinceLast));
	if(GameMgr->getTimeMgr()->isFinishedConsumingAufmTime()) {
    GameMgr->update();
    return YES;
  }
	return NO;
}

-(NSInteger)changeFromCppState: (int)state
{
  switch (state) {
    case static_cast<int>(StateType::TitleState):     return kTitleState;
    case static_cast<int>(StateType::GameWorldState): return kGameWorldState;
    case static_cast<int>(StateType::EndingState):		return kEndingState;
    case static_cast<int>(StateType::BattleState):		return kBattleState;
    case static_cast<int>(StateType::CityState):			return kCityState;
    case static_cast<int>(StateType::DungeonState):		return kDungeonState;
    case static_cast<int>(StateType::ScenarioState):	return kScenarioState;
    default:
      return kDefaultState;
  }
}

-(NSInteger)getCurrentState
{
  NSInteger state = [self changeFromCppState: static_cast<int>(GameMgr->getTopState().getState()->getStateType())];
  if(state == kDefaultState) {
    AUFM_ASSERT(false, @"Undefined state in objc");
  }
  return state;
}

-(NSInteger)changeFromCppMessageType: (int)msgType
{
  using Printer::MessageType;
  switch (msgType) {
      
#define RETURN_CHANGED_MESSAGE_TYPE_CASE(messageType) \
    case static_cast<int>(MessageType::messageType): return k ## messageType;
      
      RETURN_CHANGED_MESSAGE_TYPE_CASE(GetRareItem)
      RETURN_CHANGED_MESSAGE_TYPE_CASE(PlayerSideDeath)
      RETURN_CHANGED_MESSAGE_TYPE_CASE(AddServant)
      RETURN_CHANGED_MESSAGE_TYPE_CASE(SysEndConditionEndOfScenarioSuccess)
      RETURN_CHANGED_MESSAGE_TYPE_CASE(SysEndConditionEndOfScenarioFailure)
      RETURN_CHANGED_MESSAGE_TYPE_CASE(SysEndConditionPlayerHasDied)
      RETURN_CHANGED_MESSAGE_TYPE_CASE(SysEndConditionTimeLimitExceeded)
      RETURN_CHANGED_MESSAGE_TYPE_CASE(SysEndConditionDeadEndInDungeon)
      RETURN_CHANGED_MESSAGE_TYPE_CASE(SysEndConditionDeadEndOfRoad)
      RETURN_CHANGED_MESSAGE_TYPE_CASE(SysEndConditionCompleteTargetForDefeat)
      RETURN_CHANGED_MESSAGE_TYPE_CASE(SysBattleStart)
      RETURN_CHANGED_MESSAGE_TYPE_CASE(SysBattleEnd)
      RETURN_CHANGED_MESSAGE_TYPE_CASE(EnterCity)
      RETURN_CHANGED_MESSAGE_TYPE_CASE(EnterDungeon)
      RETURN_CHANGED_MESSAGE_TYPE_CASE(FailedToAddServantBecauseOfInvitationRatio)
      RETURN_CHANGED_MESSAGE_TYPE_CASE(FailedToAddServantBecauseOfOverflow)
      RETURN_CHANGED_MESSAGE_TYPE_CASE(SysLampMoverGenerated)
      RETURN_CHANGED_MESSAGE_TYPE_CASE(SysLampMoverKilled)
      
    case static_cast<int>(MessageType::Default): return kDefaultMessageType;
      
#undef RETURN_CHANGED_MESSAGE_TYPE_CASE
      
    default:
      AUFM_ASSERT(false, "Undefined MessageType: " << msgType);
  }
  return kDefaultMessageType;
}

-(NSMutableArray*)flushLog
{
  NSMutableArray* ret = [NSMutableArray array];
  auto const& msgBuf = GameMgr->getLogger()->getMessagesBuffer();
  for(auto const& m: msgBuf) {
    NSArray* arr = [NSArray arrayWithObjects:
                    [NSNumber numberWithInteger: [self changeFromCppMessageType: static_cast<int>(m.getType())]],
                    [NSNumber numberWithInteger: [self changeFromCppState: static_cast<int>(m.getStateType())]],
                    [NSString stringWithUTF8String: m.getText().c_str()],
                    [NSNumber numberWithDouble: m.getTimeStamp()],
                    [NSNumber numberWithBool: m.isAlert()],
                    [NSNumber numberWithBool: m.isHidden()],
                    nil];
    [ret addObject: arr];
  }
  GameMgr->getLogger()->flushLog();
  return ret;
}

-(NSMutableArray*)flushConversation
{
  NSMutableArray* ret = [NSMutableArray array];
  auto const& conversationBuf = GameMgr->getLogger()->getConversationBuffer();
  for(auto const& c: conversationBuf) {
    NSArray* arr = [NSArray arrayWithObjects:
                    [NSString stringWithUTF8String: c.getSpeakerName().c_str()],
                    [NSString stringWithUTF8String: c.getContent().c_str()],
                    [NSString stringWithUTF8String: c.getSummary().c_str()],
                    [NSNumber numberWithDouble: c.getTimeStamp()],
                    [NSNumber numberWithBool: c.isAlert()]
                    , nil];
    [ret addObject: arr];
  }
  GameMgr->getLogger()->flushConversation();
  return ret;
}

/*
-(void)notify:(int)userInput
{
  switch (userInput) {
#define RETURN_CHANGED_USER_INPUT_CASE(userInput)  \
    case k ## userInput : GameMgr->getLogger()->notifyUserInput(GameUI::UserInput::userInput); return;
      
      RETURN_CHANGED_USER_INPUT_CASE(NoInput)
      RETURN_CHANGED_USER_INPUT_CASE(PushOK)
      RETURN_CHANGED_USER_INPUT_CASE(PushTalk)
      RETURN_CHANGED_USER_INPUT_CASE(PushOpenDrawer)
      RETURN_CHANGED_USER_INPUT_CASE(Pause)
      
#undef RETURN_CHANGED_USER_INPUT_CASE
      
    default:
      NSAssert(NO, @"Undefined user input in objc");
  }
}
 */

-(int)changeNotification:(int)notification
{
  switch(notification) {
#define CASE_CHANGED_NOTIFICATION(notice)  \
case k ## notice : return GameUI::Notification::notice;
    
    CASE_CHANGED_NOTIFICATION(LogUpdated)
    CASE_CHANGED_NOTIFICATION(ConversationUpdated)
    CASE_CHANGED_NOTIFICATION(StateChanged)
    CASE_CHANGED_NOTIFICATION(MapChanged)
    CASE_CHANGED_NOTIFICATION(BezierPathChanged)
    CASE_CHANGED_NOTIFICATION(WaitingForNextEncount)
    CASE_CHANGED_NOTIFICATION(CompletedTargetForDefeat)
    CASE_CHANGED_NOTIFICATION(AllEncountProcessed)
    CASE_CHANGED_NOTIFICATION(AppearanceBlack)
    CASE_CHANGED_NOTIFICATION(AppearanceWhite)
    
#undef CASE_CHANGED_NOTIFICATION
    
    default:
    AUFM_ASSERT(false, "Undefined notification");
  }
}

-(void)notify:(NSInteger)notification
{
  GameMgr->getLogger()->notify([self changeNotification:static_cast<int>(notification)]);
}

-(BOOL)isNotified:(NSInteger)notification
{
  return GameMgr->getLogger()->isNotified([self changeNotification:static_cast<int>(notification)]);
}

-(void)notifySpecialEncountWithID:(NSString*)lampMoverID
{
  GameMgr->getLogger()->notifySpecialEncount([lampMoverID UTF8String]);
}

-(double)getCurrentRealTime
{
  return GameMgr->getTimeMgr()->getNowRealTime().get();
}

-(NSInteger)getCurrentAufmTimeDaySec
{
  return GameMgr->getTimeMgr()->getNowAufmTimeDaySec();
}

-(double)getMaxElaspedRealTime
{
  return GameMgr->getTimeMgr()->getMaxElaspedRealTime().get();
}

-(NSInteger)getMaxElaspedAufmTimeDaySec
{
  return GameMgr->getTimeMgr()->getMaxElaspedAufmTimeDaySec();
}

-(BOOL)isPlayerStatusChanged:(NSInteger)type
{
  switch (type) {
#define RETURN_CHANGED_PLAYER_STATUS_CASE(status)  \
    case k ## status : return GameMgr->getLogger()->isPlayerStatusChanged(GameUI::Status::status);
      
      RETURN_CHANGED_PLAYER_STATUS_CASE(HP)
      RETURN_CHANGED_PLAYER_STATUS_CASE(Mana)
      RETURN_CHANGED_PLAYER_STATUS_CASE(LV)
      RETURN_CHANGED_PLAYER_STATUS_CASE(ATK)
      RETURN_CHANGED_PLAYER_STATUS_CASE(DEF)
      RETURN_CHANGED_PLAYER_STATUS_CASE(SP)
      RETURN_CHANGED_PLAYER_STATUS_CASE(Exp)
      RETURN_CHANGED_PLAYER_STATUS_CASE(Money)
      RETURN_CHANGED_PLAYER_STATUS_CASE(EQP)
      RETURN_CHANGED_PLAYER_STATUS_CASE(Condition)
      
#undef RETURN_CHANGED_PLAYER_STATUS

    default:
      break;
  }
  NSAssert(NO, @"No match player status (in objc)");
  return NO;
}

-(NSString*)getPlayerName
{
  return [NSString stringWithUTF8String: GameMgr->getPlayer()->getName().c_str()];
}

-(NSDictionary*)getPlayerStatus
{
  return [NSDictionary dictionaryWithObjectsAndKeys:
          [NSNumber numberWithInt:kHP],         [NSNumber numberWithInt:GameMgr->getPlayer()->getCurrStat()->getHP()],
          [NSNumber numberWithInt:kMana],       [NSNumber numberWithInt:GameMgr->getPlayer()->getCurrStat()->getMana()],
          [NSNumber numberWithInt:kLV],         [NSNumber numberWithInt:GameMgr->getPlayer()->getBasicStat()->getLV()],
          [NSNumber numberWithInt:kATK],        [NSNumber numberWithInt:GameMgr->getPlayer()->getBasicStat()->getATK()],
          [NSNumber numberWithInt:kSP],         [NSNumber numberWithInt:GameMgr->getPlayer()->getBasicStat()->getSP()],
          [NSNumber numberWithInt:kExp],        [NSNumber numberWithInt:GameMgr->getPlayer()->getBasicStat()->getExp()],
          [NSNumber numberWithInt:kMoney],      [NSNumber numberWithInt:GameMgr->getMoney()],
          [NSNumber numberWithInt:kEQP],        [NSNumber numberWithInt:GameMgr->getPlayer()->getBasicStat()->getEquipment(Misc::EquipmentType::Weapon)],  // idOnDB -> std::string -> NSString
          [NSNumber numberWithInt:kCondition],  [NSNumber numberWithInt:GameMgr->getPlayer()->getCurrStat()->getCondition()],
          nil];
}

-(NSInteger)getPlayersMoney
{
  return GameMgr->getMoney().get();
}

/* ---------------------------------------------------------- */
#pragma mark - macro: Get player status -

#define AUFM_GET_PLAYER_CURR_STATUS_IMPL_DEF(var)  \
  AUFM_GET_PLAYER_STATUS_DEF(var) \
  { \
    return GameMgr->getPlayer()->getCurrStat()->get ## var();  \
  }

#define AUFM_GET_PLAYER_BASIC_STATUS_IMPL_DEF(var)  \
  AUFM_GET_PLAYER_STATUS_DEF(var) \
  { \
    return GameMgr->getPlayer()->getBasicStat()->get ## var(); \
  }

#define AUFM_GET_PLAYER_STATUS_EQP_IMPL_DEF(var) \
  AUFM_GET_PLAYER_STATUS_EQP_DEF(var) \
  { \
    return [NSString stringWithUTF8String:  \
    Misc::getStringCorrespondToWorld(GameMgr->getDatabase()->get ## var ## Record(GameMgr->getPlayer()->getBasicStat()->getEquipment(Misc::EquipmentType::var))["Name"], GameMgr->getWorldName()).c_str()];  \
  }
/* ---------------------------------------------------------- */

-(NSInteger)getServantIDWithIndex:(NSInteger)index { return GameMgr->getServants()[index]->getIDOnDB(); }

AUFM_GET_PLAYER_CURR_STATUS_IMPL_DEF(HP)
AUFM_GET_PLAYER_CURR_STATUS_IMPL_DEF(Mana)

AUFM_GET_PLAYER_BASIC_STATUS_IMPL_DEF(LV)
AUFM_GET_PLAYER_BASIC_STATUS_IMPL_DEF(ATK)
AUFM_GET_PLAYER_BASIC_STATUS_IMPL_DEF(DEF)
AUFM_GET_PLAYER_BASIC_STATUS_IMPL_DEF(SP)
AUFM_GET_PLAYER_BASIC_STATUS_IMPL_DEF(Exp)

-(NSInteger)getPlayerStatusMaxHP    { return GameMgr->getPlayer()->getBasicStat()->getHP();   }
-(NSInteger)getPlayerStatusMaxMana  { return GameMgr->getPlayer()->getBasicStat()->getMana(); }

AUFM_GET_PLAYER_STATUS_EQP_IMPL_DEF(Weapon)
AUFM_GET_PLAYER_STATUS_EQP_IMPL_DEF(Protector)
AUFM_GET_PLAYER_STATUS_EQP_IMPL_DEF(Accessory)

-(NSInteger)getPlayerStatusCondition
{
  return 1; // もうめんどいので適当
}

-(NSInteger)getCurrentMoney
{
  return GameMgr->getMoney();
}

-(NSInteger)getNumOfPlayers
{
  return GameMgr->getNumOfPlayers();
}

-(NSInteger)getNumOfServants
{
  return GameMgr->getNumOfServants();
}

-(NSString*)getServantNameWithIndex:(NSInteger)idx
{
  return [NSString stringWithUTF8String: GameMgr->getServants()[idx]->getName().c_str()];
}

-(NSMutableArray*)getEncountedPlayersList
{
  NSMutableArray* ret = [NSMutableArray array];
  auto st = GameMgr->getLogger()->getEncountedPlayerSet();
  for(auto const& item: st) { [ret addObject: [NSNumber numberWithInt: item]]; }
  return ret;
}


/* ---------------------------------------------------------- */
#pragma mark - macro: Timestamped list -

#define AUFM_GET_TIME_STAMPED_LIST_IMPL_DEF(type)  \
  AUFM_GET_TIME_STAMPED_LIST_DEF(type)  \
  { \
    NSMutableArray* ret = [NSMutableArray array]; \
    auto const& lst = GameMgr->getLogger()->getPlayerStatusLog(static_cast<int>(playerID)).get ## type ## List();  \
    for(auto const& item: lst) {  \
      [ret addObject: [NSArray arrayWithObjects: [NSNumber numberWithDouble: item.first.get()], [NSNumber numberWithInt: item.second->get()], nil]];  \
    } \
    return ret; \
  }

#define AUFM_GET_TIME_STAMPED_LIST_FOR_EQP_IMPL_DEF(type)  \
  AUFM_GET_TIME_STAMPED_LIST_DEF(type)  \
  { \
  NSMutableArray* ret = [NSMutableArray array]; \
  auto const& lst = GameMgr->getLogger()->getPlayerStatusLog(static_cast<int>(playerID)).get ## type ## List();  \
  for(auto const& item: lst) {  \
    [ret addObject: [NSArray arrayWithObjects: [NSNumber numberWithDouble: item.first.get()], [NSNumber numberWithInt: item.second], nil]];  \
  } \
    return ret; \
  }

/* ---------------------------------------------------------- */

AUFM_GET_TIME_STAMPED_LIST_IMPL_DEF(LV)
AUFM_GET_TIME_STAMPED_LIST_IMPL_DEF(Exp)
AUFM_GET_TIME_STAMPED_LIST_IMPL_DEF(MaxHP)
AUFM_GET_TIME_STAMPED_LIST_IMPL_DEF(MaxMana)
AUFM_GET_TIME_STAMPED_LIST_IMPL_DEF(HP)
AUFM_GET_TIME_STAMPED_LIST_IMPL_DEF(Mana)
AUFM_GET_TIME_STAMPED_LIST_IMPL_DEF(ATK)
AUFM_GET_TIME_STAMPED_LIST_IMPL_DEF(DEF)
AUFM_GET_TIME_STAMPED_LIST_IMPL_DEF(SP)

AUFM_GET_TIME_STAMPED_LIST_FOR_EQP_IMPL_DEF(Weapon)
AUFM_GET_TIME_STAMPED_LIST_FOR_EQP_IMPL_DEF(Protector)
AUFM_GET_TIME_STAMPED_LIST_FOR_EQP_IMPL_DEF(Accessory)

AUFM_GET_TIME_STAMPED_LIST_DEF(AIName)
{
  NSMutableArray* ret = [NSMutableArray array];
  auto const& lst = GameMgr->getLogger()->getPlayerStatusLog(static_cast<int>(playerID)).getAINameList();
  for(auto const& item: lst) {
    [ret addObject: [NSArray arrayWithObjects: [NSNumber numberWithDouble: item.first.get()], [NSString stringWithUTF8String: item.second.c_str()], nil]];
  }
  return ret;
}

AUFM_GET_TIME_STAMPED_LIST_DEF(Condition)
{
  NSMutableArray* ret = [NSMutableArray array];
  auto const& lst = GameMgr->getLogger()->getPlayerStatusLog(static_cast<int>(playerID)).getConditionList();
  for(auto const& item: lst) {
    [ret addObject: [NSArray arrayWithObjects: [NSNumber numberWithDouble: item.first.get()], [NSNumber numberWithInt: item.second], nil]];
  }
  return ret;
}

AUFM_GET_TIME_STAMPED_LIST_DEF(MasteredAction)
{
  NSMutableArray* ret = [NSMutableArray array];
  auto const& lst = GameMgr->getLogger()->getPlayerStatusLog(static_cast<int>(playerID)).getMasteredActionList();
  for(auto const& item: lst) {
    [ret addObject: [NSArray arrayWithObjects: [NSNumber numberWithDouble: item.first.get()], [NSString stringWithUTF8String: item.second.c_str()], nil]];
  }
  return ret;
}

AUFM_GET_TIME_STAMPED_LIST_DEF(WayOfUseMoney)
{
  NSMutableArray* ret = [NSMutableArray array];
  auto const& lst = GameMgr->getLogger()->getPlayerStatusLog(static_cast<int>(playerID)).getWayOfUseMoneyList();
  for(auto const& item: lst) {
    [ret addObject: [NSArray arrayWithObjects: [NSNumber numberWithDouble: item.first.get()], [NSString stringWithUTF8String: item.second.c_str()], nil]];
  }
  return ret;
}

AUFM_GET_TIME_STAMPED_LIST_IMPL_DEF(Money)

AUFM_GET_TIME_STAMPED_LIST_DEF(Brainkeep)
{
  NSMutableArray* ret = [NSMutableArray array];
  auto const& lst = GameMgr->getLogger()->getPlayerStatusLog(static_cast<int>(playerID)).getBrainkeepList();
  for(auto const& item: lst) {
    BrainkeepType type;
    switch(item.second.type) {
      case GameUI::BrainkeepType::TrialChangeEquipment:
        type = kBrainkeepChangeEquipment;
        break;
      case GameUI::BrainkeepType::TrialInvitation:
        type = kBrainkeepInvitation;
        break;
      case GameUI::BrainkeepType::TrialInformationGathering:
        type = kBrainkeepInformationGathering;
        break;
    }
    
    [ret addObject: [NSArray arrayWithObjects: [NSNumber numberWithDouble: item.first.get()], [NSNumber numberWithInteger: type], [NSString stringWithUTF8String: item.second.text.c_str()], [NSNumber numberWithBool: item.second.success], nil]];
  }
  return ret;
}

-(NSInteger)calcExpTillNextLevel:(NSInteger)level
{
  return ChrData::ChrBasicStat::calcExpTillNextLevel(ChrData::Level(static_cast<int>(level))).get();
}

/* ---------------------------------------------------------- */
#pragma mark - Alive servant status -

#define AUFM_GET_SERVANT_CURR_STATUS_IMPL_DEF(var)  \
  AUFM_GET_SERVANT_STATUS_DEF(var) \
  { \
    return GameMgr->getServants()[idx]->getCurrStat()->get ## var();  \
  }

#define AUFM_GET_SERVANT_BASIC_STATUS_IMPL_DEF(var)  \
  AUFM_GET_SERVANT_STATUS_DEF(var) \
  { \
    return GameMgr->getServants()[idx]->getBasicStat()->get ## var(); \
  }

#define AUFM_GET_SERVANT_STATUS_EQP_IMPL_DEF(var) \
  AUFM_GET_SERVANT_STATUS_EQP_DEF(var) \
  { \
  return [NSString stringWithUTF8String:  \
  Misc::getStringCorrespondToWorld(GameMgr->getDatabase()->get ## var ## Record(GameMgr->getServants()[idx]->getBasicStat()->getEquipment(Misc::EquipmentType::var))["Name"], GameMgr->getWorldName()).c_str()];  \
  }
/* ---------------------------------------------------------- */

AUFM_GET_SERVANT_CURR_STATUS_IMPL_DEF(HP)
AUFM_GET_SERVANT_CURR_STATUS_IMPL_DEF(Mana)

AUFM_GET_SERVANT_BASIC_STATUS_IMPL_DEF(LV)
AUFM_GET_SERVANT_BASIC_STATUS_IMPL_DEF(ATK)
AUFM_GET_SERVANT_BASIC_STATUS_IMPL_DEF(DEF)
AUFM_GET_SERVANT_BASIC_STATUS_IMPL_DEF(SP)
AUFM_GET_SERVANT_BASIC_STATUS_IMPL_DEF(Exp)
-(NSInteger)getServantStatusMaxHPWithIndex:(NSInteger)idx { return GameMgr->getServants()[idx]->getBasicStat()->getHP(); }
-(NSInteger)getServantStatusMaxManaWithIndex:(NSInteger)idx { return GameMgr->getServants()[idx]->getBasicStat()->getMana(); }
AUFM_GET_SERVANT_STATUS_EQP_IMPL_DEF(Weapon)
AUFM_GET_SERVANT_STATUS_EQP_IMPL_DEF(Protector)
AUFM_GET_SERVANT_STATUS_EQP_IMPL_DEF(Accessory)
-(NSInteger)getServantStatusConditionWithIndex:(NSInteger)idx { return GameMgr->getServants()[idx]->getCurrStat()->getCondition(); }

-(NSMutableArray*)getItemListInRuckSack
{
  NSMutableArray* ret = [NSMutableArray array];
  auto const lst = GameMgr->getRucksack()->listUpContents();
  for(auto const& item: lst) {
    [ret addObject: [NSArray arrayWithObjects: [NSNumber numberWithInt: item.first], [NSNumber numberWithInt: item.second], nil]];
  }
  return ret;
}

/* ---------------------------------------------------------- */
#pragma mark - IDTypeImpl -
#define AUFM_CLASS_ID_IMPL [classID UTF8String]
#define AUFM_INTEGER_ID_IMPL static_cast<int>(integerID)
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
#pragma mark - Get database record -

#define AUFM_DATABASE_RECORD(recordTarget, recordName, IDTypeImpl)  \
  GameMgr->getDatabase()->get ## recordTarget ## Record(IDTypeImpl)[#recordName]

#define AUFM_DATABASE_RECORD_CORRESPOND_TO_WORLD(recordTarget, recordName, IDTypeImpl)  \
  Misc::getStringCorrespondToWorld(AUFM_DATABASE_RECORD(recordTarget, recordName, IDTypeImpl), GameMgr->getWorldName())


#define AUFM_GET_DATABASE_INTEGER_RECORD_IMPL_DEF(recordTarget, recordName, correspondToWorld, IDType)  \
  AUFM_GET_DATABASE_INTEGER_RECORD_DEF(recordTarget, recordName, IDType) \
  { \
    return std::stoi(AUFM_DATABASE_RECORD ## correspondToWorld (recordTarget, recordName, IDType ## _IMPL)); \
  }

#define AUFM_GET_DATABASE_STRING_RECORD_IMPL_DEF(recordTarget, recordName, correspondToWorld, IDType)  \
  AUFM_GET_DATABASE_STRING_RECORD_DEF(recordTarget, recordName, IDType) \
  { \
    return [NSString stringWithUTF8String: AUFM_DATABASE_RECORD ## correspondToWorld (recordTarget, recordName, IDType ## _IMPL).c_str()]; \
  }
/* ---------------------------------------------------------- */

// ChrStatus
AUFM_GET_DATABASE_STRING_RECORD_IMPL_DEF(ChrStatus, Name, _CORRESPOND_TO_WORLD, AUFM_INTEGER_ID)

// Item
AUFM_GET_DATABASE_STRING_RECORD_IMPL_DEF(Item, Name, _CORRESPOND_TO_WORLD, AUFM_INTEGER_ID)
AUFM_GET_DATABASE_STRING_RECORD_IMPL_DEF(Item, Description, , AUFM_INTEGER_ID)
AUFM_GET_DATABASE_STRING_RECORD_IMPL_DEF(Item, Flavor, _CORRESPOND_TO_WORLD, AUFM_INTEGER_ID)

// Action
AUFM_GET_DATABASE_STRING_RECORD_IMPL_DEF(Action, Name, _CORRESPOND_TO_WORLD, AUFM_CLASS_ID)
AUFM_GET_DATABASE_STRING_RECORD_IMPL_DEF(Action, Description, , AUFM_CLASS_ID)
AUFM_GET_DATABASE_STRING_RECORD_IMPL_DEF(Action, Flavor, _CORRESPOND_TO_WORLD, AUFM_CLASS_ID)
AUFM_GET_DATABASE_INTEGER_RECORD_IMPL_DEF(Action, Mana, , AUFM_CLASS_ID)

// Weapon
AUFM_GET_DATABASE_STRING_RECORD_IMPL_DEF(Weapon, Name, _CORRESPOND_TO_WORLD, AUFM_INTEGER_ID)

// Protector
AUFM_GET_DATABASE_STRING_RECORD_IMPL_DEF(Protector, Name, _CORRESPOND_TO_WORLD, AUFM_INTEGER_ID)

// Accessory
AUFM_GET_DATABASE_STRING_RECORD_IMPL_DEF(Accessory, Name, _CORRESPOND_TO_WORLD, AUFM_INTEGER_ID)


-(NSMutableArray*)getCurrentTargetForDefeats
{
  NSMutableArray* ret = [NSMutableArray array];
  auto lst = GameMgr->getTargetForDefeat()->getCurrentTargetForDefeats();
  for(auto const& item: lst) {
    [ret addObject: [NSArray arrayWithObjects: [NSNumber numberWithInt: item.first], [NSNumber numberWithInt: item.second], nil]];
  }
  return ret;
}

-(NSMutableArray*)getCurrentBezierPath
{
  NSMutableArray* ret = [NSMutableArray array];
  
  PATH_SOLVER_SWITCH({
    CASE_PATH(Dungeon, {
      if(!DungeonPath->isExistCurrentRoute()) { return ret; }
      auto path = DungeonPath->getCurrentEdge()->getBezierPath();
      for(auto const& point: path) {
        [ret addObject: [NSValue valueWithCGPoint: CGPointMake(point.real(), point.imag())]];
      }
    })
    CASE_PATH(World, {
      if(!WorldPath->isExistCurrentRoute()) { return ret; }
      auto path = WorldPath->getCurrentEdge()->getBezierPath();
      for(auto const& point: path) {
        [ret addObject: [NSValue valueWithCGPoint: CGPointMake(point.real(), point.imag())]];
      }
    })
    CASE_PATH(AtCity, PATH_SOLVER_ASSERTION)
  })
  return ret;
}

-(NSMutableArray*)getBezierPathWithMapID:(NSString*)mapID edgeID:(NSInteger)edgeID
{
  
  typedef NSMutableArray* (^LoadBezierPathWithFilePath)(NSString*);
  
  LoadBezierPathWithFilePath loadBezierPathWithFilePath = ^(NSString* path) {
    NSMutableArray* ret = [NSMutableArray array];
    std::ifstream ifs([path UTF8String]);
    std::string s, json; while(ifs >> s) { json += s; }
    picojson::value v;
    auto err = picojson::parse(v, json);
    AUFM_ASSERT(err.empty(), "JSON parse error: err = " << err << ", path = " << [path UTF8String] << ", json = " << json);
    auto& o = v.get<picojson::object>();
    auto& graphArray = o["Graph"].get<picojson::array>();
    
    // 該当するEdgeIDのBezierPathをGraphから読み取る
    bool found = false;
    for(auto& v: graphArray) {
      auto& o = v.get<picojson::object>();
      if(static_cast<int>(o["ID"].get<double>()) == static_cast<int>(edgeID)) {
        auto& bezierPathArray = o["BezierPath"].get<picojson::array>();
        for(auto& v: bezierPathArray) {
          auto point = Misc::getDoublePointFromJSON(v.get<picojson::array>());
          [ret addObject: [NSValue valueWithCGPoint: CGPointMake(point.real(), point.imag())]];
        }
        found = true;
        break;
      }
    }
    AUFM_ASSERT(found, "not found bezier path with edgeID: " << edgeID << ", mapID: " << mapID);
    return ret;
  };
  
  if([mapID characterAtIndex: 0] == 'D') {
    
    // 画像に関する余分なサフィックスを取り除く
    std::string position = [mapID UTF8String];
    std::stringstream ss(position); position = "";
    for(int i=0; i<3; i++) {  // "D" + "4" + "12" => "D-4-12"
      std::string token; std::getline(ss, token, '-');
      if(i) position += '-';
      position += token;
    }
    
    NSString* path = [[NSBundle mainBundle] pathForResource: [NSString stringWithUTF8String:  ("DataResources/Worlds/" + GameMgr->getWorldName() + "/Area/" + position).c_str()] ofType: @"json"];
    return loadBezierPathWithFilePath(path);
  }
  else if([mapID characterAtIndex: 0] == 'W') {
    NSString* path = [[NSBundle mainBundle] pathForResource: [NSString stringWithUTF8String: ("DataResources/Worlds/" + GameMgr->getWorldName() + "/WorldMapGraph").c_str()] ofType: @"json"];
    return loadBezierPathWithFilePath(path);
  }
  else {
    AUFM_ASSERT(false, "Invalid mapID");
  }
}

-(NSString*)getCurrentMapID
{
  PATH_SOLVER_SWITCH({
    CASE_PATH(Dungeon, {
      // D-4-1-Map3
      return [NSString stringWithUTF8String: (WorldPath->getPosition()->print() + "-Map" + DungeonPath->getPosition()->print()).c_str()];
    })
    CASE_PATH(AtCity, {
      // C-1-1
      return [NSString stringWithUTF8String: WorldPath->getPosition()->print().c_str()];
    })
    CASE_PATH(World, {
      return @"World";
    })
  })
}

-(NSInteger)getCurrentEdgeID
{
  PATH_SOLVER_SWITCH({
    CASE_PATH(Dungeon, {
      return DungeonPath->getCurrentEdge()->getEdgeID();
    })
    CASE_PATH(World, {
      return WorldPath->getCurrentEdge()->getEdgeID();
    })
    CASE_PATH(AtCity, PATH_SOLVER_ASSERTION)
  })
}

-(int)getCurrentEdgeTotalEnemiesNum
{
  int ret = 0;
  auto lst = GameMgr->getTargetForDefeat()->getCurrentTargetForDefeats();
  for(auto const& item: lst) {
    
    PATH_SOLVER_SWITCH({
      CASE_PATH(Dungeon, {
        if(DungeonPath->getCurrentEdge()->isExistEnemyID(item.first)) {
          ret += item.second;
        }
      })
      CASE_PATH(World, {
        if(WorldPath->getCurrentEdge()->isExistEnemyID(item.first)) {
          ret += item.second;
        }
      })
      CASE_PATH(AtCity, PATH_SOLVER_ASSERTION)
    })
  }
  
  return ret;
}

static int currentEdgeTotalTargetEnemiesNum;
-(void)loadCurrentEdgeTotalEnemiesNum
{
  currentEdgeTotalTargetEnemiesNum = [self getCurrentEdgeTotalEnemiesNum];
}

-(double)getProgressRateDiffOfECOfEdge
{
  static double prevSum = 0.0;
  
  double ecrate = 0.0;
  PATH_SOLVER_SWITCH({
    CASE_PATH(Dungeon, {
      ecrate = std::min(1.0, (double) DungeonPath->getTotalEC() / DungeonPath->getCurrentEdge()->getRequiredEC());
    })
    CASE_PATH(World, {
      ecrate = std::min(1.0, (double) WorldPath->getTotalEC() / WorldPath->getCurrentEdge()->getRequiredEC());
    })
    CASE_PATH(AtCity, PATH_SOLVER_ASSERTION)
  })
  
  auto todrate = 0.0;
  if(currentEdgeTotalTargetEnemiesNum > 0) {
    todrate = (double)( currentEdgeTotalTargetEnemiesNum - [self getCurrentEdgeTotalEnemiesNum] ) / currentEdgeTotalTargetEnemiesNum;
  }
  
  auto currSum = std::min(ecrate, todrate);
  
  auto ret = currSum - prevSum;
  prevSum = currSum;
  
  if(ret == 1.0) {
    prevSum = 0.0;
    currentEdgeTotalTargetEnemiesNum = [self getCurrentEdgeTotalEnemiesNum];
  }
  
  return ret;
}

-(NSString*)getLampMoverTypeWithID:(NSString*)lampMoverID
{
  return [NSString stringWithUTF8String: GameMgr->getLogger()->getLampMoverWithID([lampMoverID UTF8String]).type.c_str()];
}

-(NSString*)getLampMoverMovingModeWithID:(NSString*)lampMoverID
{
  return [NSString stringWithUTF8String: GameMgr->getLogger()->getLampMoverWithID([lampMoverID UTF8String]).movingMode.c_str()];
}

-(double)getLampMoverSpeedWithID:(NSString*)lampMoverID
{
  return GameMgr->getLogger()->getLampMoverWithID([lampMoverID UTF8String]).speed;
}

-(CGPoint)getLampMoverStartPositionWithID:(NSString*)lampMoverID
{
  decltype(auto) pos = GameMgr->getLogger()->getLampMoverWithID([lampMoverID UTF8String]).startPosition;
  return CGPointMake(pos.real(), pos.imag());
}

-(NSString*)getLampMoverPlayerID
{
  return [NSString stringWithUTF8String: GameUI::Logger::LampMoverPlayerID.c_str()];
}

/* ----------------------------------------------------------------------------- */
#pragma mark - FOR SWIFT TEST -

@end
