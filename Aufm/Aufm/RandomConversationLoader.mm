//
//  RandomConversationLoader.m
//  Aufm
//
//  Created by moti on 2015/06/10.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

#import "RandomConversationLoader.h"
#import <UIKit/UIKit.h>
#import "../../Lib/picojson.h"
#import "../../Support/Misc.hpp"

@implementation RandomConversationLoader : NSObject

-(id)initWithJSONString:(NSString*)jsonString
{
  if (self = [super init]) {
    picojson::value v;
    auto err = picojson::parse(v, [jsonString UTF8String]);
    if(!err.empty()) { AUFM_ASSERT(false, "Can't load jsonString in RandomConversationLoader"); }
    
    auto& arr = v.get<picojson::object>()["RandomConversations"].get<picojson::array>();
    
    _dicarray = [NSMutableArray array];
    
    for(auto& item: arr) {
      auto& o = item.get<picojson::object>();
      NSMutableDictionary* ndic = [NSMutableDictionary dictionary];
      if(o.find("Text") != o.end()) {
        [ndic setObject: [NSString stringWithUTF8String: o["Text"].get<std::string>().c_str()] forKey: @"Text"];
      }
      if(o.find("SE") != o.end()) {
        [ndic setObject: [NSString stringWithUTF8String: o["SE"].get<std::string>().c_str()] forKey: @"SE"];
      }
      [_dicarray addObject: ndic];
    }
  }
  return self;
}

@end