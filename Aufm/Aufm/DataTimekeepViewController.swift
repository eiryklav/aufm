//
//  DataTimekeepViewController.swift
//  Aufm
//
//  Created by moti on 2015/07/18.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//
/*

import UIKit

class DataTimekeepViewController : UIViewController {
  
  private var commonView: DataCommonView!
  private var timekeepView: DataTimekeepView!
  private var graphView: DataTimekeepGraphView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    commonView = DataCommonView(selected: .Timekeep, notificationPrefix: Notifications.DataTimekeepPrefix, isSecondScene: true)
    super.view.addSubview(commonView)
    let UpMost = commonView.bottomOfBar
    let Rect = CGRectMake(0, UpMost, Screen.size.width, Screen.size.height - UpMost)
    
    let Left  = Rect.width * 0.025
    let Top   = Rect.height * 0.05 + UpMost
    
    timekeepView = DataTimekeepView(frame: CGRectMake(Left, Top, Rect.width * 0.4, Rect.height * 0.75))
    super.view.addSubview(timekeepView)
    graphView = DataTimekeepGraphView(frame: CGRectMake(timekeepView.frame.maxX + Rect.width * 0.025, Top, Rect.width * 0.55, Rect.height * 0.9))
    super.view.addSubview(graphView)
    
    /*
      NSNotificationCenter
    */
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "backButtonPushed", name: Notifications.DataTimekeepPrefix + Notifications.PushedBackButtonSuffix, object: nil)
  }
  
  override func viewDidAppear(animated: Bool) {
    timekeepView.setData()
    graphView.graph.reloadData()
  }
  
  func backButtonPushed() {
    self.navigationController?.popToRootViewControllerAnimated(false)
  }
}

*/
