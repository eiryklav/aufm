//
//  DataAbilityTableView.swift
//  Aufm
//
//  Created by moti on 2015/07/12.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

class DataAbilityTableView : DataTableView {
  
  private var abilityClassIDList = [String]()
  private var abilityStringList = [String]()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    self.dataSource = self
    self.delegate = self
  }
  
  required init(coder: NSCoder) { fatalError("NSCoder x") }
  
}

protocol DataAbilityTableViewProtocol {
  func setAbilitiesWithPlayerID(playerID: Int)
}

extension DataAbilityTableView : DataAbilityTableViewProtocol {
  func setAbilitiesWithPlayerID(playerID: Int) {
    let abilityListArr = cppCode.getTimeStampedMasteredActionList(playerID)
    abilityClassIDList = []
    abilityStringList = []
    for item in abilityListArr {
      abilityClassIDList.append((item as! [AnyObject])[1] as! String)
      abilityStringList.append("\(cppCode.getActionNameWithID(abilityClassIDList.last!))")
    }
    self.reloadData()
  }
}

extension DataAbilityTableView : UITableViewDataSource {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return abilityClassIDList.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! UITableViewCell
    cell.textLabel!.text = "\(abilityStringList[indexPath.row])"
    return cell
  }
}

extension DataAbilityTableView : UITableViewDelegate {
  override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    super.tableView(tableView, willDisplayCell: cell, forRowAtIndexPath: indexPath)
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    NSNotificationCenter.defaultCenter().postNotificationName(Notifications.DataAbilityClassIDSet, object: nil, userInfo: ["ID":abilityClassIDList[indexPath.row]])
  }
}