//
//  MessagesLabel.swift
//  Aufm
//
//  Created by moti on 2015/06/11.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmSupport

class MessagesLabel : UILabel {
  var LabelMarginLeft: CGFloat  = 0.0
  var LabelMarginTop: CGFloat   = 0.0
  var LabelWidth: CGFloat       = 0.0
  var LabelHeight: CGFloat      = 0.0
  
  convenience init(margin: CGRect) {  // margin は最終的にadjustされることに注意
    self.init()
    
    LabelMarginLeft = adjustX(margin.origin.x)
    LabelMarginTop  = adjustY(margin.origin.y)
    LabelWidth      = adjustX(margin.width)
    LabelHeight     = adjustY(margin.height)
    
    font = UIFont(name: "AndaleMono", sizeType: .WideSmall)
    textColor = UIColor.whiteColor()
    layer.shadowOpacity = 0.5
    text = ""
    numberOfLines = 0
  }
  
  func setAttributedStrings(attrStrs: [NSAttributedString]) {
    let attrText = NSMutableAttributedString()
    for p in attrStrs {
      attrText.appendAttributedString(p)
      attrText.appendAttributedString(NSAttributedString(string: "\n"))
    }
    frame = CGRectMake(LabelMarginLeft, LabelMarginTop, LabelWidth, LabelHeight)
    attributedText = attrText
    sizeToFit()
  }
  
  override func hitTest(point: CGPoint, withEvent event: UIEvent?) -> UIView? {
    return nil
  }
}
