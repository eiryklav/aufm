//
//  DataBriefFlavorDescriptionView
//  Aufm
//
//  Created by moti on 2015/07/12.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

class DataBriefFlavorDescriptionView : UIView {
  
  var briefDescView:        UIView!
  var nameLabel:            BaseLabel!
  var briefDescLabel:       BaseLabel!
  
  var flavorDescView:       UIView!
  var flavorDescScrollView: UIScrollView!
  var flavorDescLabel:      BaseLabel!
    
  override init(frame: CGRect) {
    
    super.init(frame: frame)
    
    var rect = CGRect()
    briefDescView = UIView(frame: CGRectMake(0, 0, frame.width, frame.height * 0.2))
    briefDescView.backgroundColor = UIColor.deepWhiteStyleBackgroundColor()
    rect = briefDescView.frame
    
    nameLabel = BaseLabel(origin: CGPointMake(rect.width * 0.05, rect.height * 0.075), text: "", sizeType: .Medium, color: UIColor.blackColor())
    briefDescView.addSubview(nameLabel)
    briefDescLabel = BaseLabel(origin: CGPointMake(nameLabel.frame.minX, rect.height * 0.5), text: "", sizeType: .Small, color: UIColor.blackColor())
    briefDescView.addSubview(briefDescLabel)
    
    flavorDescView = UIView(frame: CGRectMake(0, briefDescView.frame.maxY, frame.width, frame.height * 0.8))
    flavorDescView.backgroundColor = UIColor.whiteStyleBackgroundColor()
    rect = flavorDescView.frame
    
    flavorDescScrollView = UIScrollView(frame: CGRectMake(rect.width * 0.05, rect.height * 0.05, rect.width * 0.9, rect.height * 0.9))
    flavorDescView.addSubview(flavorDescScrollView)
    rect = flavorDescScrollView.frame
    
    flavorDescLabel = BaseLabel(frame: CGRectMake(0,0,rect.width,500), text: "", sizeType: .Small, color: UIColor.blackColor())
    flavorDescLabel.numberOfLines = -1
    flavorDescScrollView.addSubview(flavorDescLabel)
    
    self.addSubview(briefDescView)
    self.addSubview(flavorDescView)
  }
  
  required init(coder: NSCoder) { fatalError("NSCoder x") }
  
}

protocol DataBriefFlavorDescriptionViewProtocol {
  func unsetData()
}

extension DataBriefFlavorDescriptionView : DataBriefFlavorDescriptionViewProtocol {
  func unsetData() {
    nameLabel.text = ""
    briefDescLabel.text = ""
    flavorDescLabel.text = ""
  }
}