//
//  DataBrainkeepStatusView.swift
//  Aufm
//
//  Created by moti on 2015/07/19.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

class DataBrainkeepStatusView : UIView {
  
  private var trialCountView: DataBrainkeepTrialCountView!
  private var timelineTableView: DataBrainkeepTimelineTableView!
  
  required init(coder: NSCoder) { fatalError("NSCoder x") }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    trialCountView = DataBrainkeepTrialCountView(frame: CGRectMake(0, 0, frame.width, frame.height * 0.25))
    self.addSubview(trialCountView)
    timelineTableView = DataBrainkeepTimelineTableView(frame: CGRectMake(0, trialCountView.frame.maxY, frame.width, frame.height * 0.8), style: .Plain)
    self.addSubview(timelineTableView)
    
    trialCountView.setData()
  }
  
}

protocol DataBrainkeepStatusViewProtocol {
  func setData()
}

extension DataBrainkeepStatusView : DataBrainkeepStatusViewProtocol {
  func setData() {
    trialCountView.setData()
    timelineTableView.reloadData()
  }
}