//
//  DLBattleDetailView.swift
//  Aufm
//
//  Created by moti on 2015/08/07.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import SpriteKit

protocol DLBattleDetailViewInit {
  init()
  init(frame: CGRect, mapEdgeID: MapEdge)
}

class DLBattleDetailView : UIView, DLBattleDetailViewInit {
  
  private let mapEdgeID:          MapEdge
  
  private var hiderView:          UIView!
  
  private var crossButton:        UIButton!
  private var positionLabel:      BaseLabel!
  private var onPathScrollView:   UIScrollView!
  private var onPathView:         DLBattleOnPathView!
  private var battleTitleLabel:   BaseLabel!
  private var battleLogTableView: DLBattleTableView!
  private var logBackButton:      UIButton!
  private var logNextButton:      UIButton!
  
  required init() {
    mapEdgeID = MapEdge(mapID: "", edgeID: -1)
    super.init(frame: CGRectZero)
  }
  
  required init(frame: CGRect, mapEdgeID: MapEdge) {
    
    self.mapEdgeID = mapEdgeID
    
    super.init(frame: frame)
    
    self.backgroundColor = UIColor(rgba: "#CCCCCCDD")
    
    crossButton = UIButton(frame: CGRectMake(frame.width * 0.9, frame.height * 0.05, frame.width * 0.05, frame.width * 0.05))
    crossButton.setImage(UIImage(named: "cross-button")!, forState: .Normal)
    crossButton.addTarget(self, action: "crossButtonPushed", forControlEvents: .TouchUpInside)
    self.addSubview(crossButton)
    
    positionLabel = BaseLabel(frame: CGRectMake(frame.width * 0.05, frame.height * 0.1, frame.width * 0.4, frame.height * 0.05), text: "4-1「だんじょん」", sizeType: .Medium, color: UIColor.blackColor())
    self.addSubview(positionLabel)
    
    // scroll view
    onPathScrollView = UIScrollView(frame: CGRectMake(positionLabel.frame.minX, positionLabel.frame.maxY + frame.height * 0.025, positionLabel.frame.width, frame.height * 0.7))
    onPathScrollView.contentSize = onPathScrollView.frame.size
    onPathScrollView.minimumZoomScale = 1.0
    onPathScrollView.maximumZoomScale = 5.0
    onPathScrollView.delegate = self
    self.addSubview(onPathScrollView)
    
    onPathView = DLBattleOnPathView(frame: onPathScrollView.bounds, mapEdgeID: mapEdgeID)
    onPathScrollView.addSubview(onPathView)
    
    
    battleTitleLabel = BaseLabel(frame: CGRectMake(onPathScrollView.frame.maxX + frame.size.width * 0.02, positionLabel.frame.minY, frame.size.width * 0.5, positionLabel.frame.height), text: Global.battleMessages[mapEdgeID]![0].title, sizeType: .Small, color: UIColor.blackColor())
    self.addSubview(battleTitleLabel)
    
    battleLogTableView = DLBattleTableView(frame: CGRectMake(onPathScrollView.frame.maxX + frame.size.width * 0.02, onPathScrollView.frame.minY, frame.size.width * 0.5, frame.size.height * 0.7), mapEdgeID: mapEdgeID)
    self.addSubview(battleLogTableView)
    
    
    battleLogTableView.battleIndex = 0
    
    logBackButton = UIButton(frame: CGRectMake(battleLogTableView.frame.minX, battleLogTableView.frame.maxY, battleLogTableView.frame.width * 0.2, frame.height * 0.05))
    logBackButton.setImage(UIImage(named: "battle-back")!, forState: .Normal)
    logBackButton.addTarget(self, action: "logBackButtonPushed", forControlEvents: .TouchUpInside)
    unableLogBackButton()
    self.addSubview(logBackButton)
    
    logNextButton = UIButton(frame: CGRectMake(battleLogTableView.frame.maxX - battleLogTableView.frame.width * 0.2, battleLogTableView.frame.maxY, battleLogTableView.frame.width * 0.2, frame.height * 0.05))
    logNextButton.setImage(UIImage(named: "battle-next")!, forState: .Normal)
    logNextButton.addTarget(self, action: "logNextButtonPushed", forControlEvents: .TouchUpInside)
    if 1 == Global.battleMessages[mapEdgeID]!.count {
      unableLogNextButton()
    }
    self.addSubview(logNextButton)
    
    
    // Above the other subviews
    hiderView = UIView(frame: self.bounds)
    hiderView.backgroundColor = self.backgroundColor
    self.addSubview(hiderView)

  }
  
  required init(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}

protocol DLBattleDetailViewProtocol {
  func crossButtonPushed()
  func showEffect()
  func logBackButtonPushed()
  func logNextButtonPushed()
}

extension DLBattleDetailView : UIScrollViewDelegate {
  func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
    return onPathView
  }
}

extension DLBattleDetailView : DLBattleDetailViewProtocol {
  func crossButtonPushed() {
    app.gameNavigationController.popViewControllerAnimated(false)
  }
  
  func showEffect() {
    hiderView.alpha = 1.0
    hiderView.hidden = false
    UIView.animateWithDuration(
      0.2,
      animations: {
        self.hiderView.alpha = 0.0
      },
      completion: { finished in
        self.hiderView.hidden = true
      }
    )
  }
  
  func logBackButtonPushed() {
    battleLogTableView.battleIndex--
    battleTitleLabel.text = Global.battleMessages[mapEdgeID]![battleLogTableView.battleIndex].title
    battleLogTableView.reloadData()
    if battleLogTableView.battleIndex + 2 == Global.battleMessages[mapEdgeID]!.count {
      enableLogNextButton()
    }
    if battleLogTableView.battleIndex == 0 {
      unableLogBackButton()
    }
  }
  
  func logNextButtonPushed() {
    battleLogTableView.battleIndex++
    battleTitleLabel.text = Global.battleMessages[mapEdgeID]![battleLogTableView.battleIndex].title
    battleLogTableView.reloadData()
    if battleLogTableView.battleIndex == 1 {
      enableLogBackButton()
    }
    if battleLogTableView.battleIndex + 1 == Global.battleMessages[mapEdgeID]!.count {
      unableLogNextButton()
    }
  }
  
  private func enableLogBackButton() { logBackButton.enabled = true   }
  private func unableLogBackButton() { logBackButton.enabled = false  }
  private func enableLogNextButton() { logNextButton.enabled = true   }
  private func unableLogNextButton() { logNextButton.enabled = false  }
}