//
//  DataTimekeepGraphView.swift
//  Aufm
//
//  Created by moti on 2015/07/18.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import CorePlot

class DataTimekeepGraphView : CPTGraphHostingView {
  
  var graph: DataXYGraph!
  
  required init(coder: NSCoder) { fatalError("NSCoder x") }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    graph = DataXYGraph(frame: frame)
    
    let axisSet = graph.axisSet as! CPTXYAxisSet
    let x = axisSet.xAxis
    //    x.title = "Time"
    x.labelingPolicy = CPTAxisLabelingPolicy.FixedInterval
    x.minorTicksPerInterval = 0
    
    var limitTargetOfBeats = 0
    for item in Global.settingInfo.targetOfBeatsWithEnemyID {
      limitTargetOfBeats = max(limitTargetOfBeats, item.1)
    }
    limitTargetOfBeats += 9
    limitTargetOfBeats = Int(floor(Double(limitTargetOfBeats) / 10.0) * 10.0)
    
    x.majorIntervalLength = CPTDecimalFromInt(Int32(limitTargetOfBeats / 10))
    graph.plotSpace.xRange = CPTPlotRange(location: CPTDecimalFromInt(0), length: CPTDecimalFromInt(Int32(limitTargetOfBeats)))
    let y = axisSet.yAxis
    y.minorTicksPerInterval = 0
    y.majorIntervalLength = CPTDecimalFromInt(Int32(Global.currentTargetOfBeats.count) / 4)
    println("currentToB.count: \(Global.currentTargetOfBeats.count)")
    graph.plotSpace.yRange = CPTPlotRange(location: CPTDecimalFromInt(1), length: CPTDecimalFromInt(Int32(Global.currentTargetOfBeats.count)))
    
    self.hostedGraph = graph
    
    // Bar
    var barPlot = CPTBarPlot.tubularBarPlotWithColor(CPTColor(componentRed: 0.3, green: 0.3, blue: 0.9, alpha: 1.0), horizontalBars: true)
    
    barPlot.fill = CPTFill(color: CPTColor(componentRed: 0.3, green: 0.3, blue: 0.9, alpha: 1.0))
    barPlot.identifier = "Plot1"
    barPlot.lineStyle = graph.lineStyle
    barPlot.baseValue  = CPTDecimalFromInt(0)
    barPlot.dataSource = self
    barPlot.delegate = self
    barPlot.barWidth = CPTDecimalFromDouble(0.3)
    barPlot.barOffset  = CPTDecimalFromDouble(0.15)
    graph.addPlot(barPlot, toPlotSpace: graph.plotSpace)
  }
  
}

extension DataTimekeepGraphView : CPTPlotDataSource {
  func numberOfRecordsForPlot(plot: CPTPlot!) -> UInt {
    return UInt(Global.currentTargetOfBeats.count)
  }
  
  func numberForPlot(plot: CPTPlot!, field fieldEnum: UInt, recordIndex idx: UInt) -> AnyObject! {
    switch fieldEnum {
    case UInt(CPTBarPlotField.BarLocation.rawValue):
      return Int(idx) as AnyObject
      
    case UInt(CPTBarPlotField.BarTip.rawValue):
      let enemyID = Global.currentTargetOfBeats[Int(idx)].0
      return (Global.settingInfo.targetOfBeatsWithEnemyID[enemyID]! - Global.currentTargetOfBeats[Int(idx)].1) as AnyObject
      
    default:
      assert(false)
    }
    return 0 as AnyObject
  }
}