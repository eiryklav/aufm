//
//  DataBrainkeepTimelineTableView.swift
//  Aufm
//
//  Created by moti on 2015/07/19.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

class DataBrainkeepTimelineTableView : UITableView {
  
  required init(coder aDecoder: NSCoder) { fatalError("NSCoder x") }
  
  override init(frame: CGRect, style: UITableViewStyle) {
    super.init(frame: frame, style: style)
    self.estimatedRowHeight = 3
    self.rowHeight = UITableViewAutomaticDimension
    self.dataSource = self
    self.separatorColor = UIColor.clearColor()
    self.backgroundColor = UIColor.whiteStyleBackgroundColor()
    self.registerClass(DataBrainkeepTimelineTableViewCell.self, forCellReuseIdentifier: "TimelineCell")
  }
  
}

extension DataBrainkeepTimelineTableView : UITableViewDataSource {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return Global.statusOfTimeStamp[PlayerID]!["Brainkeep"]!.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("TimelineCell", forIndexPath: indexPath) as! DataBrainkeepTimelineTableViewCell
    cell.frame.size = CGSizeMake(self.frame.width, cell.frame.height)
    cell.backgroundColor = UIColor.clearColor()
    let item = Global.statusOfTimeStamp[PlayerID]!["Brainkeep"]![indexPath.row]
    cell.setData(attributedText: NSAttributedString(string: ((item.1) as! (Int, String, Bool)).1), timesec: item.0)
    cell.accessoryType = .None
    return cell
  }
}