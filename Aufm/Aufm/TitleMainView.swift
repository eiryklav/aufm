//
//  TitleMainView.swift
//  Aufm
//
//  Created by moti on 2015/06/17.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

class TitleMainView : UIView {
  
  private var containerView     = UIView(frame: CGRectMake(0,0,Screen.size.width,Screen.size.height))
  
  private var aufmLabel         = TitleAufmLabel()
  private var intoPlayButton    = TitleButton(text: "Into play")
  private var collectionButton  = TitleButton(text: "Collection")
  private var helpButton        = TitleButton(text: "Help")
  
  func arrangeViews() {
    aufmLabel.frame.origin = CGPointMake(centerOf(selfLength: aufmLabel.frame.size.width, superLength: Screen.size.width), Screen.size.height*0.15)
    intoPlayButton.frame.origin = CGPointMake(centerOf(selfLength: intoPlayButton.frame.size.width, superLength: Screen.size.width), yPosition(aboveView: aufmLabel, spaceSCRatio: 0.075))
    collectionButton.frame.origin = CGPointMake(centerOf(selfLength: collectionButton.frame.size.width, superLength: Screen.size.width), yPosition(aboveView: intoPlayButton, spaceSCRatio: 0.05))
    helpButton.frame.origin = CGPointMake(centerOf(selfLength: helpButton.frame.size.width, superLength: Screen.size.width), yPosition(aboveView: collectionButton, spaceSCRatio: 0.05))
  }
  
  func showViews() {
    self.addSubview(containerView)
    containerView.addSubview(aufmLabel)
    containerView.addSubview(intoPlayButton)
    containerView.addSubview(collectionButton)
    containerView.addSubview(helpButton)
  }
  
  private var RawYIntoPlayButton:   CGFloat = 0.0
  private var RawYCollectionButton: CGFloat = 0.0
  private var RawYHelpButton:       CGFloat = 0.0
  
  func initAnimation() {
    intoPlayButton.alpha = 0.0
    RawYIntoPlayButton = intoPlayButton.frame.origin.y
    intoPlayButton.frame.origin.y *= 0.95
    
    collectionButton.alpha = 0.0
    RawYCollectionButton = collectionButton.frame.origin.y
    collectionButton.frame.origin.y *= 0.95
    
    helpButton.alpha = 0.0
    RawYHelpButton = helpButton.frame.origin.y
    helpButton.frame.origin.y *= 0.95
  }
  
  func animate() {
    UIView.animateWithDuration(
      0.3,
      delay: 0.3,
      options: UIViewAnimationOptions.CurveEaseIn,
      animations: {
        self.intoPlayButton.alpha = 1.0
        self.intoPlayButton.frame.origin.y = self.RawYIntoPlayButton
      },
      completion: nil
    );
    
    UIView.animateWithDuration(
      0.3,
      delay: 0.4,
      options: UIViewAnimationOptions.CurveEaseIn,
      animations: {
        self.collectionButton.alpha = 1.0
        self.collectionButton.frame.origin.y = self.RawYCollectionButton
      },
      completion: nil
    );
    
    UIView.animateWithDuration(
      0.3,
      delay: 0.45,
      options: UIViewAnimationOptions.CurveEaseIn,
      animations: {
        self.helpButton.alpha = 1.0
        self.helpButton.frame.origin.y = self.RawYHelpButton
      },
      completion: { (value:Bool) in
        self.intoPlayButton.addTarget(self, action: "onClickIntoPlayButton:", forControlEvents: .TouchDown)
        self.collectionButton.addTarget(self, action: "onClickCollectionButton:", forControlEvents: .TouchDown)
        self.helpButton.addTarget(self, action: "onClickHelpButton:", forControlEvents: .TouchDown)
      }
    );
  }
  
  required init(coder aDecoder: NSCoder) {
    fatalError("NSCoder not suppported")
  }
  
  init() {
    super.init(frame: CGRectMake(0,0,Screen.size.width,Screen.size.height))
    self.backgroundColor = UIColor.whiteColor()
    arrangeViews()
    initAnimation()
    showViews()
    animate()
  }
  
  /*
    ボタンのアクション
  */
  var msgs = [UILabel]()
  
  internal func onClickIntoPlayButton(sender: UIButton) {
    
    NSNotificationCenter.defaultCenter().postNotificationName(Notifications.TransIntoMapSelect, object: nil)
    
    UIView.animateWithDuration(
      0.5,
      delay: 0.0,
      options: .TransitionCrossDissolve | .CurveEaseIn,
      animations: {
        self.intoPlayButton.alpha = 0.0
        self.collectionButton.alpha = 0.0
        self.helpButton.alpha = 0.0
        self.aufmLabel.alpha = 0.0
      },
      completion: { (Bool) -> () in

        /*
        self.aufmLabel.layer.zPosition = 3
        self.aufmLabel.center = self.center
        
        UIView.animateWithDuration(1.0, animations: {
          self.aufmLabel.alpha = 1.0
        })
        
        let NumOfMsgs = 20
        
        for var i=0; i<NumOfMsgs; i++ {
          self.msgs.append(UILabel())
          self.msgs.last!.numberOfLines = -1
          var text = ""
          for var j=0; j<250; j++ {
            text += "\(String(UnicodeScalar(30 + Int(arc4random_uniform(200)))))\n"
          }
          self.msgs.last!.font = UIFont(name: "HiraKakuProN-W3", size: adjustFontSize(6.0))
          self.msgs.last!.text = text
          self.msgs.last!.textColor = UIColor.whiteColor()
          self.msgs.last!.alpha = 0.0
          self.msgs.last!.frame.origin = CGPointMake(CGFloat(arc4random_uniform(UInt32(Screen.size.width))), CGFloat(-400 - i*30))
          self.msgs.last!.sizeToFit()
          self.addSubview(self.msgs.last!)
        }
        
        UIView.animateWithDuration(
          5.0,
          delay: 0.0,
          options: .TransitionCrossDissolve | .CurveEaseIn,
          animations: {
            self.containerView.transform = CGAffineTransformMakeScale(1.2, 1.2)
            self.aufmLabel.alpha = 1.0
            self.aufmLabel.textColor = UIColor(rgba: "#ff663388")
            self.backgroundColor = UIColor(rgba: "#3333aa88")
            for msg in self.msgs {
              msg.alpha = 1.0
              msg.textColor = UIColor(rgba: "#11118855")
              msg.frame.origin.y = msg.frame.origin.y + 300
            }
          },
          completion: { (Bool) -> () in
            NSNotificationCenter.defaultCenter().postNotificationName(Notifications.TransIntoMapSelect, object: nil)
          }
        )
        
        
        UIView.animateWithDuration(
          1.0,
          delay: 5.0,
          options: UIViewAnimationOptions.TransitionCrossDissolve,
          animations: {
            self.containerView.transform = CGAffineTransformMakeScale(2.5, 2.5)
            self.containerView.alpha = 0.0
            for msg in self.msgs {
              msg.textColor = UIColor(rgba: "#11118888")
              msg.frame.origin.y = msg.frame.origin.y + 200
            }
          },
          completion: nil
        )
        */
      }
    )
    
    AudioManagerView.sharedInstance.playSE("play")
  }
  
  internal func onClickCollectionButton(sender: UIButton) {
    
  }
  
  internal func onClickHelpButton(sender: UIButton) {
    
  }
}