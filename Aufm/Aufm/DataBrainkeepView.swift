//
//  DataBrainkeepView.swift
//  Aufm
//
//  Created by moti on 2015/07/19.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import Foundation

class DataBrainkeepView : UIView {
  
  private var label: BaseLabel!
  
  required init(coder: NSCoder) { fatalError("NSCoder x") }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    self.backgroundColor = UIColor.blackStyleTerminalBackgroundColor()
    
    label = BaseLabel(origin: CGPointMake(frame.width * 0.025, frame.height * 0.025), text: "", sizeType: .Small, color: UIColor.greenColor())
    self.addSubview(label)
  }
}

protocol DataBrainkeepViewProtocol {
  func setData()
}

extension DataBrainkeepView : DataBrainkeepViewProtocol {
  func setData() {
    // ゲームを終えて一度タイトル画面に行った後２回めのプレイであれば、初期化が呼び出されないためinitだけで済まさない
    var text = "./timekeep.afm\n\n"
    text += "人当たり = \(Global.settingInfo.attitude!)\n装備換装率 = \(Global.settingInfo.changeEquipment!)\n従者勧誘率 = \(Global.settingInfo.invitation!)\nタンス = \(Global.settingInfo.lootingDrawers!)\n"
    label.text = text
    label.sizeToFit()
  }
}