//
//  DataStatusStatusView.swift
//  Aufm
//
//  Created by moti on 2015/07/09.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

class DataStatusStatusView : UIView {
  
  private var statusLabel: BaseLabel!
  
  required init(coder: NSCoder) { fatalError("NSCoder not supported") }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    self.backgroundColor = UIColor.whiteStyleBackgroundColor()
    self.layer.borderColor = UIColor(
      red:    151.0/255.0,
      green:  151.0/255.0,
      blue:   151.0/255.0,
      alpha:  180.0/255.0).CGColor!
    self.layer.borderWidth = 1.0
    statusLabel = BaseLabel(origin: CGPointMake(frame.size.width * 0.05, 0), text: "", sizeType: .Medium, color: UIColor.blackColor())
    
    self.addSubview(statusLabel)
  }
  
}

protocol DataStatusStatusViewProtocol {
  func setStatusText(currentPlayerID: Int)
}

extension DataStatusStatusView : DataStatusStatusViewProtocol {
  func setStatusText(currentPlayerID: Int) {
    let timeStamp = Global.statusOfTimeStamp[currentPlayerID]!
    let level = (timeStamp["LV"]!).last!.1 as! Int
    let expTillNextLevel = cppCode.calcExpTillNextLevel(level)
    let expTillNextLevelString = expTillNextLevel == 0 ? "-" : "\(expTillNextLevel)"
    
    let currHP = (timeStamp["CurrHP"]!).last!.1 as! Int
    let maxHP = (timeStamp["MaxHP"]!).last!.1 as! Int
    let currMa = (timeStamp["CurrMana"]!).last!.1 as! Int
    let maxMa = (timeStamp["MaxMana"]!).last!.1 as! Int
    
    let ATK = (timeStamp["ATK"]!).last!.1 as! Int
    let DEF = (timeStamp["DEF"]!).last!.1 as! Int
    let SP  = (timeStamp["SP"]!).last!.1 as! Int
    
    let weapon = cppCode.getWeaponNameWithID((timeStamp["Weapon"]!).last!.1 as! Int)
    let protector = cppCode.getProtectorNameWithID((timeStamp["Protector"]!).last!.1 as! Int)
    let accessory = cppCode.getAccessoryNameWithID((timeStamp["Accessory"]!).last!.1 as! Int)
    
    let condition = timeStamp["Condition"]!.last!.1 as! Int
    var conditionString = ""
    if (condition & ConditionPoison) > 0 { conditionString += "毒 " }
    if (condition & ConditionPalsy) > 0 { conditionString += "麻痺 " }
    if (condition & ConditionDead) > 0 { conditionString = "死亡" }
    if condition == 0 { conditionString = "正常" }
    
    let text: String = "状態 \(conditionString)\nLv \(level)(next: \(expTillNextLevelString))\nHP \(currHP)/\(maxHP)\nTP \(currMa)/\(maxMa)\nATK \(ATK)\nDEF \(DEF)\nSP \(SP)\nWeapon \(weapon)\nArmer \(protector)\nAccessory \(accessory)"
    statusLabel.text = text
    statusLabel.sizeToFit()
  }
}