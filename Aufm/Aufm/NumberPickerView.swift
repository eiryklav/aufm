//
//  NumberPickerView.swift
//  Aufm
//
//  Created by moti on 2015/06/27.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

protocol NumberPickerViewInit {
  init(frame: CGRect, range: Range<Int>)
}

class NumberPickerView : UIPickerView, NumberPickerViewInit {
  
  var range: Range<Int>!
  var currentNumber: Int!
  
  required init(coder: NSCoder) {
    fatalError("NSCoder not supported")
  }
  
  required init(frame: CGRect, range: Range<Int>) {
    super.init(frame: frame)
    self.range = range
    currentNumber = range.startIndex
    self.delegate = self
    self.dataSource = self
    self.backgroundColor = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 0.3)
  }

}

protocol NumberPickerViewProtocol {
  func getNumber() -> Int
}

extension NumberPickerView : NumberPickerViewProtocol {
  func getNumber() -> Int { return currentNumber }
}

extension NumberPickerView : UIPickerViewDelegate {
  func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String {
    return row.description
  }
  
  func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    currentNumber = row
  }
}

extension NumberPickerView : UIPickerViewDataSource {
  func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
    return 1
  }
  
  func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return range.endIndex - range.startIndex
  }
}
