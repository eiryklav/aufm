//
//  DataMoneyTransitionView.swift
//  Aufm
//
//  Created by moti on 2015/07/14.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import CorePlot

class DataMoneyTransitionView : CPTGraphHostingView {
  
  private var graph: DataXYGraph!
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    graph = DataXYGraph(frame: frame)
    
    graph.plotAreaFrame.paddingLeft = frame.width * 0.2
    
    let axisSet = graph.axisSet as! CPTXYAxisSet
    let y = axisSet.yAxis
    y.minorTicksPerInterval = 0
    y.majorIntervalLength = CPTDecimalFromInt(500000)
    graph.plotSpace.yRange = CPTPlotRange(location: CPTDecimalFromInt(0), length: CPTDecimalFromInt(5000))
    
    self.hostedGraph = graph
    let plot = CPTScatterPlot(frame: frame)
    plot.dataSource = self
    plot.dataLineStyle = graph.lineStyleForScatter
    graph.addPlot(plot)
  }
  
  required init(coder: NSCoder) { fatalError("NSCoder x") }
  
}

protocol DataMoneyTransitionViewProtocol {
  func setData()
}

extension DataMoneyTransitionView : DataMoneyTransitionViewProtocol {
  func setData() {
    
    var maxMoney: Int32 = 0
    var maxYRange: Int32 = 0
    for item in Global.statusOfTimeStamp[PlayerID]!["Money"]! {
      maxMoney = max(maxMoney, Int32(item.1 as! Int))
    }
    for var i: Int32 = 100; i < Int32(1e9); i *= 10 {
      if maxMoney < i {
        maxYRange = i
        break
      }
    }
    
    let axisSet = graph.axisSet as! CPTXYAxisSet
    let y = axisSet.yAxis
    y.majorIntervalLength = CPTDecimalFromInt(maxYRange / 4)
    graph.plotSpace.yRange = CPTPlotRange(location: CPTDecimalFromInt(0), length: CPTDecimalFromInt(maxYRange))
    graph.reloadData()
  }
}

extension DataMoneyTransitionView : CPTPlotDataSource {
  func numberOfRecordsForPlot(plot: CPTPlot!) -> UInt {
    return UInt(Global.statusOfTimeStamp[PlayerID]!["Money"]!.count) + 1
  }
  
  func numberForPlot(plot: CPTPlot!, field fieldEnum: UInt, recordIndex idx: UInt) -> AnyObject! {
    let data: [(Double, Any)] = Global.statusOfTimeStamp[PlayerID]!["Money"]!
    
    switch fieldEnum {
    case UInt(CPTScatterPlotField.X.rawValue):
      if Int(idx) >= data.count { return (cppCode.getCurrentRealTime() / 60.0) as AnyObject }
      return (data[Int(idx)].0 / 60.0) as AnyObject
      
    case UInt(CPTScatterPlotField.Y.rawValue):
      if Int(idx) >= data.count { return (data.last!.1 as! Int) as AnyObject }
      return (data[Int(idx)].1 as! Int) as AnyObject
      
    default:
      break;
    }
    
    return 0 as AnyObject!
  }
}