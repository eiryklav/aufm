//
//  DataButtonBarView.swift
//  Aufm
//
//  Created by moti on 2015/06/30.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

protocol DataBarViewInit {
  init(selected: DataBarView.ItemType, isSecondScene: Bool)
}

class DataBarView : UIView, DataBarViewInit {
  
  required init(coder aDecoder: NSCoder) { fatalError("NSCoder x") }
  
  private var backgroundView: UIView
  private var buttons: [DataBarButton]
  
  private let Height = Screen.size.height * 0.125
  
  enum ItemType: Int { case Status = 0, Item = 1, Ability = 2, Money = 3, Log = 4, Timekeep = 5, Speed = 6, Brainkeep = 7, Back = 8, Next = 9 }
  
  required init(selected: ItemType, isSecondScene: Bool) {
    
    let frame = CGRectMake(0, 0, Screen.size.width, Height)
    
    backgroundView = UIView(frame: frame)
    backgroundView.backgroundColor = UIColor(red: 216.0/255.0, green: 216.0/255.0, blue: 216.0/255.0, alpha: 0.9)
    backgroundView.alpha = 0.21
    backgroundView.layer.setShadowForButton()
    
    let rightMostSpace = Screen.size.width * 0.1
    let ButtonWidth = (Screen.size.width - rightMostSpace) / 4.0
    let ButtonSpace: CGFloat = 1.0

    buttons = [DataBarButton]()
    
    buttons.append(DataBarButton(frame: CGRectMake(0.0, 0.0, ButtonWidth, Height), item: isSecondScene ? .Log : .Status))
    buttons.append(DataBarButton(frame: CGRectMake(ButtonSpace + ButtonWidth, 0.0, ButtonWidth, Height), item: isSecondScene ? .Timekeep : .Item))
    buttons.append(DataBarButton(frame: CGRectMake(ButtonSpace * 2.0 + ButtonWidth * 2.0, 0.0, ButtonWidth, Height), item: isSecondScene ? .Speed : .Ability))
    buttons.append(DataBarButton(frame: CGRectMake(ButtonSpace * 3.0 + ButtonWidth * 3.0, 0.0, ButtonWidth, Height), item: isSecondScene ? .Brainkeep : .Money))
    buttons.append(DataBarButton(frame: CGRectMake(ButtonSpace * 4.0 + ButtonWidth * 4.0, 0.0, Screen.size.width - (ButtonSpace * 4.0 + ButtonWidth * 4.0), Height), item: isSecondScene ? .Back : .Next))
    
    super.init(frame: frame)
    
    self.addSubview(backgroundView)
    
    for view in buttons {
      if view.item == selected {
        view.beSelected()
      }
      else {
        view.beUnselected()
      }
      self.addSubview(view)
    }
    
    
  }
}