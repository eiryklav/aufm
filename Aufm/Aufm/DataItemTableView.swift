//
//  DataItemTableView.swift
//  Aufm
//
//  Created by moti on 2015/07/11.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

class DataItemTableView : DataTableView {
  
  private var itemIDList = [Int]()
  private var itemStringList = [String]()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    self.dataSource = self
    self.delegate = self
  }
  
  required init(coder: NSCoder) { fatalError("NSCoder x") }
  
}

protocol DataItemTableViewProtocol {
  func setItems()
}

extension DataItemTableView: DataItemTableViewProtocol {
  
  func setItems() {
    let itemListArr = cppCode.getItemListInRuckSack() as [AnyObject]
    itemIDList = []
    itemStringList = []
    for item in itemListArr {
      itemIDList.append((item as! [AnyObject])[0] as! Int)
      itemStringList.append("\(cppCode.getItemNameWithID(itemIDList.last!)) (\((item as! [AnyObject])[1] as! Int))")
    }
    self.reloadData()
  }
  
}

extension DataItemTableView : UITableViewDataSource {
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! UITableViewCell
    cell.textLabel!.text = "\(itemStringList[indexPath.row])"
    return cell
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return itemIDList.count
  }
  
}

extension DataItemTableView : UITableViewDelegate {
  
  override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    super.tableView(tableView, willDisplayCell: cell, forRowAtIndexPath: indexPath)
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    NSNotificationCenter.defaultCenter().postNotificationName(Notifications.DataItemIDSet, object: nil, userInfo: ["ID":itemIDList[indexPath.row]])
  }
  
}