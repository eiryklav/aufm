//
//  DataBrainkeepTrialCountView.swift
//  Aufm
//
//  Created by moti on 2015/07/19.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

class DataBrainkeepTrialCountView : UIView {
  
  private var leftDeadSpace : UIView!
  private var view: UIView!
  private var label: BaseLabel!
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    leftDeadSpace = UIView(frame: CGRectMake(0, 0, frame.width * 0.2, frame.height))
    leftDeadSpace.backgroundColor = UIColor.deepWhiteStyleBackgroundColor()
    view = UIView(frame: CGRectMake(frame.width * 0.2, 0, frame.width * 0.8, frame.height))
    view.backgroundColor = UIColor.deepWhiteStyleBackgroundColor()
    label = BaseLabel(origin: CGPointMake(view.frame.width * 0.05, view.frame.height * 0.1), text: "", sizeType: .Small, color: UIColor.blackColor())
    view.addSubview(label)
    self.addSubview(leftDeadSpace)
    self.addSubview(view)
  }
  
  required init(coder: NSCoder) { fatalError("NSCoder x") }
  
}

protocol DataBrainkeepTrialCountViewProtocol {
  func setData()
}

extension DataBrainkeepTrialCountView : DataBrainkeepTrialCountViewProtocol {
  func setData() {
    
    var changeEquipmentCount = 0, maxChangeEquipmentCount = 0
    var invitationCount = 0, maxInvitationCount = 0
    var informationCount = 0, maxInformationCount = 0
    
    for item in Global.statusOfTimeStamp[PlayerID]!["Brainkeep"]! {
      let type = (item.1 as! (Int, String, Bool)).0
      let text = (item.1 as! (Int, String, Bool)).1
      let success = (item.1 as! (Int, String, Bool)).2
      switch type {
      case Int(kBrainkeepChangeEquipment.value):
        if success { changeEquipmentCount++ }
        maxChangeEquipmentCount++
      case Int(kBrainkeepInvitation.value):
        if success { invitationCount++ }
        maxInvitationCount++
      case Int(kBrainkeepInformationGathering.value):
        if success { informationCount++ }
        maxInformationCount++
      default:
        assert(false)
      }
    }
    
    var text = String()
    text += "換装試行：　\(changeEquipmentCount) / \(maxChangeEquipmentCount)\n"
    text += "勧誘試行：　\(invitationCount) / \(maxInvitationCount)\n"
    text += "情報収集：　\(informationCount) / \(maxInformationCount)"
    
    label.text = text
    label.sizeToFit()
  }
}