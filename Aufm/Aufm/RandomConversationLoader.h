//
//  RandomConversationLoader.h
//  Aufm
//
//  Created by moti on 2015/06/10.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

#ifndef Aufm_RandomConversationLoader_h
#define Aufm_RandomConversationLoader_h

#import <Foundation/Foundation.h>

@interface RandomConversationLoader : NSObject

@property (readwrite, strong) NSMutableArray* dicarray;

-(id)initWithJSONString:(NSString*)jsonString;

@end

#endif
