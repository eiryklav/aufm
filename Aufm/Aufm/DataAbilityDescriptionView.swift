//
//  DataAbilityDescriptionView.swift
//  Aufm
//
//  Created by moti on 2015/07/12.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

class DataAbilityDescriptionView : DataBriefFlavorDescriptionView {
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "setData:", name: Notifications.DataAbilityClassIDSet, object: nil)
  }
  
  required init(coder: NSCoder) { fatalError("NSCoder x") }
  
}

protocol DataAbilityDescriptionViewProtocol {
  func setData(notification: NSNotification)
}

extension DataAbilityDescriptionView : DataAbilityDescriptionViewProtocol {
  func setData(notification: NSNotification) {
    let classID = notification.userInfo!["ID"]! as! String
    nameLabel.text = "\(cppCode.getActionNameWithID(classID))"
    nameLabel.frame.size = CGSizeMake(5000, nameLabel.frame.height)
    nameLabel.sizeToFit()
    briefDescLabel.text = "TP: \(cppCode.getActionManaWithID(classID)) \(cppCode.getActionDescriptionWithID(classID))"
    briefDescLabel.frame.size = CGSizeMake(5000, briefDescLabel.frame.height)
    briefDescLabel.sizeToFit()
    flavorDescLabel.frame.size = CGSizeMake(flavorDescScrollView.frame.width, 5000)
    flavorDescLabel.text = "\(cppCode.getActionFlavorWithID(classID))"
    flavorDescLabel.sizeToFit()
    flavorDescScrollView.contentSize = flavorDescLabel.frame.size
  }
}