//
//  DataCommonView.swift
//  Aufm
//
//  Created by moti on 2015/07/04.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

protocol DataCommonViewInit {
  init(selected: DataBarView.ItemType, notificationPrefix: String, isSecondScene: Bool)
}

class DataCommonView : UIView, DataCommonViewInit {
  
  weak var delegate: DataCommonViewDelegate?
  private var backgroundImageView: UIImageView//GPUImageView
  private var barView: DataBarView
  private var closeButton: CloseButton
  
  var bottomOfBar : CGFloat { return barView.frame.height }
  
  var selectedItem: DataBarView.ItemType
  
  required init(selected: DataBarView.ItemType, notificationPrefix: String, isSecondScene: Bool = false) {
    
    self.selectedItem = selected
    backgroundImageView = UIImageView(frame: UIScreen.mainScreen().bounds)
    barView = DataBarView(selected: selectedItem, isSecondScene: isSecondScene)
    super.init(frame: UIScreen.mainScreen().bounds)
    
    addSubview(backgroundImageView)
    addSubview(barView)
    showBackground()
  }

  required init(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
  
  private func showBackground() {
    let image = UIImage(named: "background")!.lightedImageWithColor()
    backgroundImageView.image = image
  }
}

protocol DataCommonViewDelegate {
  func didPushButtonWithItemType(itemType: DataBarView.ItemType)
}