//
//  DataWayOfUseMoneyLabelView.swift
//  Aufm
//
//  Created by moti on 2015/07/16.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

class DataWayOfUseMoneyLabelView : UIView {
  
  private var leftDeadSpace : UIView!
  private var view: UIView!
  private var label: BaseLabel!
  
  private var wayOfUseLabel: BaseLabel!
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    leftDeadSpace = UIView(frame: CGRectMake(0, 0, frame.width * 0.2, frame.height))
    leftDeadSpace.backgroundColor = UIColor.deepWhiteStyleBackgroundColor()
    view = UIView(frame: CGRectMake(frame.width * 0.2, 0, frame.width * 0.8, frame.height))
    view.backgroundColor = UIColor.deepWhiteStyleBackgroundColor()
    label = BaseLabel(origin: CGPointMake(view.frame.width * 0.05, view.frame.height * 0.3), text: "使途", sizeType: .SemiMedium, color: UIColor.blackColor())
    view.addSubview(label)
    self.addSubview(leftDeadSpace)
    self.addSubview(view)
  }
  
  required init(coder: NSCoder) { fatalError("NSCoder x") }
  
}
