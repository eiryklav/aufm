//
//  DLTableViewCell.swift
//  Aufm
//
//  Created by moti on 2015/07/16.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

class DLTableViewCell : DataTimelineTableViewCell {
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String!) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    timeView.backgroundColor = UIColor.whiteStyleTimelineTimeBackgroundColor()
    mainView.backgroundColor = UIColor.whiteStyleTimelineWayOfUseBackgroundColor()
  }

  required init(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}
