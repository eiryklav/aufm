//
//  ViewController.swift
//  TabDialogViewDemo
//
//  Created by moti on 2015/09/13.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit
import TabDialogView

class ViewController: UIViewController {
  
  let tabCount = 5
  var subTitleViews = [UIView]()
  var contentViews = [UIView]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let b = UIButton(frame: CGRectMake(10, 200, 100, 100))
    b.setTitle("ボタン", forState: .Normal)
    view.addSubview(b)
    
    for _ in 0..<tabCount {
      subTitleViews.append(UIView())
      contentViews.append(UIView())
    }
    
    let dialog = TabDialogView(frame: UIScreen.mainScreen().bounds)
    dialog.dataSource = self
    dialog.delegate   = self
    dialog.loadSubviews()
    dialog.reload()
    
    for index in 0..<tabCount {
      
      subTitleViews[index].frame = dialog.subTitleViewFrame
      let l = UILabel(frame: CGRectMake(0, 0, 300, 50))
      l.backgroundColor = UIColor(white: 0.5, alpha: 0.5)
      l.text = "sssssssssssssssss - \(index)"
      l.textColor = UIColor.whiteColor()
      subTitleViews[index].addSubview(l)
      
      contentViews[index].frame = dialog.contentViewFrame
      let label = UILabel(frame: CGRectMake(0, 0, 300, 50))
      label.backgroundColor = UIColor(white: 0.5, alpha: 0.5)
      label.text = "aaaaaaaaaaaaaaaaa - \(index)"
      label.textColor = UIColor.whiteColor()
      contentViews[index].addSubview(label)
      
      let button = UIButton(frame: CGRectMake(30, 60, 100, 30))
      button.setTitle("ボタン", forState: .Normal)
      button.addTarget(self, action: "funca", forControlEvents: .TouchUpInside)
      contentViews[index].addSubview(button)
    }
    
    dialog.selectedIndex = 0
    
    view.addSubview(dialog)
    
  }
  
  func funca() {
    print("aaaaa")
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }


}

extension ViewController : TabDialogViewDataSource {
  
  func numberOfTabButtonsInTabDialogView() -> Int {
    return tabCount
  }
  
  func tabDialogView(tabDialogView: TabDialogView, subTitleViewForIndex index: Int) -> UIView {
    return subTitleViews[index]
  }
  
  func tabDialogView(tabDialogView: TabDialogView, contentViewForIndex index: Int) -> UIView {
    return contentViews[index]
  }
  
}

extension ViewController : TabDialogViewDelegate {
  func tabDialogView(tabDialogView: TabDialogView, didPushTabButtonWithIndex index: Int) {
    print("Pushed tab: \(index)")
  }
}