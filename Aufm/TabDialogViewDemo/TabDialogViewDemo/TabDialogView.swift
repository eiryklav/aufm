//
//  TabDialogView
//  AufmData
//
//  Created by moti on 2015/09/06.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit

class TransparentTabButton : UIButton {
  required init?(coder aDecoder: NSCoder) { fatalError() }
  override init(frame: CGRect) {
    super.init(frame: frame)
  }
}

public class TabDialogView : UIView {
  
  weak public var dataSource: TabDialogViewDataSource?
  weak public var delegate:   TabDialogViewDelegate?
  
  private var _barViewFrame: CGRect!
  public var barViewFrame: CGRect {
    get {
      return _barViewFrame
    }
  }
  
  private var _contentViewFrame: CGRect!
  public var contentViewFrame: CGRect {
    get {
      return _contentViewFrame
    }
  }
  
  private var _subTitleViewFrame: CGRect!
  public var subTitleViewFrame: CGRect {
    get {
      return _subTitleViewFrame
    }
  }
  
  private var _selectedIndex: Int = 0
  public var selectedIndex: Int {
    get {
      return _selectedIndex
    }
    set {
      assert(0 <= newValue && newValue < numberOfTabButtons)
      _selectedIndex = newValue
      didPushTabButton(transparentTabButtons[_selectedIndex])
    }
  }
  
  public var numberOfTabButtons: Int {
    get {
      guard let dataSource = dataSource else { print("'dataSource' has not been set.") ; return 0 }
      return dataSource.numberOfTabButtonsInTabDialogView()
    }
  }
  
  required public init?(coder aDecoder: NSCoder) { fatalError() }
  
  override public init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  public func reload() {
    
    for view in subviews {
      view.removeFromSuperview()
    }
    
    guard numberOfTabButtons > 0 else {
      print("TabDialogView - Error: numberOfTabButtons = 0")
      return
    }
    
    unselectedTabs = []
    let unselImgView = UIImageView(image: UIImage(named: "unselected-tab0"))
    unselImgView.contentMode = .ScaleToFill
    scaleRatio = frame.width / unselImgView.frame.width
    unselImgView.frame = CGRectMake(0, 0, frame.width, unselImgView.frame.height * scaleRatio)
    unselImgView.hidden = true
    addSubview(unselImgView)
    unselectedTabs.append(unselImgView)
    
    for index in 1..<numberOfTabButtons {
      let view = UIImageView(image: UIImage(named: "unselected-tab\(index)"))
      view.contentMode = .ScaleToFill
      view.frame = CGRectMake(0, 0, frame.width, view.frame.height * scaleRatio)
      view.hidden = false
      addSubview(view)
      unselectedTabs.append(view)
    }
    
    selectedTabs = []
    for index in 0..<numberOfTabButtons {
      let view = UIImageView(image: UIImage(named: "selected-tab\(index)"))
      view.contentMode = .ScaleToFill
      view.frame = CGRectMake(0, 0, frame.width, view.frame.height * scaleRatio)
      view.hidden = index == 0 ? false : true
      addSubview(view)
      selectedTabs.append(view)
    }
    
    let buttonWidth: CGFloat  = 180 * scaleRatio
    let buttonHeight: CGFloat = 90 * scaleRatio
    
    transparentTabButtons = []
    for index in 0..<numberOfTabButtons {
      // button
      let buttonFrame = CGRectMake(CGFloat(index) * buttonWidth, 0, buttonWidth, buttonHeight)
      let button = TransparentTabButton(frame: buttonFrame)
      
      //button.backgroundColor = UIColor(white: 0.5 + CGFloat(index) * 0.1, alpha: 0.5) // for debug
      //button.setTitle("aaaaa", forState: .Normal) // for debug
      
      button.addTarget(self, action: "didPushTabButton:", forControlEvents: .TouchUpInside)
      button.tag = index
      addSubview(button)
      transparentTabButtons.append(button)
    }
    
    let unselHeight = selectedTabs.first!.frame.height * 0.39
    _subTitleViewFrame = CGRectMake(0, unselHeight, frame.width, frame.height - unselHeight)
    _contentViewFrame = CGRectMake(0, 0, frame.width, frame.height - selectedTabs.first!.frame.height)
    
    contentViewBackgroundImageViews = []
    contentViewBackgroundImageViews.append(UIImageView(image: UIImage(named: "subview0")))
    let height = contentViewBackgroundImageViews.first!.image!.size.height * scaleRatio
    let yOffset = -height * 0.025
    contentViewBackgroundImageViews.first!.frame = CGRectMake(
      0,
      selectedTabs.first!.frame.height + yOffset,
      frame.width,
      height
    )
    addSubview(contentViewBackgroundImageViews.first!)
    
    for index in 1..<numberOfTabButtons {
      contentViewBackgroundImageViews.append(UIImageView(image: UIImage(named: "subview\(index)")))
      contentViewBackgroundImageViews.last!.frame = contentViewBackgroundImageViews.first!.frame
      contentViewBackgroundImageViews.last!.hidden = true
      addSubview(contentViewBackgroundImageViews.last!)
    }
    
    // reload subTitleViews
    subTitleViews = []
    for index in 0..<numberOfTabButtons {
      subTitleViews.append(subTitleViewForIndex(index))
      subTitleViews[index].hidden = true
      selectedTabs[index].addSubview(subTitleViews[index])
    }
    
    // reload contentViews
    contentViews = []
    for index in 0..<numberOfTabButtons {
      contentViews.append(contentViewForIndex(index))
      contentViews[index].hidden = true
      contentViewBackgroundImageViews[index].addSubview(contentViews[index])
    }
  }
  
  func didPushTabButton(sender: TransparentTabButton) {
    
    let selected = sender.tag
    
    for index in 0..<numberOfTabButtons {
      subTitleViews[index].hidden = true
      selectedTabs[index].hidden = true
      unselectedTabs[index].hidden = false
      contentViewBackgroundImageViews[index].hidden = true
      contentViews[index].hidden = true
    }
    
    subTitleViews[selected].hidden = false
    selectedTabs[selected].hidden = false
    unselectedTabs[selected].hidden = true
    contentViewBackgroundImageViews[selected].hidden = false
    contentViews[selected].hidden = false
    
    didPushTabButtonWithIndex(selected)
  }
  
  public func subTitleViewForIndex(index: Int) -> UIView {
    return dataSource?.tabDialogView(self, subTitleViewForIndex: index) ?? UIView()
  }
  
  public func contentViewForIndex(index: Int) -> UIView {
    return dataSource?.tabDialogView(self, contentViewForIndex: index) ?? UIView()
  }
  
  public func didPushTabButtonWithIndex(index: Int) {
    delegate?.tabDialogView(self, didPushTabButtonWithIndex: index)
  }
  
  // MARK: - private -
  private var subTitleViews:                    [UIView]!
  private var contentViews:                     [UIView]!
  private var contentViewBackgroundImageViews:  [UIImageView]!
  private var selectedTabs:                     [UIImageView]!
  private var unselectedTabs:                   [UIImageView]!
  private var transparentTabButtons:            [TransparentTabButton]!
  private var scaleRatio:                       CGFloat!
}

public protocol TabDialogViewDataSource: class {
  func numberOfTabButtonsInTabDialogView() -> Int
  func tabDialogView(tabDialogView: TabDialogView, subTitleViewForIndex index: Int) -> UIView?
  func tabDialogView(tabDialogView: TabDialogView, contentViewForIndex index: Int) -> UIView?
}

public protocol TabDialogViewDelegate: class {
  func tabDialogView(tabDialogView: TabDialogView, didPushTabButtonWithIndex index: Int)
}