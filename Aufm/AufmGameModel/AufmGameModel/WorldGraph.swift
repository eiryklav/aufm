//
//  WorldGraph.swift
//  AufmGameModel
//
//  Created by moti on 2015/08/23.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import AufmSupport

public class WorldGraph {
  
  public static let sharedInstance = WorldGraph()
  
  public var data: [String : [(to: String, enemySet: Set<Int>)]] = [:]
  public var targetEnemyUnwrapped: [Int:Bool] = [:]
  public let mapDescriptionLoader = MapDescriptionLoader()
  
  private init(){}
  
  public func loadMapData() {
    mapDescriptionLoader.loadEnemiesInEdgeFromJSON(String.getJSONStringFromPath("DataResources/Worlds/\(SettingInfo.sharedInstance.worldName!)/WorldMap"))
    
    let tarr = mapDescriptionLoader.targetEnemies
    for v in tarr {
      let arr = v as! [AnyObject]
      targetEnemyUnwrapped[arr[0] as! Int] = arr[1] as? Bool
    }
    
    
    let dic = mapDescriptionLoader.enemiesGraph as Dictionary
    for val in dic {
      let from  = val.0 as! String
      let arr   = val.1 as! [AnyObject]
      
      for item in arr {
        let arr = item as! [AnyObject]
        let to = (arr[0] as! NSString) as String
        let set = (arr[1] as! NSSet) as! Set<Int>
        if data[from] == nil {
          data[from] = []
        }
        data[from]!.append(to: to, enemySet: set)
      }
    }
  }
}