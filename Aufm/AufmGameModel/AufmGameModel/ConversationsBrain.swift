//
//  ConversationsBrain.swift
//  AufmGameModel
//
//  Created by moti on 2015/09/02.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import Foundation

import UIKit
import AufmObjCppWrapper

public class ConversationsBrain {
  
  public struct Notifications {
    public static let ConversationUpdated = "CBConversationUpdated"
  }
  
  public var allConversations = Conversations()
  
  public init() {}
  
  public func update() {
    guard cppCode.isNotified(kConversationUpdated) else { return }
    
    let buf = cppCode.flushConversation()
    for var i=0; i<buf.count; i++ {
      
      let speaker   = (buf[i] as! [AnyObject])[0] as! String
      let content   = (buf[i] as! [AnyObject])[1] as! String
      let summary   = (buf[i] as! [AnyObject])[2] as! String
      let timeStamp = (buf[i] as! [AnyObject])[3] as! Double
      let alert     = (buf[i] as! [AnyObject])[4] as! Bool
      
      allConversations.conversations.append(Conversation(speaker: speaker, text: content, summary: summary, timeStamp: timeStamp, alert: alert))
    }
    
    NSNotificationCenter.defaultCenter().postNotificationName(Notifications.ConversationUpdated, object: nil)    
  }
}
