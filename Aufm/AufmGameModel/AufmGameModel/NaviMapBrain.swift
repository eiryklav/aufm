//
//  NaviMapBrain.swift
//  AufmGameModel
//
//  Created by moti on 2015/08/22.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import SpriteKit
import AufmObjCppWrapper
import AufmSupport
/*
  SKSpriteNode の原点 -> 中心, 軸 -> 右上方向にXY
  キャラクタが上に移動 -> マップが下に移動 -> マップが -Y 方向に移動
*/

public class NaviMapBrain : NSObject {
  
  public struct Constants {
    public static let MapImageName                  = "NBConstantMapImageName"
    public static let BezierPath                    = "NBConstantBezierPath"
  }
  
  public struct Notifications {
    public static let LampMoverNodeInstanceCreated  = "NBLampMoverNodeInstanceCreated"
  }
  
  private let parentScene:        SKScene
  private var hero:               LampMoverHero!
  private var enemyWithID         = [String:LampMoverEnemy]()
  private var strongEnemyWithID   = [String:LampMoverStrongEnemy]()
  private var eventWithID         = [String:LampMoverEvent]()
  
  private var mapSprite:          SKSpriteNode!
  
  private let localSizeOfRawImage = CGSizeMake(300, 300)
  private let DurationOnAPath     = 2.0
  private var localSizeRatio:     CGSize
  
  private var center:             CGPoint
  
  // MARK: - init -
  
  public init(parentScene: SKScene) {
    self.parentScene = parentScene
    center = CGPointMake(parentScene.frame.width/2, parentScene.frame.height/2)
    localSizeRatio = CGSizeMake(parentScene.frame.width / localSizeOfRawImage.width, parentScene.frame.height / localSizeOfRawImage.height)
    
    super.init()
    
    parentScene.physicsWorld.contactDelegate = self
    hero = createHero()
        
    // Physics World
    parentScene.physicsWorld.gravity = CGVector(dx: 0.0, dy: 0.0)
    
    // Map sprite
    mapSprite = SKSpriteNode(color: UIColor.clearColor(), size: CGSizeMake(10, 10))
    mapSprite.position = center
    parentScene.addChild(mapSprite)
    
    // ---------------------------------------------------------------------------
    // MARK: NSNotificationCenter observers
    
    // MessagesBrain Notifications
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "didBattleEnd", name: MessagesBrain.Notifications.HasBattleEnd, object: nil)
    
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "didGenerateLampMover:", name: MessagesBrain.Notifications.LampMoverGenerated, object: nil)
    
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "didKillLampMover:", name: MessagesBrain.Notifications.LampMoverKilled, object: nil)
    
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "didPlayerDie", name: MessagesBrain.Notifications.PlayerHasDied, object: nil)
  }
  
  // MARK: - Private functions -
  
  private func didSetCurrentMap() {
    mapSprite.removeFromParent()
    mapSprite = SKSpriteNode(imageNamed: "\(SettingInfo.sharedInstance.worldName!)-\(cppCode.getCurrentMapID())")
    mapSprite.size = adjustLocal(width: mapSprite.size.width, height: mapSprite.size.height)
    mapSprite.hidden = true
    parentScene.addChild(mapSprite)
  }
  
  private func didSetBezierPath() {
    let pathArray = cppCode.getCurrentBezierPath()
    let bezierPath = UIBezierPath()
    for var i=0; i<pathArray.count; {
      var point = (pathArray[i] as! NSValue).CGPointValue()
      if(i == 0) {
        // "BezierPath"の1要素目の点は、その曲線の始点。ただし、(-9999, -9999)は現在の座標を維持
        if point != CGPointMake(-9999, -9999) {
          point = adjustLocal(x: point.x, y: point.y)
          mapSprite.position = center + CGPointMake(mapSprite.size.width / 2 - point.x, -mapSprite.size.height / 2 + point.y)
        }
        bezierPath.moveToPoint(CGPointZero)
        i++
      }
      else {
        // "BezierPath"の2要素目以降は、始点を基準とした相対的な終点と2つの制御点
        point = adjustLocal(x: -point.x, y: -point.y)
        var cpoint1 = (pathArray[i+1] as! NSValue).CGPointValue()
        var cpoint2 = (pathArray[i+2] as! NSValue).CGPointValue()
        cpoint1 = adjustLocal(x: -cpoint1.x, y: -cpoint1.y)
        cpoint2 = adjustLocal(x: -cpoint2.x, y: -cpoint2.y)
        bezierPath.addCurveToPoint(point, controlPoint1: cpoint1, controlPoint2: cpoint2)
        i += 3
      }
    }
    
    mapSprite.hidden = false
    mapSprite.removeActionForKey("map-move")
    mapSprite.runAction(SKAction.followPath(bezierPath.CGPath, asOffset: true, orientToPath: false, duration: DurationOnAPath), withKey: "map-move")
    
    // 現在のエッジで討伐すべき敵の総数を読み込む
    cppCode.loadCurrentEdgeTotalEnemiesNum()
    
    // 停止して戦闘
    pauseWorld()
    cppCode.notify(kWaitingForNextEncount)
  }
  
  private func moveOnPathWithProgress(progress: Double) {
    // 現在のエッジ上の進捗率分だけ移動する
    resumeWorld()
    NSTimer.scheduledTimerWithTimeInterval(DurationOnAPath * progress, target: NSBlockOperation(block: {
      // 停止して戦闘
      self.pauseWorld()
      cppCode.notify(kWaitingForNextEncount);
    }), selector: "main", userInfo: nil, repeats: false)
  }
  
  private func pauseWorld() {
    parentScene.paused = true
    /*
    hero.node.paused = true
    mapSprite.paused = true
    for e in enemies        { e.node.paused = true }
    for e in strongEnemies  { e.node.paused = true }
    for e in events         { e.node.paused = true }
    */
  }
  
  private func resumeWorld() {
    parentScene.paused = false
    /*
    hero.node.paused = false
    mapSprite.paused = false
    for e in enemies        { e.node.paused = false }
    for e in strongEnemies  { e.node.paused = false }
    for e in events         { e.node.paused = false }
    */
  }
  
  private func createHero() -> LampMoverHero {
    return LampMoverHero(id: cppCode.getLampMoverPlayerID(), parentScene: parentScene)
  }
  
  private func createEvent(id: String) -> LampMoverEvent {
    return LampMoverEvent(id: id, parentScene: parentScene)
  }
  
  private func createEnemy(id: String) -> LampMoverEnemy {
    return LampMoverEnemy(id: id, parentScene: parentScene)
  }
  
  private func createStrongEnemy(id: String) -> LampMoverStrongEnemy {
    return LampMoverStrongEnemy(id: id, parentScene: parentScene)
  }
  
  private func adjustLocal(width width: CGFloat, height: CGFloat) -> CGSize {
    return CGSizeMake(width * localSizeRatio.width, height * localSizeRatio.height)
  }
  
  private func adjustLocal(x x: CGFloat, y: CGFloat) -> CGPoint {
    return CGPointMake(x * localSizeRatio.width, y * localSizeRatio.height)
  }
}

// --------------------------------------------------------------------------------
// MARK: - Public functions -

extension NaviMapBrain {
  
  public func update() {
    if cppCode.isNotified(kMapChanged) {
      didSetCurrentMap()
    }
    if cppCode.isNotified(kBezierPathChanged) {
      didSetBezierPath()
    }
    
    for (_, enemy) in enemyWithID { enemy.update() }
  }
  
}

// --------------------------------------------------------------------------------
// MARK: - MessagesBrain notification observe selectors -

extension NaviMapBrain {
  
  func didGenerateLampMover(notification: NSNotification) {
    
    let id = (notification.userInfo as! [String:AnyObject])[MessagesBrain.Constants.ID] as! String
    let type = cppCode.getLampMoverTypeWithID(id)
    var symbol: LampMover!
    
    // Create symbol for type
    switch type {
    case "Enemy":
      symbol = createEnemy(id)
      enemyWithID[id] = symbol as? LampMoverEnemy
    case "StrongEnemy":
      symbol = createStrongEnemy(id)
      strongEnemyWithID[id] = symbol as? LampMoverStrongEnemy
    case "Event":
      symbol = createEvent(id)
      eventWithID[id] = symbol as? LampMoverEvent
    default:
      assertionFailure("Undefined lamp mover type: '\(type)'")
    }
    
    // Configure symbol behaviors
    let movingMode  = cppCode.getLampMoverMovingModeWithID(id)
    let speed       = cppCode.getLampMoverSpeedWithID(id)
    switch movingMode {
    case "FollowPlayer":
      symbol.currentActions.append(LMNodeActionFollow(owner: symbol.node, target: hero.node, speed: speed))
    case "Vector":
      assertionFailure("Unimplemented moving mode : Vector")
    default:
      assertionFailure("Undefined moving mode: '\(movingMode)'")
    }
    
    let startPosition = cppCode.getLampMoverStartPositionWithID(id)
    symbol.node.position = CGPointMake(parentScene.frame.width * startPosition.x, parentScene.frame.height * startPosition.y)
  }
  
  func didKillLampMover(notification: NSNotification) {
    let id = (notification.userInfo as! [String:AnyObject])[MessagesBrain.Constants.ID] as! String
    let type = cppCode.getLampMoverTypeWithID(id)
    switch type {
    case "Enemy":
      enemyWithID[id]?.node.removeFromParent()
      enemyWithID[id] = nil
      
    case "StrongEnemy":
      strongEnemyWithID[id]?.node.removeFromParent()
      strongEnemyWithID[id] = nil
      
    case "Event":
      eventWithID[id]?.node.removeFromParent()
      eventWithID[id] = nil
      
    default:
      assertionFailure("Undefined lamp mover type: '\(type)'")
    }
  }
  
  func didBattleEnd() {
    moveOnPathWithProgress(cppCode.getProgressRateDiffOfECOfEdge())
  }
  
  func didPlayerDie() {
    hero.die()
  }
}


// --------------------------------------------------------------------------------
// MARK: - SKPhysicsContactDelegate -

extension NaviMapBrain : SKPhysicsContactDelegate {
  
  private func hitBodies(bodyA: SKPhysicsBody, _ bodyB: SKPhysicsBody, categories: (UInt32, UInt32)) -> Bool {
    return (
        bodyA.categoryBitMask & categories.0  > 0 &&
        bodyB.categoryBitMask & categories.1  > 0
      )
        ||
      (
        bodyA.categoryBitMask & categories.1  > 0 &&
        bodyB.categoryBitMask & categories.0  > 0
    )
  }
  
  public func didBeginContact(contact: SKPhysicsContact) {
    var bodyA, bodyB: SKPhysicsBody!
    if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
      bodyA = contact.bodyA
      bodyB = contact.bodyB
    }
    else {
      bodyA = contact.bodyB
      bodyB = contact.bodyA
    }
    if hitBodies(bodyA, bodyB, categories: (PhysicsCategory.Hero, PhysicsCategory.Enemy)) {
      // 特殊エンカウント
      pauseWorld()
      cppCode.notifySpecialEncountWithID(bodyB.node!.name!);
    }
    if hitBodies(bodyA, bodyB, categories: (PhysicsCategory.Hero, PhysicsCategory.StrongEnemy)) {
      // 特殊エンカウント
      pauseWorld()
      cppCode.notifySpecialEncountWithID(bodyB.node!.name!);
    }
    if hitBodies(bodyA, bodyB, categories: (PhysicsCategory.Hero, PhysicsCategory.Person)) {
      // シナリオ発生
      pauseWorld()
      cppCode.notifySpecialEncountWithID(bodyB.node!.name!);
    }
  }
  
}