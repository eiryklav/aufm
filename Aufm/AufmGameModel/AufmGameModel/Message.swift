//
//  LogModel.swift
//  Aufm
//
//  Created by moti on 2015/04/27.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmObjCppWrapper

class Message {
  let type:       Int
  let stateType:  Int
  let message:    String
  let timeStamp:  Double
  let alert:      Bool
  
  init(type: Int, stateType: Int, message: String, timeStamp: Double, alert: Bool) {
    self.type       = type
    self.stateType  = stateType
    self.message    = message
    self.timeStamp  = timeStamp
    self.alert      = alert
  }
  
  init(type: Int) {
    self.type = type
    // 以下の値はどこからも参照させない
    self.stateType = kDefaultState
    self.message = ""
    self.timeStamp = -1
    self.alert = false
  }
  
}