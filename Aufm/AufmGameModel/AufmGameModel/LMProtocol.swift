//
//  LampMoverProtocol.swift
//  AufmGameModel
//
//  Created by moti on 2015/08/28.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import AufmSupport
import SpriteKit

// MARK: - Constants -
private let NormalSize  = CGSizeMake(10, 10).adjustedSize()
private let LargeSize   = CGSizeMake(15, 15).adjustedSize()

private let BlinkAction = SKAction.sequence([SKAction.fadeAlphaTo(0.4, duration: 0.5), SKAction.fadeAlphaTo(0.7, duration: 0.5)])
private let BlinkFastAction = SKAction.sequence([SKAction.fadeAlphaTo(0.4, duration: 0.25), SKAction.fadeAlphaTo(0.7, duration: 0.25)])

public struct PhysicsCategory {
  static public let Unset:        UInt32  = 0x1 << 0
  static public let Hero:         UInt32  = 0x1 << 1
  static public let Person:       UInt32  = 0x1 << 2
  static public let Enemy:        UInt32  = 0x1 << 3
  static public let StrongEnemy:  UInt32  = 0x1 << 4
  static public let Enemies:      UInt32  = Enemy | StrongEnemy
  static public let ExceptHero:   UInt32  = 0xFFFFFFFF ^ Hero
}

// MARK: - IntrinsicLampMover -

class LampMover {
  
  let id: String
  var node: SKShapeNode!
  let parentScene: SKScene
  
  var currentActions = [LMNodeAction]()
  
  init(id: String, parentScene: SKScene) {
    self.id = id
    self.parentScene = parentScene
  }
  
  func update() {
    for action in currentActions {
      action.runAction()
    }
  }
  
}

// MARK: - LMNodeStyle -

protocol LMNodeStyleApplicable {
  func applyNodeStyle()
}

protocol LMBrilliantBlink {}
extension LMBrilliantBlink {
  func applyNodeStyle() {
    let o = self as! LampMover
    o.node = SKShapeNode(ellipseOfSize: NormalSize)
    o.node.fillColor = UIColor.redColor()
    o.node.strokeColor = UIColor.clearColor()
    o.node.runAction(SKAction.repeatActionForever(BlinkAction), withKey: "blink")
    o.parentScene.addChild(o.node)
  }
}

protocol LMAttractive {}
extension LMAttractive {
  func applyNodeStyle() {
    let o = self as! LampMover
    o.node = SKShapeNode(rectOfSize: NormalSize)
    o.node.fillColor = UIColor.cyanColor()
    o.node.strokeColor = UIColor.clearColor()
    o.parentScene.addChild(o.node)
  }
}

protocol LMWarningBlink : LMNodeStyleApplicable {}
extension LMWarningBlink {
  func applyNodeStyle() {
    let o = self as! LampMover
    o.node = SKShapeNode(ellipseOfSize: NormalSize)
    o.node.fillColor = UIColor.redColor()
    o.node.strokeColor = UIColor.clearColor()
    o.node.runAction(SKAction.repeatActionForever(BlinkFastAction), withKey: "blink")
    o.parentScene.addChild(o.node)
  }
}

protocol LMCautionBlink : LMNodeStyleApplicable {}
extension LMCautionBlink {
  func applyNodeStyle() {
    let o = self as! LampMover
    o.node = SKShapeNode(ellipseOfSize: LargeSize)
    o.node.fillColor = UIColor.redColor()
    o.node.strokeColor = UIColor.clearColor()
    o.node.runAction(SKAction.repeatActionForever(BlinkFastAction), withKey: "blink")
    o.parentScene.addChild(o.node)
  }
}

// MARK: - Physics Body -

protocol LMNodeBodyApplicable {
  func applyNodeBody()
}

protocol LMHeroBody : LMNodeBodyApplicable {}
extension LMHeroBody {
  func applyNodeBody() {
    let o = self as! LampMover
    o.node.physicsBody = SKPhysicsBody(rectangleOfSize: NormalSize)
    o.node.physicsBody!.categoryBitMask     = PhysicsCategory.Hero
    o.node.physicsBody!.contactTestBitMask  = PhysicsCategory.ExceptHero // 衝突判定は主人公側が満たす
    o.node.position = CGPointMake(o.parentScene.frame.width * 0.5, o.parentScene.frame.height * 0.5)
  }
}

protocol LMEnemyBody : LMNodeBodyApplicable {}
extension LMEnemyBody {
  func applyNodeBody() {
    let o = self as! LampMover
    o.node.physicsBody = SKPhysicsBody(rectangleOfSize: NormalSize)
    o.node.physicsBody!.categoryBitMask   = PhysicsCategory.Enemy
    o.node.physicsBody!.collisionBitMask  = PhysicsCategory.Hero
    o.node.name = String(o.id)
  }
}

protocol LMStrongEnemyBody : LMNodeBodyApplicable {}
extension LMStrongEnemyBody {
  func applyNodeBody() {
    let o = self as! LampMover
    o.node.physicsBody = SKPhysicsBody(rectangleOfSize: LargeSize)
    o.node.physicsBody!.categoryBitMask   = PhysicsCategory.StrongEnemy
    o.node.physicsBody!.collisionBitMask  = PhysicsCategory.Hero
    o.node.name = String(o.id)
  }
}

protocol LMEventBody : LMNodeBodyApplicable {}
extension LMEventBody {
  func applyNodeBody() {
    let o = self as! LampMover
    o.node.physicsBody = SKPhysicsBody(rectangleOfSize: NormalSize)
    o.node.physicsBody!.categoryBitMask   = PhysicsCategory.Person
    o.node.physicsBody!.collisionBitMask  = PhysicsCategory.Hero
    o.node.name = String(o.id)
  }
}

protocol LMNodeProtocol : LMNodeStyleApplicable, LMNodeBodyApplicable {}

// MARK: - Handle LifeSpan -
protocol LMCanHandleLifeSpan {
  func die()
}

protocol LMCanHandleHeroLifeSpan : LMCanHandleLifeSpan {}
extension LMCanHandleHeroLifeSpan {
  func die() {
    let o = self as! LampMover
    o.node.removeActionForKey("blink")
    o.node.runAction(SKAction.fadeAlphaTo(0.8, duration: 0.8))
    o.node.fillColor = UIColor(rgba: "#330000FF")
//    o.node.runAction(SKAction.colorizeWithColor(UIColor.blackColor(), colorBlendFactor: 1.0, duration: 0.8)) // for SKSpriteNode
  }
}

protocol LMCanHandleNPCLifeSpan : LMCanHandleLifeSpan {}
extension LMCanHandleNPCLifeSpan {
  func die() {
    let o = self as! LampMover
    o.node.removeFromParent()
  }
}

// MARK: - LMProtocol -
protocol LMProtocol : LMNodeProtocol, LMCanHandleLifeSpan {}
