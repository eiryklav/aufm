//
//  Appearance.swift
//  AufmGameModel
//
//  Created by moti on 2015/08/22.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import Foundation
import AufmObjCppWrapper

public class Appearance {
  
  public enum Style { case Undefined, Black, White }
  public var currentStyle: Style = .Undefined
  
  public struct Notifications {
    public static let StyleBlack = "APStyleBlack"
    public static let StyleWhite = "APStyleWhite"
  }
  
  public static let sharedInstance = Appearance()
  private init() {}
  
  public func update() {
    if cppCode.isNotified(kAppearanceBlack) {
      NSNotificationCenter.defaultCenter().postNotificationName(Notifications.StyleBlack, object: nil, userInfo: nil)
      currentStyle = .Black
    }
    if cppCode.isNotified(kAppearanceWhite) {
      NSNotificationCenter.defaultCenter().postNotificationName(Notifications.StyleWhite, object: nil, userInfo: nil)
      currentStyle = .White
    }
    if currentStyle == .Undefined {
      assertionFailure("Appearance更新前にAppearance〜をnotifyせよ")
    }
  }
}