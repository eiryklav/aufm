//
//  MapEdge.swift
//  AufmGameModel
//
//  Created by moti on 2015/08/22.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import Foundation

struct MapEdge : Hashable {
  let mapID: String
  let edgeID: Int
  var hashValue : Int {
    return (mapID + "-" + edgeID.description).hashValue
  }
  
  init(mapID: String, edgeID: Int) {
    self.mapID = mapID
    self.edgeID = edgeID
  }
}

func ==(lhs: MapEdge, rhs: MapEdge) -> Bool {
  return lhs.mapID == rhs.mapID && rhs.edgeID == rhs.edgeID
}