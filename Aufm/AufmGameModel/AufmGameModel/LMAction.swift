//
//  LMAction.swift
//  AufmGameModel
//
//  Created by moti on 2015/10/06.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import AufmSupport
import SpriteKit

protocol LMNodeAction {
  weak var owner: SKNode? { get }
  func runAction()
}


class LMNodeActionFollow : LMNodeAction {
  
  weak var owner:   SKNode?
  weak var target:  SKNode?
  
  let speed: Double
  
  init(owner: SKNode, target: SKNode, speed: Double) {
    self.owner = owner
    self.target = target
    self.speed = speed
  }
  
  func runAction() {
    let vector = (target!.position - owner!.position).normalizedPoint()
    owner!.position += vector * CGFloat(speed)
  }
  
}