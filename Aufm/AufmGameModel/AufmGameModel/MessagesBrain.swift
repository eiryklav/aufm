//
//  MessagesBrain.swift
//  AufmGameModel
//
//  Created by moti on 2015/08/22.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmObjCppWrapper

public class MessagesBrain {

  public struct Notifications {
    public static let HasBattleEnd        = "MBHasBattleEnd"
    public static let PlayerHasDied       = "MBPlayerHasDied"
    public static let MessagesUpdated     = "MBMessagesUpdated"
    public static let ShowAlertMessage    = "MBShowAlertMessage"
    public static let LampMoverGenerated  = "MBLampMoverGenerated"
    public static let LampMoverKilled     = "MBLampMoverKilled"
  }
  
  public struct Constants {
    public static let ID = "MBConstantID"
  }
  
  public var allMessages = Messages()
  var battleMessages = [MapEdge : [BattleMessages]]()
  var passedMapEdges = [MapEdge]()
  
  public init() {}
  
  public func update() {
    
    guard cppCode.isNotified(kLogUpdated) else { return }
    
    let buf = cppCode.flushLog()
    
    for var messageIndex = 0; messageIndex < buf.count; messageIndex++ {
      
      let type      = (buf[messageIndex] as! [AnyObject])[0] as! Int
      let stateType = (buf[messageIndex] as! [AnyObject])[1] as! Int
      let message   = (buf[messageIndex] as! [AnyObject])[2] as! String
      let timeStamp = (buf[messageIndex] as! [AnyObject])[3] as! Double
      let alert     = (buf[messageIndex] as! [AnyObject])[4] as! Bool
      let hidden    = (buf[messageIndex] as! [AnyObject])[5] as! Bool
      
      if(!hidden) {
        allMessages.messages.appendContentsOf([
          Message(type: type, stateType: stateType, message: message, timeStamp: timeStamp, alert: alert)
          ])
      }
      
      switch type {
        // TODO: SysBattleStart 〜 SysBattleEnd までの allMessages に対する idx を記録。その区間を個別に抽出できるようにする。
      case kSysBattleStart:
        
        let mapEdge = MapEdge(
          mapID:  cppCode.getCurrentMapID(),
          edgeID: cppCode.getCurrentEdgeID()
        )
        
        if battleMessages[mapEdge] == nil { battleMessages[mapEdge] = [] }
        // eg. message = "通常エンカウント 3体"
        battleMessages[mapEdge]!.append(BattleMessages(title: message))
        
        if passedMapEdges.indexOf(mapEdge) == nil { passedMapEdges.append(mapEdge) }
        
      case kSysBattleEnd:
        NSNotificationCenter.defaultCenter().postNotificationName(Notifications.HasBattleEnd, object: nil, userInfo: nil)
        allMessages.messages.appendContentsOf([Message(type: kHiddenFromLog)])
        
      case kSysEndConditionPlayerHasDied:
        NSNotificationCenter.defaultCenter().postNotificationName(Notifications.PlayerHasDied, object: nil, userInfo: nil)
        
      case kSysLampMoverGenerated:
        NSNotificationCenter.defaultCenter().postNotificationName(Notifications.LampMoverGenerated, object: nil, userInfo: [Constants.ID : message])
        
      case kSysLampMoverKilled:
        NSNotificationCenter.defaultCenter().postNotificationName(Notifications.LampMoverKilled, object: nil, userInfo: [Constants.ID : message])
        
      default:
        guard !hidden else { break /* switch type */ }
        stateTypeSwitchInDefault(stateType)
        
      } // switch type
    } // for messageIndex
    
    NSNotificationCenter.defaultCenter().postNotificationName(Notifications.MessagesUpdated, object: nil, userInfo: nil)
    
    if allMessages.messages.last!.alert {
      NSNotificationCenter.defaultCenter().postNotificationName(Notifications.ShowAlertMessage, object: nil, userInfo: ["AlertMessage":allMessages.messages.last!.message])
    }
  }
  
  private func stateTypeSwitchInDefault(stateType: Int) {
    switch stateType {
    case kBattleState:
      let mapEdge = MapEdge(
        mapID: cppCode.getCurrentMapID(),
        edgeID: cppCode.getCurrentEdgeID()
      )
      battleMessages[mapEdge]!.last!.messages.append(allMessages.messages.last!)
    default:
      break
    } // switch stateType
  }
  
}