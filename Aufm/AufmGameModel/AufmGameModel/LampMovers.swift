//
//  LampMovers.swift
//  AufmGameModel
//
//  Created by moti on 2015/08/29.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit
import SpriteKit

class LampMoverHero : LampMover, LMProtocol,
  LMBrilliantBlink, LMHeroBody, LMCanHandleHeroLifeSpan
  /* , LMFollowable*/ {
  override init(id: String, parentScene: SKScene) {
    super.init(id: id, parentScene: parentScene)
    applyNodeStyle()
    applyNodeBody()
  }
}

class LampMoverEnemy : LampMover, LMProtocol,
  LMWarningBlink, LMEnemyBody, LMCanHandleNPCLifeSpan
  /* , LMFollowable*/ {
  override init(id: String, parentScene: SKScene) {
    super.init(id: id, parentScene: parentScene)
    applyNodeStyle()
    applyNodeBody()
  }
}

class LampMoverStrongEnemy : LampMover, LMProtocol,
  LMCautionBlink, LMStrongEnemyBody, LMCanHandleNPCLifeSpan
  /* , LMFollowable*/ {
  override init(id: String, parentScene: SKScene) {
    super.init(id: id, parentScene: parentScene)
    applyNodeStyle()
    applyNodeBody()
  }
}

class LampMoverEvent : LampMover, LMProtocol,
  LMAttractive, LMEventBody, LMCanHandleNPCLifeSpan
  /* , LMFollowable*/ {
  override init(id: String, parentScene: SKScene) {
    super.init(id: id, parentScene: parentScene)
    applyNodeStyle()
    applyNodeBody()
  }
}