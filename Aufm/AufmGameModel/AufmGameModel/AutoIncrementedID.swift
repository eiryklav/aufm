//
//  AutoIncrementedID.swift
//  Aufm
//
//  Created by moti on 2015/08/01.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

class AutoIncrementedID {
  private var nextID: Int = 0
  func getNextID() -> Int { return nextID++ }
}