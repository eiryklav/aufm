//
//  Messages.swift
//  Aufm
//
//  Created by moti on 2015/06/11.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmObjCppWrapper

public class Messages {
  
  var lineNum: Int = 0
  let MaxNumOfMessages = 18
  
  public var attrStringsForView: [NSAttributedString] = []
  public var allAttrStrings: [NSAttributedString] = []
  
  var messages: [Message] = [] {
    willSet {
      attrStringsForView  = [NSAttributedString]()
      allAttrStrings      = [NSAttributedString]()
      for message in newValue {
        makeAttrStringFromMessage(message)
      }
      
      if attrStringsForView.count > MaxNumOfMessages {
        // println(attrStringsForView.count.description + "----------------")  // 何故MaxNumOf...で止まらず無限に増加するのかがわからない
        attrStringsForView.removeRange(0..<(attrStringsForView.count-MaxNumOfMessages))
      }
    }
  }
  
  /*
    All / Battle / Event
    Battle: BattleState
    Event:  ScenarioState
  */
  
  enum MessageClassification {
    case All, Battle, Event, Talk
  }
  
  func extractMessages(classification: MessageClassification) -> [NSAttributedString] {
    switch classification {
    case .All:
      return allAttrStrings
      
    case .Battle:
      // Battle
      assert(false, "has not been implemented yet")
      
    case .Event:
      var ret = [NSAttributedString]()
      for  var i=0; i<messages.count; i++ {
        if messages[i].stateType == kScenarioState {
          ret.append(NSAttributedString(string: messages[i].message))
        }
      }
      return ret
      
    case .Talk:
      return []
    }
  }
  
  // for debug
  func getAllPlainMessages() -> [String] {
    var ret: [String] = []
    for var i=0; i<messages.count; i++ {
      ret.append(messages[i].message)
    }
    return ret
  }
  
  // for debug
  func getPlainText() -> String {
    var ret = String()
    for var i=0; i<messages.count; i++ {
      ret += "\n" + messages[i].message
    }
    return ret
  }
  
}

extension Messages {
  
  private func makeAttrStringFromMessage(message: Message) {
    var attrStrForView: NSAttributedString!
    var attrStr:        NSAttributedString!
    
    attrStrForView = NSAttributedString(string: message.message)
    attrStr = NSAttributedString(string: message.message)
    
    // StateType分類
    switch message.stateType {
    case kBattleState:
      var str = "　　" + message.message
      str = str.stringByReplacingOccurrencesOfString("\n", withString: "\n　　")
      attrStrForView = NSAttributedString(string: str)
      attrStr = NSAttributedString(string: message.message)
      
    default:
      break
    }
    
    // MessageType分類
    switch message.type {
    case kEnterCity:
      attrStrForView = NSAttributedString(string: message.message,
        attributes: [NSForegroundColorAttributeName : UIColor.blueColor()])
      attrStr = NSAttributedString(string: message.message, attributes: [NSForegroundColorAttributeName : UIColor.blueColor()])
      
    case kEnterDungeon:
      attrStrForView = NSAttributedString(string: message.message,
        attributes: [NSForegroundColorAttributeName : UIColor.redColor()])
      attrStr = NSAttributedString(string: message.message, attributes: [NSForegroundColorAttributeName : UIColor.redColor()])
      
    case kSysEndConditionCompleteTargetForDefeat:
      attrStrForView = NSAttributedString(string: message.message, attributes: [NSForegroundColorAttributeName : UIColor.redColor()])
      attrStr = NSAttributedString(string: message.message, attributes: [NSForegroundColorAttributeName : UIColor.redColor()])
      
    default:
      break;
    }
    attrStringsForView.append(attrStrForView)
    
    if(message.type != kHiddenFromLog) {
      allAttrStrings.append(attrStr)
    }
    
  }
  
}