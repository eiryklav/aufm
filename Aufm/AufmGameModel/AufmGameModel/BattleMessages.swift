//
//  BattleMessages.swift
//  Aufm
//
//  Created by moti on 2015/07/23.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

class BattleMessages : Messages {
  var title: String
  
  init(title: String) {
    self.title = title
  }
}