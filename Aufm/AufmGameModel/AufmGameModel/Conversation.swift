//
//  Conversation.swift
//  Aufm
//
//  Created by moti on 2015/06/16.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

public class Conversation {
  let speaker:    String
  let text:       String
  let summary:    String
  let timeStamp:  Double
  let alert:      Bool
  
  init(speaker: String, text: String, summary: String, timeStamp: Double, alert: Bool) {
    self.speaker    = speaker
    self.text       = text
    self.summary    = summary
    self.timeStamp  = timeStamp
    self.alert      = alert
  }
}
