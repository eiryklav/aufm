//
//  Conversations.swift
//  Aufm
//
//  Created by moti on 2015/06/16.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmObjCppWrapper

public class Conversations {

  public var conversations: [Conversation]
  
  init() {
    conversations = []
  }
  
  public func conversationContentForView() -> String? {
    return conversations.last?.text
  }
  
}
