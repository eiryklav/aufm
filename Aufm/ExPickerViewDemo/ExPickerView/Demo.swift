//
//  ViewController.swift
//  ExPickerView
//
//  Created by moti on 2015/09/11.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit

class Data {
  let view: SelectionView
  let id:   String
  init(view: SelectionView, id: String) {
    self.view = view
    self.id   = id
  }
}

class ViewController: UIViewController {
  
  var data = [Data]()
  var pickerView: ExPickerView!

  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    
    view.backgroundColor = UIColor(red: 0.8, green: 0.6, blue: 0.6, alpha: 1.0)
    
    let view1 = SelectionView(frame: CGRectMake(0, 0, 300, 40))
    view1.text = "aaa"
    view1.image = UIImage(named: "poison")
    
    let view2 = SelectionView(frame: CGRectMake(0, 0, 300, 40))
    view2.text = "bbb"
    view2.image = UIImage(named: "medicine")
    
    let view3 = SelectionView(frame: CGRectMake(0, 0, 300, 40))
    view3.text = "ccc"
    view3.image = UIImage(named: "medicine")
    
    let view4 = SelectionView(frame: CGRectMake(0, 0, 300, 40))
    view4.text = "ddd"
    view4.image = UIImage(named: "poison")
    
    let view5 = SelectionView(frame: CGRectMake(0, 0, 300, 40))
    view5.text = "eee"
    view5.image = UIImage(named: "medicine")

    data += [Data(view: view1, id: "v1"), Data(view: view2, id: "v2"), Data(view: view3, id: "v3"), Data(view: view4, id: "v4"), Data(view: view5, id: "v5")]
    
    pickerView = ExPickerView(frame: CGRectMake(50, 300, 300, 125))
    pickerView.dataSource = self
    pickerView.delegate   = self
    pickerView.reload()
    view.addSubview(pickerView)
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }


}

extension ViewController : ExPickerViewDataSource {
  func numberOfRowsInExPickerView(exPickerView: ExPickerView) -> Int {
    return data.count
  }
}

extension ViewController : ExPickerViewDelegate {
  func exPickerView(exPickerView: ExPickerView, viewForRow row: Int) -> SelectionView {
    return data[row].view
  }
  
  func exPickerView(exPickerView: ExPickerView, didSelectRow row: Int) {
    print("select: \(data[row].id)")
  }
  
  func widthInExPickerView(exPickerView: ExPickerView) -> CGFloat {
    return pickerView.frame.width
  }
  
  func rowHeightInExPickerView(exPickerView: ExPickerView) -> CGFloat {
    return data[0].view.frame.height
  }
}

let demo = ViewController()