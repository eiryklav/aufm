//
//  ObjCWrapper.h
//  Aufm
//
//  Created by moti on 2015/04/26.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

#ifndef Aufm_ObjCWrapper_h
#define Aufm_ObjCWrapper_h

#import <Foundation/Foundation.h>

enum
{
  kDefaultState = 0,
  
  kTitleState,
  kGameWorldState,
  kEndingState,
  kBattleState,
  kCityState,
  kEnterDungeon,
  kDungeonState,
  kScenarioState
};

enum
{
  kNoInput,
  kPushOK,
  kPushCancel,
  kPushTalk,
  kPushOpenDrawer,
  kPause,
  kPushEsc
};

enum
{
  kHP,
  kMana,
  kLV,
  kATK,
  kDEF,
  kSP,
  kExp,
  kMoney,
  kEQP,
  kCondition
};

enum
{
  /*
   主テロップ: 状態変化, 仲間になる, 仲間(プレイヤー)の死亡
   副テロップ: ?
   */
  
  kAllMessages,
  kEnterCity,
  kGetRareItem,
  kAddServant,
  kPlayerSideDeath,
  kDefaultMessageType,
  kSysEndConditionEndOfScenarioSuccess,
  kSysEndConditionEndOfScenarioFailure,
  kSysEndConditionPlayerHasDied,
  kSysEndConditionTimeLimitExceeded,
  kSysEndConditionDeadEndInDungeon,
  kSysEndConditionDeadEndOfRoad,
  kSysEndConditionCompleteTargetForDefeat,
  kSysBattleStart,
  kSysBattleEnd,
  kSysNoEncount,
  kSysLampMoverGenerated,
  kSysLampMoverKilled,
  kFailedToAddServantBecauseOfOverflow,
  kFailedToAddServantBecauseOfInvitationRatio,
  
  // Swiftから使用される
  kHiddenFromLog,
};

enum
{
  kUserConversation,
  kScenarioConversation,
  kTelop,
};

enum BrainkeepType
{
  kBrainkeepChangeEquipment,
  kBrainkeepInvitation,
  kBrainkeepInformationGathering,
};

enum
{
  kLogUpdated,
  kConversationUpdated,
	kStateChanged,
  kMapChanged,
  kBezierPathChanged,
  kWaitingForNextEncount,
	kCompletedTargetForDefeat,
  kAllEncountProcessed,
  kAppearanceBlack,
  kAppearanceWhite,
};

extern NSInteger const ConditionPoison;
extern NSInteger const ConditionPalsy;
extern NSInteger const ConditionDead;

extern NSInteger const PlayerID;

@interface ObjCppWrapper : NSObject
-(void)open:(NSString*)worldName;
-(void)initGameWithPlayerName:(NSString*)playerName targetForDefeat:(NSArray*)tForD attitude:(NSInteger)atti invitation:(NSInteger)invi changeEqp:(NSInteger)chEqp lootingDrawers:(NSInteger)loot;
-(BOOL)updateWithTimeSinceLast:(double)timeSincleLast;
-(NSInteger)changeFromCppState: (int)state;
-(NSInteger)getCurrentState;
//-(NSInteger)changeFromCppMessageType: (int)msgType;
-(NSInteger)changeFromCppMessageType: (int)msgType;
-(NSMutableArray*)flushLog;
-(NSMutableArray*)flushConversation;
-(int)changeNotification:(int)notification;
-(void)notify:(NSInteger)notification;
-(BOOL)isNotified:(NSInteger)notification;
-(void)notifySpecialEncountWithID:(NSString*)lampMoverID;
-(double)getCurrentRealTime;
-(NSInteger)getCurrentAufmTimeDaySec;
-(double)getMaxElaspedRealTime;
-(NSInteger)getMaxElaspedAufmTimeDaySec;
-(BOOL)isPlayerStatusChanged:(NSInteger)type;
-(NSString*)getPlayerName;
-(NSDictionary*)getPlayerStatus;
-(NSInteger)getPlayersMoney;

-(NSMutableArray*)getEncountedPlayersList;

#define AUFM_GET_TIME_STAMPED_LIST_DEF(type)  \
-(NSMutableArray*)getTimeStamped ## type ## List: (NSInteger)playerID

AUFM_GET_TIME_STAMPED_LIST_DEF(LV);
AUFM_GET_TIME_STAMPED_LIST_DEF(Exp);
AUFM_GET_TIME_STAMPED_LIST_DEF(MaxHP);
AUFM_GET_TIME_STAMPED_LIST_DEF(MaxMana);
AUFM_GET_TIME_STAMPED_LIST_DEF(HP);
AUFM_GET_TIME_STAMPED_LIST_DEF(Mana);
AUFM_GET_TIME_STAMPED_LIST_DEF(ATK);
AUFM_GET_TIME_STAMPED_LIST_DEF(DEF);
AUFM_GET_TIME_STAMPED_LIST_DEF(SP);

AUFM_GET_TIME_STAMPED_LIST_DEF(Weapon);
AUFM_GET_TIME_STAMPED_LIST_DEF(Protector);
AUFM_GET_TIME_STAMPED_LIST_DEF(Accessory);

AUFM_GET_TIME_STAMPED_LIST_DEF(AIName);
AUFM_GET_TIME_STAMPED_LIST_DEF(Condition);
AUFM_GET_TIME_STAMPED_LIST_DEF(MasteredAction);
AUFM_GET_TIME_STAMPED_LIST_DEF(WayOfUseMoney);
AUFM_GET_TIME_STAMPED_LIST_DEF(Money);
AUFM_GET_TIME_STAMPED_LIST_DEF(Brainkeep);

#define AUFM_GET_PLAYER_STATUS_DEF(var) \
  -(NSInteger)getPlayerStatus ## var

AUFM_GET_PLAYER_STATUS_DEF(Exp);
AUFM_GET_PLAYER_STATUS_DEF(MaxHP);
AUFM_GET_PLAYER_STATUS_DEF(MaxMana);
AUFM_GET_PLAYER_STATUS_DEF(HP);
AUFM_GET_PLAYER_STATUS_DEF(Mana);
AUFM_GET_PLAYER_STATUS_DEF(LV);
AUFM_GET_PLAYER_STATUS_DEF(ATK);
AUFM_GET_PLAYER_STATUS_DEF(DEF);
AUFM_GET_PLAYER_STATUS_DEF(SP);
AUFM_GET_PLAYER_STATUS_DEF(Condition);

#define AUFM_GET_PLAYER_STATUS_EQP_DEF(var) \
-(NSString*)getPlayerStatus ## var

AUFM_GET_PLAYER_STATUS_EQP_DEF(Weapon);
AUFM_GET_PLAYER_STATUS_EQP_DEF(Protector);
AUFM_GET_PLAYER_STATUS_EQP_DEF(Accessory);

-(NSInteger)getCurrentMoney;

-(NSInteger)getNumOfPlayers;
-(NSInteger)getNumOfServants;

-(NSInteger)getServantIDWithIndex:(NSInteger)index;

-(NSString*)getServantNameWithIndex:(NSInteger)idx;

#define AUFM_GET_SERVANT_STATUS_DEF(var) \
  -(NSInteger)getServantStatus ## var ## WithIndex:(NSInteger)idx

AUFM_GET_SERVANT_STATUS_DEF(HP);
AUFM_GET_SERVANT_STATUS_DEF(Mana);
AUFM_GET_SERVANT_STATUS_DEF(MaxHP);
AUFM_GET_SERVANT_STATUS_DEF(MaxMana);
AUFM_GET_SERVANT_STATUS_DEF(LV);
AUFM_GET_SERVANT_STATUS_DEF(ATK);
AUFM_GET_SERVANT_STATUS_DEF(DEF);
AUFM_GET_SERVANT_STATUS_DEF(SP);
AUFM_GET_SERVANT_STATUS_DEF(Exp);
AUFM_GET_SERVANT_STATUS_DEF(Condition);

-(NSInteger)calcExpTillNextLevel:(NSInteger)level;

#define AUFM_GET_SERVANT_STATUS_EQP_DEF(var) \
  -(NSString*)getServantStatus ## var ## WithIndex:(NSInteger)idx

AUFM_GET_SERVANT_STATUS_EQP_DEF(Weapon);
AUFM_GET_SERVANT_STATUS_EQP_DEF(Protector);
AUFM_GET_SERVANT_STATUS_EQP_DEF(Accessory);

-(NSMutableArray*)getItemListInRuckSack;

-(NSMutableArray*)getCurrentTargetForDefeats;

-(NSMutableArray*)getCurrentBezierPath;
-(NSMutableArray*)getBezierPathWithMapID:(NSString*)mapID edgeID:(NSInteger)edgeID;
-(NSString*)getCurrentMapID;
-(NSInteger)getCurrentEdgeID;

-(void)loadCurrentEdgeTotalEnemiesNum;
-(double)getProgressRateDiffOfECOfEdge;

-(NSString*)getLampMoverTypeWithID:(NSString*)lampMoverID;
-(NSString*)getLampMoverMovingModeWithID:(NSString*)lampMoverID;
-(CGPoint)getLampMoverStartPositionWithID:(NSString*)lampMoverID;
-(double)getLampMoverSpeedWithID:(NSString*)lampMoverID;

-(NSString*)getLampMoverPlayerID;

/* ---------------------------------------------------------- */
# pragma mark - macro: IDType -

#define AUFM_CLASS_ID (NSString*)classID
#define AUFM_INTEGER_ID (NSInteger)integerID
/* ---------------------------------------------------------- */

/* ---------------------------------------------------------- */
# pragma mark - database record -

#define AUFM_GET_DATABASE_INTEGER_RECORD_DEF(recordTarget, recordName, IDType)  \
  -(NSInteger)get ## recordTarget ## recordName ## WithID:IDType

#define AUFM_GET_DATABASE_STRING_RECORD_DEF(recordTarget, recordName, IDType)  \
  -(NSString*)get ## recordTarget ## recordName ## WithID:IDType
/* ---------------------------------------------------------- */

// ChrStatus
AUFM_GET_DATABASE_STRING_RECORD_DEF(ChrStatus, Name, AUFM_INTEGER_ID);

// Item
AUFM_GET_DATABASE_STRING_RECORD_DEF(Item, Name, AUFM_INTEGER_ID);
AUFM_GET_DATABASE_STRING_RECORD_DEF(Item, Description, AUFM_INTEGER_ID);
AUFM_GET_DATABASE_STRING_RECORD_DEF(Item, Flavor, AUFM_INTEGER_ID);

// Action
AUFM_GET_DATABASE_STRING_RECORD_DEF(Action, Name, AUFM_CLASS_ID);
AUFM_GET_DATABASE_STRING_RECORD_DEF(Action, Description, AUFM_CLASS_ID);
AUFM_GET_DATABASE_STRING_RECORD_DEF(Action, Flavor, AUFM_CLASS_ID);
AUFM_GET_DATABASE_INTEGER_RECORD_DEF(Action, Mana, AUFM_CLASS_ID);

// Weapon
AUFM_GET_DATABASE_STRING_RECORD_DEF(Weapon, Name, AUFM_INTEGER_ID);

// Protector
AUFM_GET_DATABASE_STRING_RECORD_DEF(Protector, Name, AUFM_INTEGER_ID);

// Accessory
AUFM_GET_DATABASE_STRING_RECORD_DEF(Accessory, Name, AUFM_INTEGER_ID);


@end


#endif
