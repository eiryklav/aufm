//
//  MapDescriptionLoader.h
//  Aufm
//
//  Created by moti on 2015/06/23.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

#ifndef Aufm_MapDescriptionLoader_h
#define Aufm_MapDescriptionLoader_h

#import <Foundation/Foundation.h>

@interface MapDescriptionLoader : NSObject

@property (readwrite, strong) NSMutableArray* dicarray;

// world config
@property (readwrite, strong) NSString* fullName;
@property (readwrite, strong) NSString* shortName;
@property (readwrite, strong) NSString* subName;
@property (readwrite, strong) NSString* flavor;
//@property (readwrite, strong) NSMutableDictionary* areaButtonOrigin;
@property (readwrite, strong) NSMutableArray* areaButtonIDOriginArray;

// world map
@property (readwrite, strong) NSMutableDictionary* enemiesGraph;
@property (readwrite, strong) NSMutableArray* targetEnemies;

// city data
@property (readwrite, strong) NSString* cityName;
@property (readwrite, strong) NSString* cityFlavor;


//-(id)initWithJSONString:(NSString*)jsonString;
-(id)init;
-(void)loadWorldConfigFromJSON:(NSString*)jsonString;
-(void)loadEnemiesInEdgeFromJSON:(NSString*)jsonString;
-(void)loadCityDataFromJSON:(NSString*)jsonString;
@end

#endif
