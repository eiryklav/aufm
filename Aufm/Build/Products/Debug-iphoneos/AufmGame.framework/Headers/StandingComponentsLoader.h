//
//  StandingComponentLoader.h
//  Aufm
//
//  Created by moti on 2015/05/25.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

#ifndef Aufm_StandingComponentLoader_h
#define Aufm_StandingComponentLoader_h

#import <Foundation/Foundation.h>

@interface StandingComponentsLoader : NSObject

@property (readwrite, strong) NSMutableArray* dicarray;

-(id)initWithJSONString:(NSString*)jsonString;

@end

#endif
