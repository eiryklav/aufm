//
//  AufmSupport.h
//  AufmSupport
//
//  Created by moti on 2015/08/17.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for AufmSupport.
FOUNDATION_EXPORT double AufmSupportVersionNumber;

//! Project version string for AufmSupport.
FOUNDATION_EXPORT const unsigned char AufmSupportVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AufmSupport/PublicHeader.h>

#import "MapDescriptionLoader.h"
