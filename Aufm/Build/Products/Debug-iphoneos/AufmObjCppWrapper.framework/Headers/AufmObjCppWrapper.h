//
//  AufmObjCppWrapper.h
//  AufmObjCppWrapper
//
//  Created by moti on 2015/08/18.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for AufmObjCppWrapper.
FOUNDATION_EXPORT double AufmObjCppWrapperVersionNumber;

//! Project version string for AufmObjCppWrapper.
FOUNDATION_EXPORT const unsigned char AufmObjCppWrapperVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AufmObjCppWrapper/PublicHeader.h>


#import "ObjCppWrapper.h"