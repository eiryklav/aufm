//
//  UIImageExtension.swift
//  UpperTabButtonView
//
//  Created by moti on 2015/09/12.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit

extension UIImage {
  class func drawImage(fgImage: UIImage, inImage bgImage: UIImage, atPoint point: CGPoint) -> UIImage {
    UIGraphicsBeginImageContextWithOptions(bgImage.size, false, 0)
    bgImage.drawInRect(CGRectMake(0, 0, bgImage.size.width, bgImage.size.height))
    fgImage.drawInRect(CGRectMake(point.x, point.y, fgImage.size.width, fgImage.size.height), blendMode: .Overlay, alpha: 1.0)
    
    let ret = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return ret
  }
  
  class func imageFromView(view: UIView) -> UIImage {
    UIGraphicsBeginImageContextWithOptions(view.frame.size, false, UIScreen.mainScreen().scale)
    view.drawViewHierarchyInRect(view.bounds, afterScreenUpdates: true)
    let ret = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return ret
  }
  
  class func imageFromView(view: UIView, rect: CGRect) -> UIImage {
    UIGraphicsBeginImageContextWithOptions(rect.size, false, UIScreen.mainScreen().scale)
    let ctx = UIGraphicsGetCurrentContext()
    CGContextTranslateCTM(ctx, -rect.origin.x, -rect.origin.y)
    if(!view.drawViewHierarchyInRect(view.bounds, afterScreenUpdates: true)) {
      print("notice: 'renderInContext:' worked. (alternative)")
      view.layer.renderInContext(ctx!)
    }
    let ret = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return ret
  }
  
  func scaleAspectFitResize(size: CGSize) -> UIImage {
    let widthRatio = size.width / self.size.width
    let heightRatio = size.height / self.size.height
    let ratio = (widthRatio < heightRatio) ? widthRatio : heightRatio
    let resizedSize = CGSize(width: (self.size.width * ratio), height: (self.size.height * ratio))
    UIGraphicsBeginImageContext(resizedSize)
    drawInRect(CGRect(x: 0, y: 0, width: resizedSize.width, height: resizedSize.height))
    let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return resizedImage
  }
  
  // 比率だけ指定する場合
  func scaleAspectFitResize(ratio ratio: CGFloat) -> UIImage {
    let resizedSize = CGSize(width: Int(self.size.width * ratio), height: Int(self.size.height * ratio))
    UIGraphicsBeginImageContext(resizedSize)
    drawInRect(CGRect(x: 0, y: 0, width: resizedSize.width, height: resizedSize.height))
    let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return resizedImage
  }
}
