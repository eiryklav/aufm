//
//  UpperTabButtonView.swift
//  AufmData
//
//  Created by moti on 2015/09/06.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit

class TransparentTabButton : UIButton {
  let buttonTag: Int
  required init?(coder aDecoder: NSCoder) { fatalError() }
  init(frame: CGRect, buttonTag: Int) {
    self.buttonTag = buttonTag
    super.init(frame: frame)
  }
}

public class UpperTabButtonView : UIView {
  
  weak public var dataSource: UpperTabButtonViewDataSource?
  weak public var delegate:   UpperTabButtonViewDelegate?
  
  public var contentViewFrame:  CGRect!
  private var _selectedIndex:   Int = 0
  public var selectedIndex:     Int {
    get {
      return _selectedIndex
    }
  }
  public var hasCloseButton:  Bool  = true
  
  public var numberOfTabButtons: Int {
    get {
      guard let dataSource = dataSource else { print("dataSource is unset") ; return 0 }
      return dataSource.numberOfTabButtonsInUpperTabButtonView()
    }
  }
  
  required public init?(coder aDecoder: NSCoder) { fatalError() }
  
  override public init(frame: CGRect) {
    super.init(frame: frame)
    
    let barRawImage = UIImage(named: "upper-tab-view-menubar")!
      .scaleAspectFitResize(CGSizeMake(frame.width, 10000))
    contentViewFrame = CGRectMake(0, barRawImage.size.height, frame.width, frame.height - barRawImage.size.height)
  }

  public func reloadData() {
    
    for view in subviews { view.removeFromSuperview() }
    
    //
    closeButtonExtraWidth = hasCloseButton ? frame.width * 0.025 : 0
    
    // MARK: Menubar
    
    // MARK: Menubar Background
    let bgview = backgroundView() // weak にする必要ない
    addSubview(bgview!)
    
    let barRawImage = UIImage(named: "upper-tab-view-menubar")!
      .scaleAspectFitResize(CGSizeMake(bounds.width, 10000))
    
    let barRawBgImage = UIImage.imageFromView(
      bgview!,
      rect: CGRectMake(0, 0, barRawImage.size.width, barRawImage.size.height)
    )

    let barImage = UIImage.drawImage(
      barRawImage,
      inImage: barRawBgImage,
      atPoint: CGPointZero
    )
    
    barBackgroundImageView = UIImageView(image: barImage)
    bgview!.addSubview(barBackgroundImageView)
    
    
    // MARK: Menubar Foreground SelectImage
    
    let selectRawImage = UIImage(named: "upper-tab-view-select")!
      .scaleAspectFitResize(CGSizeMake(1000, barBackgroundImageView.frame.size.height))
    
    buttonWidth = selectRawImage.size.width
    buttonHeight = barBackgroundImageView.frame.height
    
    leftSpaceWidth = (frame.width - buttonWidth * CGFloat(numberOfTabButtons)) / 2 - closeButtonExtraWidth
    
    if leftSpaceWidth <= 0 {
      print("UpperTabButtonView - Warning: leftSpaceWidth <= 0\nSelection image's width should be shorter")
    }
    
    guard numberOfTabButtons > 0 else {
      print("UpperTabButtonView - Error: numberOfTabButtons = 0")
      return
    }
    
    selectBackgroundImageViews = []
    for index in 0..<numberOfTabButtons {
      
      // Create each selected button's background
      let selectRawBgImage = UIImage.imageFromView(
        barBackgroundImageView,
        rect: CGRectMake(
          leftSpaceWidth + buttonWidth * CGFloat(index),
          0,
          buttonWidth,
          buttonHeight
        )
      )
      
      let selectImage = UIImage.drawImage(
        selectRawImage,
        inImage: selectRawBgImage,
        atPoint: CGPointZero
      )
      
      let view = UIImageView(image: selectImage)
      view.frame.origin.x = leftSpaceWidth + buttonWidth * CGFloat(index)
      selectBackgroundImageViews.append(view)
      barBackgroundImageView.addSubview(view)
      view.hidden = true
      
      // Create each button's label
      let label = UILabel()
      label.font = UIFont(name: "LucidaGrande", size: 36.0)
      label.text = titleForTabButtonWithIndex(index)
      label.textColor = UIColor.whiteColor()
      label.shadowOffset = CGSizeMake(1, 1)
      label.shadowColor = UIColor.blackColor()
      label.frame = view.frame
      label.textAlignment = .Center
      barBackgroundImageView.addSubview(label)
      
      // Create each button's trannsparent button
      let button = TransparentTabButton(frame: view.frame, buttonTag: index)
      button.addTarget(self, action: "didPushTabButton:", forControlEvents: .TouchUpInside)
      addSubview(button)
    }
    
    // Set close button
    let closeSelectRawBgImage = UIImage.imageFromView(
      barBackgroundImageView,
      rect: CGRectMake(
        leftSpaceWidth + buttonWidth * CGFloat(numberOfTabButtons),
        0,
        buttonWidth,
        buttonHeight)
    )
    
    let closeSelectImage = UIImage.drawImage(
      selectRawImage,
      inImage: closeSelectRawBgImage,
      atPoint: CGPointZero
    )
    
    let closeView = UIImageView(image: closeSelectImage)
    closeView.frame.origin.x = leftSpaceWidth + buttonWidth * CGFloat(numberOfTabButtons)
    selectBackgroundImageViews.append(closeView)
    barBackgroundImageView.addSubview(closeView)
    closeView.hidden = true
    
    // Create each button's label
    let closeLabel = UILabel()
    closeLabel.font = UIFont(name: "LucidaGrande", size: 64.0)
    closeLabel.text = "    ×"
    closeLabel.textColor = UIColor.whiteColor()
    closeLabel.shadowOffset = CGSizeMake(2, 2)
    closeLabel.shadowColor = UIColor.blackColor()
    closeLabel.frame = closeView.frame
    closeLabel.textAlignment = .Left
    barBackgroundImageView.addSubview(closeLabel)
    
    // Create each button's trannsparent button
    let closeButton = TransparentTabButton(frame: closeView.frame, buttonTag: numberOfTabButtons)
    closeButton.addTarget(self, action: "didPushTabButton:", forControlEvents: .TouchUpInside)
    addSubview(closeButton)
    
    // Set contentView
    contentView = contentViewForIndex(selectedIndex)
    bgview!.addSubview(contentView)

    selectBackgroundImageViews[selectedIndex].hidden = false
    
  }
  
  func didPushTabButton(sender: TransparentTabButton) {
    if sender.buttonTag == numberOfTabButtons {
      // Pushed close button
      guard !closing else { return }
      closing = true
      selectBackgroundImageViews[numberOfTabButtons].hidden = false
      didPushCloseButtonForControlEvents(.TouchDown)
      NSTimer.scheduledTimerWithTimeInterval(0.25, target: NSBlockOperation(block: {
        self.selectBackgroundImageViews[self.numberOfTabButtons].hidden = true
        self.closing = false
        self.didPushCloseButtonForControlEvents(.TouchUpInside)
      }), selector: "main", userInfo: nil, repeats: false)
    }
    else {
      selectBackgroundImageViews[selectedIndex].hidden = true
      _selectedIndex = sender.buttonTag
      selectBackgroundImageViews[selectedIndex].hidden = false
      didPushTabButtonWithIndex(selectedIndex)
      contentView.removeFromSuperview()
      contentView = contentViewForIndex(selectedIndex)
      weak var bgview = backgroundView()
      bgview!.addSubview(contentView)
    }
  }
  
  public func contentViewForIndex(index: Int) -> UIView {
    return dataSource!.upperTabButtonView(self, contentViewForIndex: index)
  }
  
  public func titleForTabButtonWithIndex(index: Int) -> String {
    return dataSource!.upperTabButtonView(self, titleForTabButtonWithIndex: index)
  }
  
  public func didPushTabButtonWithIndex(index: Int) {
    delegate?.upperTabButtonView(self, didPushTabButtonWithIndex: index, forControlEvents: .TouchUpInside)
  }
  
  public func didPushCloseButtonForControlEvents(state: UIControlEvents) {
    delegate?.upperTabButtonView(self, didPushCloseButtonForControlEvents: state)
  }
  
  private func backgroundView() -> UIView? {
    return delegate?.backgroundViewInUpperTabButtonView(self)
  }
  
  // MARK: - private -
  private var barBackgroundImageView:     UIImageView!
  private var tabView:                    UIView!
  private var contentView:                UIView!
  private var buttonWidth:                CGFloat!
  private var buttonHeight:               CGFloat!
  private var leftSpaceWidth:             CGFloat!
  private var closeButtonExtraWidth:      CGFloat!
  private var selectBackgroundImageViews: [UIImageView]!
  private var closing:                    Bool = false
  
}

public protocol UpperTabButtonViewDataSource: class {
  func numberOfTabButtonsInUpperTabButtonView() -> Int
  func upperTabButtonView(upperTabButtonView: UpperTabButtonView, titleForTabButtonWithIndex index: Int) -> String
  func upperTabButtonView(upperTabButtonView: UpperTabButtonView, contentViewForIndex index: Int) -> UIView
}

public protocol UpperTabButtonViewDelegate: class {
  func upperTabButtonView(upperTabButtonView: UpperTabButtonView, didPushTabButtonWithIndex index: Int, forControlEvents controlEvents: UIControlEvents)
  func upperTabButtonView(upperTabButtonView: UpperTabButtonView, didPushCloseButtonForControlEvents controlEvents: UIControlEvents)
  func backgroundViewInUpperTabButtonView(upperTabButtonView: UpperTabButtonView) -> UIView
}