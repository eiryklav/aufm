//
//  TabDialogView
//  AufmData
//
//  Created by moti on 2015/09/06.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit

class TransparentTabButton : UIButton {
  let buttonTag: Int
  required init?(coder aDecoder: NSCoder) { fatalError() }
  init(frame: CGRect, buttonTag: Int) {
    self.buttonTag = buttonTag
    super.init(frame: frame)
  }
}

public class TabDialogView : UIView {
  
  weak public var dataSource: TabDialogViewDataSource?
  weak public var delegate:   TabDialogViewDelegate?
  
  public var barViewFrame: CGRect {
    get {
      return _barViewFrame
    }
  }
  
  public var contentViewFrame: CGRect {
    get {
      return _contentViewFrame
    }
  }
  
  public var subTitleViewFrame: CGRect {
    get {
      return _subTitleViewFrame
    }
  }
  
  public var selectedIndex: Int {
    get {
      return _selectedIndex
    }
  }
  
  public var numberOfTabButtons: Int {
    get {
      guard let dataSource = dataSource else { print("'dataSource' has not been set.") ; return 0 }
      return dataSource.numberOfTabButtonsInTabDialogView()
    }
  }
  
  required public init?(coder aDecoder: NSCoder) { fatalError() }
  
  override public init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  public func loadSubviews() {
    
    for view in subviews {
      view.removeFromSuperview()
    }
    
    guard numberOfTabButtons > 0 else {
      print("TabDialogView - Error: numberOfTabButtons = 0")
      return
    }
    
    unselectedTabs = []
    let unselImgView = UIImageView(image: UIImage(named: "unselected-tab0"))
    unselImgView.contentMode = .ScaleToFill
    scaleRatio = frame.width / unselImgView.image!.size.width
    unselImgView.frame = CGRectMake(0, 0, frame.width, unselImgView.frame.height * scaleRatio)
    unselImgView.hidden = true
    addSubview(unselImgView)
    unselectedTabs.append(unselImgView)
    
    for index in 1..<numberOfTabButtons {
      let view = UIImageView(image: UIImage(named: "unselected-tab\(index)"))
      view.contentMode = .ScaleToFill
      view.frame = CGRectMake(0, 0, frame.width, view.frame.height * scaleRatio)
      view.hidden = false
      addSubview(view)
      unselectedTabs.append(view)
    }
    
    selectedTabs = []
    for index in 0..<numberOfTabButtons {
      let view = UIImageView(image: UIImage(named: "selected-tab\(index)"))
      view.contentMode = .ScaleToFill
      view.frame = CGRectMake(0, 0, frame.width, view.frame.height * scaleRatio)
      view.hidden = index == 0 ? false : true
      addSubview(view)
      selectedTabs.append(view)
    }
    
    let buttonWidth: CGFloat  = 180 * scaleRatio
    let buttonHeight: CGFloat = 90 * scaleRatio
    
    transparentTabButtons = []
    for index in 0..<numberOfTabButtons {
      // button
      let buttonFrame = CGRectMake(CGFloat(index) * buttonWidth, 0, buttonWidth, buttonHeight)
      let button = TransparentTabButton(frame: buttonFrame, buttonTag: index)
      
      //button.backgroundColor = UIColor(white: 0.5 + CGFloat(index) * 0.1, alpha: 0.5) // for debug
      //button.setTitle("aaaaa", forState: .Normal) // for debug
      
      button.addTarget(self, action: "didPushTabButton:", forControlEvents: .TouchUpInside)
      addSubview(button)
      transparentTabButtons.append(button)
    }
    
    let unselHeight = selectedTabs.first!.frame.height * 0.39
    _contentViewFrame = CGRectMake(0, 0, frame.width, frame.height - selectedTabs.first!.frame.height)
    _subTitleViewFrame = CGRectMake(0, unselHeight, frame.width, frame.height - unselHeight - _contentViewFrame.height)
    
    contentViewBackgroundImageViews = []
    contentViewBackgroundImageViews.append(UIImageView(image: UIImage(named: "subview0")))
    let height = contentViewBackgroundImageViews.first!.image!.size.height * scaleRatio
    let yOffset = -height * 0.025
    contentViewBackgroundImageViews.first!.frame = CGRectMake(
      0,
      selectedTabs.first!.frame.height + yOffset,
      frame.width,
      height
    )
    addSubview(contentViewBackgroundImageViews.first!)
    
    hiddenWithIndex = Array<Bool>(count: numberOfTabButtons, repeatedValue: false)
    
    for index in 1..<numberOfTabButtons {
      contentViewBackgroundImageViews.append(UIImageView(image: UIImage(named: "subview\(index)")))
      contentViewBackgroundImageViews.last!.frame = contentViewBackgroundImageViews.first!.frame
      contentViewBackgroundImageViews.last!.hidden = true
      addSubview(contentViewBackgroundImageViews.last!)
    }
    
  }
  
  public func reload() {
    // reload subTitleViews
    for index in 0..<numberOfTabButtons {
      for view in selectedTabs[index].subviews { view.removeFromSuperview() }
      selectedTabs[index].addSubview(subTitleViewForIndex(index))
    }
    
    // reload contentViews
    for index in 0..<numberOfTabButtons {
      for view in contentViewBackgroundImageViews[index].subviews { view.removeFromSuperview() }
      // UIImageViewは通常 userInteractionEnabled = false であることに注意
      // ref) http://stackoverflow.com/questions/11427320/uiviews-touches-not-being-recognized
      contentViewBackgroundImageViews[index].userInteractionEnabled = true
      contentViewBackgroundImageViews[index].addSubview(contentViewForIndex(index))
    }
    
    selectIndex(0)
    
  }
  
  func selectIndex(selectedIndex: Int) {
    
    for index in 0..<numberOfTabButtons {
      if !hiddenWithIndex[index] {
        subTitleViewForIndex(index).hidden = true
        selectedTabs[index].hidden = true
        unselectedTabs[index].hidden = false
        contentViewBackgroundImageViews[index].hidden = true
        contentViewForIndex(index).hidden = true
      }
    }
    
    subTitleViewForIndex(selectedIndex).hidden = false
    selectedTabs[selectedIndex].hidden = false
    unselectedTabs[selectedIndex].hidden = true
    contentViewBackgroundImageViews[selectedIndex].hidden = false
    contentViewForIndex(selectedIndex).hidden = false
    
    didPushTabButtonWithIndex(selectedIndex)
  }
  
  func didPushTabButton(sender: TransparentTabButton) {
    selectIndex(sender.buttonTag)
  }
  
  public func subTitleViewForIndex(index: Int) -> UIView {
    return dataSource!.tabDialogView(self, subTitleViewForIndex: index)
  }
  
  public func contentViewForIndex(index: Int) -> UIView {
    return dataSource!.tabDialogView(self, contentViewForIndex: index)
  }
  
  public func didPushTabButtonWithIndex(index: Int) {
    delegate?.tabDialogView(self, didPushTabButtonWithIndex: index)
  }
  
  public func hideTabButtonWithIndex(index: Int) {
    guard index < numberOfTabButtons else { fatalError("TabDialogView: out of range") }
    hiddenWithIndex[index] = true
    selectedTabs[index].hidden = true
    unselectedTabs[index].hidden = true
    transparentTabButtons[index].hidden = true
  }
  
  public func showTabButtonWithIndex(index: Int) {
    guard index < numberOfTabButtons else { fatalError("TabDialogView: out of range") }
    hiddenWithIndex[index] = false
    selectedTabs[index].hidden = false
    unselectedTabs[index].hidden = false
    transparentTabButtons[index].hidden = false
  }
  
  // MARK: - private -
  private var _barViewFrame: CGRect!
  private var _contentViewFrame: CGRect!
  private var _subTitleViewFrame: CGRect!
  private var _selectedIndex: Int = 0

  private var hiddenWithIndex:                  [Bool]!
  private var contentViewBackgroundImageViews:  [UIImageView]!
  private var selectedTabs:                     [UIImageView]!
  private var unselectedTabs:                   [UIImageView]!
  private var transparentTabButtons:            [TransparentTabButton]!
  private var scaleRatio:                       CGFloat!
}

public protocol TabDialogViewDataSource: class {
  func numberOfTabButtonsInTabDialogView() -> Int
  func tabDialogView(tabDialogView: TabDialogView, subTitleViewForIndex index: Int) -> UIView
  func tabDialogView(tabDialogView: TabDialogView, contentViewForIndex index: Int) -> UIView
}

public protocol TabDialogViewDelegate: class {
  func tabDialogView(tabDialogView: TabDialogView, didPushTabButtonWithIndex index: Int)
}