//
//  TitleRootViewController.swift
//  AufmTitle
//
//  Created by moti on 2015/08/28.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import Foundation
import AufmSupport

public class TitleViewControllersContainer : ViewControllersContainerType {
  public weak var delegate: CanNavigateFromTitle?
  let titleViewController = TitleViewController()
  internal init(){}
}

extension TitleViewControllersContainer : HasStartPointOfViewController {
  public func startPointOfViewController() -> UIViewController {
    return titleViewController
  }
}

public protocol CanNavigateFromTitle: class {
  func instanceOfSettingViewController() -> UIViewController
}

public let titleContainer = TitleViewControllersContainer()