//
//  NaviMapFlavorTextView.swift
//  Aufm
//
//  Created by moti on 2015/07/20.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmCommonComponentViews

public class NaviMapFlavorTextView : UIView,
  BGStyleDependent {
  
  private var titleLabel:       UILabel
  private var descriptionLabel: UILabel
    
  required public init(coder: NSCoder) { fatalError("NSCoder x") }
  
  override public init(frame: CGRect) {
    
    titleLabel = UILabel(frame: CGRect(origin: CGPointMake(frame.width * 0.1, frame.height * 0.05), size: CGSizeZero))
    titleLabel.font = UIFont(name: "Baskerville", sizeType: .Large)
    descriptionLabel = UILabel(frame: CGRect(origin: CGPointMake(frame.width * 0.1, titleLabel.frame.maxY + frame.height * 0.025), size: CGSizeZero))
    descriptionLabel.font = UIFont(name: "Baskerville", sizeType: .SemiMedium)
    
    super.init(frame: frame)
    
  }
  
}
