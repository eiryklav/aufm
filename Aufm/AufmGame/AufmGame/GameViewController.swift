//
//  GameViewController.swift
//  Aufm
//
//  Created by moti on 2015/04/26.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import SpriteKit
import AVFoundation
import AufmObjCppWrapper
import AufmGameModel
import AufmVisualSoundEffect

class GameViewController: UIViewController {
  
  private   let kSCWidth  = UIScreen.mainScreen().bounds.width
  private   let kSCHeight = UIScreen.mainScreen().bounds.height
  
  private   var backgroundView:       BackgroundView!
  private   var naviMapView:          NaviMapView!
  private   var statusView:           StatusView!
  internal  var messagesView:         MessagesView!
  private   var conversationView:     ConversationView!
  private   var pauseButton:          PauseButton!
  private   var updateTimer:          NSTimer!
  
  
  private   let messagesBrain       = MessagesBrain()
  private   let conversationsBrain  = ConversationsBrain()
  
  let UpdateInterval  = 0.1
  
  override func viewDidLoad() {
    super.viewDidLoad()
    backgroundView = BackgroundView(frame: UIScreen.mainScreen().bounds)
    view.addSubview(backgroundView)
    
    // ----------------------------------------------------------------------
    // MARK: LEFT SIDE
    
    // --------------------------------------
    // NaviMapView
    
    naviMapView = NaviMapView(frame: CGRectMake(0, 0, kSCWidth * 0.25, kSCHeight * 0.35))
    view.addSubview(naviMapView)
    
    // --------------------------------------
    // StatusView
    
    statusView  = StatusView(frame: CGRectMake(0, naviMapView.frame.maxY, naviMapView.frame.width, kSCHeight - naviMapView.frame.height))
    statusView.delegate   = self
    statusView.dataSource = self
    view.addSubview(statusView)
    
    // ----------------------------------------------------------------------
    // MARK: RIGHT SIDE
    
    // --------------------------------------
    // MessagesView
    
    messagesView = MessagesView(frame: CGRectMake(naviMapView.frame.maxX, 0, kSCWidth - naviMapView.frame.maxX, kSCHeight))
    messagesView.dataSource = self
    messagesView.delegate   = self
    view.addSubview(messagesView)
    
    // --------------------------------------
    // MessagesView::CharacterView
    messagesView.characterView.delegate = self
    
    // --------------------------------------
    // ConversationView
    
    conversationView = ConversationView(frame: CGRectMake(naviMapView.frame.maxX, kSCHeight * 0.725, kSCWidth - naviMapView.frame.maxX, kSCHeight * 0.2))
    view.addSubview(conversationView)

    // --------------------------------------
    // Pause Button
    
    pauseButton = PauseButton()
    pauseButton.addTarget(self, action: "didPushPauseButton", forControlEvents: .TouchDown)
    view.addSubview(pauseButton)
    
    // ----------------------------------------------------------------------
    // MARK: NSNotificationCenter
    
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "reloadMessages", name: MessagesBrain.Notifications.MessagesUpdated, object: nil)
    
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "presentAlertMessage:", name: MessagesBrain.Notifications.ShowAlertMessage, object: nil)
        
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "presentConversation", name: ConversationsBrain.Notifications.ConversationUpdated, object: nil)

    
    // ----------------------------------------------------------------------
    // MARK: Start Game
    startGame()
    
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    
    func validateUpdateTimer() {
      updateTimer = NSTimer.scheduledTimerWithTimeInterval(UpdateInterval, target: self, selector: "onUpdate:", userInfo: nil, repeats: true)
    }
    
    validateUpdateTimer()
  }
  
  private func startGame() {
    var targetForBeats = [NSArray]()
    for item in SettingInfo.sharedInstance.targetOfBeatsWithEnemyID {
      let enemyID = item.0
      let num     = item.1
      targetForBeats.append(NSArray(array: [enemyID as AnyObject, num as AnyObject]))
    }
    
    cppCode.initGameWithPlayerName(
      SettingInfo.sharedInstance.name,
      targetForDefeat: targetForBeats,
      attitude:       SettingInfo.sharedInstance.attitude!,
      invitation:     SettingInfo.sharedInstance.invitation!,
      changeEqp:      SettingInfo.sharedInstance.changeEquipment!,
      lootingDrawers: SettingInfo.sharedInstance.lootingDrawers!
    )
    
    Audio.sharedInstance.playBGM("its-ending-and-die")
    messagesView.characterView.startAnimating()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  func onUpdate(timer: NSTimer) {
    
    guard cppCode.updateWithTimeSinceLast(UpdateInterval) else { return }
    
    printStateForDebug()
    statusView.reload()
    naviMapView.reload()  // call NaviMapBrain in NaviMapView
    messagesBrain.update()
    conversationsBrain.update()
  }
  
  private var pushedPauseButtonFlag = false
  func didPushPauseButton() {
    invalidateUpdateTimer()
    guard !pushedPauseButtonFlag else { return }
    pushedPauseButtonFlag = true
    NSTimer.scheduledTimerWithTimeInterval(0.25, target: NSBlockOperation(block: {  // クリック音再生時間
      self.pushedPauseButtonFlag = false
      gameContainer.optionWindowViewController.prevScreenImage = UIImage.imageFromView()
      self.navigationController?.pushViewController(gameContainer.optionWindowViewController, animated: false)
    }), selector: "main", userInfo: nil, repeats: false)
  }
  
  func invalidateUpdateTimer() {
    updateTimer.invalidate()
  }
  
}


// --------------------------------------------------------------------------------
// MARK: - Notification Observe Selectors -

extension GameViewController {
  
  func reloadMessages() { messagesView.reload() }
  
  func presentAlertMessage(n: NSNotification) { conversationView.presentAlertText(n.userInfo!["AlertMessage"]! as! String) }
  
  func presentConversation() { conversationView.presentText(conversationsBrain.allConversations.conversationContentForView()!) }
  
}


// --------------------------------------------------------------------------------
// MARK: - Delegate -


// MARK: StatusView Delegate

extension GameViewController : StatusViewDelegate {
  func didPushStatusButton() {
    invalidateUpdateTimer()
    navigationController?.pushViewController(gameContainer.delegate!.instanceOfStatusViewController(), animated: false)
  }
}

extension GameViewController : StatusViewDataSource {
  func numberOfColumn() -> Int {
    return cppCode.getNumOfPlayers()
  }
  
  func titleInStatusName(statusName: String, column: Int) -> String {
    if column == 0 {
      return playerStatus(statusName)
    }
    else {
      return servantStatus(statusName, index: column - 1)
    }
  }
  
  private func playerStatus(statusName: String) -> String {
    switch statusName {
      case "Name":  return cppCode.getPlayerName()
      case "LV":    return "\(cppCode.getPlayerStatusLV())"
      case "HP":    return "\(cppCode.getPlayerStatusHP())/\(cppCode.getPlayerStatusMaxHP())"
      case "Mana":  return "\(cppCode.getPlayerStatusMana())/\(cppCode.getPlayerStatusMaxMana())"
      case "ATK":   return "\(cppCode.getPlayerStatusATK())"
      case "DEF":   return "\(cppCode.getPlayerStatusDEF())"
      case "SP":    return "\(cppCode.getPlayerStatusSP())"
    default:
      fatalError("Undefined player status")
    }
  }
  
  private func servantStatus(statusName: String, index: Int) -> String {
    switch statusName {
      case "Name":  return "\(cppCode.getServantNameWithIndex(index))"
      case "LV":    return "\(cppCode.getServantStatusLVWithIndex(index))"
      case "HP":    return "\(cppCode.getServantStatusHPWithIndex(index))/\(cppCode.getServantStatusMaxHPWithIndex(index))"
      case "Mana":  return "\(cppCode.getServantStatusManaWithIndex(index))/\(cppCode.getServantStatusMaxManaWithIndex(index)))"
      case "ATK":   return "\(cppCode.getServantStatusATKWithIndex(index))"
      case "DEF":   return "\(cppCode.getServantStatusDEFWithIndex(index))"
      case "SP":    return "\(cppCode.getServantStatusSPWithIndex(index))"
    default:
      fatalError("Undefined servant status")
    }
  }
}


// MARK: MessagesView Delegate

extension GameViewController : MessagesViewDataSource {
  func attributedStringsForView() -> [NSAttributedString] {
    return messagesBrain.allMessages.attrStringsForView
  }
}

extension GameViewController : MessagesViewDelegate {
  func didTouchMessagesView() {
    // ログダーン！
  }
}


// MARK: CharacterView Delegate

extension GameViewController : CharacterAnimationViewDelegate {
  func didTouchCharacterAnimationView() {
    // conversationView.presentText(...)
  }
}

// --------------------------------------------------------------------------------
// MARK: - Debug -

extension GameViewController {
  private func printStateForDebug() {
    if cppCode.isNotified(kStateChanged) {
      let state = cppCode.getCurrentState()
      switch(state)
      {
      case kTitleState:     print("[TestApp]: OKをクリック")
      case kGameWorldState: print("[TestApp]: 道中")
      case kScenarioState:  print("[TestApp]: シナリオ")
      case kBattleState:    print("[TestApp]: 戦闘中")
      case kEndingState:    print("[TestApp]: エンディング")
      case kCityState:      print("[TestApp]: 街")
      case kDungeonState:   print("[TestApp]: ダンジョン")
      default:              assert(false)
      }
    }
  }
}
