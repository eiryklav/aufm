//
//  NaviMapView.swift
//  Aufm
//
//  Created by moti on 2015/06/12.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import SpriteKit
import AufmSupport
import AufmGameModel
import AufmObjCppWrapper
import AufmVisualSoundEffect


class NaviMapView : SKView {
  
  weak var delegate: NaviMapViewDelegate?
  private var skScene:        SKScene!
  private var naviMapBrain:   NaviMapBrain!
  
  required init?(coder aDecoder: NSCoder) { fatalError() }
  
  override init(frame: CGRect) {
    
    super.init(frame: frame)
    
    allowsTransparency = true
    userInteractionEnabled = true
    contentMode = .ScaleAspectFill
    clipsToBounds = true
    
    // bottomErasedBorder
    let bottomErasedBorder = CALayer()
    bottomErasedBorder.borderColor = UIColor(rgba: "#2c3e5088").CGColor
    bottomErasedBorder.borderWidth = 1
    bottomErasedBorder.frame = CGRectMake(0, 0, frame.width, frame.height + 1)
    layer.addSublayer(bottomErasedBorder)
    
    // Main scenen
    skScene = SKScene(size: frame.size)
    skScene.backgroundColor = UIColor(rgba: "#11111100")
    naviMapBrain = NaviMapBrain(parentScene: skScene)
    presentScene(skScene)
  }

  override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
    delegate?.didTouchNaviMapView()
  }

  func reload() {
    naviMapBrain.update()
  }
  
}

protocol NaviMapViewDelegate: class {
  func didTouchNaviMapView()
}