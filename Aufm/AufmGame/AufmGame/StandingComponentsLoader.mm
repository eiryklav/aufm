//
//  StandingComponentsLoader.mm
//  Aufm
//
//  Created by moti on 2015/05/25.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

#import "StandingComponentsLoader.h"
#import <UIKit/UIKit.h>
#import <string>
#import "../../../Lib/picojson.h"
#import "../../../Support/Misc.hpp"

@implementation StandingComponentsLoader : NSObject

-(id)initWithJSONString:(NSString*)jsonString
{
  if(self = [super init]) {
    picojson::value v;
    auto err = picojson::parse(v, [jsonString UTF8String]);
    if(!err.empty()) { AUFM_ASSERT(false, "Can't load jsonString in StandingComponentsLoader"); }
    auto& arr = v.get<picojson::array>();
    
    _dicarray = [NSMutableArray array];
    
    for(auto& item: arr) {
      auto& o = item.get<picojson::object>();
      
      auto pos = Misc::getIntPointFromJSON(o["Position"].get<picojson::array>());
      
      [_dicarray addObject:
       [NSMutableDictionary dictionaryWithObjectsAndKeys:
        [NSString stringWithUTF8String: o["ComponentName"].get<std::string>().c_str()],
          @"ComponentName",
        [NSNumber numberWithInt: static_cast<int>(o["OrderInLayer"].get<double>())],
          @"OrderInLayer",
        [NSValue valueWithCGPoint: CGPointMake(pos.real(), pos.imag())],
          @"Position",
        [NSNumber numberWithDouble: o["TimeInterval"].get<double>()],
          @"TimeInterval",
        [NSNumber numberWithInt: static_cast<int>(o["MaxCount"].get<double>())],
          @"MaxCount",
        nil]
       ];
      
      auto iter = o.find("AutoReverse");
      if(iter != o.end()) {
        [[_dicarray lastObject] setObject: [NSNumber numberWithBool: iter->second.get<bool>()] forKey: @"AutoReverse"];
      }
      
      iter = o.find("ScheduledWithTimeInterval");
      if(iter != o.end()) {
        [[_dicarray lastObject] setObject: [NSNumber numberWithDouble: iter->second.get<double>()] forKey: @"ScheduledWithTimeInterval"];
      }
      
      iter = o.find("ScheduledWithTimeIntervalFineAdjustment");
      if(iter != o.end()) {
        [[_dicarray lastObject] setObject: [NSNumber numberWithDouble: iter->second.get<double>()] forKey: @"ScheduledWithTimeIntervalFineAdjustment"];
      }
    }
  }
  return self;
}
@end