//
//  OptionWindowController.swift
//  Aufm
//
//  Created by moti on 2015/08/05.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmCommonComponentViews

class OptionWindowViewController : UIViewController {
  
  var prevScreenImage:          UIImage?
  private var backgroundView:   CoveredBackground!
  private var optionWindowView: OptionWindowView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let kSCWidth  = UIScreen.mainScreen().bounds.width
    let kSCHeight = UIScreen.mainScreen().bounds.height
    optionWindowView = OptionWindowView(frame: CGRectMake(kSCWidth * 0.225, kSCHeight * 0.175, kSCWidth * 0.55, kSCHeight * 0.65))
    optionWindowView.layer.zPosition = 2
    view.addSubview(optionWindowView)
  }
    
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    guard let image = prevScreenImage?.lightedImageWithColor() else { fatalError("Unset previous screen image") }
    if backgroundView != nil {
      backgroundView.image = image
    }
    else {
      backgroundView = CoveredBackground(darkenImage: image, popWindowFrame: optionWindowView.frame)
      backgroundView.layer.zPosition = 1
      backgroundView.delegate = self
      view.addSubview(backgroundView)
    }
  }
  
  override func viewDidDisappear(animated: Bool) {
    super.viewDidDisappear(animated)
    prevScreenImage = nil
  }
  
}

extension OptionWindowViewController : CoveredBackgroundDelegate {
  func didTouch() {
    prevScreenImage = nil
    navigationController?.popViewControllerAnimated(false)
  }
}