//
//  OptionWindow.swift
//  Aufm
//
//  Created by moti on 2015/08/05.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import NYSegmentedControl
import AufmVisualSoundEffect

class OptionWindowView : UIView {
  
  var breakAwayButton:  BreakAwayButton!
  var interruptButton:  UIButton! // not instance of BTProtocol
  
  var observationLabel: UILabel!
  var BGMLabel:         UILabel!
  
  var observationSegmentedControl: NYSegmentedControl!
  var BGMSegmentedControl: NYSegmentedControl!
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    backgroundColor = UIColor.clearColor() //UIColor(rgba: "#dadfe1aa")
    let blurEffect = UIBlurEffect(style: .Light)
    let visualEffectView = UIVisualEffectView(effect: blurEffect)
    visualEffectView.frame = bounds
    visualEffectView.layer.cornerRadius = 4.0
    visualEffectView.clipsToBounds = true
    addSubview(visualEffectView)
    /*
    let gradient = CAGradientLayer()
    gradient.colors = [UIColor(rgba: "#898c90ff").CGColor, UIColor(rgba: "#dbddeeff").CGColor]
    gradient.colors = [UIColor(rgba: "#2b2b2bff").CGColor, UIColor(rgba: "#4a4a4aff").CGColor]
    gradient.frame = bounds
    gradient.cornerRadius = 4.0
    layer.insertSublayer(gradient, above: layer)
    */
    
    // MARK: BreakAwayButton
    
    breakAwayButton = BreakAwayButton(frame: CGRectMake(frame.width * 0.08, frame.height * 0.125, frame.width * 0.84, frame.height * 0.15))
    breakAwayButton.addTarget(self, action: "didPushBreakAwayButton", forControlEvents: .TouchUpInside)
    addSubview(breakAwayButton)

    
    // Normal
    var gradient = CAGradientLayer()
    gradient.colors = [UIColor(rgba: "#FF5E3AFF").CGColor, UIColor(rgba: "#FF2A68FF").CGColor]
    gradient.frame = breakAwayButton.bounds
    gradient.cornerRadius = 4.0
    
    let borderLayer = CALayer()
    borderLayer.cornerRadius = 4.0
    borderLayer.borderColor = UIColor.blackColor().CGColor
    borderLayer.frame = breakAwayButton.bounds
    borderLayer.borderWidth = 1.0
    
    gradient.insertSublayer(borderLayer, above: layer)
    
    // Highlighted
    gradient = CAGradientLayer()
    gradient.colors = [UIColor(rgba: "#DF3E1AFF").CGColor, UIColor(rgba: "#DF0A48FF").CGColor]
    gradient.frame = breakAwayButton.bounds
    gradient.cornerRadius = 4.0

    breakAwayButton.setImage(UIImage.imageFromLayer(gradient), forState: .Highlighted)
    breakAwayButton.addTarget(self, action: "didPushBreakAwayButton", forControlEvents: .TouchUpInside)
    addSubview(breakAwayButton)
    
    interruptButton = UIButton(frame: CGRectOffset(breakAwayButton.frame, 0, frame.height * 0.17))
    interruptButton.setTitle("中断", forState: .Normal)
    interruptButton.titleLabel?.font = UIFont(sizeType: .Medium)
    interruptButton.layer.borderColor = UIColor.whiteStyleBorderColor().CGColor
    interruptButton.layer.cornerRadius = 2.0
    //    interruptButton.backgroundColor = UIColor(rgba: "#7B6F6FFF")
    gradient = CAGradientLayer()
    gradient.colors = [UIColor(rgba: "#8E8E93FF").CGColor, UIColor(rgba: "#898C90FF").CGColor]
    gradient.frame = breakAwayButton.bounds
    gradient.cornerRadius = 4.0
    
    interruptButton.layer.insertSublayer(gradient, atIndex: 0)
    interruptButton.addTarget(self, action: "didPushInterruptButton", forControlEvents: .TouchUpInside)
    addSubview(interruptButton)
    
    observationLabel = UILabel(frame: CGRectMake(interruptButton.frame.minX, interruptButton.frame.maxY + frame.height * 0.05, frame.width * 0.4, frame.height * 0.15))
    observationLabel.text = "視覚観測"
    observationLabel.font = UIFont(name: "AvenirNext-Medium", sizeType: .Medium)
    observationLabel.textColor = UIColor.whiteColor()
    addSubview(observationLabel)
    
    BGMLabel = UILabel(frame: CGRectOffset(observationLabel.frame, 0, frame.height * 0.2))
    BGMLabel.text = "BGM"
    BGMLabel.font = UIFont(name: "AvenirNext-Medium", sizeType: .Medium)
    BGMLabel.textColor = UIColor.whiteColor()
    addSubview(BGMLabel)
    
    observationSegmentedControl = NYSegmentedControl(items: ["On", "Off"])
    observationSegmentedControl.frame = CGRectOffset(observationLabel.frame, frame.width * 0.425, 0)
    observationSegmentedControl.addTarget(self, action: "didSelectObservationSegment", forControlEvents: .ValueChanged)
    
    observationSegmentedControl.borderWidth = 2.0
    observationSegmentedControl.borderColor = UIColor(white: 0.15, alpha: 1.0)
    observationSegmentedControl.titleFont = UIFont(name: "AvenirNext-Medium", sizeType: .WideSmall)
    observationSegmentedControl.titleTextColor = UIColor(white: 0.3, alpha: 1.0)
    observationSegmentedControl.selectedTitleFont = UIFont(name: "AvenirNext-DemiBold", sizeType: .WideSmall)
    observationSegmentedControl.selectedTitleTextColor = UIColor(white: 0.2, alpha: 1.0)
    observationSegmentedControl.drawsGradientBackground = true
    observationSegmentedControl.gradientTopColor = UIColor(white: 0.17, alpha: 1.0)
    observationSegmentedControl.gradientBottomColor = UIColor(white: 0.05, alpha: 1.0)
    observationSegmentedControl.segmentIndicatorAnimationDuration = 0.2
    observationSegmentedControl.segmentIndicatorInset = 3.0
    observationSegmentedControl.drawsSegmentIndicatorGradientBackground = true
    observationSegmentedControl.segmentIndicatorGradientTopColor = UIColor(red: 0.75, green: 0.9, blue: 0.4, alpha: 1.0)
    observationSegmentedControl.segmentIndicatorGradientBottomColor = UIColor(red: 0.47, green: 0.72, blue: 0.29, alpha: 1.0)
    observationSegmentedControl.segmentIndicatorBorderWidth = 0.0
    observationSegmentedControl.cornerRadius = CGRectGetHeight(observationSegmentedControl.frame) / 2.0


    addSubview(observationSegmentedControl)
    
    BGMSegmentedControl = NYSegmentedControl(items: ["On", "Off"])
    BGMSegmentedControl.frame = CGRectMake(observationSegmentedControl.frame.minX, BGMLabel.frame.minY, observationSegmentedControl.frame.width, observationSegmentedControl.frame.height)
    BGMSegmentedControl.addTarget(self, action: "didSelectBGMSegment", forControlEvents: .ValueChanged)
    
    BGMSegmentedControl.borderWidth = 2.0
    BGMSegmentedControl.borderColor = UIColor(white: 0.15, alpha: 1.0)
    BGMSegmentedControl.titleFont = UIFont(name: "AvenirNext-Medium", sizeType: .WideSmall)
    BGMSegmentedControl.titleTextColor = UIColor(white: 0.3, alpha: 1.0)
    BGMSegmentedControl.selectedTitleFont = UIFont(name: "AvenirNext-DemiBold", sizeType: .WideSmall)
    BGMSegmentedControl.selectedTitleTextColor = UIColor(white: 0.2, alpha: 1.0)
    BGMSegmentedControl.drawsGradientBackground = true
    BGMSegmentedControl.gradientTopColor = UIColor(white: 0.17, alpha: 1.0)
    BGMSegmentedControl.gradientBottomColor = UIColor(white: 0.05, alpha: 1.0)
    BGMSegmentedControl.segmentIndicatorAnimationDuration = 0.2
    BGMSegmentedControl.segmentIndicatorInset = 3.0
    BGMSegmentedControl.drawsSegmentIndicatorGradientBackground = true
    BGMSegmentedControl.segmentIndicatorGradientTopColor = UIColor(red: 0.75, green: 0.9, blue: 0.4, alpha: 1.0)
    BGMSegmentedControl.segmentIndicatorGradientBottomColor = UIColor(red: 0.47, green: 0.72, blue: 0.29, alpha: 1.0)
    BGMSegmentedControl.segmentIndicatorBorderWidth = 0.0
    BGMSegmentedControl.cornerRadius = CGRectGetHeight(BGMSegmentedControl.frame) / 2.0
    
    addSubview(BGMSegmentedControl)
    
  }
  
  func didPushBreakAwayButton() {
    // 離脱
    Audio.sharedInstance.playSE("click")
  }
  
  func didPushInterruptButton() {
    // 中断
    Audio.sharedInstance.playSE("click")
  }
  
  func didSelectObservationSegment() {
    if observationSegmentedControl.selectedSegmentIndex == 0 {
      gameContainer.gameViewController.messagesView.characterView.hidden = false
      // userInteractionEnabledよりhitTestが優先されるので、hitTestのフラグとして利用する
      gameContainer.gameViewController.messagesView.characterView.userInteractionEnabled = true
    }
    else {
      gameContainer.gameViewController.messagesView.characterView.hidden = true
      gameContainer.gameViewController.messagesView.characterView.userInteractionEnabled = false
    }
  }
  
  func didSelectBGMSegment() {
    if BGMSegmentedControl.selectedSegmentIndex == 0 {
      Audio.sharedInstance.onBGM()
    }
    else {
      Audio.sharedInstance.offBGM()
    }
  }

  required init(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
