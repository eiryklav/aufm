//
//  NaviMapDescriptionViewController.swift
//  Aufm
//
//  Created by moti on 2015/07/20.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmSupport
import AufmVisualSoundEffect

class NaviMapDescriptionViewController : UIViewController {
  
  private var backgroundImageView:  UIImageView!
  private var flavorTextView:       NaviMapFlavorTextView!
  private var backButton:           UIButton! // AufmCommonComponentViewsとは別物
  private var underMapView:         UIView!
  private var mapImageView:         UIImageView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    backgroundImageView = UIImageView(image: UIImage(named: "background")!)
    backgroundImageView.frame = UIScreen.mainScreen().bounds
    view.addSubview(backgroundImageView)
    
    // Left side
    flavorTextView = NaviMapFlavorTextView(frame: CGRectMake(Screen.size.width * 0.025, Screen.size.height * 0.025, Screen.size.width * 0.4, Screen.size.height * 0.8))
    view.addSubview(flavorTextView)
    
    backButton = UIButton(frame: CGRect(origin: CGPointZero, size: CGSizeMake(Screen.size.width * 0.3, Screen.size.height * 0.10)))
    backButton.frame.origin = CGPointMake(flavorTextView.frame.minX + centerOf(selfLength: backButton.frame.width, superLength: flavorTextView.frame.width), flavorTextView.frame.maxY + Screen.size.height * 0.015)
    backButton.setTitle("Back", forState: .Normal)
    backButton.titleLabel?.font = UIFont(name: "Baskerville", sizeType: .Large)
    backButton.addTarget(self, action: "didPushBackButton", forControlEvents: .TouchUpInside)
    backButton.layer.borderColor      = UIColor.whiteStyleBorderColor().CGColor
    backButton.layer.backgroundColor  = UIColor.whiteStyleBackgroundColor().CGColor
    view.addSubview(backButton)
    
    underMapView = UIView(frame: CGRectMake(flavorTextView.frame.width + Screen.size.width * 0.05, flavorTextView.frame.minY, Screen.size.width * 0.55, Screen.size.height * 0.9))
    underMapView.backgroundColor = UIColor.whiteStyleBackgroundColor()
    underMapView.layer.borderColor = UIColor.whiteStyleBorderColor().CGColor
    underMapView.layer.borderWidth = 1.0
    view.addSubview(underMapView)
    
    mapImageView = UIImageView(frame: CGRectOffset(flavorTextView.frame, Screen.size.width * 0.5, 0))
    mapImageView.image = UIImage(named: "map-all")
    view.addSubview(mapImageView)
  }
  
  func didPushBackButton() {
    Audio.sharedInstance.playBGM("click")
    navigationController?.popToRootViewControllerAnimated(false)
  }
  
}
