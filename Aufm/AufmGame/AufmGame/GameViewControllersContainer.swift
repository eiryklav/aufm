//
//  GameViewControllersContainer.swift
//  AufmGame
//
//  Created by moti on 2015/08/28.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmSupport

public class GameViewControllersContainer : ViewControllersContainerType {
  
  public weak var delegate: CanNavigateFromGame?
  
  let gameViewController                = GameViewController()
  let naviMapDescriptionViewController  = NaviMapDescriptionViewController()
  let optionWindowViewController        = OptionWindowViewController()
  
  init() {}
}

extension GameViewControllersContainer : HasStartPointOfViewController {
  public func startPointOfViewController() -> UIViewController {
    return gameViewController
  }
}

public protocol CanNavigateFromGame: class {
  func instanceOfStatusViewController() -> UIViewController
}

public let gameContainer = GameViewControllersContainer()