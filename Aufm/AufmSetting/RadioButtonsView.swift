//
//  RadioButtonGroup.swift
//  AufmSetting
//
//  Created by moti on 2015/08/22.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

class RadioButtonsView : UIView {
  
  weak var delegate:      RadioButtonsViewDelegate?
  
  private var origin:     CGPoint
  private var classID:    String
  private var buttonSize: CGSize
  private var space:      CGFloat
  
  private var buttons:    [RadioButton]
  private var checkFirstTime: Bool
  
  init(origin: CGPoint, classID: String, buttonSize: CGSize, space: CGFloat) {
    self.origin     = origin
    self.classID    = classID
    self.buttonSize = buttonSize
    self.space      = space
    self.buttons    = []
    checkFirstTime  = false
    super.init(frame: CGRect(origin: origin, size: CGSizeZero))
  }
  
  required init?(coder aDecoder: NSCoder) { fatalError("NSCoder x") }
  
  func appendRadioButton(uniqueID: String) {
    buttons.append(RadioButton(frame: CGRectMake(CGFloat(buttons.count) * (buttonSize.width + space), 0, buttonSize.width, buttonSize.height), uniqueID: uniqueID))
    addSubview(buttons.last!)
    buttons.last!.addTarget(self, action: "didCheckRadioButton", forControlEvents: .TouchDown)
    frame = CGRect(origin: frame.origin, size: CGSizeMake(buttonSize.width * CGFloat(buttons.count) + space * CGFloat(buttons.count - 1), buttonSize.height))
  }
  
  func buttonWithUniqueID(uniqueID: String) -> RadioButton? {
    for button in buttons {
      if button.uniqueID == uniqueID { return button }
    }
    return nil
  }
  
  func didCheckRadioButton(radioButton: RadioButton) {
    if !checkFirstTime { delegate?.didCheckFirstTimeWithClassID(classID) }
    checkFirstTime = true
    for button in buttons {
      if button.uniqueID == radioButton.uniqueID { button.didCheck() }
      else { button.didUncheck() }
    }
  }
}

protocol RadioButtonsViewDelegate: class {
  func didCheckFirstTimeWithClassID(classID: String)
}