//
//  SlideShowImageView.swift
//  AufmSetting
//
//  Created by moti on 2015/08/23.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

class SlideShowView : UIView {
  
  private var backgroundImageView = [UIImageView]()
  private var currentIndex: Int
  var pictures: [String]! {
    didSet {
      currentIndex = 0
    }
  }
  var timer: NSTimer!
  
  init(pictures: [String]) {
    assert(pictures.count > 0)
    self.currentIndex = 0
    super.init(frame: UIScreen.mainScreen().bounds)
    self.backgroundImageView[0].image = UIImage(named: pictures[0])
    self.backgroundImageView[1].alpha = 0
    self.pictures = pictures
    reload()
  }
  
  required init?(coder: NSCoder) { fatalError("NSCoder x") }
  
  private func transition() {
    let nextIndex = (currentIndex + 1) % pictures.count
    backgroundImageView[nextIndex].image = UIImage(named: pictures[nextIndex])
    UIView.animateWithDuration(0.5, animations: {
      self.backgroundImageView[self.currentIndex].alpha = 0.0
      self.backgroundImageView[nextIndex].alpha = 1.0
    })
  }
  
  func reload() {
    if timer != nil { timer.invalidate() }
    if pictures.count > 0 {
      timer = NSTimer.scheduledTimerWithTimeInterval(3.0, target: NSBlockOperation(block: {
        self.transition()
        self.currentIndex++
        if self.currentIndex >= self.pictures.count { self.currentIndex = 0 }
      }), selector: "main", userInfo: nil, repeats: true)
    }
  }
}
