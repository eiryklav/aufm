//
//  BGProtocol.swift
//  AufmSetting
//
//  Created by moti on 2015/08/26.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmVisualSoundEffect

public protocol BGProtocol {
  func applyBackground()
}

public protocol BGNone : BGProtocol {}
extension BGNone {
  public func applyBackground() {
    (self as! UIView).backgroundColor = UIColor.clearColor()
  }
}

public protocol BGStyleDependent : BGProtocol {}
extension BGStyleDependent {
  public func styleBlack() {
    let o = self as! UIView
    o.layer.borderColor     = UIColor.blackStyleBorderColor().CGColor
    o.layer.backgroundColor = UIColor.blackStyleBackgroundColor().CGColor
  }
  public func styleWhite() {
    let o = self as! UIView
    o.layer.borderColor     = UIColor.whiteStyleBorderColor().CGColor
    o.layer.backgroundColor = UIColor.whiteStyleBackgroundColor().CGColor
  }
  public func applyBackground() {
    let o = self as! UIView
    o.layer.borderWidth = 1.0
    o.opaque = true
    NSNotificationCenter.defaultCenter().addObserver(o, selector: "styleBlack", name: Appearance.Notifications.StyleBlack, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(o, selector: "styleWhite", name: Appearance.Notifications.StyleWhite, object: nil)
  }
}

public protocol BGModesty : BGProtocol {}
extension BGModesty {
  public func applyBackground() {
    let o = self as! UIView
    o.backgroundColor   = UIColor(rgba: "#ccccccff")
    o.layer.borderColor = UIColor(rgba: "#888888ff").CGColor
  }
}

public protocol BGShadow : BGProtocol {}
extension BGShadow {
  public func applyBackground() {
    let o = self as! UIView
    o.layer.setShadowWithOffset(o.frame.size)
  }
}

public protocol BGSwitchableModesty : BGProtocol {
  var selected: Bool { get set }
}
extension BGSwitchableModesty {
  
  public func applyBackground() {
    let o = self as! UIView
    o.backgroundColor   = UIColor(rgba: "#888888FF")
    o.layer.borderColor = UIColor(rgba: "#333333FF").CGColor
  }
  
  public mutating func switchBackground() {
    let o = self as! UIView
    if !selected {
      selected = true
      o.backgroundColor   = UIColor(rgba: "#888888FF")
      o.layer.borderColor = UIColor(rgba: "#333333FF").CGColor
    }
    else {
      selected = false
      o.backgroundColor = UIColor(rgba: "#D8D8D8FF")
    }
  }
}