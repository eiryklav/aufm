//
//  SoundEffectProtocol.swift
//  AufmSetting
//
//  Created by moti on 2015/08/26.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmVisualSoundEffect

// ------------------------------------------------------------------------------
// MARK: - PlaySoundEffectDelegator -

class PlaySoundEffectDelegator : NSObject {
  override init() { super.init() }
  func SEClick() { Audio.sharedInstance.playSE("click")        }
  func SEPlay()  { Audio.sharedInstance.playSE("play")         }
  func SEAufm()  { Audio.sharedInstance.playSE("aufm-button")  }
}

let playSoundEffectDelegator = PlaySoundEffectDelegator()


// ------------------------------------------------------------------------------
// MARK: - Touch Button -

public protocol SEButtonTouchProtocol {
  func applySEButtonTouchForControlEvents(controlEvents: UIControlEvents)
}

public protocol SEButtonTouchNone : SEButtonTouchProtocol {}
extension SEButtonTouchNone {
  public func applySEButtonTouchForControlEvents(controlEvents: UIControlEvents) {
    (self as! UIButton).addTarget(playSoundEffectDelegator, action: "SEClick", forControlEvents: controlEvents)
  }
}

public protocol SEButtonTouchClick : SEButtonTouchProtocol {}
extension SEButtonTouchClick {
  public func applySEButtonTouchForControlEvents(controlEvents: UIControlEvents) {
    (self as! UIButton).addTarget(playSoundEffectDelegator, action: "SEClick", forControlEvents: controlEvents)
  }
}

public protocol SEButtonTouchPlay : SEButtonTouchProtocol {}
extension SEButtonTouchPlay {
  public func applySEButtonTouchForControlEvents(controlEvents: UIControlEvents) {
    (self as! UIButton).addTarget(playSoundEffectDelegator, action: "SEClick", forControlEvents: controlEvents)
  }
}

public protocol SEButtonTouchAufm : SEButtonTouchProtocol {}
extension SEButtonTouchAufm {
  public func applySEButtonTouchForControlEvents(controlEvents: UIControlEvents) {
    (self as! UIButton).addTarget(playSoundEffectDelegator, action: "SEClick", forControlEvents: controlEvents)
  }
}