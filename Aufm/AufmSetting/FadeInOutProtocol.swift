//
//  FadeInOutProtocol.swift
//  AufmSetting
//
//  Created by moti on 2015/08/26.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit

public protocol CanFadeInOut {}
extension CanFadeInOut {
  public func repeatFadeInOut() {
    let o = self as! UIView
    let animation = CABasicAnimation(keyPath: "opacity")
    animation.duration      = 0.5
    animation.autoreverses  = true
    animation.fromValue     = 0.2
    animation.toValue       = 0.7
    animation.repeatCount   = HUGE
    animation.fillMode      = kCAFillModeBoth
    o.layer.addAnimation(animation, forKey: "repeat-fadeinout")
  }
  
  public func stopRepeatFadeInOut() {
    let o = self as! UIView
    o.layer.removeAnimationForKey("repeat-fadeinout")
    o.alpha = 0.0
  }
}