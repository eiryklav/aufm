//
//  SlotView.swift
//  AufmSetting
//
//  Created by moti on 2015/08/22.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

public class SlotView : UIView {
  
  public weak var dataSource : SlotViewDataSource? {
    didSet {
      reload()
    }
  }
  
  private var upArrowButton:    UpArrowButton!
  private var downArrowButton:  DownArrowButton!
  private var slotLabel:        UILabel!
  
  private var currentIndex:     Int
  
  public init(frame: CGRect, arrowButtonWidthRatio buttonRatio: CGFloat) {
    
    currentIndex = 0
    
    super.init(frame: frame)
    
    upArrowButton = UpArrowButton(frame: CGRectMake(0, 0, frame.width * buttonRatio, frame.height / 3))
    downArrowButton = DownArrowButton(frame: CGRectMake(0, frame.height * 2 / 3, frame.width * buttonRatio, frame.height / 3))
    slotLabel = UILabel(frame: CGRectMake(upArrowButton.frame.width + frame.width * 0.025, 0, frame.width - (upArrowButton.frame.width + frame.width * 0.025), frame.height))
    slotLabel.layer.borderColor = UIColor.blackStyleBorderColor().CGColor
    
    addSubview(upArrowButton)
    addSubview(downArrowButton)
    addSubview(slotLabel)
    
    upArrowButton.addTarget(self, action: "didPushUpArrowButton", forControlEvents: .TouchUpInside)
    downArrowButton.addTarget(self, action: "didPushDownArrowButton", forControlEvents: .TouchUpInside)
  }
  
  required public init?(coder: NSCoder) { fatalError("NSCoder x") }
  
  public func reload() {
    slotLabel.text = dataSource?.slotView(self, titleForRow: currentIndex)
    upArrowButton.beEnabled()
    downArrowButton.beEnabled()
  }
  
  func didPushUpArrowButton() {
    if currentIndex - 1 >= 0 {
      currentIndex--
      reload()
    }
    if currentIndex == 0 {
      upArrowButton.beUnabled()
    }
    if currentIndex + 2 == dataSource?.numberOfRows() {
      downArrowButton.beEnabled()
    }
  }
  
  func didPushDownArrowButton() {
    if currentIndex + 1 < dataSource?.numberOfRows() {
      currentIndex++
      reload()
    }
    if currentIndex + 1 == dataSource?.numberOfRows() {
      downArrowButton.beUnabled()
    }
    if currentIndex == 1 {
      upArrowButton.beEnabled()
    }
  }
  
}

public protocol SlotViewDataSource: class {
  func slotView(slotView: SlotView, titleForRow row: Int) -> String
  func numberOfRows() -> Int
}