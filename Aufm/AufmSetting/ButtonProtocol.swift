//
//  BaseButton.swift
//  AufmSetting
//
//  Created by moti on 2015/08/26.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit

// ------------------------------------------------------------------------------------
// MARK: - Button Protocol -

public protocol BTTouchEffectProtocol {
  func didTouchDownEffect()
  func didTouchUpInsideEffect()
  func didTouchUpOutsideEffect()
}

public let BTActionTouchDownEffect      = Selector("didTouchDownEffect")
public let BTActionTouchUpInsideEffect  = Selector("didTouchUpInsideEffect")
public let BTActionTouchUpOutsideEffect = Selector("didTouchUpOutsideEffect")

public protocol IntrinsicBTProtocol : BTTouchEffectProtocol {
  func applyTitle()
}

extension IntrinsicBTProtocol {
  public func applyBTActionTouch() {
    let o = self as! UIButton
    o.addTarget(o, action: BTActionTouchDownEffect,
      forControlEvents: .TouchDown)
    o.addTarget(o, action: BTActionTouchUpInsideEffect,
      forControlEvents: .TouchUpInside)
    o.addTarget(o, action: BTActionTouchUpOutsideEffect,
      forControlEvents: .TouchUpOutside)
  }
}

public protocol BTProtocol : BGProtocol, SEButtonTouchProtocol, IntrinsicBTProtocol {}


// ------------------------------------------------------------------------------------
// MARK: - Title -

public protocol BTNoTitle {}
extension BTNoTitle {
  public func applyTitle() {}
}

public protocol BTLabel {
  var text: String { get }
  var font_: UIFont { get }
  var textColor: UIColor { get }
}
extension BTLabel {
  public func applyTitle() {
    let o = self as! UIButton
    o.setTitle(text, forState: .Normal)
    o.titleLabel!.font = font_
    o.setTitleColor(textColor, forState: .Normal)
  }
}

public protocol BTSwitchableLabel {
  var text:               String  { get }
  var font_:              UIFont  { get }
  var titleColor:         UIColor { get }
  var selectedText:       String  { get }
  var selectedTitleColor: UIColor { get }
}
extension BTSwitchableLabel {
  public func applyTitle() {
    let o = self as! UIButton
    o.setTitle(text, forState: .Normal)
    o.titleLabel!.font = font_
    o.setTitleColor(titleColor, forState: .Normal)
    o.addTarget(o, action: "switchTitle", forControlEvents: .TouchUpInside)
  }
  public func switchTitle() {
    let o = self as! UIButton
    if !o.selected {
      o.selected = true
      o.setTitle(selectedText, forState: .Normal)
      o.setTitleColor(selectedTitleColor, forState: .Normal)
    }
    else {
      o.selected = false
      o.setTitle(text, forState: .Normal)
      o.setTitleColor(titleColor, forState: .Normal)
    }
  }
}

public protocol BTImage {
  var image: UIImage { get }
}
extension BTImage {
  public func applyTitle() {
    let o = self as! UIButton
    o.setImage(image, forState: .Normal)
    o.imageView!.contentMode = .ScaleAspectFit
  }
}

public protocol BTSwitchableImage {
  var image: UIImage { get }
  var selectedImage: UIImage { get }
  var selected: Bool { get set }
}
extension BTSwitchableImage {
  
  public func applyTitle() {
    let o = self as! UIButton
    o.setImage(image, forState: .Normal)
    o.imageView!.contentMode = .ScaleToFill
    o.addTarget(o, action: "switchTitle", forControlEvents: .TouchUpInside)
  }
  
  public func switchTitle() {
    let o = self as! UIButton
    if !o.selected {
      o.selected = true
      o.setImage(selectedImage, forState: .Normal)
    }
    else {
      o.selected = false
      o.setImage(image, forState: .Normal)
    }
  }
  
}
class LabelAboveLayerControl : UIControl {
  
  let layer_:           CALayer
  let highlightedLayer: CALayer
  
  required init?(coder aDecoder: NSCoder) { fatalError() }
  
  init(frame: CGRect, layer_: CALayer, highlightedLayer: CALayer) {
    
    self.layer_ = layer_
    self.highlightedLayer = highlightedLayer
    
    super.init(frame: frame)
    
    addTarget(self, action: "didTouchDownLayerEffect",
      forControlEvents: .TouchDown)
    addTarget(self, action: "didTouchUpInsideLayerEffect",
      forControlEvents: .TouchUpInside)
    addTarget(self, action: "didTouchUpOutsideLayerEffect",
      forControlEvents: .TouchUpOutside)
  }
  
  func didTouchDownLayerEffect() {
    let view = layer_.superlayer!
    layer_.removeFromSuperlayer()
    view.insertSublayer(highlightedLayer, atIndex: 0)
  }
  
  func didTouchUpInsideLayerEffect() {
    let view = highlightedLayer.superlayer!
    highlightedLayer.removeFromSuperlayer()
    view.insertSublayer(layer_, atIndex: 0)
  }
  
  func didTouchUpOutsideLayerEffect() {
    let view = highlightedLayer.superlayer!
    highlightedLayer.removeFromSuperlayer()
    view.insertSublayer(layer_, atIndex: 0)
  }
  
}

public protocol BTLabelAboveLayer {
  var label:            UILabel!  { get }
  var layer_:           CALayer!  { get } // Implicitly Unwrapped Optional Type
  var highlightedLayer: CALayer!  { get } // Implicitly Unwrapped Optional Type
}
extension BTLabelAboveLayer {
  public func applyTitle() {
    let o = self as! UIButton
    o.setTitle("", forState: .Normal)
    o.setTitle("", forState: .Highlighted)
    
    let control = LabelAboveLayerControl(frame: o.bounds, layer_: layer_, highlightedLayer: highlightedLayer)
    o.layer.addSublayer(layer_)
    control.addSubview(label)
    o.addSubview(control)
  }
  
}

public protocol BTLabelAboveSwitchableLayer {
  var label:          UILabel!  { get } // Implicitly Unwrapped Optional Type
  var layer_:         CALayer!  { get } // Implicitly Unwrapped Optional Type
  var selectedLayer:  CALayer!  { get } // Implicitly Unwrapped Optional Type
}
extension BTLabelAboveSwitchableLayer {
  public func applyTitle() {
    let o = self as! UIButton
    o.setTitle("", forState: .Normal)
    o.setTitle("", forState: .Highlighted)
    
    o.layer.addSublayer(layer_)
    o.addSubview(label)
  }
  
  public func switchLayer() {
    let o = self as! UIButton
    if !o.selected {
      o.selected = true
      let view = layer_.superlayer!
      layer_.removeFromSuperlayer()
      view.insertSublayer(selectedLayer, atIndex: 0)
    }
    else {
      o.selected = false
      let view = selectedLayer.superlayer!
      selectedLayer.removeFromSuperlayer()
      view.insertSublayer(layer_, atIndex: 0)
    }
  }
  
}

// ------------------------------------------------------------------------------------
// MARK: - Touch Action -

public protocol BTTouchActionNoEffect{}
extension BTTouchActionNoEffect {
  public func didTouchDownEffect() {}
  public func didTouchUpInsideEffect() {}
  public func didTouchUpOutsideEffect() {}
}

public protocol BTTouchActionSholve {}
extension BTTouchActionSholve {
  
  public func didTouchDownEffect() {
    let o = self as! UIButton
    o.frame.origin = CGPointMake(
      o.frame.origin.x, o.frame.origin.y + o.frame.size.height*0.05
    )
  }
  
  public func didTouchUpInsideEffect() {
    let o = self as! UIButton
    o.frame.origin = CGPointMake(
      o.frame.origin.x, o.frame.origin.y - o.frame.size.height*0.05
    )
  }
  
  public func didTouchUpOutsideEffect() {
    let o = self as! UIButton
    o.frame.origin = CGPointMake(
      o.frame.origin.x, o.frame.origin.y - o.frame.size.height*0.05
    )
  }
  
}