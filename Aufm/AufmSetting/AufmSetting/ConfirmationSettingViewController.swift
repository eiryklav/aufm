//
//  ConfirmationSettingViewController.swift
//  Aufm
//
//  Created by moti on 2015/06/22.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmCommonComponentViews
import AufmGameModel
import AufmObjCppWrapper
import AufmSupport

class ConfirmationViewController : UIViewController {
  
  private var backgroundImageView:  UIImageView!
  private var backButton:           BackButton!
  private var leftSideView:         UIView! // backButtonの位置調整のため
  private var leftTextLabel:        UILabel!
  private var rightTextLabel:       UILabel!
  private var aufmButton:           UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    backgroundImageView = UIImageView(image: UIImage(named: "background-code")!)
    backgroundImageView.frame = UIScreen.mainScreen().bounds
    self.view.addSubview(backgroundImageView)
    
    leftSideView = UIView(frame: CGRectMake(Screen.size.width*0.02, Screen.size.height*0.01, Screen.size.width*0.5, Screen.size.height*0.96))
    let ViewSize = leftSideView.frame.size
    let LeftMost = ViewSize.width * 0.01
    backButton = BackButton(frame: CGRectMake(LeftMost, ViewSize.height*0.85, ViewSize.width*0.15, Screen.size.height/10.0))
    backButton.addTarget(self, action: "didPushBackButton", forControlEvents: .TouchUpInside)
    leftSideView.addSubview(backButton)
    view.addSubview(leftSideView)
    
    leftTextLabel = UILabel(frame: CGRect(origin: CGPointMake(Screen.size.width*0.1, Screen.size.height*0.15), size: CGSizeZero))
    leftTextLabel.font = UIFont(name: "AnonymousPro", sizeType: .Small)
    view.addSubview(leftTextLabel)
    
    rightTextLabel = UILabel(frame: CGRect(origin: CGPointMake(Screen.size.width*0.6, Screen.size.height*0.15), size: CGSizeZero))
    rightTextLabel.font = UIFont(name: "AnonymousPro", sizeType: .Small)
    view.addSubview(rightTextLabel)
    
    aufmButton = UIButton()
    aufmButton.setImage(UIImage(named: "aufm-button"), forState: .Normal)
    let buttonSize = Screen.size.height*0.2
    aufmButton.frame.size = CGSizeMake(buttonSize, buttonSize)
    aufmButton.center = self.view.center
    aufmButton.addTarget(self, action: "didPushEnterButton", forControlEvents: .TouchDown)
    view.addSubview(aufmButton)
    
  }
  
  func showText() {

    leftTextLabel.text =
    "System all green.\nEmergency escape = 1\n./emergency-escape.afm\n\n"
    "./humannet.afm\n"
    "target world: \(SettingInfo.sharedInstance.worldName!)\n"
    "converted time: 5sec, Earth\ntime out: 'or die'\n"
    "your aim: 'to find him'\n\n"
    let (text, _) = DataSourceUtil.timeKeepTextWithCompletion(targetIDName: settingContainer.targetOfBeatsViewController.targetIDName)
    leftTextLabel.text?.appendContentsOf(text)
    leftTextLabel.frame.size = CGSizeMake(5000, 5000)
    leftTextLabel.sizeToFit()
    
    
    rightTextLabel.text = "./timekeep.afm\n"
    "\(SettingInfo.codeOfConductNames[SettingInfo.CodeOfConduct.Attitude.rawValue]) = \(SettingInfo.sharedInstance.attitude!)\n"
    "\(SettingInfo.codeOfConductNames[SettingInfo.CodeOfConduct.ChangeEquipment.rawValue]) = \(SettingInfo.sharedInstance.changeEquipment!)\n"
    "\(SettingInfo.codeOfConductNames[SettingInfo.CodeOfConduct.Invitation.rawValue]) = \(SettingInfo.sharedInstance.invitation!)\n"
    "\(SettingInfo.codeOfConductNames[SettingInfo.CodeOfConduct.LootingDrawers.rawValue]) = \(SettingInfo.sharedInstance.lootingDrawers!)\n"
    
    rightTextLabel.frame.size = CGSizeMake(5000, 5000)
    rightTextLabel.sizeToFit()
  }
  
}

extension ConfirmationViewController {
  func didPushEnterButton() {
    navigationController?.popViewControllerAnimated(false)
    navigationController?.pushViewController(settingContainer.gameTitleNameViewController, animated: false)
  }
  
  func didPushBackButton() {
    navigationController?.popViewControllerAnimated(false)
    navigationController?.pushViewController(settingContainer.codeOfConductViewController, animated: false)
  }
}