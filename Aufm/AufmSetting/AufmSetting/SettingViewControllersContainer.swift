//
//  RootViewController.swift
//  AufmSetting
//
//  Created by moti on 2015/08/28.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmSupport

public class SettingViewControllersContainer : ViewControllersContainerType {
  
  public weak var delegate: CanNavigateFromSetting?
    
  let mapSelectViewController       = MapSelectViewController()
  
  // View
  let mapDetailViewController       = MapDetailViewController()
  
  // Setting
  let activationViewController      = ActivationViewController()
  let targetOfBeatsViewController   = TargetOfBeatsViewController()
  let codeOfConductViewController   = CodeOfConductViewController()
  let confirmationViewController    = ConfirmationViewController()
  
  let gameTitleNameViewController   = GameTitleNameViewController()
  
  var currentSettingClass:  String  = NSStringFromClass(ActivationViewController)
  var settingControllerFromString   = [String:UIViewController]()
  
  internal init() {
    settingControllerFromString[NSStringFromClass(ActivationViewController)] = activationViewController
    settingControllerFromString[NSStringFromClass(TargetOfBeatsViewController)] = targetOfBeatsViewController
    settingControllerFromString[NSStringFromClass(CodeOfConductViewController)] = codeOfConductViewController
    settingControllerFromString[NSStringFromClass(ConfirmationViewController)] = confirmationViewController
    settingControllerFromString[NSStringFromClass(GameTitleNameViewController)] = gameTitleNameViewController
  }
  
}

extension SettingViewControllersContainer : HasStartPointOfViewController {
  public func startPointOfViewController() -> UIViewController {
    return mapSelectViewController
  }
}

public let settingContainer = SettingViewControllersContainer()

public protocol CanNavigateFromSetting: class {
  func instanceOfGameViewController() -> UIViewController
}
