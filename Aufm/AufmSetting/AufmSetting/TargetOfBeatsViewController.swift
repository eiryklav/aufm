//
//  TargetOfBeatsViewController.swift
//  Aufm
//
//  Created by moti on 2015/06/25.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmSupport
import AufmGameModel
import AufmCommonComponentViews
import AufmObjCppWrapper

class TargetOfBeatsViewController : UIViewController {
  
  private var leftSideView:           UIView!
  private var descLabel:              UILabel!
  private var enemyNameSlotView:      SlotView!
  private var pushButtonPickerView:   PushButtonPickerView!
  private var backButton:             BackButton!
  private var enterButton:            EnterButton!
  private var messageView:            TargetOfBeatsMessageView!
  private var segmentedControl:       ChangeViewSettingSegmentedControl!
  
  var targetIDName: [(id: Int, name: String)] = []
    
  override func viewDidLoad() {
    super.viewDidLoad()
    
    /*
      Left side view
    */
    
    leftSideView = UIView(frame: CGRectMake(Screen.size.width*0.02, Screen.size.height*0.01, Screen.size.width*0.5, Screen.size.height*0.96))
    
    let ViewSize = leftSideView.frame.size
    
    descLabel = UILabel(frame: CGRect(origin: CGPointMake(ViewSize.width*0.015, ViewSize.height*0.04), size: CGSizeZero))
    descLabel.text = "討伐目標の設定をおこなってください。"
    descLabel.font = UIFont(sizeType: .Medium)
    leftSideView.addSubview(descLabel)
    
    let LeftMost = ViewSize.width * 0.01    
    enemyNameSlotView = SlotView(frame: CGRectMake(LeftMost, descLabel.frame.maxY + ViewSize.height * 0.025, ViewSize.width, ViewSize.height * 0.18), arrowButtonWidthRatio: 0.2)
    enemyNameSlotView.dataSource = self
    leftSideView.addSubview(enemyNameSlotView)
    
    pushButtonPickerView = PushButtonPickerView(frame: CGRectMake(LeftMost, enemyNameSlotView.frame.maxY + ViewSize.height * 0.025, ViewSize.width, ViewSize.height * 0.35), pushButtonWidthRatio: 0.2)
    pushButtonPickerView.delegate = self
    leftSideView.addSubview(pushButtonPickerView)
    
    // Enter & Back buttons
    enterButton = EnterButton()
    enterButton.frame.origin = CGPointMake(ViewSize.width * 0.2, ViewSize.height * 0.85)
    enterButton.addTarget(self, action: "didPushEnterButton", forControlEvents: .TouchUpInside)
    leftSideView.addSubview(enterButton)
    
    backButton = BackButton(frame: CGRectMake(LeftMost, ViewSize.height*0.85, ViewSize.width*0.15, enterButton.frame.size.height))
    backButton.addTarget(self, action: "didPushBackButton", forControlEvents: .TouchUpInside)
    leftSideView.addSubview(backButton)
    
    self.view.addSubview(leftSideView)
    
    /*
      Right side view
    */
    messageView = TargetOfBeatsMessageView(frame: CGRectMake(leftSideView.frame.maxX+Screen.size.width*0.01, Screen.size.height*0.05, Screen.size.width*0.4, Screen.size.height*0.7))        
    self.view.addSubview(messageView)
    
    segmentedControl = ChangeViewSettingSegmentedControl(selectedSegmentIndex: 1)
    segmentedControl.addTarget(self, action: "didSelectSegment", forControlEvents: .ValueChanged)
    self.view.addSubview(segmentedControl)
    
    targetIDName = DataSourceUtil.loadTimeKeepText()
  }
}

extension TargetOfBeatsViewController {
  func didPushEnterButton() {
    navigationController?.popViewControllerAnimated(false)
    navigationController?.pushViewController(settingContainer.codeOfConductViewController, animated: false)
  }
  
  func didPushBackButton() {
    navigationController?.popViewControllerAnimated(false)
    navigationController?.pushViewController(settingContainer.activationViewController, animated: false)
  }
  
  func didSelectSegment() {
    navigationController?.popViewControllerAnimated(false)
    navigationController?.pushViewController(settingContainer.mapDetailViewController, animated: false)
    segmentedControl.selectedSegmentIndex = 1
  }
}

extension TargetOfBeatsViewController: SlotViewDataSource {
  func slotView(slotView: SlotView, titleForRow row: Int) -> String {
    return targetIDName[row].name
  }
  
  func numberOfRows() -> Int {
    return targetIDName.count
  }
}

extension TargetOfBeatsViewController : PushButtonPickerViewDelegate {
  func pushButtonPickerView(pushButtonPickerView: PushButtonPickerView, didPushRow row: Int) {
    SettingInfo.sharedInstance.targetOfBeatsWithEnemyID[targetIDName[row].id] = row + 1
    let (text, completion) = DataSourceUtil.timeKeepTextWithCompletion(targetIDName: targetIDName)
    messageView.setTargetOfBeatsText(text)
    if completion { enterButton.didEnable() }
  }
}
