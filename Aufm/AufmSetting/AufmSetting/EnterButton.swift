//
//  EnterButton.swift
//  Aufm
//
//  Created by moti on 2015/06/22.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmSupport
import AufmGameModel
import AufmCommonComponentViews

public class EnterButton : UIButton, BTProtocol,
  BGStyleDependent, SEButtonTouchClick, BTLabel, BTTouchActionSholve {
  
  // requrement for BTLabel
  public let text = "Enter"
  public let font_ = UIFont(name: "AnonymousPro", size: adjustFontSize(21.0))!
  public let textColor = UIColor(
    red:    209.0/255.0,
    green:  144.0/255.0,
    blue:   144.0/255.0,
    alpha:  1.0)
  
  required public init?(coder: NSCoder) {
    fatalError("NSCoding not supported")
  }
  
  required public init() {
    super.init(frame: CGRectZero)
    self.frame.size = CGSizeMake(Screen.size.width/3.0, Screen.size.height/10.0)
    self.enabled = false
    
    applyBackground()
    applyTitle()
    applySEButtonTouchForControlEvents(.TouchUpInside)
    applyBTActionTouch()
  }
  
  public func didEnable() {
    self.setTitleColor(UIColor(
      red:    255.0/255.0,
      green:  144.0/255.0,
      blue:   144.0/255.0,
      alpha:  1.0), forState: .Normal)
    self.enabled = true
  }
  
  public func didUnable() {
    self.setTitleColor(UIColor(
      red:    209.0/255.0,
      green:  144.0/255.0,
      blue:   144.0/255.0,
      alpha:  1.0), forState: .Normal)
    self.enabled = false
  }
}