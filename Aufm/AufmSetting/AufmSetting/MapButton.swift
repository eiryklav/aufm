//
//  MapButton.swift
//  Aufm
//
//  Created by moti on 2015/06/18.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmSupport
import AufmCommonComponentViews

class MapButton : UIButton, BTProtocol,
  BGNone, SEButtonTouchClick, BTNoTitle, BTTouchActionNoEffect {
  
  private var mapImageView: UIImageView!
  private var descLabel: UILabel!
  private var worldName: String?
  
  private func setContent(imageName imageName: String, buttonSize: CGSize, text: String) {
    mapImageView = UIImageView(image: UIImage(named: imageName)!)
    mapImageView.contentMode = UIViewContentMode.ScaleAspectFit
    mapImageView.frame.size = CGSizeMake(buttonSize.width*0.8, buttonSize.height*0.8)
    mapImageView.frame.origin.x = centerOf(selfLength: mapImageView.frame.size.width, superLength: buttonSize.width)
    mapImageView.frame.origin.y = buttonSize.height*0.1
    
    addSubview(mapImageView)
    
    descLabel = UILabel()
    descLabel.text = text
    descLabel.font = UIFont(name: "AndaleMono", sizeType: .Small)
    descLabel.sizeToFit()
    descLabel.frame.origin.x = centerOf(selfLength: descLabel.frame.size.width, superLength: buttonSize.width)
    descLabel.frame.origin.y = self.frame.size.height*1.2
    descLabel.textColor = UIColor.whiteColor()
    
    addSubview(descLabel)
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setContent(imageName: "question-mark", buttonSize: frame.size, text: "近日公開")
  }
  
  var textData = [
    // Property Tree(.plist)を読み込むことで、無駄にJSONを読み取らずに済む
    "Tutorial" : "Aufm-0",
    "SicereliaKingdom" : "シスレリア"
  ]
  
  var buttonNameData = [
    "Tutorial" : "tutorial-map-button",
    "SicereliaKingdom" : "sicerelia-map-button"
  ]
  
  convenience init(frame: CGRect, worldName: String) {
    self.init(frame: frame)
    self.worldName = worldName
    setContent(imageName: buttonNameData[worldName]!, buttonSize: frame.size, text: textData[worldName]!)
  }
  
  required init?(coder: NSCoder) {
    fatalError("NSCoder not supported")
  }
  
}
