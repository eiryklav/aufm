//
//  MapButtonsContainerView.swift
//  Aufm
//
//  Created by moti on 2015/06/17.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmSupport

public class MapButtonsContainerView : UIView {
  
  public weak var delegate: MapButtonsContainerViewDelegate?
  
  private var mapButtons = [MapButton]()
  private var worldNames: [String]
  
  public init(worldNames: [String]) {
    
    self.worldNames = worldNames
    
    super.init(frame: CGRectMake(Screen.size.width * 0.1, Screen.size.height * 0.10, Screen.size.width * 0.8, Screen.size.height * 0.6))
    
    let NumOfColumn = 5
    let ButtonSize  = self.frame.size.height * 0.35
    let HorSpace    = (self.frame.size.width - ButtonSize * 5) / 4
    let VertSpace   = (self.frame.size.height - ButtonSize * 2)
    
    for var i = 0; i<10; i++ {
      let posX = CGFloat(i % NumOfColumn) * (ButtonSize + HorSpace)
      let posY = i < NumOfColumn ? 0 : ButtonSize + VertSpace
      if i >= worldNames.count {
        // Wrapped
        mapButtons.append(MapButton(frame: CGRectMake(posX, posY, ButtonSize, ButtonSize)))
      }
      else {
        // Unwrapped
        mapButtons.append(MapButton(frame: CGRectMake(posX, posY, ButtonSize, ButtonSize), worldName: worldNames[i]))
      }
      mapButtons.last!.tag = i
      mapButtons.last!.addTarget(self, action: "didPushButton:", forControlEvents: UIControlEvents.TouchDown)
      addSubview(mapButtons.last!)
    }
  }
  
  required public init?(coder: NSCoder) { fatalError("NSCoder not supported") }
  
  func didPushButton(sender: MapButton) {
    delegate?.mapButtonsContainerView(self, didPushButtonAtIndex: sender.tag)
  }
  
}

public protocol MapButtonsContainerViewDelegate: class {
  func mapButtonsContainerView(mapButtonsContainerView: MapButtonsContainerView, didPushButtonAtIndex index: Int)
}