//
//  ChangeViewSettingSegmentedControl.swift
//  Aufm
//
//  Created by moti on 2015/06/22.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import NYSegmentedControl
import AufmSupport

public class ChangeViewSettingSegmentedControl : NYSegmentedControl {
  
  required public init(coder: NSCoder) {
    fatalError("NSCoder not supported")
  }
  
  public override init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  public init(selectedSegmentIndex: UInt) {
    super.init(items: ["View", "Set"])
    self.selectedSegmentIndex = selectedSegmentIndex
    
    // Customize and size the control
    self.titleFont = UIFont(name: "AnonymousPro", size: adjustFontSize(14.0))
    self.selectedTitleFont = UIFont(name: "AnonymousPro", size: adjustFontSize(14.0))
    self.selectedTitleTextColor = UIColor.whiteColor()
    self.borderWidth = 1.0
    self.borderColor = UIColor(white: 0.15, alpha: 1.0)
    self.drawsGradientBackground = true
    self.segmentIndicatorInset = 2.0
    self.drawsSegmentIndicatorGradientBackground = true
    self.segmentIndicatorGradientTopColor = UIColor(red: 0.3, green: 0.5, blue: 0.88, alpha: 1.0)
    self.segmentIndicatorGradientBottomColor = UIColor(red: 0.2, green: 0.35, blue: 0.35, alpha: 1.0)
    self.segmentIndicatorAnimationDuration = 0.3
    self.segmentIndicatorBorderWidth = 0.0
    self.sizeToFit()
    self.frame = CGRectMake(Screen.size.width * 0.7, Screen.size.height * 0.8, Screen.size.width * 0.23, Screen.size.height / 8.0)
  }
}
