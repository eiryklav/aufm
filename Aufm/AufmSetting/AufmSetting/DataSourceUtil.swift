//
//  DataSourceUtil.swift
//  AufmSetting
//
//  Created by moti on 2015/08/27.
//  Copyright © 2015年 PCyan. All rights reserved.
//

import AufmObjCppWrapper
import AufmGameModel

class DataSourceUtil {

  class func loadTimeKeepText() -> [(id: Int, name: String)] {
    var ret = [(id: Int, name: String)]()
    let arr = cppCode.getCurrentTargetForDefeats()
    for item in arr {
      let id = (item as! NSArray)[0] as! Int
      let name = cppCode.getChrStatusNameWithID(id) as String
      ret.append(id: id, name: name)
    }
    return ret
  }
  
  class func timeKeepTextWithCompletion(targetIDName targetIDName: [(id: Int, name: String)]) -> (text: String, completion: Bool) {
    var text = String()
    text = "./timekeep.afm\n"
    
    var i = 0, complete = true
    let wrappedName  = "？？？"
    for (id, name) in targetIDName {
      text += "No.\(i++) \(WorldGraph.sharedInstance.targetEnemyUnwrapped[id]! ? name : wrappedName) = "
      if let num = SettingInfo.sharedInstance.targetOfBeatsWithEnemyID[id] {
        text += "\(num)\n"
      }
      else {
        complete = false
        text += "<unset>"
      }
    }
    
    if complete { text.appendContentsOf("\nThe targeting system is now ready.\n_\n") }
    else { text.appendContentsOf("_\n") }
    return (text, complete)
  }
  
}