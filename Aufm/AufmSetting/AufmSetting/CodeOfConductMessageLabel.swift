//
//  CodeOfConductMessageLabel.swift
//  Aufm
//
//  Created by moti on 2015/06/28.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

protocol CodeOfConductMessageLabelInit {
  init(viewSize: CGSize)
}

class CodeOfConductMessageLabel : UILabel {
  required init?(coder: NSCoder) {
    fatalError("NSCoder not supported")
  }
  
  required init(viewSize: CGSize) {
    super.init(frame:
      CGRect(
        origin: CGPointMake(viewSize.width*0.015, viewSize.height*0.01),
        size: CGSizeZero))
    text = "./brainkeep.afm"
    font = UIFont(name: "AnonymousPro", sizeType: .Small)
  }
  
  func setCodeOfConductTextWithAttitude(
    attitude:       Int?,
    changeEqp:      Int?,
    invitation:     Int?,
    lootingDrawers: Int?
  ) {
    
    frame.size = CGSizeMake(5000, 500)
    
    let nilString = "nil"
    let completeString = "\nThe conduct system is now ready.\n_\n"
    let incompleteString = "_\n"
    
    text = "./brainkeep.afm\n\n"
    "\(attitude?.description ?? nilString)"
    "\(changeEqp?.description ?? nilString)"
    "\(invitation?.description ?? nilString)"
    "\(lootingDrawers?.description ?? nilString)"
    "\((attitude != nil && changeEqp != nil && invitation != nil && lootingDrawers != nil) ? completeString : incompleteString)"
    sizeToFit()
//      NSNotificationCenter.defaultCenter().postNotificationName(Notifications.EnableEnterButton, object: nil)
    
  }
  
}

