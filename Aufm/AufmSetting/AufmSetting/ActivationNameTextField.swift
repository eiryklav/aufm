//
//  ActivationNameTextField.swift
//  Aufm
//
//  Created by moti on 2015/06/22.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

class ActivationNameTextField : UITextField {
  
  weak var delegate_: ActivationNameTextFieldDelegate?
  
  required init?(coder: NSCoder) {
    fatalError("NSCoder not supported")
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    addTarget(self, action: "didChange", forControlEvents: .EditingChanged)
    delegate = self
    layer.backgroundColor = UIColor.grayColor().CGColor
    borderStyle = UITextBorderStyle.Bezel
    placeholder = "Type something"
    clearButtonMode = .WhileEditing
  }
  
  func didChange() {
    if text!.isEmpty {
      delegate_?.didChangeEmpty()
    }
    else {
      delegate_?.didChangeNotEmpty()
    }
  }
  
}

extension ActivationNameTextField : UITextFieldDelegate {
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
}

protocol ActivationNameTextFieldDelegate: class {
  func didChangeEmpty()
  func didChangeNotEmpty()
}