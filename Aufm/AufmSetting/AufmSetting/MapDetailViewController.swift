//
//  MapDetailViewController.swift
//  Aufm
//
//  Created by moti on 2015/06/18.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmObjCppWrapper
import AufmGameModel
import AufmSupport
import NYSegmentedControl

class MapDetailViewController : UIViewController, UIViewControllerTransitioningDelegate {
  
  private var slideShowView:            SlideShowView!
  private var containerView:            UIView!
  private var changeDescriptionButton:  MapDetailSelectButton!
  private var mapView:                  MapDetailMapView!
  private var segmentedControl:         ChangeViewSettingSegmentedControl!
  private var descriptionViews = [UIView]()
  private var cityData = [String:String]()
  
  override func viewDidLoad() {
    super.viewDidLoad()

    let mapDescriptionLoader = WorldGraph.sharedInstance.mapDescriptionLoader
    
    slideShowView = SlideShowView(pictures: ["background", "background2"])
    view.addSubview(slideShowView)
    
    containerView = UIView(frame: CGRectMake(0, 0, Screen.size.width, Screen.size.height))
    
    /*
      Left side of containerView
    */
    descriptionViews.append(
      MapDetailAreaDescriptionView(frame: CGRectMake(Screen.size.width*0.05, Screen.size.height*0.05, Screen.size.width*0.4, Screen.size.height*0.7))
    )
    (descriptionViews.last! as! MapDetailAreaDescriptionView).dataSource = self
    
    descriptionViews.append(
      MapDetailWorldFlavorView(frame: CGRectMake(Screen.size.width*0.05, Screen.size.height*0.05, Screen.size.width*0.4, Screen.size.height*0.7))
    )
    
    descriptionViews[1].hidden = true
    containerView.addSubview(descriptionViews[0])
    containerView.addSubview(descriptionViews[1])
    mapDescriptionLoader.loadWorldConfigFromJSON(String.getJSONStringFromPath("DataResources/Worlds/\(SettingInfo.sharedInstance.worldName!)/WorldConfig"))
    
    let fullName  = mapDescriptionLoader.fullName
    let subName   = mapDescriptionLoader.subName
    let flavor    = mapDescriptionLoader.flavor
    
    changeDescriptionButton = MapDetailSelectButton(normalText: fullName, selectedText: subName, buttonName: "change-description")
    changeDescriptionButton.addTarget(self, action: "didPushChangeDescriptionButton", forControlEvents: .TouchUpInside)
    containerView.addSubview(changeDescriptionButton)
    
    // global funciton loadMapData()
    WorldGraph.sharedInstance.loadMapData()
    
    (descriptionViews[1] as! MapDetailWorldFlavorView).setTitleText(subName)
    (descriptionViews[1] as! MapDetailWorldFlavorView).setFlavorText(flavor)
    
    /*
      Right side of containerView
    */
    let baseRect = descriptionViews[0].frame
    mapView = MapDetailMapView(frame: CGRectOffset(CGRectMake(baseRect.maxX, baseRect.minY, Screen.size.width * 0.5, Screen.size.height * 0.7), Screen.size.width * 0.02, 0), wholeImageName: "\(SettingInfo.sharedInstance.worldName!)-MapView-Whole")
    mapView.delegate = self
    
    let arr = (mapDescriptionLoader.areaButtonIDOriginArray as [AnyObject]) as! [[AnyObject]]
    for item in arr {
      let areaID = item[0] as! String
      let origin = (item[1] as! NSValue).CGPointValue()
      mapView.addButton(imageName: "\(SettingInfo.sharedInstance.worldName!)-MapView-\(areaID)", origin: origin, imageID: areaID)
    }
    
    containerView.addSubview(mapView)
    
    segmentedControl = ChangeViewSettingSegmentedControl(selectedSegmentIndex: 0)
    segmentedControl.addTarget(self, action: "didSelectSegment", forControlEvents: .ValueChanged)
    containerView.addSubview(segmentedControl)
    
    // add containerView to self
    view.addSubview(containerView)
  }
  
}

// MARK: - Targets -
extension MapDetailViewController {
  
  func didPushChangeDescriptionButton() {
    if changeDescriptionButton.selected {
      descriptionViews[0].hidden = true
      descriptionViews[1].hidden = false
    }
    else {
      descriptionViews[0].hidden = false
      descriptionViews[1].hidden = true
    }
  }
  
  func didSelectSegment() {
    navigationController?.popViewControllerAnimated(false)
    navigationController?.pushViewController(settingContainer.settingControllerFromString[settingContainer.currentSettingClass]!, animated: false)
    segmentedControl.selectedSegmentIndex = 0
  }
  
}

extension MapDetailViewController : MapDetailMapViewDelegate {
  func didPushButtonWithAreaID(areaID: String) {
    WorldGraph.sharedInstance.mapDescriptionLoader.loadCityDataFromJSON(
      String.getJSONStringFromPath("DataResources/Worlds/\(SettingInfo.sharedInstance.worldName!)/Area/\(areaID)")
    )
    cityData["AreaID"]  = areaID
    cityData["Name"]    = WorldGraph.sharedInstance.mapDescriptionLoader.cityName
    cityData["Flavor"]  = WorldGraph.sharedInstance.mapDescriptionLoader.cityFlavor
    (descriptionViews[0] as! MapDetailAreaDescriptionView).reload()
  }
}

extension MapDetailViewController : MapDetailAreaDescriptionViewDataSource {
  
  func numberOfEdges() -> Int {
    return WorldGraph.sharedInstance.data[cityData["AreaID"]!]!.count
  }
  
  func cityName() -> String {
    return cityData["Name"]!
  }
  
  func cityFlavor() -> String {
    return cityData["Flavor"]!
  }
  
  func enemiesForEdgeIndex(index: Int) -> String {
    var ret = String()
    let edges = WorldGraph.sharedInstance.data[cityData["AreaID"]!]!
    for (_, enemySet) in edges {
      for enemyID in enemySet {
        ret += cppCode.getChrStatusNameWithID(enemyID)
      }
    }
    return ret
  }
  
}