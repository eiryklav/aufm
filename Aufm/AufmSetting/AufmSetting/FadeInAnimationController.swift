//
//  FadeInAnimationController.swift
//  Aufm
//
//  Created by moti on 2015/06/17.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

public class FadeInAnimationController : NSObject, UIViewControllerAnimatedTransitioning {
  
  private var duration: Double!
  private var delay: Double!
  
  public convenience init(duration: Double, delay: Double = 0.0) {
    self.init()
    self.duration = duration
    self.delay = delay
  }
  
  public func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
    return duration
  }
  
  public func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
    /*
      Point
      1. toViewController, finalFrame, containerViewを揃える
      2. let duration = transitionDuration(transitionContext)
      3. transitionContext.completeTransition(true)
    */
    let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
    let finalFrame = transitionContext.finalFrameForViewController(toViewController)
    let containerView = transitionContext.containerView()
    
    toViewController.view.alpha = 0.0
    containerView!.addSubview(toViewController.view)
    
    let duration = transitionDuration(transitionContext)
    
    UIView.animateWithDuration(
      duration,
      delay: self.delay,
      options: UIViewAnimationOptions.CurveLinear,
      animations: {
        toViewController.view.alpha = 1.0
        toViewController.view.frame = finalFrame
      }, completion: { finished in
        transitionContext.completeTransition(true)
      }
    )
  }
  
}
