//
//  PushButtonPickerView.swift
//  Aufm
//
//  Created by moti on 2015/08/18.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

public class PushButtonPickerView : UIView {
  
  public weak var dataSource: PushButtonPickerViewDataSource? {
    didSet {
      pickerView.dataSource = self
    }
  }
  
  public weak var delegate: PushButtonPickerViewDelegate? {
    didSet {
      pickerView.delegate = self
    }
  }
  
  private let pushButtonView: PushButtonView
  private let pickerView:     UIPickerView
  private var currentRow:     Int = 0
  
  public init(frame: CGRect, pushButtonWidthRatio buttonRatio: CGFloat) {
    
    pushButtonView = PushButtonView(frame: CGRectMake(0, 0, frame.width * buttonRatio, frame.height))
    
    // ref: //ameblo.jp/kazzya-jp/entry-11375957145.html
    let RawWidth:   CGFloat = 320.0   // arbitary
    let RawHeight:  CGFloat = 216.0   // 162.0, 180.0, 216.0
    pickerView = UIPickerView(frame: CGRectMake(frame.width * (buttonRatio + 0.025), 0, RawWidth, RawHeight))
    
    super.init(frame: frame)
    
    pushButtonView.addTarget(self, action: "didPushPushButtonView", forControlEvents: .TouchUpInside)
    addSubview(pushButtonView)
    
    pickerView.transform = CGAffineTransformMakeScale(frame.width * (1 - (buttonRatio + 0.025)) / RawWidth, frame.height / RawHeight)
    addSubview(pickerView)
  }
  
  required public init?(coder: NSCoder) { fatalError("NSCoder has not been implemented") }
  
  func didPushPushButtonView() {
    delegate?.pushButtonPickerView(self, didPushRow: currentRow)
  }
}

extension PushButtonPickerView : UIPickerViewDataSource {
  public func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
    return 1
  }
  
  public func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    return dataSource?.pushButtonPickerView(self, titleForRow: row) ?? ""
  }
  
  public func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return dataSource?.numberOfRowsInPushButtonPickerView(self) ?? 0
  }
}

extension PushButtonPickerView : UIPickerViewDelegate {
  public func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    currentRow = row
  }
}

public protocol PushButtonPickerViewDataSource: class {
  func numberOfRowsInPushButtonPickerView(pushButtonPickerView: PushButtonPickerView) -> Int
  func pushButtonPickerView(pushButtonPickerView: PushButtonPickerView, titleForRow row: Int) -> String
}

public protocol PushButtonPickerViewDelegate: class {
  func pushButtonPickerView(pushButtonPickerView: PushButtonPickerView, didPushRow row: Int)
}