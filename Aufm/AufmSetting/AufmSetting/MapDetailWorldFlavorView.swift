//
//  MapDetailWorldFlavorView.swift
//  Aufm
//
//  Created by moti on 2015/06/18.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

public class MapDetailWorldFlavorView : UIView {
  
  required public init?(coder aDecoder: NSCoder) {
    fatalError("NSCoder not supported")
  }
  
  private var titleDropCapText = DropCapText()
  private var scrollView: UIScrollView!
  private var flavorTextLabel: UILabel!
  
  override public init(frame: CGRect) {
    super.init(frame: frame)
    backgroundColor = UIColor(rgba: "#dfe5e9aa")
    paddingX = frame.size.width*0.05
    paddingY = frame.size.height*0.03
  }
  
  var paddingX: CGFloat!
  var paddingY: CGFloat!
  
  public func setTitleText(text: String) {
    titleDropCapText = DropCapText(origin: CGPointMake(paddingX, paddingY), text: text)
    addSubview(titleDropCapText)
  }
  
  public func setFlavorText(text: String) {
    scrollView = UIScrollView(frame: CGRectMake(paddingX*2.0, titleDropCapText.frame.maxY+paddingY*2.0, frame.width-paddingX*3.0, frame.height-titleDropCapText.frame.maxY-paddingY*4.0))
    flavorTextLabel = UILabel(frame: CGRectMake(0, 0, scrollView.frame.size.width, 5000))
    flavorTextLabel.text = text
    flavorTextLabel.font = UIFont(sizeType: .Small)
    flavorTextLabel.sizeToFit()
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, flavorTextLabel.frame.size.height)
    scrollView.addSubview(flavorTextLabel)
    addSubview(scrollView)
  }
  
}
