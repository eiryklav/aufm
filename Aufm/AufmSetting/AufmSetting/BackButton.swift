//
//  BackButton.swift
//  Aufm
//
//  Created by moti on 2015/06/25.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmSupport
import AufmVisualSoundEffect

public class BackButton : UIButton, BTProtocol,
  BGStyleDependent, SEButtonTouchClick, BTNoTitle, BTTouchActionSholve {
  
  required public init?(coder: NSCoder) { fatalError("NSCoder not supported") }
  
  override public init(frame: CGRect) {
    super.init(frame: frame)
    applyBackground()
    applySEButtonTouchForControlEvents(.TouchUpInside)
    applyBTActionTouch()
  }
  
  override public func drawRect(rect: CGRect) {
    let ctx = UIGraphicsGetCurrentContext();
    let style = Appearance.sharedInstance.currentStyle
    
    CGContextBeginPath        (ctx)
    CGContextMoveToPoint      (ctx, rect.width*0.65, rect.height*0.2)
    CGContextAddLineToPoint   (ctx, rect.width*0.25, rect.height*0.5)
    CGContextAddLineToPoint   (ctx, rect.width*0.65, rect.height*0.8)
    CGContextClosePath        (ctx)
    
    switch style {
    case .Undefined:  fatalError()
    case .Black:      CGContextSetRGBFillColor(ctx, 0.2, 0.2, 0.2, 1)
    case .White:      CGContextSetRGBFillColor(ctx, 0.6, 0.6, 0.6, 1)
    }
    CGContextFillPath         (ctx)
    
    CGContextBeginPath        (ctx)
    CGContextMoveToPoint      (ctx, rect.width*0.65, rect.height*0.2)
    CGContextAddLineToPoint   (ctx, rect.width*0.25, rect.height*0.5)
    CGContextAddLineToPoint   (ctx, rect.width*0.65, rect.height*0.8)
    CGContextClosePath        (ctx)
    
    switch style {
    case .Undefined:  fatalError()
    case .Black:      CGContextSetRGBStrokeColor(ctx, 0.3, 0.3, 0.3, 1)
    case .White:      CGContextSetRGBStrokeColor(ctx, 0.5, 0.5, 0.5, 1)
    }
    CGContextStrokePath       (ctx)
  }
  
}
