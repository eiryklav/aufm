//
//  TitleButton.swift
//  Aufm
//
//  Created by moti on 2015/06/17.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmCommonComponentViews
import AufmSupport

class TitleButton : UIButton, BTProtocol,
  BGNone, SEButtonTouchClick, BTImage, BTTouchActionSholve {
  
  let image: UIImage
  
  required init?(coder: NSCoder) {
    fatalError("NSCoding not supported")
  }
  
  init(imageName: String) {
    image = UIImage(named: imageName)!
    super.init(frame: CGRect(origin: CGPointZero, size: image.size.adjustedSize()))
    applyBackground()
    applyTitle()
    applySEButtonTouchForControlEvents(.TouchUpInside)
    applyBTActionTouch()
  }
}
