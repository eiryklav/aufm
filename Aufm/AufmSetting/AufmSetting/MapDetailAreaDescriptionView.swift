//
//  MapDetailAreaDescriptionView.swift
//  Aufm
//
//  Created by moti on 2015/06/18.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmSupport

public class MapDetailAreaDescriptionView : UIView {
  
  public weak var dataSource: MapDetailAreaDescriptionViewDataSource? {
    didSet {
      reload()
    }
  }
  
  private var titleTextLabel:   UILabel!
  private var descriptionLabel: UILabel!
  private var adviceLabel:      UILabel!
  private var scrollView:       UIScrollView!
  
  private let predictionColors = [UIColor.redColor(), UIColor.blueColor(), UIColor.yellowColor(), UIColor.greenColor(), UIColor.blackColor(), UIColor.purpleColor(), UIColor.cyanColor(), UIColor.magentaColor()] // 一つのノードに対して最大8個のノードの接続を予定
  
  required public init?(coder aDecoder: NSCoder) {
    fatalError("NSCoder not supported")
  }
  
  public override init(frame: CGRect) {
    super.init(frame: frame)
    backgroundColor = UIColor(rgba: "#dfe5e9aa")
    adviceLabel = UILabel(frame: CGRect(origin: CGPointMake(frame.size.width*0.14, frame.size.height*0.45), size: CGSizeZero))
    adviceLabel.text = "国をタップして確認"
    adviceLabel.font = UIFont(sizeType: .Large)
    adviceLabel.textColor = UIColor(rgba: "#eeeeee88")
    addSubview(adviceLabel)
  }
  
  public func reload() {
    adviceLabel.hidden = true
    for subview in subviews { subview.removeFromSuperview() }
    
    titleTextLabel = UILabel(frame: CGRect(origin: CGPointMake(0, self.frame.height*0.05), size: CGSizeZero))
    titleTextLabel.text = dataSource!.cityName()
    titleTextLabel.font = UIFont(sizeType: .Large)
    titleTextLabel.centeringInView(self)
    addSubview(titleTextLabel)
    scrollView = UIScrollView(frame: CGRectMake(self.frame.width*0.1, titleTextLabel.frame.maxY+self.frame.height*0.05, self.frame.width*0.8, self.frame.height-titleTextLabel.frame.maxY-self.frame.height*0.05))
    
    descriptionLabel = UILabel()
    descriptionLabel.text = dataSource!.cityFlavor()
    descriptionLabel.font = UIFont(sizeType: .Small)
    descriptionLabel.frame = CGRectMake(0, 0, scrollView.frame.width, 5000)
    
    let attr = NSMutableAttributedString(string: descriptionLabel.text! + "\n\n出現予測\n\n")
    let count = dataSource!.numberOfEdges()
    for var i=0; i<count; i++ {
      attr.appendAttributedString(
        NSAttributedString(string: dataSource!.enemiesForEdgeIndex(i), attributes: [NSForegroundColorAttributeName : predictionColors[i]])
      )
    }
    descriptionLabel.attributedText = attr
    descriptionLabel.sizeToFit()
    scrollView.contentSize = CGSizeMake(descriptionLabel.frame.width, descriptionLabel.frame.height)
    scrollView.addSubview(descriptionLabel)
    
    addSubview(scrollView)
  }
}

public protocol MapDetailAreaDescriptionViewDataSource: class {
  func numberOfEdges() -> Int
  func cityName() -> String
  func cityFlavor() -> String
  func enemiesForEdgeIndex(index: Int) -> String
}
