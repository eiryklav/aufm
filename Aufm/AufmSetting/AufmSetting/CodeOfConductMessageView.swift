//
//  CodeOfConductMessageView.swift
//  Aufm
//
//  Created by moti on 2015/06/28.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

public class CodeOfConductMessageView : UIView {
  
  private var messageLabel: CodeOfConductMessageLabel!
  
  required public init?(coder: NSCoder) {
    fatalError("NSCoder not supported")
  }
  
  override public init(frame: CGRect) {
    super.init(frame: frame)
    
    backgroundColor = UIColor(
      red:    151.0/255.0,
      green:  151.0/255.0,
      blue:   151.0/255.0,
      alpha:  40.0/255.0)
    
    messageLabel = CodeOfConductMessageLabel(viewSize: frame.size)
    addSubview(messageLabel)
  }
  
  public func setCodeOfConductTextWithAttitude(
    attitude:       Int?,
    changeEqp:      Int?,
    invitation:     Int?,
    lootingDrawers: Int?
    ) {
    messageLabel.setCodeOfConductTextWithAttitude(attitude, changeEqp: changeEqp, invitation: invitation, lootingDrawers: lootingDrawers)
  }
}