//
//  TitleMainView.swift
//  Aufm
//
//  Created by moti on 2015/06/17.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmSupport

public class TitleMainView : UIView {
  
  weak var delegate: TitleMainViewDelegate?
  
  private var containerView     = UIView(frame: CGRectMake(0,0,Screen.size.width,Screen.size.height))
  
  private var aufmLabel         = TitleAufmLabel()
  private var intoPlayButton    = TitleButton(imageName: "title-into-play")
  private var collectionButton  = TitleButton(imageName: "title-collection")
  private var helpButton        = TitleButton(imageName: "title-help")
  
  func arrangeViews() {
    aufmLabel.frame.origin = CGPointMake(centerOf(selfLength: aufmLabel.frame.size.width, superLength: Screen.size.width), Screen.size.height*0.15)
    intoPlayButton.frame.origin = CGPointMake(centerOf(selfLength: intoPlayButton.frame.size.width, superLength: Screen.size.width), yPosition(aboveView: aufmLabel, spaceSCRatio: 0.075))
    collectionButton.frame.origin = CGPointMake(centerOf(selfLength: collectionButton.frame.size.width, superLength: Screen.size.width), yPosition(aboveView: intoPlayButton, spaceSCRatio: 0.05))
    helpButton.frame.origin = CGPointMake(centerOf(selfLength: helpButton.frame.size.width, superLength: Screen.size.width), yPosition(aboveView: collectionButton, spaceSCRatio: 0.05))
  }
  
  func showViews() {
    self.addSubview(containerView)
    containerView.addSubview(aufmLabel)
    containerView.addSubview(intoPlayButton)
    containerView.addSubview(collectionButton)
    containerView.addSubview(helpButton)
  }
  
  private var RawYIntoPlayButton:   CGFloat = 0.0
  private var RawYCollectionButton: CGFloat = 0.0
  private var RawYHelpButton:       CGFloat = 0.0
  
  func initAnimation() {
    intoPlayButton.alpha = 0.0
    RawYIntoPlayButton = intoPlayButton.frame.origin.y
    intoPlayButton.frame.origin.y *= 0.95
    
    collectionButton.alpha = 0.0
    RawYCollectionButton = collectionButton.frame.origin.y
    collectionButton.frame.origin.y *= 0.95
    
    helpButton.alpha = 0.0
    RawYHelpButton = helpButton.frame.origin.y
    helpButton.frame.origin.y *= 0.95
  }
  
  func animate() {
    UIView.animateWithDuration(
      0.3,
      delay: 0.3,
      options: UIViewAnimationOptions.CurveEaseIn,
      animations: {
        self.intoPlayButton.alpha = 1.0
        self.intoPlayButton.frame.origin.y = self.RawYIntoPlayButton
      },
      completion: nil
    );
    
    UIView.animateWithDuration(
      0.3,
      delay: 0.4,
      options: UIViewAnimationOptions.CurveEaseIn,
      animations: {
        self.collectionButton.alpha = 1.0
        self.collectionButton.frame.origin.y = self.RawYCollectionButton
      },
      completion: nil
    );
    
    UIView.animateWithDuration(
      0.3,
      delay: 0.45,
      options: UIViewAnimationOptions.CurveEaseIn,
      animations: {
        self.helpButton.alpha = 1.0
        self.helpButton.frame.origin.y = self.RawYHelpButton
      },
      completion: { (value:Bool) in
        self.intoPlayButton.addTarget(self, action: "didClickIntoPlayButton:", forControlEvents: .TouchDown)
        self.collectionButton.addTarget(self, action: "didClickCollectionButton:", forControlEvents: .TouchDown)
        self.helpButton.addTarget(self, action: "didClickHelpButton:", forControlEvents: .TouchDown)
      }
    );
  }
  
  required public init?(coder aDecoder: NSCoder) {
    fatalError("NSCoder not suppported")
  }
  
  public init() {
    super.init(frame: CGRectMake(0,0,Screen.size.width,Screen.size.height))
    self.backgroundColor = UIColor.whiteColor()
    arrangeViews()
    initAnimation()
    showViews()
    animate()
  }
  
  /*
    ボタンのアクション
  */
  var msgs = [UILabel]()
  
  func didClickIntoPlayButton(sender: UIButton) {
    
//    NSNotificationCenter.defaultCenter().postNotificationName(Notifications.TransIntoMapSelect, object: nil)
    
    UIView.animateWithDuration(
      0.5,
      delay: 0.0,
      options: [.TransitionCrossDissolve, .CurveEaseIn],
      animations: {
        self.intoPlayButton.alpha = 0.0
        self.collectionButton.alpha = 0.0
        self.helpButton.alpha = 0.0
        self.aufmLabel.alpha = 0.0
      },
      completion: { (Bool) -> () in
        delegate?.didClickIntoPlayButton()
      }
    )
  }
  
  func didClickCollectionButton(sender: UIButton) {
    
  }
  
  func didClickHelpButton(sender: UIButton) {
    
  }
}

protocol TitleMainViewDelegate: class {
  func didClickIntoPlayButton()
}