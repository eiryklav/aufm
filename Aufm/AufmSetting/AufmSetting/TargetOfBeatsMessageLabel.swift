//
//  TargetOfBeatsMessageLabel.swift
//  Aufm
//
//  Created by moti on 2015/06/26.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmGameModel

class TargetOfBeatsMessageLabel : UILabel {
  
  required init?(coder: NSCoder) {
    fatalError("NSCoder not supported")
  }
  
  init(viewSize: CGSize) {
    super.init(frame: CGRect(origin: CGPointMake(viewSize.width*0.015, viewSize.height*0.01), size: CGSizeZero))
    text = "./timekeep.afm\n"
    font = UIFont(name: "AnonymousPro" ,sizeType: .Small)
  }
  
  func setTargetOfBeatsText(text: String) {
    frame.size = CGSizeMake(5000, 500)
    self.text = text
    /*
    let joinedNames = names.joinWithSeparator("\n")
    text = "./timekeep.afm\n\n\(joinedNames)"
    if complete { text!.appendContentsOf("\nThe targeting system is now ready.\n_\n") }
    else { text!.appendContentsOf("_\n") }
*/
    sizeToFit()
    /*
    var ok = true
    for (id, unwrapped) in WorldGraph.sharedInstance.targetEnemies {
      var name: String!
      if unwrapped {
        name = cppCode.getChrStatusNameWithID(Global.targetEnemies[i].0)
      }
      else {
        name = "？？？"
      }
      self.text = self.text! + "No\(i+1). \(name) = "
      if Global.settingInfo.targetOfBeatsWithEnemyID[ Global.targetEnemies[i].0 ] != nil {
        self.text = self.text! + "\(Global.settingInfo.targetOfBeatsWithEnemyID[ Global.targetEnemies[i].0 ]!)"
      }
      else {
        ok = false
      }
      self.text = self.text! + "\n"
    }
    if ok {
      self.text = self.text! + "\nThe targeting system is now ready.\n_\n"
      NSNotificationCenter.defaultCenter().postNotificationName(Notifications.EnableEnterButton, object: nil)
    }
    else {
      self.text = self.text! + "_\n";
    }
*/
  }
}