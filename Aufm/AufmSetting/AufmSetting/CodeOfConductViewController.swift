//
//  CodeOfConductViewController.swift
//  Aufm
//
//  Created by moti on 2015/06/22.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmSupport
import AufmGameModel
import AufmCommonComponentViews

class CodeOfConductViewController : UIViewController {
  
  private var leftSideView:         UIView!
  private var descLabel:            UILabel!
  private var conductSlotView:      SlotView!
  private var pushButtonPickerView: PushButtonPickerView!
  private var backButton:           BackButton!
  private var enterButton:          EnterButton!
  private var messageView:          CodeOfConductMessageView!
  
  var segmentedControl:             ChangeViewSettingSegmentedControl!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    /*
      MARK: - Left side view -
    */
    
    leftSideView = UIView(frame: CGRectMake(Screen.size.width*0.02, Screen.size.height*0.01, Screen.size.width*0.5, Screen.size.height*0.96))
    
    let ViewSize = leftSideView.frame.size
    
    descLabel = UILabel(frame: CGRect(origin: CGPointMake(ViewSize.width*0.015, ViewSize.height*0.04), size: CGSizeZero))
    descLabel.text = "行動指針の設定をおこなってください。"
    descLabel.font = UIFont(sizeType: .Medium)
    leftSideView.addSubview(descLabel)
    
    let LeftMost = ViewSize.width * 0.01
    conductSlotView = SlotView(frame: CGRectMake(LeftMost, descLabel.frame.maxY + ViewSize.height * 0.025, ViewSize.width, ViewSize.height * 0.18), arrowButtonWidthRatio: 0.2)
    conductSlotView?.dataSource = self
    leftSideView.addSubview(conductSlotView)
    
    pushButtonPickerView = PushButtonPickerView(frame: CGRectMake(LeftMost, conductSlotView.frame.maxY + ViewSize.height * 0.025, ViewSize.width, ViewSize.height * 0.35), pushButtonWidthRatio: 0.2)
    pushButtonPickerView?.dataSource = self
    pushButtonPickerView?.delegate = self
    leftSideView.addSubview(pushButtonPickerView)
    
    // Enter & Back buttons
    enterButton = EnterButton()
    enterButton.frame.origin = CGPointMake(ViewSize.width * 0.2, ViewSize.height * 0.85)
    enterButton.addTarget(self, action: "didPushEnterButton", forControlEvents: .TouchUpInside)
    leftSideView.addSubview(enterButton)
    
    backButton = BackButton(frame: CGRectMake(LeftMost, ViewSize.height*0.85, ViewSize.width*0.15, enterButton.frame.size.height))  // 固定位置
    backButton.addTarget(self, action: "didPushBackButton", forControlEvents: .TouchUpInside)
    leftSideView.addSubview(backButton)
    
    view.addSubview(leftSideView)
    
    /*
      MARK: - Right side view -
    */
    messageView = CodeOfConductMessageView(frame: CGRectMake(leftSideView.frame.maxX+Screen.size.width*0.01, Screen.size.height*0.05, Screen.size.width*0.4, Screen.size.height*0.7))
    view.addSubview(messageView)
    
    segmentedControl = ChangeViewSettingSegmentedControl(selectedSegmentIndex: 1)
    view.addSubview(segmentedControl)
    
    segmentedControl.addTarget(self, action: "didSelectSegment", forControlEvents: .ValueChanged)
  }
  
}

// MARK: - Targets -
extension CodeOfConductViewController {
  func didPushEnterButton() {
    navigationController?.popViewControllerAnimated(false)
    navigationController?.pushViewController(settingContainer.confirmationViewController, animated: false)
  }
  
  func didPushBackButton() {
    navigationController?.popViewControllerAnimated(false)
    navigationController?.pushViewController(settingContainer.targetOfBeatsViewController, animated: false)
  }
  
  func didSelectSegment() {
    navigationController?.popViewControllerAnimated(false)
    navigationController?.pushViewController(settingContainer.mapDetailViewController, animated: false)
    segmentedControl.selectedSegmentIndex = 1
  }
}

extension CodeOfConductViewController : SlotViewDataSource {
  func slotView(slotView: SlotView, titleForRow row: Int) -> String {
    return SettingInfo.codeOfConductNames[row]
  }
  
  func numberOfRows() -> Int {
    return SettingInfo.codeOfConductNames.count
  }
}

extension CodeOfConductViewController : PushButtonPickerViewDataSource {
  func numberOfRowsInPushButtonPickerView(pushButtonPickerView: PushButtonPickerView) -> Int {
    return 101
  }
  
  func pushButtonPickerView(pushButtonPickerView: PushButtonPickerView, titleForRow row: Int) -> String {
    return row.description
  }
}

extension CodeOfConductViewController : PushButtonPickerViewDelegate {
  func pushButtonPickerView(pushButtonPickerView: PushButtonPickerView, didPushRow row: Int) {
    messageView.setCodeOfConductTextWithAttitude(SettingInfo.sharedInstance.attitude, changeEqp: SettingInfo.sharedInstance.changeEquipment, invitation: SettingInfo.sharedInstance.invitation, lootingDrawers: SettingInfo.sharedInstance.lootingDrawers)
  }
}