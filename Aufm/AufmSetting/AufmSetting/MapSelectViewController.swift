//
//  MapSelectViewController.swift
//  Aufm
//
//  Created by moti on 2015/06/17.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmSupport
import AufmGameModel

class MapSelectViewController : UIViewController {
  
  let animationController = FadeInAnimationController(duration: 0.5)
  
  var containerView:                    UIView!
  private var selectMapLabel:           UILabel!
  private var mapButtonsContainerView:  MapButtonsContainerView!
  
  var worldNames: [String]!
  
  override func viewDidLoad() {
    super.viewDidLoad()

    containerView = UIView(frame: UIScreen.mainScreen().bounds)
    
    selectMapLabel = UILabel()
    selectMapLabel.text = "SELECT THE WORLD"
    selectMapLabel.font = UIFont(sizeType: .Medium)
    selectMapLabel.textColor = UIColor.whiteColor()
    selectMapLabel.frame.origin = CGPointMake(centerOf(selfLength: selectMapLabel.frame.size.width, superLength: Screen.size.width), Screen.size.height*0.85)
    containerView.addSubview(selectMapLabel)
    worldNames = NSDictionary(contentsOfFile: NSBundle.mainBundle().pathForResource("Config", ofType: "plist")!)!["WorldNames"] as! [String]
    mapButtonsContainerView = MapButtonsContainerView(worldNames: worldNames)
    mapButtonsContainerView.delegate = self
    containerView.addSubview(mapButtonsContainerView)
    view.addSubview(containerView)
  }
  
}

extension MapSelectViewController : MapButtonsContainerViewDelegate {
  func mapButtonsContainerView(mapButtonsContainerView: MapButtonsContainerView, didPushButtonAtIndex index: Int) {
    SettingInfo.sharedInstance.worldName = worldNames[index]
    navigationController?.pushViewController(settingContainer.mapDetailViewController, animated: true)
  }
}

extension MapSelectViewController : UIViewControllerTransitioningDelegate {
  func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    return animationController
  }
}