//
//  MapDetailMapView.swift
//  Aufm
//
//  Created by moti on 2015/06/20.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmSupport
import AufmCommonComponentViews

public class MapDetailMapView : UIView {
  
  public weak var delegate: MapDetailMapViewDelegate?
  
  private var wholeImageView: UIImageView!
  private var sizeRatio: CGPoint!
  private var areaButtons = [ProteanImageButton]()
  
  required public init?(coder aDecoder: NSCoder) {
    fatalError("NSCoder not supported")
  }
  
  public init(frame: CGRect, wholeImageName: String) {
    super.init(frame: frame)
    let image = UIImage(named: wholeImageName)
    let wholeImageRawSize = image!.size
    sizeRatio = CGPointMake(frame.width/wholeImageRawSize.width, frame.height/wholeImageRawSize.height)
    wholeImageView = UIImageView(image: image)
    wholeImageView.frame = CGRectMake(0, 0, frame.size.width, frame.size.height)
    addSubview(self.wholeImageView)
  }
  
  public func addButton(imageName imageName: String, origin: CGPoint, imageID: String) {
    let image = UIImage(named: imageName)
    let imageRawSize = image!.size
    areaButtons.append(
      ProteanImageButton(frame: CGRectMakeSizeAdjusted(origin.x, origin.y, imageRawSize.width, imageRawSize.height, sizeRatio: sizeRatio), image: image!, imageID: imageID)
    )
    areaButtons.last!.alpha = 0.0
    areaButtons.last!.addTarget(self, action: "didPushButton:", forControlEvents: .TouchDown)
    addSubview(areaButtons.last!)
  }
  
  func didPushButton(sender: ProteanImageButton) {
    
    for button in areaButtons {
      if sender.imageID == button.imageID {
        sender.repeatFadeInOut()
      }
      else {
        sender.stopRepeatFadeInOut()
      }
    }
    
    delegate?.didPushButtonWithAreaID(sender.imageID)
  }
  
}

public protocol MapDetailMapViewDelegate: class {
  func didPushButtonWithAreaID(areaID: String)
}