//
//  TitleViewController.swift
//  Aufm
//
//  Created by moti on 2015/06/17.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmSupport

public class TitleViewController : UIViewController {
  
  private let animationController = FadeInAnimationController(duration: 1.0)
  private var titleMainView: TitleMainView!
  
  override public func viewDidLoad() {
    super.viewDidLoad()
    titleMainView = TitleMainView()
    titleMainView.delegate = self
    navigationController?.delegate = self
    view.addSubview(titleMainView)
  }
  
}

extension TitleViewController : UINavigationControllerDelegate {
  public func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    return animationController
  }
}

extension TitleViewController : TitleMainViewDelegate {
  func didClickIntoPlayButton() {
    // endpoint
  }
}