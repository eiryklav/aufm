//
//  GameTitleNameViewController.swift
//  Aufm
//
//  Created by moti on 2015/06/28.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import AufmSupport
import AufmVisualSoundEffect
import AufmGameModel
import SpriteKit

class GameTitleNameViewController : UIViewController {
  
  private let animationController = FadeInAnimationController(duration: 0.5)
  private var skView:               SKView!
  private var skScene:              SKScene!
  private var titleSprite:          SKSpriteNode!
  
  private var videoObserver:        NSObjectProtocol!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    navigationController?.delegate = self
    
    skView  = SKView(frame: UIScreen.mainScreen().bounds)
    view.addSubview(skView)
    skScene = SKScene(size: Screen.size)
    skView.presentScene(skScene)
    
    let playerItem = AVPlayerItem(URL: NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("warp", ofType: "mp4")!))
    let player = AVPlayer(playerItem: playerItem)
    let videoNode = SKVideoNode(AVPlayer: player)
    videoNode.position = skView.center
    videoNode.size = Screen.size
    skScene.addChild(videoNode)
    player.play()
    
    Audio.sharedInstance.playSE("warp")
    
    titleSprite = SKSpriteNode(fileNamed: "\(SettingInfo.sharedInstance.worldName!)-Title")
    titleSprite.size = Screen.size
    titleSprite.alpha = 0.0
    skScene.addChild(titleSprite)
    
    /*
    videoNode.runAction(SKAction.sequence([
      SKAction.waitForDuration(1.5),
      SKAction.fadeOutWithDuration(0.5)
    ]))
    */
    titleSprite.runAction(SKAction.sequence([
      SKAction.waitForDuration(1.5),
      SKAction.fadeInWithDuration(0.5)
    ]))
    
    videoObserver = NSNotificationCenter.defaultCenter().addObserverForName(AVPlayerItemDidPlayToEndTimeNotification, object: nil, queue: NSOperationQueue.mainQueue(), usingBlock: { finished in
      
      NSTimer.scheduledTimerWithTimeInterval(3.0, target: NSBlockOperation(block: {
//        settingContainer.delegate?.endPointOfViewController(self)
        /*
        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let gameViewController = storyboard.instantiateInitialViewController() as! UIViewController
        gameViewController.modalTransitionStyle = UIModalTransitionStyle.CrossDissolve
        self.navigationController!.pushViewController(gameViewController, animated: true)
        */
      }), selector: "main", userInfo: nil, repeats: false)
      
    })
  }
  
  override func viewDidDisappear(animated: Bool) {
    NSNotificationCenter.defaultCenter().removeObserver(videoObserver)
  }
}

extension GameTitleNameViewController : UINavigationControllerDelegate {
  func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    return animationController
  }
}