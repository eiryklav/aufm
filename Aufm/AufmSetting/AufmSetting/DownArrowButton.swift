//
//  DownArrowButton.swift
//  Aufm
//
//  Created by moti on 2015/06/25.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmVisualSoundEffect

class DownArrowButton : UIButton, BTProtocol,
  BGStyleDependent, SEButtonTouchClick, BTNoTitle, BTTouchActionSholve {
  
  required init?(coder: NSCoder) {
    fatalError("NSCoder not supported")
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    applyBackground()
    applyTitle()
    applySEButtonTouchForControlEvents(.TouchUpInside)
    applyBTActionTouch()
  }
  
  override func drawRect(rect: CGRect) {
    
    let style = Appearance.sharedInstance.currentStyle
    
    let ctx = UIGraphicsGetCurrentContext();
    
    CGContextBeginPath        (ctx)
    CGContextMoveToPoint      (ctx, rect.width*0.5, rect.height*0.8)
    CGContextAddLineToPoint   (ctx, rect.width*0.2, rect.height*0.2)
    CGContextAddLineToPoint   (ctx, rect.width*0.8, rect.height*0.2)
    CGContextClosePath        (ctx)
    
    if style == .Black {  CGContextSetRGBFillColor(ctx, 0.2, 0.2, 0.2, 1) }
    else {                CGContextSetRGBFillColor(ctx, 0.6, 0.6, 0.6, 1) }
    CGContextFillPath         (ctx)
    
    CGContextBeginPath        (ctx)
    CGContextMoveToPoint      (ctx, CGRectGetMidX(rect), rect.height*0.8)
    CGContextAddLineToPoint   (ctx, rect.width*0.2, rect.height*0.2)
    CGContextAddLineToPoint   (ctx, rect.width*0.8, rect.height*0.2)
    CGContextClosePath        (ctx)
    
    if style == .Black {  CGContextSetRGBStrokeColor(ctx, 0.3, 0.3, 0.3, 1) }
    else {                CGContextSetRGBStrokeColor(ctx, 0.5, 0.5, 0.5, 1) }
    CGContextStrokePath       (ctx)
  }
  
  func beEnabled() { enabled = true }
  func beUnabled() { enabled = false }
}