//
//  DropCapText.swift
//  Aufm
//
//  Created by moti on 2015/06/24.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

class DropCapText : UIView {
  private var dropCap:    UILabel
  private var normalText: UILabel
  
  var textColor: UIColor {
    get {
      return dropCap.textColor
    }
    set {
      dropCap.textColor = textColor
      normalText.textColor = textColor
    }
  }
  
  required init?(coder: NSCoder) { fatalError("NSCoder not supporeted") }
  
  init() {
    dropCap = UILabel()
    normalText = UILabel()
    super.init(frame: CGRectZero)
  }
  
  init(origin: CGPoint, text: String) {
    dropCap = UILabel(frame: CGRect(origin: origin, size: CGSizeZero))
    dropCap.text = text.substringToIndex(text.startIndex.advancedBy(1))
    dropCap.font = UIFont(sizeType: .Large)
    dropCap.sizeToFit()
    
    normalText = UILabel()
    normalText.text = text.substringFromIndex(text.startIndex.advancedBy(1))
    normalText.font = UIFont(sizeType: .Medium)
    normalText.sizeToFit()
    normalText.frame.origin = CGPointMake(dropCap.frame.maxX, dropCap.frame.maxY-normalText.frame.size.height)
    
    super.init(frame: CGRectMake(origin.x, origin.y, dropCap.frame.size.width+normalText.frame.size.width, dropCap.frame.size.height))
    addSubview(dropCap)
    addSubview(normalText)
  }
  
}
