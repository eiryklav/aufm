//
//  TargetOfBeatsMessageView.swift
//  Aufm
//
//  Created by moti on 2015/06/26.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit

public class TargetOfBeatsMessageView : UIView {
  
  private var messageLabel: TargetOfBeatsMessageLabel!
  
  required public init?(coder: NSCoder) {
    fatalError("NSCoder not supported")
  }
  
  override public init(frame: CGRect) {
    super.init(frame: frame)
    
    self.backgroundColor = UIColor(
      red:    151.0/255.0,
      green:  151.0/255.0,
      blue:   151.0/255.0,
      alpha:  40.0/255.0)
    
    messageLabel = TargetOfBeatsMessageLabel(viewSize: self.frame.size)
    addSubview(messageLabel)
  }
  
  public func setTargetOfBeatsText(text: String) {
    messageLabel.setTargetOfBeatsText(text)
  }
}