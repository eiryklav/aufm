//
//  MapDetailSelectButton.swift
//  Aufm
//
//  Created by moti on 2015/06/17.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmSupport

public class MapDetailSelectButton : SelectButton {
  
  required public init?(coder: NSCoder) {
    fatalError("NSCoding not supported")
  }

  public init(normalText: String, selectedText: String, buttonName: String) {
    super.init(
      frame: CGRectMake(
        Screen.size.width*0.05, Screen.size.height*0.8,
        Screen.size.width/3.0, Screen.size.height/8.0),
      normalText: normalText,
      selectedText: selectedText,
      buttonName: buttonName)
    titleLabel!.font = UIFont(name: "AndaleMono", sizeType: .SemiMedium)
  }
}
