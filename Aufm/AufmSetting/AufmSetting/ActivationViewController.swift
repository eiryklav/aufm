//
//  ActivationViewController.swift
//  Aufm
//
//  Created by moti on 2015/06/21.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmSupport
import AufmGameModel

class ActivationViewController : UIViewController {
  
  private var leftSideView:           UIView!
  
  private var descLabel:              UILabel!
  
  private var nameLabel:              UILabel!
  private var nameTextField:          ActivationNameTextField!
  
  private var maleLabel:              UILabel!
  private var femaleLabel:            UILabel!
  private var normalLabel:            UILabel!
  private var hardLabel:              UILabel!
  private var brutalLabel:            UILabel!
  
  private var sexRadioButtonsView:    RadioButtonsView!
  private var difficultyButtonsView:  RadioButtonsView!
  
  private var enterButton:            EnterButton!
  
  private var rightSideView:          UIView!
  private var activationMessageLabel: ActivationMessageLabel!
  
  private var inputCompletion: Int = 0
  private enum InputCompletion: Int {
    case Name       = 0b001
    case Sex        = 0b010
    case Difficulty = 0b100
    case All        = 0b111
  }
  
  var segmentedControl: ChangeViewSettingSegmentedControl!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    /*
      MARK: - Left side view -
    */
    
    leftSideView = UIView(frame: CGRectMake(Screen.size.width*0.02, Screen.size.height*0.01, Screen.size.width*0.5, Screen.size.height*0.96))
    
    let ViewSize = leftSideView.frame.size
    
    descLabel = UILabel(frame: CGRect(origin: CGPointMake(ViewSize.width*0.015, ViewSize.height*0.04), size: CGSizeZero))
    descLabel.text = "アクティベーションをおこないます。"
    descLabel.font = UIFont(sizeType: .Medium)
    descLabel.sizeToFit()
    leftSideView.addSubview(descLabel)
    
    nameLabel = UILabel(frame: CGRect(origin: CGPointMake(descLabel.frame.minX, descLabel.frame.maxY + ViewSize.height*0.1), size: CGSizeZero))
    nameLabel.text = "Name"
    nameLabel.font = UIFont(sizeType: .Medium)
    nameLabel.sizeToFit()
    leftSideView.addSubview(nameLabel)
    
    nameTextField = ActivationNameTextField(
      frame: CGRectMake(
        nameLabel.frame.maxX + ViewSize.width*0.01,
        nameLabel.frame.minY - (ViewSize.height*0.1-nameLabel.frame.size.height)/2.0,
        ViewSize.width*0.7,
        ViewSize.height*0.1)
    )
    nameTextField.delegate_ = self
    leftSideView.addSubview(nameTextField)
    
    maleLabel = UILabel()
    maleLabel.text = "male"
    maleLabel.font = UIFont(sizeType: .Medium)
    maleLabel.sizeToFit()
    maleLabel.center = CGPointMake(ViewSize.width/3.0, nameTextField.frame.maxY+ViewSize.height*0.08)
    leftSideView.addSubview(maleLabel)
    
    femaleLabel = UILabel()
    femaleLabel.text = "female"
    femaleLabel.font = UIFont(sizeType: .Medium)
    femaleLabel.sizeToFit()
    femaleLabel.center = CGPointMake(ViewSize.width*2.0/3.0, maleLabel.frame.midY)
    leftSideView.addSubview(femaleLabel)
    
    let radioButtonSize = CGSizeMake(ViewSize.width*0.12, ViewSize.width*0.12)
    
    sexRadioButtonsView = RadioButtonsView(origin: CGPointMake(ViewSize.width / 3.0 - radioButtonSize.width / 2, maleLabel.frame.maxY), classID: "sex", buttonSize: radioButtonSize,
      space: ViewSize.width / 3.0 - radioButtonSize.width)
    sexRadioButtonsView.delegate = self
    sexRadioButtonsView.appendRadioButton("male")
    sexRadioButtonsView.appendRadioButton("female")
    leftSideView.addSubview(sexRadioButtonsView)
    
    normalLabel = UILabel()
    normalLabel.text = "normal"
    normalLabel.font = UIFont(sizeType: .Medium)
    normalLabel.sizeToFit()
    normalLabel.center = CGPointMake(ViewSize.width / 4.0, sexRadioButtonsView.frame.maxY + ViewSize.height * 0.08)
    leftSideView.addSubview(normalLabel)
    
    hardLabel = UILabel()
    hardLabel.text = "hard"
    hardLabel.font = UIFont(sizeType: .Medium)
    hardLabel.sizeToFit()
    hardLabel.center = CGPointMake(ViewSize.width/2.0, normalLabel.frame.midY)
    leftSideView.addSubview(hardLabel)
    
    brutalLabel = UILabel()
    brutalLabel.text = "brutal"
    brutalLabel.font = UIFont(sizeType: .Medium)
    brutalLabel.sizeToFit()
    brutalLabel.center = CGPointMake(ViewSize.width*3.0/4.0, normalLabel.frame.midY)
    leftSideView.addSubview(brutalLabel)
    
    difficultyButtonsView = RadioButtonsView(origin: CGPointMake(ViewSize.width / 4.0 - radioButtonSize.width / 2, normalLabel.frame.maxY), classID: "difficulty", buttonSize: radioButtonSize, space: ViewSize.width / 4.0 - radioButtonSize.width)
    difficultyButtonsView.delegate = self
    difficultyButtonsView.appendRadioButton("normal")
    difficultyButtonsView.appendRadioButton("hard")
    difficultyButtonsView.appendRadioButton("brutal")
    leftSideView.addSubview(difficultyButtonsView)
    
    enterButton = EnterButton()
    enterButton.frame.origin = CGPointMake(ViewSize.width*0.2, ViewSize.height*0.85)
    enterButton.addTarget(self, action: "didPushEnterButton", forControlEvents: .TouchUpInside)
    leftSideView.addSubview(enterButton)
    
    self.view.addSubview(leftSideView)
    
    /*
      MARK: - Right side view -
    */
    rightSideView = UIView(frame: CGRectMake(leftSideView.frame.maxX+Screen.size.width*0.01, Screen.size.height*0.05, Screen.size.width*0.4, Screen.size.height*0.7))
    rightSideView.backgroundColor = UIColor(
      red:    151.0/255.0,
      green:  151.0/255.0,
      blue:   151.0/255.0,
      alpha:  40.0/255.0)
    
    activationMessageLabel = ActivationMessageLabel(viewSize: ViewSize)
    rightSideView.addSubview(activationMessageLabel)
    
    self.view.addSubview(rightSideView)
    
    segmentedControl = ChangeViewSettingSegmentedControl(selectedSegmentIndex: 1)
    segmentedControl.addTarget(self, action: "didSelectSegment", forControlEvents: .TouchUpInside)
    self.view.addSubview(segmentedControl)
    
    // MARK: (DEBUG)Preset SettingInfo
    if !SettingInfo.sharedInstance.name.isEmpty {
      nameTextField.text = SettingInfo.sharedInstance.name
      nameTextField.didChange()
    }
    switch SettingInfo.sharedInstance.sex {
      case .Male:       sexRadioButtonsView.buttonWithUniqueID("male")?.didCheck()
      case .Female:     sexRadioButtonsView.buttonWithUniqueID("female")?.didCheck()
      case .Unselected: break
    }
    switch SettingInfo.sharedInstance.difficulty {
      case .Normal:     difficultyButtonsView.buttonWithUniqueID("normal")?.didCheck()
      case .Hard:       difficultyButtonsView.buttonWithUniqueID("hard")?.didCheck()
      case .Brutal:     difficultyButtonsView.buttonWithUniqueID("brutal")?.didCheck()
      case .Unselected: break
    }
    
    // resignFirstResponder
    nameTextField.resignFirstResponder()
  }

  func checkCompleteInput() {
    if inputCompletion == InputCompletion.All.rawValue {
      enterButton.didEnable()
    }
    else {
      enterButton.didUnable()
    }
  }

}

// MARK: - Targets -
extension ActivationViewController {
  
  func didPushEnterButton() {
    navigationController?.popViewControllerAnimated(false)
    navigationController?.pushViewController(settingContainer.targetOfBeatsViewController, animated: false)
  }
  
  func didSelectSegment() {
    navigationController?.popViewControllerAnimated(false)
    navigationController?.pushViewController(settingContainer.mapDetailViewController, animated: false)
    segmentedControl.selectedSegmentIndex = 1
  }

}

extension ActivationViewController : RadioButtonsViewDelegate {
  func didCheckFirstTimeWithClassID(classID: String) {
    switch classID {
      case "sex":         inputCompletion |= InputCompletion.Sex.rawValue
      case "difficulty":  inputCompletion |= InputCompletion.Difficulty.rawValue
    default:
      assertionFailure("到達しない")
    }
    checkCompleteInput()
  }
}

extension ActivationViewController : ActivationNameTextFieldDelegate {
  func didChangeEmpty() {
    if inputCompletion & InputCompletion.Name.rawValue > 0 {
      inputCompletion ^= InputCompletion.Name.rawValue
      checkCompleteInput()
    }
  }
  
  func didChangeNotEmpty() {
    inputCompletion |= InputCompletion.Name.rawValue
    checkCompleteInput()
  }
}