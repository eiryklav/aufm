//
//  RadioButton.swift
//  Aufm
//
//  Created by moti on 2015/06/21.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmCommonComponentViews

class RadioButton : UIButton, BTProtocol,
  SEButtonTouchClick, BTNoTitle, BTTouchActionNoEffect {
  
  var uniqueID: String
  
  required init?(coder: NSCoder) {
    fatalError("NSCoder not required")
  }
  
  init(frame: CGRect, uniqueID: String) {
    self.uniqueID = uniqueID
    super.init(frame: frame)
    applyBackground()
    applySEButtonTouchForControlEvents(.TouchDown)
  }
  
  func applyBackground() {
    backgroundColor = UIColor(rgba: "#5e5e5eff")
  }
  
  func didUncheck() {
    self.selected = false
    self.setImage(UIImage(named: "none"), forState: .Normal)
  }
  
  func didCheck() {
    selected = true
    setImage(UIImage(named: "check-mark"), forState: .Normal)
  }
}