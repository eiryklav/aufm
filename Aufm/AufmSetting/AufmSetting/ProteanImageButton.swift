//
//  ProteanImageButton.swift
//  Aufm
//
//  Created by moti on 2015/06/20.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmSupport
import AufmGameModel
import AufmCommonComponentViews

class ProteanImageButton : UIButton, BTProtocol,
  BGNone, SEButtonTouchClick, BTImage, BTTouchActionNoEffect,
  CanFadeInOut {
  
  let image:    UIImage // requirement for BTImage
  var imageID:  String!
  
  required init?(coder: NSCoder) {
    fatalError("NSCoder not supported")
  }
  
  init(frame: CGRect, image: UIImage, imageID: String) {
    
    self.image = image
    super.init(frame: frame)
    applyBackground()
    applyTitle()
    applySEButtonTouchForControlEvents(.TouchDown)
    
    self.imageID = imageID
    adjustsImageWhenHighlighted = false
    alpha = 0.0 // because of white image for blink
    addTarget(self, action: "didPushButton:", forControlEvents: .TouchDown)
  }
  
  override func hitTest(point: CGPoint, withEvent event: UIEvent?) -> UIView? {
    if !CGRectContainsPoint(bounds, point) { return nil }
    let color = imageView!.image!.pixelColorAtPoint(
      CGPointMake(
        point.x * imageView!.image!.size.width  / frame.size.width,
        point.y * imageView!.image!.size.height / frame.size.height
      )
    )
    var r: CGFloat = 0.0
    var g: CGFloat = 0.0
    var b: CGFloat = 0.0
    var a: CGFloat = 0.0
    if color.getRed(&r, green: &g, blue: &b, alpha: &a) {
      let check = Int(a * 255.0)
      if check == 0 {
        return nil
      }
      else {
        return self
      }
    }
    return nil
  }
}