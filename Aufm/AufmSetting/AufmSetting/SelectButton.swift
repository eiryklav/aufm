//
//  SelectButton.swift
//  Aufm
//
//  Created by moti on 2015/06/18.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmGameModel
import AufmSupport
import AufmCommonComponentViews

public class SelectButton : UIButton, BTProtocol, BGSwitchableModesty, SEButtonTouchClick, BTSwitchableLabel, BTTouchActionNoEffect {

  private var buttonName: String?
  
  public let text:               String
  public let font_:              UIFont
  public let titleColor:         UIColor
  public let selectedText:       String
  public let selectedTitleColor: UIColor
  
  required public init?(coder: NSCoder) {
    fatalError("NSCoder not supported")
  }
  
  public init(frame: CGRect, normalText: String, selectedText: String? = nil, titleColor: UIColor = UIColor.blackColor(), selectedTitleColor: UIColor? = nil, buttonName: String) {
    
    text = normalText
    font_ = UIFont(name: "Hiragino Kaku Gothic W3", size: adjustFontSize(24.0))!
    self.titleColor = titleColor
    self.selectedText = selectedText ?? normalText
    self.selectedTitleColor = selectedTitleColor ?? titleColor
    self.buttonName = buttonName
    super.init(frame: frame)
    
    applyBackground()
    applyTitle()
    applySEButtonTouchForControlEvents(.TouchUpInside)
  }
  
}
