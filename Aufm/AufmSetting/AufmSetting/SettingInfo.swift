//
//  SettingInfo.swift
//  Aufm
//
//  Created by moti on 2015/06/21.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import Foundation

public class SettingInfo {
  
  public static let sharedInstance = SettingInfo()
  
  private init() {
    // FOR DEBUG
    worldName = "SicereliaKingdom"
    name = "烏骨鶏職人さとうすずき"
    sex = .Male
    difficulty = .Brutal
    targetOfBeatsWithEnemyID[1] = 2
    targetOfBeatsWithEnemyID[2] = 2
    targetOfBeatsWithEnemyID[3] = 2
    targetOfBeatsWithEnemyID[4] = 2
    attitude = 5
    changeEquipment = 40
    invitation = 100
    lootingDrawers = 30
  }
  
  public enum Sex { case Unselected, Male, Female }
  public enum Difficulty { case Unselected, Normal, Hard, Brutal }
  
  public var worldName: String?
  public var name = String()
  public var sex = Sex.Unselected
  public var difficulty = Difficulty.Unselected
  
  public var targetOfBeatsWithEnemyID = [Int:Int]() // ID -> Num
  public static let codeOfConductNames = ["人当たり", "装備換装率", "従者勧誘率", "タンス"]
  public enum CodeOfConduct: Int { case Attitude=0, ChangeEquipment=1, Invitation=2, LootingDrawers=3 }
  
  public var attitude: Int?         // 人当たり
  public var changeEquipment: Int?  // 装備換装率
  public var invitation: Int?       // 従者勧誘率
  public var lootingDrawers: Int?   // タンス
}