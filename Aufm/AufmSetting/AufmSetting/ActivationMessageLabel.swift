//
//  ActivationMessageLabel.swift
//  Aufm
//
//  Created by moti on 2015/06/22.
//  Copyright (c) 2015年 PCyan. All rights reserved.
//

import UIKit
import AufmGameModel

public class ActivationMessageLabel: UILabel {
  
  required public init?(coder: NSCoder) {
    fatalError("NSCoder not supported")
  }
  
  var name:       String?
  var sex:        String?
  var difficulty: String?
  
  public init(viewSize: CGSize) {
    
    super.init(frame: CGRect(
      origin: CGPointMake(viewSize.width*0.015, viewSize.height*0.01),
      size: CGSizeZero)
    )
    text = "./activation.afm"
    font = UIFont(name: "AnonymousPro", sizeType: .Small)
    setActivationText()
  }
  
  public func setActivationText() {
    frame.size = CGSizeMake(5000, 500)
    let startString = "# ./activation.afm\n\ntarget agent:"
    var nameString  = "name......... "
    if self.name != nil { nameString += self.name! } else { nameString += "nil" }
    var sexString   = "sex.......... "
    if self.sex != nil { sexString += self.sex! } else { sexString += "nil" }
    var dfString    = "difficulty... "
    if self.difficulty != nil { dfString += self.difficulty! } else { dfString += "nil" }
    self.text = "\(startString)\n\(nameString)\n\(sexString)\n\(dfString)\n"
    if self.name != nil && self.sex != nil && self.difficulty != nil {
      // エンターキー押してからこれを順に表示した方がかっこいいと思う。色付きや、斜め文字もいいかも。
      // また、その間全ての入力のenable = falseにすべき
      self.text = self.text! + "\nCreating the activation key...\n\nAuthentication allowed.\nThe key is ********\n\nActivation phase finished successfully.\nPlease press enter key to continue\nconfiguration...\n_"
    }
    else {
      self.text = self.text! + "\n_"
    }
    self.sizeToFit()
  }
  
  public func setTargetName(name: String) {
    self.name = name
    if name == "" { self.name = nil }
    setActivationText()
  }
  
  public func setTargetSex(sex: SettingInfo.Sex) {
    switch sex {
    case .Unselected:
      self.sex = nil
    case .Male:
      self.sex = "male"
    case .Female:
      self.sex = "female"
    }
    setActivationText()
  }
  
  public func setDifficulty(difficulty: SettingInfo.Difficulty) {
    switch difficulty {
    case .Unselected:
      self.difficulty = nil
    case .Normal:
      self.difficulty = "normal"
    case .Hard:
      self.difficulty = "hard"
    case .Brutal:
      self.difficulty = "brutal"
    }
    setActivationText()
  }
}
