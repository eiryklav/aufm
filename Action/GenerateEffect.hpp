#pragma once

#include "../GameState/GameState.hpp"
#include "../Character/ChrData.hpp"
#include "../Support/Misc.hpp"

#include <vector>
#include <memory>

#define ACT_GEN_EFFECT_VALUE_DEF(effect_name, status_type)  \
  inline auto gen ## effect_name( \
    std::shared_ptr<ChrData::Character> target, \
    ChrData::status_type val)

#define ACT_GEN_EFFECT_COND_DEF(effect_name)  \
  inline auto gen ## effect_name( \
    std::shared_ptr<ChrData::Character> target, \
    double prob)  \
  -> ResultOfCond

namespace Action
{
  namespace GenerateEffect
  {
    enum class ResultOfCond { Success, Fail, Already };

    ACT_GEN_EFFECT_VALUE_DEF(Damage, HitPoint)
    {
      target->gainDamage(val);
    }
    
    ACT_GEN_EFFECT_VALUE_DEF(Heal, HitPoint)
    {
      auto const& befHP = target->getCurrStat()->getHP();
      target->gainRecover(val);
      return target->getCurrStat()->getHP() - befHP; // 実際の回復量を返す
    }

    ACT_GEN_EFFECT_COND_DEF(Poison)
    {
      if(target->hasPoison()) { return ResultOfCond::Already; }
      if(Misc::randomBernoulli(prob)) {
        target->gainPoison();
        return ResultOfCond::Success;
      }
      return ResultOfCond::Fail;
    }

    ACT_GEN_EFFECT_COND_DEF(Palsy)
    {
      if(target->hasPalsy()) { return ResultOfCond::Already; }
      if(Misc::randomBernoulli(prob)) {
        target->gainPalsy();
        return ResultOfCond::Success;
      }
      return ResultOfCond::Fail;
    }

    ACT_GEN_EFFECT_COND_DEF(Death)
    {
      // if(...) { return ResultOfCond::Already; }
      if(Misc::randomBernoulli(prob)) {
        target->gainDeath();
        return ResultOfCond::Success;
      }
      return ResultOfCond::Fail;
    }

  }
}







