#pragma once

#include <string>

namespace Action
{
  namespace ClassStrings
  {
    namespace Prefix
    {
      extern std::string const
      NormalAttack,
      SpellRecover,
      SpellAscent
      ;
    }
    
    namespace ID
    {
      extern std::string const
      Heal
      ;
    }
    
    namespace Suffix
    {
      extern std::string const
      High,
      Mid,
      Low
      ;
    }
  }
}