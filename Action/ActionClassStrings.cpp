#include "ActionClassStrings.hpp"

namespace Action
{
  namespace ClassStrings
  {
    namespace Prefix
    {
      /*
       A: 通常攻撃
       I: 道具
       S: 魔法
       P: 物理攻撃
       */
      std::string const
      NormalAttack    = "AN",
      CriticalAttack  = "AC",
      /*
       ＜呪文表＞
       Ａ：治癒の呪文
       Ｂ：マナの呪文
       Ｃ：炎の呪文
       Ｄ：水の呪文
       Ｅ：雷の呪文
       Ｆ：風の呪文
       Ｇ：死の呪文
       Ｈ：妨害の呪文
       Ｉ：増強の呪文
       Ｊ：状態異常の呪文
       */
      SpellRecover    = "SA",
      SpellForMana    = "SB",
      SpellFlame      = "SC",
      SpellWater      = "SD",
      SpellThunder    = "SE",
      SpellWind       = "SF",
      SpellDeath      = "SG",
      SpellDisturb    = "SH",
      SpellReinforce  = "SI",
      SpellAbState    = "SJ"
      ;
    }

    namespace ID
    {
      std::string const
      Heal = "0001"
      ;
    }
    
    namespace Suffix
    {
      std::string const
      High  = "H",
      Mid   = "M",
      Low   = "L"
      ;
    }
  }
}