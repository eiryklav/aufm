#pragma once

#include <queue>
#include <memory>

#include "../Printer.hpp"
#include "../Game/Game.hpp"

#include "../Character/ChrData.hpp"

namespace EffectParser { class Parser; enum class target_type; class statement; }

namespace BattleSystem
{

  enum class Result { Running, Win, Lose };
  
  using ChrContainer = std::vector<std::shared_ptr<ChrData::Character>>;
  using ActionClass = std::string;

  class ExecutableAction
  {
  private:
    ActionClass actionClass;
    std::shared_ptr<ChrData::Character> actor;
    std::shared_ptr<ChrData::Character> target;

  public:
    ExecutableAction(ActionClass const&,
        std::shared_ptr<ChrData::Character> const&,
        std::shared_ptr<ChrData::Character> const&);

    inline auto getActionClass() const -> decltype(auto) { return (actionClass); }
    inline auto getActor() const -> decltype(auto) { return (actor); }
    inline auto getTarget() const -> decltype(auto) { return (target); }
  };

  
#define IP_EFFECT_STATEMENT_DEF(name, prefix) \
  inline auto prefix statement ## name(EffectParser::statement const& st, std::shared_ptr<ExecutableAction> const& action)

  class Battle
  {
  private:
    std::shared_ptr<ChrContainer> container_;

  private:
    // 中間言語ステートメントの実行
    IP_EFFECT_STATEMENT_DEF(Text,);
    IP_EFFECT_STATEMENT_DEF(ConsumeItem,);  // アイテム消費　魔法や特技としてのアイテム消費に使える
    IP_EFFECT_STATEMENT_DEF(Damage,);
    IP_EFFECT_STATEMENT_DEF(Heal,);
    IP_EFFECT_STATEMENT_DEF(Poison,);
    IP_EFFECT_STATEMENT_DEF(Palsy,);
    IP_EFFECT_STATEMENT_DEF(Death,);
    IP_EFFECT_STATEMENT_DEF(Invocation,);

  private:
    auto insertEnemyGroupIntoContainer(std::vector<int> const&);
    auto insertPlayersIntoContainer();
    auto initBattlersCurrStat();

    auto selectNextActor();
    auto isEnemyGroupAlive();
    auto makeTargetPtrs(EffectParser::target_type, std::shared_ptr<ExecutableAction> const&);
    auto interpretEffectInterlang(EffectParser::Parser&, std::shared_ptr<ExecutableAction> const&) -> void;
    auto replaceVariablesInEffectString(std::string&,std::shared_ptr<ExecutableAction> const&);
    auto execAction(std::shared_ptr<ExecutableAction> const&);
    auto printSystemInvocationText(std::string const&, std::string const&, std::string const&);
    
    auto calcExperience();
    auto getExperience();
    auto getItemDrops();
    auto parseItemDrops(std::string const&);
    auto getMoney();
    auto defeatedEnemies();
    auto buryDeadServants();
    
  public:
    Battle(std::vector<int> const&);
    inline auto getContainer() const -> decltype(auto) { return (container_); }

    auto resume() -> Result;
  };
  
}
