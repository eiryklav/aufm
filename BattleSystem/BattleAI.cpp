/*
 Name:  BattleAI
 Desc:  単一キャラが現在の自分のターンでとる行動を決定。一切の実行処理を伴わない。
        各AIの実装: BATTLE_AI_DECISION_IMPL_DEF(AIName)
                   Return: shared_ptr<ExecutableAction>(...)
 */
#include "../Action/ActionClassStrings.hpp"
#include "BattleSystem.hpp"
#include "BattleAI.hpp"

#include <boost/optional.hpp>

namespace BattleSystem
{
  BATTLE_AI_DESCRIPTION_IMPL_DEF(Dummy, "ダミー")
  BATTLE_AI_DESCRIPTION_IMPL_DEF(Simpleton, "猪突猛進")
  BATTLE_AI_DESCRIPTION_IMPL_DEF(Standard, "只者")
  BATTLE_AI_DESCRIPTION_IMPL_DEF(AttackerYosuga, "軋る救済のヨスガ")
  BATTLE_AI_DESCRIPTION_IMPL_DEF(DefenderYosuga, "生汐の魔術師")
  BATTLE_AI_DESCRIPTION_IMPL_DEF(BalancerYosuga, "静けき御空のヨスガ")

  BATTLE_AI_DECISION_IMPL_DEF(Dummy) { AUFM_ASSERT(false, "This is dummy battle AI."); }
  
  
  BATTLE_AI_DECISION_IMPL_DEF(AttackerYosuga) { AUFM_ASSERT(false, "This battle AI has not been implemented."); }
  BATTLE_AI_DECISION_IMPL_DEF(DefenderYosuga) { AUFM_ASSERT(false, "This battle AI has not been implemented."); }
  BATTLE_AI_DECISION_IMPL_DEF(BalancerYosuga) { AUFM_ASSERT(false, "This battle AI has not been implemented."); }
  
  /*
   Name: searchMostSeriousHPOnEnemySide()
   Desc: 最小のHPをもつ敵を見つける
   */
  auto searchMostSeriousHPOnEnemySide(std::shared_ptr<ChrContainer> const& c, std::shared_ptr<ChrData::Character> const& anAlly)
  {
    auto target = (*c)[0];
    auto minHP = ChrData::Enemy::CounterStop.getHP();
    
    for(auto p: *c) {
      if(anAlly->isAlly(p) || !p->isAlive()) { continue; }
      if(minHP > p->getCurrStat()->getHP()) {
        minHP = p->getCurrStat()->getHP();
        target = p;
      }
    }
    return target;
  }
  
  /*
   Name:  SimpletonAI
   Desc:  最小HPを持つ敵に対して通常攻撃をする。敵の陣形が考慮されないAI
   */
  BATTLE_AI_DECISION_IMPL_DEF(Simpleton)
  {
    auto actor = getOwner();
    auto c = getContainer();
    
    return std::make_shared<ExecutableAction>(Action::ClassStrings::Prefix::NormalAttack, actor, searchMostSeriousHPOnEnemySide(c, actor));
  }
  
  /*
   Name: searchMostSeriousEnergyOnAllySide()
   Desc: 指定したキャラクタと味方のうちから、最も元気のないやつを見つける
   */
  auto searchMostSeriousEnergyOnAllySide(std::shared_ptr<ChrContainer> const& c, std::shared_ptr<ChrData::Character> const& anAlly, double& leastEnergy)
  {
    AUFM_ASSERT(!(*c).empty(), "chr container is empty");
    
    std::shared_ptr<ChrData::Character> ret = (*c)[0];
    for(auto const& p: *c) {
      if(!p->isAlly(anAlly) || !p->isAlive()) { continue; }
      double tar = 1.0 * p->getCurrStat()->getHP() / p->getBasicStat()->getHP();
      if(leastEnergy > tar) {
        leastEnergy = tar;
        ret = p;
      }
    }
    return ret;
  }
  
  constexpr double
    EnergyFullOfVitarity    = 1.0,
    EnergySlightlyInjured   = 0.75,
    EnergyInjured           = 0.45,
    EnergySeriousInjured    = 0.3,
    EnergyCriticalCondition = 0.2;
  
  enum class HowSerious
  {
    FullOfVitarity,     // 全快
    SlightlyInjured,    // 軽傷
    Injured,            // 負傷
    SeriousInjured,     // 大怪我
    CriticalCondition,  // 重篤
    Dying,              // 瀕死
  };
  
  auto isSeriousLessThan(HowSerious howSerious, double energy)
  {
    switch(howSerious) {
      case HowSerious::FullOfVitarity:
        return energy < EnergyFullOfVitarity;
      case HowSerious::SlightlyInjured:
        return energy < EnergySlightlyInjured;
      case HowSerious::Injured:
        return energy < EnergyInjured;
      case HowSerious::SeriousInjured:
        return energy < EnergySeriousInjured;
      case HowSerious::CriticalCondition:
        return energy < EnergyCriticalCondition;
      default:
        AUFM_ASSERT(false, "Cannot define how serious less than" << std::to_string(static_cast<int>(howSerious)));
    }
  }
  
  auto isSeriousGreaterOrEqualThan(HowSerious howSerious, double energy)
  {
    switch(howSerious) {
      case HowSerious::FullOfVitarity:
        return energy >= EnergyFullOfVitarity;
      case HowSerious::SlightlyInjured:
        return energy >= EnergySlightlyInjured;
      case HowSerious::Injured:
        return energy >= EnergyInjured;
      case HowSerious::SeriousInjured:
        return energy >= EnergySeriousInjured;
      case HowSerious::CriticalCondition:
        return energy >= EnergyCriticalCondition;
      default:
        AUFM_ASSERT(false, "Cannot define how serious greater or equal than" << std::to_string(static_cast<int>(howSerious)));
    }
  }
  
  auto checkHowSerious(double energy) {
    if(energy == EnergyFullOfVitarity) {
      return HowSerious::FullOfVitarity;
    }
    if(energy >= EnergySlightlyInjured)  {
      return HowSerious::SlightlyInjured;
    }
    if(energy >= EnergyInjured) {
      return HowSerious::Injured;
    }
    if(energy >= EnergySeriousInjured) {
      return HowSerious::SeriousInjured;
    }
    if(energy >= EnergyCriticalCondition) {
      return HowSerious::CriticalCondition;
    }
    if(energy > 0) {
      return HowSerious::Dying;
    }
    AUFM_ASSERT(false, "energy is " << energy << " (under or equal to 0)");
  }
  
  enum class ActionQuality {
    /*
     明確に強度を区別できる場合は、ClassIDのSuffixとして積極的に活用していくと良い
     */
    High,
    Middle,
    Low
  };

  auto chooseRecoverQuality(double energy)
  {
    auto howSerious = checkHowSerious(energy);
    switch(howSerious) {
      case HowSerious::SeriousInjured:
        return ActionQuality::Middle;
        
      case HowSerious::CriticalCondition:
      case HowSerious::Dying:
        return ActionQuality::High;
        
      default:
        return ActionQuality::Low;
    }
  }
  
  /*
   classID = (genre, identifier, quality)
   (genre, identifier, quality)から具体的にactorが行使できる技を決める。
   qualityを満たす技が行使できない場合（覚えていないorTPが足りない）妥協できる技を選択する(fall through)
   それでも無理な場合は無効値を返却する.
   actorはshared_ptr<>であるが、.lock()されたものであることに注意。
   */
  auto chooseActionQuality(std::string const& genre, std::string const& identifier, ActionQuality quality, std::shared_ptr<ChrData::Character> const& actor)
    -> boost::optional<std::string>
  {
    // 該当するSuffixを持つIDが存在しない[/習得済みでない/あるいはManaが足りない]場合はfall throughしていく
#define RETURN_IF_SELECTED_CLASSID_WITH_SUFFIX(suffix)  {\
  auto classID = genre+identifier+suffix;  \
  if(GameMgr->getDatabase()->isExistActionRecord(classID)) {  \
    auto rec = GameMgr->getDatabase()->getActionRecord(classID);  \
    if(actor->isMasteredAction(classID)) {  \
      auto mana = std::stoi(rec["Mana"]); \
      if(mana <= actor->getCurrStat()->getMana()) { \
      return boost::optional<std::string>(classID); \
      } \
    }}}
    switch(quality) {
      case ActionQuality::High:   RETURN_IF_SELECTED_CLASSID_WITH_SUFFIX(Action::ClassStrings::Suffix::High) // fall through
      case ActionQuality::Middle: RETURN_IF_SELECTED_CLASSID_WITH_SUFFIX(Action::ClassStrings::Suffix::Mid)  // fall through
      case ActionQuality::Low:    RETURN_IF_SELECTED_CLASSID_WITH_SUFFIX(Action::ClassStrings::Suffix::Low)  // fall through
    }
    return boost::none;
#undef RETURN_IF_SELECTED_CLASSID_WITH_SUFFIX
  }
  
  auto chooseRecoveryItem(ActionQuality quality)
    -> boost::optional<std::string>
  {
    
    // TODO: 道具のIDもstringにせよ。Action::...::Suffix::High, Mid, Lowを活用せよ
    
#define RETURN_IF_SELECTED_ITEMID(id) \
  if(GameMgr->getRucksack()->hasItem(id)) {  \
    auto rec = GameMgr->getDatabase()->getItemRecord(id);  \
    return boost::optional<std::string>("I000" + std::to_string(id)); \
  }
  
    switch(quality) {
      case ActionQuality::High:   RETURN_IF_SELECTED_ITEMID(3)  // fall through
      case ActionQuality::Middle: RETURN_IF_SELECTED_ITEMID(2)  // fall through
      case ActionQuality::Low:    RETURN_IF_SELECTED_ITEMID(1)  // fall through
    }
    return boost::none;
#undef RETURN_IF_SELECTED_ITEMID
  }
  
  /*
   Name:  recoveryWithSpellOrTool
   Desc:  単体味方回復 / 優先順位 魔法->道具
   */
  auto recoveryWithSpellOrTool(std::shared_ptr<ChrData::Character> const& actor,
                               std::shared_ptr<ChrContainer> const& container)
    -> boost::optional<std::shared_ptr<ExecutableAction>>
  {
    double leastEnergy = 1.0;
    auto target = searchMostSeriousEnergyOnAllySide(container, actor, leastEnergy);
    
    if(isSeriousLessThan(HowSerious::SlightlyInjured, leastEnergy)) {
      auto quality = chooseRecoverQuality(leastEnergy);
      if(auto r = chooseActionQuality(Action::ClassStrings::Prefix::SpellRecover, Action::ClassStrings::ID::Heal, quality, actor)) {
        auto actionClass = *r;
        return std::make_shared<ExecutableAction>(actionClass, actor, target);
      }
      
      if(!actor->isAlly(GameMgr->getPlayer())) { return boost::none; }  // 道具はプレイヤー側しか使えない
      if(auto r = chooseRecoveryItem(quality)) {
        auto itemIDString = *r;
        return std::make_shared<ExecutableAction>(itemIDString, actor, target);
      }
      
    }
    return boost::none;
  }
  
  BATTLE_AI_DECISION_IMPL_DEF(Standard)
  {
    auto actor = getOwner();
    auto c = getContainer();
    
    if(auto r = recoveryWithSpellOrTool(actor, c)) {
      auto execAction = *r;
      return execAction;
    }
    
    /*
     // 陣形の戦闘を通常攻撃
    auto chrs = GameMgr->getStandingPosition()->getMostFrontChrs(); // size > 0のvalidation済を保証
    auto const& target = Misc::randomRange(0, (int)chrs.size()-1);
     */
    
    // 最もHPの少ない敵を狙う
    return std::make_shared<ExecutableAction>(Action::ClassStrings::Prefix::NormalAttack, actor,
                                              searchMostSeriousHPOnEnemySide(c, actor));
  }
  
}
