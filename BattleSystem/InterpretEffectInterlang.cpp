/*
 Name: InterpretEffectInterlang.cppn
 Desc: 中間言語ステートメントの実行
 */

#include "../Game/Game.hpp"
#include "BattleSystem.hpp"
#include "../Data/BaseParser.hpp"
#include "../Data/EffectParser.hpp"
#include "../Action/GenerateEffect.hpp"
#include <algorithm>

using EffectParser::statement_type;
using EffectParser::target_type;
namespace MSG = Printer::Messages;

namespace BattleSystem
{
  auto Battle::makeTargetPtrs(
      EffectParser::target_type type,
      std::shared_ptr<ExecutableAction> const& action)
  {
    std::vector<std::shared_ptr<ChrData::Character>> ret;
    using EffectParser::target_type;
    switch(type) {
    case target_type::for_one_target:
    {
      ret.emplace_back(action->getTarget());
      return ret;
    }

    case target_type::for_all_targets:
    {
      for(auto const& p: *container_) {
        if(action->getTarget()->isAlly(p)) {
          ret.emplace_back(p);
        }
      }
      return ret;
    }

    case target_type::for_myself:
    {
      ret.emplace_back(action->getActor());
      return ret;
    }

    case target_type::for_ourselves:
    {
      for(auto const& p: *container_) {
        if(action->getActor()->isAlly(p)) {
          ret.emplace_back(p);
        }
      }
      return ret;
    }

    default:
      AUFM_ASSERT(false, "Cannot determine target.");
    }
  }

  IP_EFFECT_STATEMENT_DEF(Text, Battle::)
  {
    GameMgr->getLogger()->pushMessage(Printer::Message(Printer::MessageType::Default, boost::get<std::string>(st.args[0])));
  }
  
  IP_EFFECT_STATEMENT_DEF(ConsumeItem, Battle::)
  {
    GameMgr->getRucksack()->popItem(boost::get<int>(st.args[0]));
  }

  IP_EFFECT_STATEMENT_DEF(Damage, Battle::)
  {
    auto const targets = makeTargetPtrs(boost::get<target_type>(st.args[0]), action);
    auto damageVal = static_cast<ChrData::HitPoint>(
      std::max(0.0, boost::get<BaseParser::number_type>(st.args[1]))
    );
    for(auto const& p: targets) {
      GameMgr->getLogger()->pushMessage(MSG::getDamage(p->getName(), damageVal));
      Action::GenerateEffect::genDamage(p, damageVal);
    }
  }

  IP_EFFECT_STATEMENT_DEF(Heal, Battle::)
  {
    auto const targets = makeTargetPtrs(boost::get<target_type>(st.args[0]), action);
    auto healVal = static_cast<ChrData::HitPoint>(boost::get<BaseParser::number_type>(st.args[1]));
    for(auto const& p: targets) {
      auto realVal = Action::GenerateEffect::genHeal(p, healVal);
      GameMgr->getLogger()->pushMessage(MSG::getHeal(p->getName(), realVal));
    }
  }

  IP_EFFECT_STATEMENT_DEF(Poison, Battle::)
  {
    using RES = Action::GenerateEffect::ResultOfCond;

    auto const targets = makeTargetPtrs(boost::get<target_type>(st.args[0]), action);
    auto prob = static_cast<double>(boost::get<BaseParser::number_type>(st.args[1]));
    for(auto const& p: targets) {
      auto r = Action::GenerateEffect::genPoison(p, prob);
      switch(r) {
        case RES::Fail:
          GameMgr->getLogger()->pushMessage(MSG::getUnaffected(p->getName()));
          break;

        case RES::Already:
          GameMgr->getLogger()->pushMessage(MSG::alreadyGetPoison(p->getName()));
          break;
        case RES::Success: break;
      }
    }
  }

  IP_EFFECT_STATEMENT_DEF(Palsy, Battle::)
  {
    using RES = Action::GenerateEffect::ResultOfCond;

    auto const targets = makeTargetPtrs(boost::get<target_type>(st.args[0]), action);
    auto prob = static_cast<double>(boost::get<BaseParser::number_type>(st.args[1]));
    for(auto const& p: targets) {
      auto r = Action::GenerateEffect::genPalsy(p, prob);
      switch(r) {
        case RES::Fail:
          GameMgr->getLogger()->pushMessage(MSG::getUnaffected(p->getName()));
          break;
        case RES::Already:
          GameMgr->getLogger()->pushMessage(MSG::alreadyGetPalsy(p->getName()));
          break;
        case RES::Success: break;
      }
    }
  }

  IP_EFFECT_STATEMENT_DEF(Death, Battle::)
  {
    using RES = Action::GenerateEffect::ResultOfCond;

    auto const targets = makeTargetPtrs(boost::get<target_type>(st.args[0]), action);
    auto prob = static_cast<double>(boost::get<BaseParser::number_type>(st.args[1]));
    for(auto const& p: targets) {
      auto r = Action::GenerateEffect::genDeath(p, prob);
      switch(r) {
        case RES::Fail:
          GameMgr->getLogger()->pushMessage(MSG::getUnaffected(p->getName()));
          break;

        case RES::Already:
          // 起こらない予定
          AUFM_ASSERT(false, "Unpredected case.");
        case RES::Success: break;
      }
    }
  }

  IP_EFFECT_STATEMENT_DEF(Invocation, Battle::)
  {
    // TODO
  }
  
  auto Battle::interpretEffectInterlang(
    EffectParser::Parser& pa,
    std::shared_ptr<ExecutableAction> const& action)
    -> void
  {
    EffectParser::statement st;

    while(pa >> st) {
      switch(st.type) {
        case statement_type::text:          statementText(st, action);        break;
        case statement_type::consume_item:  statementConsumeItem(st, action); break;
        case statement_type::damage:        statementDamage(st, action);      break;
        case statement_type::heal:          statementHeal(st, action);        break;
        case statement_type::poison:        statementPoison(st, action);      break;
        case statement_type::palsy:         statementPalsy(st, action);       break;
        case statement_type::death:         statementDeath(st, action);       break;
        case statement_type::invocation:    statementInvocation(st, action);  break;
        
        case statement_type::none:
          std::cerr << "statement_type::none\n";
        default:
          std::runtime_error("Undefined statement");
      } // switch(st.type)
    } // while(pa >> st)
  }

} // namespace BattleSystem