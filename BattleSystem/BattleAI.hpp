#pragma once

#include <vector>
#include <memory>

#include "BattleSystem.hpp"

namespace BattleSystem
{
  class BattleAI
  {
  protected:
    using Identifier  = std::string;
    
  protected:
    Identifier                        id_;
    std::weak_ptr<ChrData::Character> owner_;
    std::weak_ptr<ChrContainer>       container_;
    
  public:
    
    inline BattleAI(Identifier const& id): id_(id) {}
    virtual ~BattleAI() {}
    virtual auto getName() const -> std::string = 0;
    virtual auto makeDecision() -> std::shared_ptr<ExecutableAction> = 0;
    
    inline auto getID() const -> decltype(auto) { return (id_); }
    
    auto getOwner()
      -> std::shared_ptr<ChrData::Character>
    {
      if(owner_.expired()) { AUFM_ASSERT(false, "Expired owner_"); }
      return owner_.lock();
    }
    
    auto getContainer()
      -> std::shared_ptr<ChrContainer>
    {
      if(container_.expired()) { AUFM_ASSERT(false, "Expired container_"); }
      return container_.lock();
    }

    auto isExpired() -> bool { return owner_.expired() || container_.expired(); }
    auto setOwner(std::weak_ptr<ChrData::Character> owner) -> void { owner_ = owner; }
    auto setContainer(std::weak_ptr<ChrContainer> container) -> void { container_ = container; }

  };
  
#define BATTLE_AI_DECISION_CLASS_DEF(ID) \
  class ID ## AI : public BattleAI \
  { \
  public: \
    inline ID ## AI(): BattleAI(#ID "AI"){} \
    auto getName() const -> std::string; \
    auto makeDecision() -> std::shared_ptr<ExecutableAction>; \
  };

  BATTLE_AI_DECISION_CLASS_DEF(Dummy)
  BATTLE_AI_DECISION_CLASS_DEF(Simpleton)
  BATTLE_AI_DECISION_CLASS_DEF(Standard)
  BATTLE_AI_DECISION_CLASS_DEF(AttackerYosuga)
  BATTLE_AI_DECISION_CLASS_DEF(DefenderYosuga)
  BATTLE_AI_DECISION_CLASS_DEF(BalancerYosuga)
  
#define BATTLE_AI_DESCRIPTION_IMPL_DEF(ID, desc)  \
  auto ID ## AI ::getName() const -> std::string { return desc; }

#define BATTLE_AI_DECISION_IMPL_DEF(ID) \
  auto ID ## AI ::makeDecision() \
    -> std::shared_ptr<ExecutableAction>

}
