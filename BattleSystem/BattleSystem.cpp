#include <memory>
#include <fstream>

#include "BattleSystem.hpp"
#include "BattleAI.hpp"

#include "../Lib/picojson.h"
#include "../Game/GameStrategy.hpp"
#include "../Support/Misc.hpp"
#include "../Data/Database.hpp"
#include "../Data/EffectParser.hpp"
#include "../Printer.hpp"

#include "../Game/TimeManager.hpp"

#include <boost/algorithm/string.hpp>

/*
  TODO: EncountSystemに変えるべきか？
        EncountSystemであれば、各ステートのエンカウント処理もここに持ってくることになる
 */

namespace BattleSystem
{
  
  auto Battle::insertEnemyGroupIntoContainer(std::vector<int> const& enemyIDOnDBList)
  {
    for(auto e: enemyIDOnDBList) {
      auto ptr = ChrData::makeEnemyPtr(e);
      container_->emplace_back(ptr);
    }
  }

  auto Battle::insertPlayersIntoContainer()
  {
    container_->push_back(GameMgr->getPlayer());
    for(auto const& aServant: GameMgr->getServants()) {
      container_->push_back(aServant);
    }
  }
    
  auto Battle::initBattlersCurrStat()
  {
    for(auto p: *container_) {
      // 戦闘時に初期化の必要があるもののみ
      p->getCurrStat()->setATK(p->getBasicStat()->getATK());
      p->getCurrStat()->setDEF(p->getBasicStat()->getDEF());
      p->getCurrStat()->setSP(ChrData::Speed(0));
      p->getBasicStat()->getBattleAI()->setOwner(p);
      p->getBasicStat()->getBattleAI()->setContainer(container_);
    }
  }

  Battle::Battle(std::vector<int> const& enemyIDOnDBList)
  {
    container_ = std::make_shared<ChrContainer>();
    insertPlayersIntoContainer();
    insertEnemyGroupIntoContainer(enemyIDOnDBList);
    initBattlersCurrStat();
  }

  auto Battle::selectNextActor()
  {
    std::shared_ptr<ChrData::Character> nextActor;

    int max = -1;
    for(auto p: *container_) {
      if(!p->isAlive()) { continue; }
      if(max < p->getCurrStat()->getSP()) {
        max = p->getCurrStat()->getSP();
        nextActor = p;
      }
      p->getCurrStat()->setSP(p->getCurrStat()->getSP() + p->getBasicStat()->getSP());
    }
    if(max == -1) { AUFM_ASSERT(false, "Battle: getNextActor(); Has any character been allocated?"); }
    // 戦闘のスピード計算はSPに左右されるようにしたい
    GameMgr->getTimeMgr()->requestConsumingAufmTime(
      TimeManager::AufmTime(TimeManager::TimeSpan::TimePerSpeed / nextActor->getCurrStat()->getSP()
    ));
    nextActor->getCurrStat()->setSP(ChrData::Speed(0));
    return nextActor;
  }

  auto Battle::isEnemyGroupAlive()
  {
    bool ret = false;
    for(auto const& p: *container_) {
      if(p->getChrType() == ChrData::Character::ChrType::Enemy && p->isAlive()) {
        ret = true;
      }
    }
    return ret;
  }

  auto Battle::printSystemInvocationText(
    std::string const& actionClass,
    std::string const& actorName,
    std::string const& actionName)
  {
    namespace MSG = Printer::Messages;
    
    GameMgr->getLogger()->pushMessage([&](){
      switch(actionClass[0]) {
        case 'A': return MSG::normalAttack        (actorName);
        case 'I': return MSG::useItemInBattle     (actorName, actionName); // itemName = actionName
        case 'P': return MSG::castSpecialAbility  (actorName, actionName);
        case 'S': return MSG::castSpell           (actorName, actionName);
        default:  AUFM_ASSERT(false, "Undefined spell");
      }}()
    );
  }

  auto Battle::replaceVariablesInEffectString(
    std::string& effectString,
    std::shared_ptr<ExecutableAction> const& action)
  {

    #define REPLACE_BASIC_STAT(who, type)  \
      boost::algorithm::replace_all(effectString, \
        #who "." #type , std::to_string(action->get ## who()->getBasicStat()->get ## type()));

    #define REPLACE_CURR_STAT(who, type)  \
      boost::algorithm::replace_all(effectString, \
        #who "." #type , std::to_string(action->get ## who()->getCurrStat()->get ## type()));

    REPLACE_BASIC_STAT(Actor, LV)
    REPLACE_BASIC_STAT(Actor, Exp)
    REPLACE_BASIC_STAT(Actor, ATK)
    REPLACE_BASIC_STAT(Actor, DEF)
    REPLACE_BASIC_STAT(Actor, SP)
    REPLACE_BASIC_STAT(Actor, LCK)
    REPLACE_BASIC_STAT(Actor, INT)
    boost::algorithm::replace_all(effectString, "Actor.MaxHP" ,   std::to_string(action->getActor()->getBasicStat()->getHP()));
    boost::algorithm::replace_all(effectString, "Actor.MaxMana" , std::to_string(action->getActor()->getBasicStat()->getMana()));
    REPLACE_CURR_STAT(Actor, HP)
    REPLACE_CURR_STAT(Actor, Mana)
    

    REPLACE_BASIC_STAT(Target, LV)
    REPLACE_BASIC_STAT(Target, Exp)
    REPLACE_BASIC_STAT(Target, ATK)
    REPLACE_BASIC_STAT(Target, DEF)
    REPLACE_BASIC_STAT(Target, SP)
    REPLACE_BASIC_STAT(Target, LCK)
    REPLACE_BASIC_STAT(Target, INT)
    boost::algorithm::replace_all(effectString, "Target.MaxHP" ,   std::to_string(action->getActor()->getBasicStat()->getHP()));
    boost::algorithm::replace_all(effectString, "Target.MaxMana" , std::to_string(action->getActor()->getBasicStat()->getMana()));
    REPLACE_CURR_STAT(Target, HP)
    REPLACE_CURR_STAT(Target, Mana)

    #undef REPLACE_BASIC_STAT
    #undef REPLACE_CURR_STAT

  }
  
  auto Battle::execAction(std::shared_ptr<ExecutableAction> const& action)
  {
    auto const& actionClass = action->getActionClass();
    auto        effectString = [&]() -> std::string {
      switch (actionClass[0]) {
        case 'A': {
          printSystemInvocationText(actionClass, action->getActor()->getName(), "-");
          return "Damage: \"MAX[0,Actor.ATK-Target.DEF/2+RAND[-2,2]]\"";
        }
        case 'I': {
          auto itemID = std::stoi(actionClass.substr(1));
          auto record = GameMgr->getDatabase()->getItemRecord(itemID);
          printSystemInvocationText(actionClass, action->getActor()->getName(), Misc::getStringCorrespondToWorld(record["Name"], GameMgr->getWorldName()));
          // 道具消費
          AUFM_ASSERT(action->getActor()->isAlly(GameMgr->getPlayer()), "道具はプレイヤー側しか使用することが出来ない");
          GameMgr->getRucksack()->popItem(itemID);
          return record["Effect"];
        }
        default: {
          // 魔法、特技
          auto record = GameMgr->getDatabase()->getActionRecord(actionClass);
          auto const actionNameString = Misc::getStringCorrespondToWorld(record["Name"], GameMgr->getWorldName());
          if(!std::stoi(record["NoSystemText"])) {
            printSystemInvocationText(actionClass, action->getActor()->getName(), actionNameString);
          }
          // Mana消費
          action->getActor()->getCurrStat()->decreaseMana(ChrData::Mana(std::stoi(record["Mana"])));
          return record["Effect"];
        }
      }
    }();

    replaceVariablesInEffectString(effectString, action);
    EffectParser::Parser pa(effectString);
    interpretEffectInterlang(pa, action);
  }

  ExecutableAction::ExecutableAction(ActionClass const& actionClass,
      std::shared_ptr<ChrData::Character> const& actor,
      std::shared_ptr<ChrData::Character> const& target)
    : actionClass(actionClass),
      actor(actor),
      target(target)
    {}
  
  
  
  auto Battle::calcExperience()
  {
    ChrData::Experience gain(0);
    
    std::list<std::shared_ptr<ChrData::Character>> alivePlayers;
    
    for(auto const& p: *container_) {
      if(p->getChrType() == ChrData::Character::ChrType::Enemy) {
        gain = gain + p->getBasicStat()->getExp();
      }
      else {
        alivePlayers.push_back(p);
      }
    }
    if(alivePlayers.empty()) { std::runtime_error("No one is alive! Did you properly check whether player lost the game?"); }
    
    // 生存している従者で割り勘（整数除算）
    gain /= static_cast<int>(alivePlayers.size());
    
    // レベルアップのステータス差分は、事前事後のデータから引き算する
    for(auto const& p: alivePlayers) {
      ChrData::ChrBasicStat bef;
      bef = *(p->getBasicStat());
      if(p->getBasicStat()->acquireExperience(gain)) {
        GameMgr->getLogger()->pushMessage(Printer::Messages::levelUp(p->getName()));
        GameMgr->getLogger()->pushMessage(Printer::Message(Printer::MessageType::Default, (*(p->getBasicStat())-bef).print()));
      }
    }
  }
  
  auto Battle::getExperience()
  {
    calcExperience();
  }
  
  auto Battle::parseItemDrops(std::string const& json_string)
  {
    picojson::value v;
    auto err = picojson::parse(v, json_string);
    auto const& items = v.get<picojson::array>();
    
    Misc::ProbRangeTester probRngTst;
    
    std::vector<std::pair<int, int>> ret;
    for(auto const& item_value: items) {
      auto const& item_array = item_value.get<picojson::array>();
      int const percent = item_array[0].get<double>();
      probRngTst.add(percent);
      ret.emplace_back(percent, static_cast<int>(item_array[1].get<double>()));
    }
    probRngTst.validateSum();
    
    std::sort(ret.begin(), ret.end());
    
    return ret;
  }
  
  auto Battle::getItemDrops()
  {
    for(auto const& enemy_ptr: *container_) {
      
      if(enemy_ptr->getChrType() != ChrData::Character::ChrType::Enemy) {	/* 敵のみを抽出 */	continue; }
      
      auto enemyOnDB = GameMgr->getDatabase()->getChrStatusRecord(enemy_ptr->getIDOnDB());
      auto vec = parseItemDrops(enemyOnDB["ItemDrops"]);
      
      auto rid = Misc::getRandomIndexAtProbTVec<int>(vec);
      auto const& itemIDOnDB = vec[rid].second;
      if(itemIDOnDB < 0) { /* ハズレ */ continue; }
      // １体の敵が落としたアイテムを所持品に突っ込む処理
      namespace MSG = Printer::Messages;
      GameMgr->getLogger()->pushMessage(MSG::enemyDroppedItem(enemy_ptr->getName()));
      GameMgr->getRucksack()->pushItem(itemIDOnDB);
    }
  }
  
  auto Battle::getMoney()
  {
    int amount = 0;
    for(auto const& chrs: *container_) {
      if(chrs->getChrType() != ChrData::Character::ChrType::Enemy) { continue; }
      // !!! ChrType::Enemy のみ。
      amount += chrs->getBasicStat()->getMoney();
    }
    GameMgr->addMoney(ChrData::Money(amount));
    //  GameMgr->getLogger()->pushMessage(Printer::Message(Printer::MessageType::Default, ));
    GameMgr->getLogger()->pushMessage(Printer::Messages::getMoney(amount, GameMgr->getMonetaryUnitName()));
  }
  
  auto Battle::defeatedEnemies()
  {
    int defeatedTargetCount = 0;
    for(auto const& e: *container_) {
      if(e->getChrType() == ChrData::Character::ChrType::Enemy) {
        defeatedTargetCount += GameMgr->getTargetForDefeat()->defeatEnemy(e->getIDOnDB());
      }
    }
    return defeatedTargetCount;
  }
  
  auto Battle::buryDeadServants()
  {
    
    auto& servants = GameMgr->getServants();
    if(servants.empty()) { return; }
    
    for(auto iter=container_->begin(); iter!=container_->end();) {
      if((*iter)->getChrType() == ChrData::Character::ChrType::Player &&
         !(*iter)->isAlive()) { iter = container_->erase(iter); }
      else { iter++; }
    }
    
    for(auto iter=servants.begin(); iter!=servants.end();) {
      if(!(*iter)->isAlive()) { iter = servants.erase(iter); }
      else { iter++; }
    }
    
    PRINT(GameMgr->getNumOfServants())
    
  }
    
  auto Battle::resume()
    -> Result
  {
    if(!GameMgr->getPlayer()->isAlive()) {
      // グループに負けると、一体も倒したことにならない
      GameMgr->getLogger()->pushMessage(Printer::Messages::sysBattleEnd(0));
      return Result::Lose;
    }
    
    if(!isEnemyGroupAlive()) {
      buryDeadServants();
      auto targetCount = defeatedEnemies();
      GameMgr->getLogger()->pushMessage(Printer::Messages::defeatedEnemyGroup());
      getExperience();
      getItemDrops();
      getMoney();
      
      GameMgr->getLogger()->pushMessage(Printer::Messages::sysBattleEnd(targetCount));
      
      return Result::Win;
    }
    
    auto actor = selectNextActor();
    auto action = actor->getBasicStat()->getBattleAI()->makeDecision();
    execAction(action);
    return Result::Running;
  }

}